﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data;
using System.Web.DataAccess;
using System.Data.SqlClient;
using System.Collections;
using System.Web.Script.Serialization;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using HttpMultipartParser;
using System.Web.Configuration;
using System.Data.Linq.SqlClient;

namespace WCF_McsApp
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class Service1 : IServices
    {
        string ConnMcsApp = ConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString;
        string ConnMcsQc = ConfigurationManager.ConnectionStrings["McsQCConnectionString"].ConnectionString;
        string ConnMcsHrMs = ConfigurationManager.ConnectionStrings["McsHrMsConnectionString"].ConnectionString;
        string ConnMcsfG2010 = ConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString;

        #region PD
        #region เก็บค่า config และ log ผู้ใช้งานเข้าสู่ระบบ McsPdPc
        public List<mConfig> Getconfig()
        {
            McsAppDataContext dc = new McsAppDataContext();

            List<mConfig> results = new List<mConfig>();

            foreach (PDPC_config item in dc.PDPC_configs.OrderBy(s => s.config_key))
            {
                results.Add(new mConfig
                {
                    CONFIG_KEY = item.config_key,
                    CONFIG_VALUE = item.config_value
                });
            }
            return results;
        }

        public mResult CreateDivice(SetDevice input)
        {
            mResult result = new mResult();
            DateTime now = DateTime.Now;

            try
            {
                McsAppDataContext dc = new McsAppDataContext();

                PDPC_device icheck = dc.PDPC_devices.Where(s => s.device_id == input.Device_id).FirstOrDefault();

                if (icheck == null)
                {
                    PDPC_device device = new PDPC_device()
                    {
                        device_id = new string(input.Device_id.Take(20).ToArray()),
                        device_brand = new string(input.Device_brand.ToUpper().Take(10).ToArray()),
                        device_model = new string(input.Device_model.ToUpper().Take(10).ToArray()),
                        device_version = new string(input.Device_version.Take(20).ToArray()),
                        device_imei = new string(input.Device_imei.Take(20).ToArray()),
                        device_time = (DateTime)(now),
                        device_status = input.Device_status
                    };

                    dc.PDPC_devices.InsertOnSubmit(device);
                }
                else
                {
                    icheck.device_id = new string(input.Device_id.Take(20).ToArray());
                    icheck.device_brand = new string(input.Device_brand.ToUpper().Take(10).ToArray());
                    icheck.device_model = new string(input.Device_model.ToUpper().Take(10).ToArray());
                    icheck.device_version = new string(input.Device_version.Take(20).ToArray());
                    icheck.device_imei = new string(input.Device_imei.Take(20).ToArray());
                    icheck.device_time = (DateTime)(now);
                    icheck.device_status = input.Device_status;
                }

                dc.SubmitChanges();

                result.RESULT = "true";
                result.EXCEPTION = "";

                return result;
            }
            catch (Exception ex)
            {
                result.RESULT = "false";
                result.EXCEPTION = ex.Message;

                return result;
            }
        }
        #endregion

        #region Login ป้อน User and Password
        public ResultUser CheckUsernamePassword(InputLogin input)
        {
            ResultUser UserInfo = new ResultUser();

            string check = "";

            object[] prm = { input.Username, input.Password, 0 };

            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_LOGIN", prm);

            while (reader.Read())
            {
                check = reader["RESULT"].ToString();
            }

            if (check == "T")
            {
                using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
                {
                    connection.Open();

                    using (SqlCommand command1 = new SqlCommand())
                    {
                        command1.Connection = connection;
                        command1.CommandText = "SELECT US_ID ,US_FNAME + ' ' + US_LNAME AS US_FNAME FROM McsAppCenter.dbo.USERS WHERE US_ID=@US_ID";
                        command1.CommandType = CommandType.Text;
                        command1.Parameters.Add("@US_ID", SqlDbType.VarChar, 7).Value = input.Username;

                        SqlDataReader dr = command1.ExecuteReader();

                        while (dr.Read())
                        {
                            UserInfo.US_ID = dr["US_ID"].ToString();
                            UserInfo.US_FNAME = dr["US_FNAME"].ToString();
                        }
                    }

                    connection.Close();
                }
            }
            else
            {
                UserInfo.US_ID = null;
                UserInfo.US_FNAME = null;
            }
            return UserInfo;
        }

        //แก้ไขรหัสผ่าน
        public string SetUsernamePassword(SetUserPass input)
        {
            object[] prm = { input.Username, input.OldPassword, input.NewPassword };

            string returnMessage = "";
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SET_UPDATE_CHANGE_PASSWORD", prm);
            while (reader.Read())
            {
                returnMessage = (string)reader["RETURN_MESSAGE"];
            }

            return returnMessage;
        }
        #endregion

        public mResult CheckUserAuthen(CheckAuthen input)
        {
            mResult result = new mResult();

            mcsappcenterDataContext db = new mcsappcenterDataContext();

            int ua_auth = 0;
            string subfilename = input.sMn_File_Name.Substring(6);

            if (subfilename == "status")
            {
                ua_auth = 1;
            }
            else if (subfilename == "inform_ncr" || subfilename == "recheck_ncr")
            {
                ua_auth = 2;
            }
            else if (subfilename == "confirm_ncr")
            {
                ua_auth = 4;
            }

            V306_Group us_check = db.V306_Groups.Where(ua => ua.US_ID == input.sUsername && ua.MN_FILE_NAME == input.sMn_File_Name && ua.UA_AUTH >= ua_auth).FirstOrDefault();

            if (us_check == null)
            {
                result.RESULT = "false";
                result.EXCEPTION = "";
            }
            else
            {
                result.RESULT = "true";
                result.EXCEPTION = "";
            }
            return result;
        }

        public mResult CheckGroupAuthen(CheckAuthen input)
        {
            mResult result = new mResult();

            mcsappcenterDataContext db = new mcsappcenterDataContext();


            V306_Group gu_check = db.V306_Groups.Where(ua => ua.US_ID == input.sUsername && ua.MN_FILE_NAME == input.sMn_File_Name).FirstOrDefault();

            if (gu_check == null)
            {
                result.RESULT = "false";
                result.EXCEPTION = "";
            }
            else
            {
                result.RESULT = "true";
                result.EXCEPTION = "";
            }
            return result;
        }

        #region SP_GET_PRODUCT_STATUS เช็คสถานะชิ้นงาน
        public GetProductInfo GetProductStatus(InputBarcode input)
        {
            GetProductInfo GetProductStatus = new GetProductInfo();
            GetProductStatus.getProductStatus = new List<GetProductStatus>();

            object[] prm = { input.nPJ_ID, input.nPd_Item };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_PRODUCT_STATUS", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    GetProductStatus.FPROJ = (string)reader["FPROJ"];
                    GetProductStatus.TYPE = (string)reader["TYPE"];
                    GetProductStatus.SPROJ = (string)reader["SPROJ"];
                    GetProductStatus.SHARP = (Int16)reader["SHARP"];
                    GetProductStatus.ITEM = (Int16)reader["ITEM"];
                    GetProductStatus.CODE = (string)reader["CODE"];
                    GetProductStatus.DWG = (string)reader["DWG"];
                    GetProductStatus.SIZE = reader["SIZE"].ToString();
                    GetProductStatus.LENGTH = (decimal)reader["LENGTH"];
                    GetProductStatus.WEIGHT = (decimal)reader["WEIGHT"];
                }
            }
            else
            {
                GetProductStatus.FPROJ = "-";
                GetProductStatus.TYPE = "-";
                GetProductStatus.SPROJ = "-";
                GetProductStatus.SHARP = 0;
                GetProductStatus.ITEM = 0;
                GetProductStatus.CODE = "-";
                GetProductStatus.DWG = "-";
                GetProductStatus.SIZE = "-";
                GetProductStatus.LENGTH = 0;
                GetProductStatus.WEIGHT = 0;
            }


            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        GetProductStatus.getProductStatus.Add(new GetProductStatus
                        {
                            PC_NAME = (string)reader["PC_NAME"],
                            PA_ACTUAL_DATE = (string)reader["PA_ACTUAL_DATE"],
                            ANAME = (string)reader["ANAME"]
                        });
                    }

                }
                else
                {
                    GetProductStatus.getProductStatus.Add(new GetProductStatus
                    {
                        PC_NAME = "-",
                        PA_ACTUAL_DATE = "-",
                        ANAME = "-"
                    });
                }
            }
            return GetProductStatus;
        }
        #endregion

        #region GET_PRODUCT_SEND เช็คสถานะส่งงาน
        public List<GetProcessPD> GetProductProcess()
        {
            return new List<GetProcessPD>
            {
                new GetProcessPD {TYPE_ID = 1, TYPE_NAME = "Built Box" },
                new GetProcessPD {TYPE_ID = 2, TYPE_NAME = "Built Beam" },
                new GetProcessPD {TYPE_ID = 3, TYPE_NAME = "Fab" },
                new GetProcessPD {TYPE_ID = 4, TYPE_NAME = "Weld" }
            };
        }

        public GetProductSend GetProductSend(InputBarcodeType input)
        {
            GetProductSend GetProductSend = new GetProductSend();
            GetProductSend.getListProductSend = new List<GetListProductSend>();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.Type };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_PRODUCT_SEND", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    GetProductSend.PJ_NAME = (string)reader["pj_name"];
                }
            }
            else
            {
                GetProductSend.PJ_NAME = "-";
            }

            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        GetProductSend.getListProductSend.Add(new GetListProductSend
                        {
                            PC_NAME = (string)reader["PC_NAME"],
                            TIMES = (string)reader["TIMES"],
                            NAMES = (string)reader["NAMES"],
                            ACTUAL = (int)reader["ACTUAL"]
                        });
                    }
                }
                else
                {
                    GetProductSend.getListProductSend.Add(new GetListProductSend
                    {
                        PC_NAME = "-",
                        TIMES = "-",
                        NAMES = "-",
                        ACTUAL = 0
                    });
                }
            }

            return GetProductSend;
        }
        #endregion

        #region GET_BUILT_PRODUCT_DETAIL_STEP_1
        public BuiltDetailProject GetBuiltProjectDetails(InputBuiltProject input)
        {
            BuiltDetailProject BuiltInfo = new BuiltDetailProject();

            BuiltInfo.getItem = new List<GetItem>();

            BuiltInfo.getBuiltType = new List<GetBuiltType>();

            BuiltInfo.getProcess = new List<GetProcess>();

            BuiltInfo.PC_ID = new int();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.Users };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_M_GET_BUILT_PRODUCT_DETAIL_STEP_1", prm);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    BuiltInfo.PJ_ID = (int)reader["PJ_ID"];
                    BuiltInfo.PROJECT = (string)reader["PROJECT"];
                    BuiltInfo.PD_ITEM = (Int16)reader["PD_ITEM"];
                    BuiltInfo.PD_WEIGHT = (decimal)reader["PD_WEIGHT"];
                }
            }
            else
            {
                BuiltInfo.PJ_ID = 0;
                BuiltInfo.PROJECT = "-";
                BuiltInfo.PD_ITEM = 0;
                BuiltInfo.PD_WEIGHT = 0;
            }

            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        BuiltInfo.getItem.Add(new GetItem
                        {
                            ITEM_NO = (Int16)reader["ITEM_NO"],
                            ITEM_NO_NAME = (Int16)reader["ITEM_NO_NAME"]
                        });
                    }
                }
                else
                {
                    BuiltInfo.getItem.Add(new GetItem
                    {
                        ITEM_NO = 0,
                        ITEM_NO_NAME = 0
                    });
                }
            }

            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        BuiltInfo.getBuiltType.Add(new GetBuiltType
                        {
                            BUILT_TYPE_ID = (Int16)reader["BUILT_TYPE_ID"],
                            BUILT_TYPE_NAME = (string)reader["BUILT_TYPE_NAME"]
                        });
                    }
                }
                else
                {
                    BuiltInfo.getBuiltType.Add(new GetBuiltType
                    {
                        BUILT_TYPE_ID = 0,
                        BUILT_TYPE_NAME = "-"
                    });
                }
            }

            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        BuiltInfo.getProcess.Add(new GetProcess
                        {
                            PC_ID = (int)reader["PC_ID"],
                            PC_NAME = (string)reader["PC_NAME"]
                        });
                    }
                }
                else
                {
                    BuiltInfo.getProcess.Add(new GetProcess
                    {
                        PC_ID = 0,
                        PC_NAME = "-"
                    });
                }
            }
            if (reader.NextResult())
            {
                while (reader.Read())
                {
                    BuiltInfo.PC_ID = (int)reader["PC_ID"];
                }
            }
            return BuiltInfo;
        }
        #endregion

        #region GET_BUILT_PRODUCT_DETAIL_STEP_2
        public GetBuiltStatus GetbuiltStatus(InputCheckBuilt input)
        {
            GetBuiltStatus getBuiltStatus = new GetBuiltStatus();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.nItem_no, input.nBuilt_Type, input.nProcess };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_M_GET_BUILT_PRODUCT_DETAIL_STEP_2", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    getBuiltStatus.PC_ID = (int)reader["PC_ID"];
                    getBuiltStatus.PC_NAME = (string)reader["PC_NAME"];
                    getBuiltStatus.UG_NAME = (string)reader["UG_NAME"];
                    getBuiltStatus.US_FNAME = (string)reader["US_FNAME"];
                    getBuiltStatus.PA_PLAN_DATE = (string)reader["PA_PLAN_DATE"];
                    getBuiltStatus.PA_ACTUAL_DATE = (string)reader["PA_ACTUAL_DATE"];
                }
            }
            else
            {
                getBuiltStatus.PC_ID = 0;
                getBuiltStatus.PC_NAME = "-";
                getBuiltStatus.UG_NAME = "-";
                getBuiltStatus.US_FNAME = "-";
                getBuiltStatus.PA_PLAN_DATE = "-";
                getBuiltStatus.PA_ACTUAL_DATE = "-";
            }
            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        getBuiltStatus.PC_ID2 = (int)reader["PC_ID"];
                        getBuiltStatus.PC_NAME2 = (string)reader["PC_NAME"];
                        getBuiltStatus.UG_NAME2 = (string)reader["UG_NAME"];
                        getBuiltStatus.PA_PLAN_DATE2 = Convert.ToDateTime(reader["PA_PLAN_DATE"]).ToString("yyyy-MM-dd");
                    }
                }
                else
                {
                    getBuiltStatus.PC_ID2 = 0;
                    getBuiltStatus.PC_NAME2 = "-";
                    getBuiltStatus.UG_NAME2 = "-";
                    getBuiltStatus.PA_PLAN_DATE2 = "-";
                }
            }
            return getBuiltStatus;
        }

        #endregion

        #region SP_CHECK_SET_BUILT_ACTUAL ส่งงาน built
        public ResultStatus setBuiltActualSend(InputBuiltActualSend input)
        {
            ResultStatus ResultStatus = new ResultStatus();
            object[] prm = { input.nPJ_ID, input.nPd_Item, input.nItem_no, input.nBuilt_Type, input.nProcess, input.sUser_work, input.sPlace, input.sUser_id, input.nResult };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_SET_BUILT_SENT", prm);
            while (reader.Read())
            {
                ResultStatus.RESULT = (string)reader["ERROR"];
            }
            return ResultStatus;
        }
        #endregion


        #region SP_GET_FAB_PRODUCT_DETAIL_1
        public FabGetProcess GetFabProcessDetail(InputFabBarcode input)
        {
            FabGetProcess FabGetProcess = new FabGetProcess();
            FabGetProcess.getProcessFab = new List<GetProcessFab>();

            McsAppDataContext dc = new McsAppDataContext();

            var query = (from f in dc.FAB_SUMs
                         where f.pc_id != 81000 && f.pj_id == input.nPJ_ID && f.pd_item == input.nPd_Item
                         orderby f.pc_id
                         select new { PC_ID = f.pc_id, PC_NAME = dc.sf_ProcessName(f.pc_id.ToString()) }).FirstOrDefault();

            if (query == null)
            {
                FabGetProcess.getProcessFab.Add(new GetProcessFab
                {
                    PC_ID = 0,
                    PC_NAME = "-"
                });
            }
            else
            {
                var fab_pc = (from f in dc.FAB_SUMs
                              where f.pc_id != 81000 && f.pj_id == input.nPJ_ID && f.pd_item == input.nPd_Item
                              select new { PC_ID = f.pc_id, PC_NAME = dc.sf_ProcessName(f.pc_id.ToString()) });

                foreach (var pc_fab in fab_pc)
                {
                    FabGetProcess.getProcessFab.Add(new GetProcessFab
                    {
                        PC_ID = (int)pc_fab.PC_ID,
                        PC_NAME = (string)pc_fab.PC_NAME
                    });
                }
            }
            return FabGetProcess;
        }
        #endregion

        #region SP_GET_FAB_PRODUCT_DETAIL_2
        public FabGetProject GetFabProjectDetail(InputFabBarcodePC input)
        {
            FabGetProject FabGetProject = new FabGetProject();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.nProcess };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_FAB_PRODUCT_DETAIL_2", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    FabGetProject.PJ_ID = (int)reader["PJ_ID"];
                    FabGetProject.PD_ITEM = (Int16)reader["PD_ITEM"];
                    FabGetProject.PJ_NAME = (string)reader["PJ_NAME"];
                    FabGetProject.PT_NAME = (string)reader["PT_NAME"];
                    FabGetProject.UG_NAME = (string)reader["UG_NAME"];
                    FabGetProject.PLANDATE = (string)reader["PLANDATE"];
                    FabGetProject.PC_ID = (int)reader["PC_ID"];
                    FabGetProject.PC_NAME = (string)reader["PC_NAME"];
                    FabGetProject.FAB_PLAN = (int)reader["FAB_PLAN"];
                    FabGetProject.FAB_ACTUAL = (int)reader["FAB_ACTUAL"];
                }
            }
            else
            {
                FabGetProject.PJ_ID = 0;
                FabGetProject.PD_ITEM = 0;
                FabGetProject.PJ_NAME = "-";
                FabGetProject.PT_NAME = "-";
                FabGetProject.UG_NAME = "-";
                FabGetProject.PLANDATE = "-";
                FabGetProject.PC_ID = 0;
                FabGetProject.PC_NAME = "-";
                FabGetProject.FAB_PLAN = 0;
                FabGetProject.FAB_ACTUAL = 0;
            }
            return FabGetProject;
        }
        #endregion

        #region SP_GET_FAB_PRODUCT_DETAIL_3
        public List<FabGetGroupUser> GetFabGroupUser(InputFabGroup input)
        {
            object[] prm = { input.sUG_Name };
            List<FabGetGroupUser> listFabGroupUser = new List<FabGetGroupUser>();
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_FAB_PRODUCT_DETAIL_3", prm);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    listFabGroupUser.Add(new FabGetGroupUser
                    {
                        US_ID = (string)reader["US_ID"],
                        FULLNAME = reader["FULLNAME"].ToString()
                    });
                }
            }
            else
            {
                listFabGroupUser.Add(new FabGetGroupUser
                {
                    US_ID = "-",
                    FULLNAME = "-"
                });
            }
            return listFabGroupUser;
        }
        #endregion

        #region SP_SET_FAB_SENT_REPORT ส่งงาน Fab
        public ResultStatus SetFabActual(InputFabActualSend input)
        {
            ResultStatus REsultstatus = new ResultStatus();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.nProcess, input.nVal, input.sUserActual, input.sPlace, input.sUserID, input.Result };

            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_SET_FAB_SENT_REPORT", prm);

            while (reader.Read())
            {
                REsultstatus.RESULT = (string)reader["ERROR"];
            }
            return REsultstatus;
        }
        #endregion


        #region SP_GET_WELD_PRODUCT_DETAIL_1
        public WeldGetProcess GetWeldProcessDetail(InputWeldBarcode input)
        {
            WeldGetProcess WeldGetProcess = new WeldGetProcess();
            WeldGetProcess.getProcessWeld = new List<GetProcessWeld>();

            McsAppDataContext dc = new McsAppDataContext();

            var query = (from w in dc.WELD_SUMs
                         where w.pc_id != 82000 && w.pj_id == input.nPJ_ID && w.pd_item == input.nPd_Item
                         orderby w.pc_id
                         select new { PC_ID = w.pc_id, PC_NAME = dc.sf_ProcessName(w.pc_id.ToString()) }).FirstOrDefault();

            if (query == null)
            {
                WeldGetProcess.getProcessWeld.Add(new GetProcessWeld
                {
                    PC_ID = 0,
                    PC_NAME = "-"
                });
            }
            else
            {
                var weld_pc = (from w in dc.WELD_SUMs
                               where w.pc_id != 82000 && w.pj_id == input.nPJ_ID && w.pd_item == input.nPd_Item
                               orderby w.pc_id
                               select new { PC_ID = w.pc_id, PC_NAME = dc.sf_ProcessName(w.pc_id.ToString()) });

                foreach (var pc_weld in weld_pc)
                {
                    WeldGetProcess.getProcessWeld.Add(new GetProcessWeld
                    {
                        PC_ID = (int)pc_weld.PC_ID,
                        PC_NAME = (string)pc_weld.PC_NAME
                    });
                }
            }
            return WeldGetProcess;
        }
        #endregion

        #region SP_GET_WELD_PRODUCT_DETAIL_2
        public WeldGetProject GetWeldProjectDetail(InputWeldBarcodePC input)
        {
            WeldGetProject WeldGetProject = new WeldGetProject();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.nProcessID };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_WELD_PRODUCT_DETAIL_2", prm);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    WeldGetProject.PJ_ID = (int)reader["PJ_ID"];
                    WeldGetProject.PD_ITEM = (Int16)reader["PD_ITEM"];
                    WeldGetProject.PJ_NAME = (string)reader["PJ_NAME"];
                    WeldGetProject.PT_NAME = (string)reader["PT_NAME"];
                    WeldGetProject.UG_NAME = (string)reader["UG_NAME"];
                    WeldGetProject.PLANDATE = (string)reader["PLANDATE"];
                    WeldGetProject.PC_ID = (int)reader["PC_ID"];
                    WeldGetProject.PC_NAME = (string)reader["PC_NAME"];
                    WeldGetProject.WELD_PLAN = (int)reader["WELD_PLAN"];
                    WeldGetProject.WELD_ACTUAL = (int)reader["WELD_ACTUAL"];
                }
            }
            else
            {
                WeldGetProject.PJ_ID = 0;
                WeldGetProject.PD_ITEM = 0;
                WeldGetProject.PJ_NAME = "-";
                WeldGetProject.PT_NAME = "-";
                WeldGetProject.UG_NAME = "-";
                WeldGetProject.PLANDATE = "-";
                WeldGetProject.PC_ID = 0;
                WeldGetProject.PC_NAME = "-";
                WeldGetProject.WELD_PLAN = 0;
                WeldGetProject.WELD_ACTUAL = 0;
            }
            return WeldGetProject;
        }
        #endregion

        #region SP_GET_WELD_PRODUCT_DETAIL_3
        public List<WeldGetGroupUser> GetWeldGroupUser(InputWeldGroup input)
        {
            object[] prm = { input.sUG_Name };
            List<WeldGetGroupUser> listWeltGroupUser = new List<WeldGetGroupUser>();
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_WELD_PRODUCT_DETAIL_3", prm);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    listWeltGroupUser.Add(new WeldGetGroupUser
                    {
                        US_ID = (string)reader["US_ID"],
                        FULLNAME = (string)reader["FULLNAME"]
                    });
                }
            }
            else
            {
                listWeltGroupUser.Add(new WeldGetGroupUser
                {
                    US_ID = "-",
                    FULLNAME = "-"
                });
            }
            return listWeltGroupUser;
        }
        #endregion

        #region SP_SET_WELD_SENT_REPORT ส่งงาน Weld
        public ResultStatus SetWeldActual(InputWeldActualSend input)
        {
            ResultStatus ResultSTatus = new ResultStatus();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.nProcess, input.nVal, input.sUserActual, input.sPlace, input.sUserID, input.Result };

            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_SET_WELD_SENT_REPORT", prm);
            while (reader.Read())
            {
                ResultSTatus.RESULT = (string)reader["ERROR"];
            }
            return ResultSTatus;
        }
        #endregion


        #region SP_M_GET_OTHER_PROCESS_DETAIL_STEP_1
        public GetListPcRev GetOtherProcess(InputOtherStep1 input)
        {
            GetListPcRev oTherGet = new GetListPcRev();
            oTherGet.getListPC = new List<GetOtherProcess>();
            oTherGet.getListRev = new List<GetOtherRev>();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.sUserID };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_M_GET_OTHER_PROCESS_DETAIL_STEP_1", prm);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    oTherGet.getListPC.Add(new GetOtherProcess
                    {
                        PC_ID = (int)reader["PC_ID"],
                        PC_NAME = (string)reader["PC_NAME"]
                    });
                }
            }
            else
            {
                oTherGet.getListPC.Add(new GetOtherProcess
                {
                    PC_ID = 0,
                    PC_NAME = "-"
                });
            }

            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        oTherGet.getListRev.Add(new GetOtherRev
                        {
                            PD_REV = (Int16)reader["PD_REV"],
                            PD_REV_NAME = (Int16)reader["PD_REV_NAME"]
                        });
                    }
                }
                else
                {
                    oTherGet.getListRev.Add(new GetOtherRev
                    {
                        PD_REV = 0,
                        PD_REV_NAME = 0
                    });
                }
            }

            if (reader.Read())
            {
                while (reader.Read())
                {
                    oTherGet.PC_ID = (int)reader["PC_ID"];
                    oTherGet.PD_REV = (Int16)reader["PD_REV"];
                }
            }
            return oTherGet;
        }
        #endregion

        #region SP_M_GET_OTHER_PROCESS_DETAIL_STEP_2
        public OtherGetProcessDetail GetOtherProcessDetail(InputOtherStep2 input)
        {
            OtherGetProcessDetail OtherGEtProcess = new OtherGetProcessDetail();
            OtherGEtProcess.getOtherUser = new List<GetOtherUser>();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.nPd_Rev, input.nProcess };

            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_M_GET_OTHER_PROCESS_DETAIL_STEP_2", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    OtherGEtProcess.PJ_ID = (int)reader["PJ_ID"];
                    OtherGEtProcess.PROJECT = (string)reader["PROJECT"];
                    OtherGEtProcess.PD_ITEM = (Int16)reader["PD_ITEM"];
                    OtherGEtProcess.PD_REV = (Int16)reader["PD_REV"];
                    OtherGEtProcess.UG_NAME = (string)reader["UG_NAME"];
                    OtherGEtProcess.PA_PLAN_DATE = (string)reader["PA_PLAN_DATE"];
                    OtherGEtProcess.PA_STATUS = (string)reader["PA_STATUS"];
                    OtherGEtProcess.PA_STATE = (Int16)reader["PA_STATE"];
                }
            }
            else
            {
                OtherGEtProcess.PJ_ID = 0;
                OtherGEtProcess.PROJECT = "-";
                OtherGEtProcess.PD_ITEM = 0;
                OtherGEtProcess.PD_REV = 0;
                OtherGEtProcess.UG_NAME = "-";
                OtherGEtProcess.PA_PLAN_DATE = "-";
                OtherGEtProcess.PA_STATUS = "-";
                OtherGEtProcess.PA_STATE = 0;
            }
            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        OtherGEtProcess.getOtherUser.Add(new GetOtherUser
                        {
                            US_ID = (string)reader["US_ID"],
                            FULLNAME = (string)reader["FULLNAME"]
                        });
                    }
                }
                else
                {
                    OtherGEtProcess.getOtherUser.Add(new GetOtherUser
                    {
                        US_ID = "-",
                        FULLNAME = "-"
                    });
                }

            }
            return OtherGEtProcess;
        }
        #endregion

        #region SP_SET_OTHER_ACTUAL_SEND ส่งงานอื่นๆ
        public ResultStatus SetOtherActual(InputOtherActualSend input)
        {
            ResultStatus reSultsTatus = new ResultStatus();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.nPd_Rev, input.nProcess, input.sUserID, input.sPlace, input.sUser_Actual, input.nResult };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_SET_OTHER_SENT", prm);
            while (reader.Read())
            {
                reSultsTatus.RESULT = (string)reader["RESULT"];
            }
            return reSultsTatus;
        }
        #endregion


        #region SP_GET_REVISE_PLAN_GROUP  //แสดงกรุ๊ป
        public List<GetGruopRevise> GetGroupReviseInfo()
        {
            McsAppDataContext dc = new McsAppDataContext();

            List<GetGruopRevise> GroupData = new List<GetGruopRevise>();

            var item = ((from Fab in dc.V301_FabRevs select Fab.ug_name)
                .Union(from Weld in dc.V301_WeldRevs select Weld.ug_name));
            foreach (string items in item)
            {
                GroupData.Add(new GetGruopRevise()
                {
                    UG_NAME = items
                });
            }
            return GroupData;
        }

        public GetRevisePlanGroup GetReviseInfo(InputReviseGroup input) //แสดงแผนงานประกอบ - เชื่อม
        {
            McsAppDataContext dc = new McsAppDataContext();

            GetRevisePlanGroup getRevisePlan = new GetRevisePlanGroup();
            getRevisePlan.getRevise = new List<GetRevisePlan>();

            var FabWeld = ((from f in dc.V301_FabRevs where f.ug_name == input.sUG_Name select new { f.pj_id, f.pd_item, f.mp_sname, f.pt_ItemType, f.pj_sharp, f.ug_name, RevDate = f.revDate })
            .Union(from w in dc.V301_WeldRevs where w.ug_name == input.sUG_Name select new { w.pj_id, w.pd_item, w.mp_sname, w.pt_ItemType, w.pj_sharp, w.ug_name, RevDate = w.revDateWeld }));

            foreach (var items in FabWeld)
            {
                getRevisePlan.getRevise.Add(new GetRevisePlan()
                {
                    PD_ITEM = items.pd_item,
                    MP_SNAME = items.mp_sname,
                    PT_ITEMTYPE = items.pt_ItemType,
                    PJ_SHARP = (Int16)items.pj_sharp,
                    UG_NAME = items.ug_name,
                    REVDATE = items.RevDate
                });
            }

            var total = FabWeld.Count();

            {
                getRevisePlan.TOTAL = total;
            }
            return getRevisePlan;
        }
        #endregion


        #region SP_GET_REVISE_REV_NO
        public GetReviseRev GetreviseRevNo(InputRevisePJ input)
        {
            GetReviseRev GetreviseREv = new GetReviseRev();
            GetreviseREv.getRevno = new List<GetRevno>();
            GetreviseREv.getProcessRev = new List<GetProcessRev>();

            McsAppDataContext dc = new McsAppDataContext();

            REV_PLAN icheck = dc.REV_PLANs.Where(r => r.PJ_ID == input.nPJ_ID && r.ITEM == input.nPd_Item).FirstOrDefault();

            if (icheck != null)
            {
                var query_rev = (from r in dc.REV_PLANs where r.PJ_ID == input.nPJ_ID && r.ITEM == input.nPd_Item orderby r.REV_NO select r.REV_NO).Distinct();
                foreach (var items in query_rev)
                {
                    GetreviseREv.getRevno.Add(new GetRevno
                    {
                        REV_NO = (int)items
                    });
                }
            }
            else
            {
                GetreviseREv.getRevno.Add(new GetRevno
                {
                    REV_NO = 0
                });
            }

            REV_PROCESS icheck_pc = dc.REV_PROCESSes.OrderBy(pc => pc.PC_ID).FirstOrDefault();

            if (icheck_pc != null)
            {
                var query_pc = (from p in dc.REV_PROCESSes orderby p.PC_ID select new { p.PC_NAME, p.PC_ID }).Distinct();
                foreach (var item in query_pc)
                {
                    GetreviseREv.getProcessRev.Add(new GetProcessRev
                    {
                        PROCESS_NAME = item.PC_NAME.ToString(),
                        PC_ID = (int)item.PC_ID
                    });
                }
            }
            else
            {
                GetreviseREv.getProcessRev.Add(new GetProcessRev
                {
                    PROCESS_NAME = "-",
                    PC_ID = 0
                });
            }
            return GetreviseREv;
        }
        #endregion

        #region SP_GET_REVISE_PLAN_DETAIL
        public GetReviseDetail GetreviseDetail(InputReviseDT input)
        {
            GetReviseDetail GetReviseFab = new GetReviseDetail();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.nRev_no, input.nProcess };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_REVISE_PLAN_DETAIL", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    GetReviseFab.PJ_ID = (int)reader["PJ_ID"];
                    GetReviseFab.PROJECT = (string)reader["PROJECT"];
                    GetReviseFab.ITEM = (Int16)reader["ITEM"];
                    GetReviseFab.REV_NO = (int)reader["REV_NO"];
                    GetReviseFab.DESIGN_CHANGE = (string)reader["DESIGN_CHANGE"];
                    GetReviseFab.PC_ID = (int)reader["PC_ID"];
                    GetReviseFab.PROCESS = (string)reader["PROCESS"];
                    GetReviseFab.TYPES = (string)reader["TYPES"];
                    GetReviseFab.PLAN_GROUP = (string)reader["PLAN_GROUP"];
                    GetReviseFab.PLAN_ID = (int)reader["PLAN_ID"];
                    GetReviseFab.PLAN_DATE = (string)reader["PLAN_DATE"];
                    GetReviseFab.PLAN = (int)reader["PLAN"];
                }

                if (reader.NextResult())
                {

                    if (reader.NextResult())
                    {
                        if (reader.NextResult())
                        {
                            while (reader.Read())
                            {
                                GetReviseFab.ACTUAL = (int)reader["ACTUAL"];
                            }
                        }
                    }
                }
            }
            else
            {
                GetReviseFab.PJ_ID = 0;
                GetReviseFab.PROJECT = "-";
                GetReviseFab.ITEM = 0;
                GetReviseFab.REV_NO = 0;
                GetReviseFab.DESIGN_CHANGE = "-";
                GetReviseFab.PC_ID = 0;
                GetReviseFab.PROCESS = "-";
                GetReviseFab.TYPES = "-";
                GetReviseFab.PLAN_GROUP = "-";
                GetReviseFab.PLAN_ID = 0;
                GetReviseFab.PLAN_DATE = "-";
                GetReviseFab.PLAN = 0;
            }
            return GetReviseFab;
        }
        #endregion

        #region SP_GET_USER_IN_GROUP
        public List<GetReviseGroup> GetreviseGroup(InputGroupRevise input)
        {
            List<GetReviseGroup> GetReviseGroup = new List<GetReviseGroup>();

            object[] prm = { input.nUG_ID, input.sUG_Name };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_USER_IN_GROUP", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    GetReviseGroup.Add(new GetReviseGroup
                    {
                        US_ID = (string)reader["US_ID"],
                        FULLNAME = (string)reader["FULLNAME"]
                    });
                }
            }
            else
            {
                GetReviseGroup.Add(new GetReviseGroup
                {
                    US_ID = "-",
                    FULLNAME = "-"
                });
            }
            return GetReviseGroup;
        }
        #endregion

        #region SP_SET_REVISE_ACTUAL
        public ResultStatus SetREviseActual(InputReviseActual input)
        {
            ResultStatus SetReviseActual = new ResultStatus();
            int check = 0;

            object[] prm = { input.sUserID, input.nPJ_ID, input.nPd_Item, input.nRev_no, input.nProcess };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_CHECK_GROUP", prm);
            while (reader.Read())
            {
                check = (int)reader["RESULT"];
            }

            if (check > 0)
            {
                object[] prm2 = { input.nPJ_ID, input.nPd_Item, input.nRev_no, input.nProcess, input.sUserActual, input.nActual_QTY, input.sUserID, input.QTY };
                var reader2 = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_SET_REVISE_ACTUAL", prm2);
                while (reader2.Read())
                {
                    SetReviseActual.RESULT = (string)reader2["RESULT"];
                }
            }
            else
            {
                SetReviseActual.RESULT = "ไม่มีสิทธ์ส่งงานข้ามกรุ๊ป";
            }
 
            return SetReviseActual;
        }
        #endregion


        #region Get NCR REV_NO CHECK_NO LIST ลิสต์ Rev_no และ check_no
        public NcrGetRevCheckList GetNcrRevCheckList(InputNcrBarcodePC input)
        {
            NcrGetRevCheckList GetNcrRevCheckList = new NcrGetRevCheckList();
            GetNcrRevCheckList.GetRevNo = new List<GetlistRevNo>();
            GetNcrRevCheckList.GetCheckNo = new List<GetlistCheckNo>();

            object[] prm = { input.sBarcode, input.nType_PC };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_NCR_REV_CHECK_NO_LIST", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    GetNcrRevCheckList.GetRevNo.Add(new GetlistRevNo
                    {
                        REV_NO = (int)reader["REV_NO"],
                        REV_NAME = (string)reader["REV_NAME"]
                    });
                }
            }
            else
            {
                GetNcrRevCheckList.GetRevNo.Add(new GetlistRevNo
                {
                    REV_NO = 0,
                    REV_NAME = "-"
                });
            }
            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        GetNcrRevCheckList.GetCheckNo.Add(new GetlistCheckNo
                        {
                            CHECK_NO = ((int)reader["CHECK_NO"] <= 0) ? 1 : (int)reader["CHECK_NO"],
                            CHECK_NAME = ((int)reader["CHECK_NAME"] <= 0) ? 1 : (int)reader["CHECK_NAME"]
                        });
                    }
                }
                else
                {
                    GetNcrRevCheckList.GetCheckNo.Add(new GetlistCheckNo
                    {
                        CHECK_NO = 1,
                        CHECK_NAME = 1
                    });
                }


            }
            return GetNcrRevCheckList;
        }
        #endregion

        #region SP_GET_DIM_PRODUCT   NCR แสดงรายละเอียดโปรดักส์
        public NcrProduct GetDimProduct(InputNcrPD input)
        {
            NcrProduct GetNcrProduct = new NcrProduct();

            McsAppDataContext dc = new McsAppDataContext();

            int project = 0;
            int items = 0;
            int point = input.sBarcode.IndexOf("-");

            if (point == -1)
            {
                project = 0;
                items = 0;
            }
            else
            {
                project = Convert.ToInt32(input.sBarcode.Substring(0, point));
                items = Convert.ToInt16(input.sBarcode.Substring(point + 1));
            }

            var query_dp = (from pd in dc.Products
                            join pt in dc.Product_Types on pd.pt_id equals pt.pt_id
                            where pd.pj_id == project && pd.pd_item == items
                            select new { pj_name = dc.sf_sname(project.ToString()), pt.pt_ItemType, pd.pd_item, pd.pd_dwg, pd.pd_code }).FirstOrDefault();

            if (query_dp != null)
            {
                GetNcrProduct.PROJECT = (string)query_dp.pj_name;
                GetNcrProduct.PT_ITEMTYPE = (string)query_dp.pt_ItemType;
                GetNcrProduct.PD_ITEM = (Int16)query_dp.pd_item;
                GetNcrProduct.PD_DWG = (string)query_dp.pd_dwg;
                GetNcrProduct.PD_CODE = (string)query_dp.pd_code;
            }
            else
            {
                GetNcrProduct.PROJECT = "-";
                GetNcrProduct.PT_ITEMTYPE = "-";
                GetNcrProduct.PD_ITEM = 0;
                GetNcrProduct.PD_DWG = "-";
                GetNcrProduct.PD_CODE = "-";
            }
            return GetNcrProduct;
        }
        #endregion

        #region GET NCR REPORT DESC ลิสต์รายการ ncr
        public List<NcrGetReport> GetNcrReport(InputNcrReport input)
        {
            List<NcrGetReport> NcrGetReport = new List<NcrGetReport>();
            string sNc_id = input.nPC_ID + input.sBarcode + "0" + input.nRev_No + input.nCheck_No;
            string sNc_state = "2";
            string sNd_state = "1";
            string sGroup_name = "";

            object[] us_id = { input.sUS_ID };
            var group_name = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "sp_Group", us_id);
            while (group_name.Read())
            {
                sGroup_name = (string)group_name["ug_name"];
            }

            object[] prm = { sNc_id, sNc_state, sNd_state, sGroup_name, input.nPC_ID, input.sBarcode, input.nRev_No };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_REPORT_NCR", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    NcrGetReport.Add(new NcrGetReport
                    {
                        GROUP_NAME = (string)reader["GROUP_NAME"],
                        NCR_NO = (string)reader["NCR_NO"],
                        NT_NAME = (string)reader["NT_NAME"],
                        DESCRIPTIONS = (string)reader["DESCRIPTIONS"],
                        NC_ID = (string)reader["NC_ID"],
                        OTHER_NO = (int)reader["OTHER_NO"],
                        IMG_BEFORE = (string)reader["IMG_BEFORE"],
                        PD_COMMENT = reader["PD_COMMENT"].ToString()
                    });
                }
            }
            else
            {
                NcrGetReport.Add(new NcrGetReport
                {
                    GROUP_NAME = "-",
                    NCR_NO = "-",
                    NT_NAME = "-",
                    DESCRIPTIONS = "-",
                    NC_ID = "-",
                    OTHER_NO = 0,
                    IMG_BEFORE = "No.jpg",
                    PD_COMMENT = "-"
                });
            }
            return NcrGetReport;
        }
        #endregion

        #region ส่งงาน  NCR
        public ResultStatus SetNcrSend(InputNcrPassNcr input)
        {
            DateTime now = DateTime.Now;
            McsQcDataContext dc = new McsQcDataContext();
            ResultStatus Result = new ResultStatus();

            NCR_DIM_CHECK items = dc.NCR_DIM_CHECKs.Where(nc => nc.NC_ID == input.sNC_ID && nc.NCR_NO == input.sNcr_No && nc.OTHER_NO == input.nOther_No).FirstOrDefault();

            try
            {
                if (input.getImage == false)
                {
                    items.ND_STATE = 3;
                    items.IMG_AFTER = "No.jpg";
                    items.USERS = input.sUS_ID;
                    items.TIMES = (DateTime)(now);
                    items.USER_UPDATE = input.sUS_ID;
                    items.TIME_UPDATE = (DateTime)(now);
                    items.PD_COMMENT = input.sPd_Comment;
                    items.PD_ACTUAL = input.sPd_Actual;
                    dc.SubmitChanges();

                    Result.RESULT = "T";

                    return Result;
                }
                else
                {
                    string img_before = "";

                    var img_name = from ncr in dc.NCR_DIM_CHECKs.Where(nc => nc.NC_ID == input.sNC_ID && nc.NCR_NO == input.sNcr_No && nc.OTHER_NO == input.nOther_No) 
                                   select ncr.IMG_BEFORE;
                    foreach (string item in img_name)
                    {
                        img_before = item;
                    }

                    if (img_before == "No.jpg")
                    {
                        items.ND_STATE = 3;
                        items.IMG_AFTER = input.sNC_ID + input.sNcr_No + "-" + input.nOther_No + ".jpg";
                        items.USERS = input.sUS_ID;
                        items.TIMES = (DateTime)(now);
                        items.USER_UPDATE = input.sUS_ID;
                        items.TIME_UPDATE = (DateTime)(now);
                        items.PD_COMMENT = input.sPd_Comment;
                        items.PD_ACTUAL = input.sPd_Actual;
                        dc.SubmitChanges();

                        Result.RESULT = "T";

                        return Result;
                    }
                    else
                    {
                        items.ND_STATE = 3;
                        items.IMG_AFTER = img_before;
                        items.USERS = input.sUS_ID;
                        items.TIMES = (DateTime)(now);
                        items.USER_UPDATE = input.sUS_ID;
                        items.TIME_UPDATE = (DateTime)(now);
                        items.PD_COMMENT = input.sPd_Comment;
                        items.PD_ACTUAL = input.sPd_Actual;
                        dc.SubmitChanges();

                        Result.RESULT = "T";

                        return Result;
                    }
                }
            }
            catch (Exception ex)
            {
                Result.RESULT = ex.Message;

                return Result;
            }
        }
        #endregion

        #region  แจ้งตรวจ Dimension
        public ResultStatus SetNCR_Request(InputNcrReport input)
        {
            ResultStatus Result = new ResultStatus();

            object[] prm = { input.nPC_ID, input.sBarcode, input.nRev_No, input.nCheck_No, input.sPlace, input.sUS_ID };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_SET_QA_DIMENSION_AD", prm);

            while (reader.Read())
            {
                Result.RESULT = (string)reader["RESULT"];
            }
            return Result;
        }
        #endregion

        public mResult uploadImages_PD(Stream fileUpload)
        {
            mResult result = new mResult();

            try
            {
                McsAppDataContext dc = new McsAppDataContext();

                string servPath = "";
                string fileSize = "";

                int point = 0;
                int width = 0;
                int height = 0;

                foreach (PDPC_config items in dc.PDPC_configs)
                {
                    if (items.config_key.Equals("PATH_AFTER")) servPath = items.config_value;
                    if (items.config_key.Equals("PICTURE")) fileSize = items.config_value;
                }

                if (fileSize != "")
                {
                    point = fileSize.IndexOf("x");
                    width = Convert.ToInt32(fileSize.Substring(0, point));
                    height = Convert.ToInt32(fileSize.Substring(point + 1));
                }

                //------------------------------------------------------------------------------------------------------

                var parser = new MultipartFormDataParser(fileUpload);

                var username = parser.Parameters["username"].Data;
                var barcode = parser.Parameters["barcode"].Data;
                var otherno = parser.Parameters["otherno"].Data;
                var imagename = parser.Parameters["imagename"].Data;

                var file = parser.Files.FirstOrDefault();

                Stream stream = file.Data;

                //------------------------------------------------------------------------------------------------------

                if (!Directory.Exists(servPath)) Directory.CreateDirectory(servPath);

                FileStream targetStream = null;
                Stream sourceStream = stream;

                string fileName = imagename + "-" + otherno + "-X.jpeg";
                string filePath = Path.Combine(servPath, fileName);

                using (targetStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    const int bufferLen = 4096;
                    byte[] buffer = new byte[bufferLen];
                    int count = 0;
                    int totalBytes = 0;

                    while ((count = sourceStream.Read(buffer, 0, bufferLen)) > 0)
                    {
                        totalBytes += count;

                        targetStream.Write(buffer, 0, count);
                    }

                    targetStream.Close();

                    sourceStream.Close();
                }

                resizeImage(filePath, width, height);

                result.RESULT = "true";
                result.EXCEPTION = "";
            }
            catch (Exception ex)
            {
                result.RESULT = "false";
                result.EXCEPTION = ex.Message;
            }

            return result;
        }


        private byte[] StreamToByte(Stream stream)
        {
            byte[] buffer = new byte[16 * 1024];

            using (MemoryStream ms = new MemoryStream())
            {
                int read;

                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                return ms.ToArray();
            }
        }


        private void resizeImage(string path, int width, int height)
        {
            Bitmap tmp = new Bitmap(path);
            Bitmap bmp = new Bitmap(tmp, width, height);

            bmp.Save(path.Substring(0, path.Length - 7) + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

            tmp.Dispose();
            bmp.Dispose();

            tmp = null;
            bmp = null;

            File.Delete(path);
        }

        #region พื้นที่
        public List<GetLocationList> getLocationList(InputLocation input)
        {
            List<GetLocationList> GetLocationList = new List<GetLocationList>();

            object[] prm = { input.sPlace };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_LOCATION_LIST", prm);

            while (reader.Read())
            {
                GetLocationList.Add(new GetLocationList
                {
                    PLACE = (string)reader["PLACE"],
                    PLACE_NAME = (string)reader["PLACE_NAME"]
                });
            }
            return GetLocationList;
        }
        #endregion

        #region UT Request
        public setRequestReport PD_UTRequest(getRequestReport input)
        {
            setRequestReport result = new setRequestReport();

            object[] obj = { input.pj_id, input.pd_item, input.item_process, input.user_request, input.place };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_SET_UT_REQUEST_REPORT", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.query = reader["ERROR"].ToString();
                }
            }

            return result;
        }
        #endregion

        #region Report UT Request
        public List<getProcessUt> PD_GetProcessUt()
        {
            return new List<getProcessUt>
            {
                new getProcessUt {process_id = "G", process_name  = "GMAW" },
                new getProcessUt {process_id = "S", process_name = "SAW" },
                new getProcessUt {process_id = "E", process_name = "ESW" },
            };
        }

        public List<setReportUTRequest> PD_ReportUTRequest(getReportUTRequest input)
        {
            List<setReportUTRequest> list = new List<setReportUTRequest>();

            object[] obj = { input.pj_id, input.pd_item, input.item_process, 1 };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_REPORT_UT_REQUEST", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new setReportUTRequest
                    {
                        pj_id = reader["PJ_ID"].ToString(),
                        pj_name = reader["PJ_NAME"].ToString(),
                        pd_item = reader["PD_ITEM"].ToString(),
                        pd_code = reader["PD_CODE"].ToString(),
                        pd_qty = reader["PD_QTY"].ToString(),
                        pd_dwg = reader["PD_DWG"].ToString(),
                        pd_size = reader["PD_SIZE"].ToString(),
                        place = reader["PLACE"].ToString(),
                        barcode = reader["BARCODE"].ToString(),
                        pa_no = reader["PA_NO"].ToString(),
                        item_process = reader["ITEM_PROCESS"].ToString(),
                        pc_name = reader["PC_NAME"].ToString(),
                        user_request = reader["USER_REQUEST"].ToString(),
                        request_name = reader["REQUEST_NAME"].ToString(),
                        request_date = reader["REQUEST_DATE"].ToString(),
                        ug_name = reader["UG_NAME"].ToString(),
                        ut_status = reader["UT_STATUS"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #endregion

        #region QC
        // Qc Plan Check----------------------------------------------------------------------------------------------
        #region List ชื่อโปรเจค
        public List<mProject> PCgetProjectName(iProject input)
        {
            List<mProject> results = new List<mProject>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT  MCSAPP.DBO.SF_FULLNAME(P.PJ_ID) PJ_NAME,P.PJ_ID 
                                            FROM 
                                            (SELECT P.PJ_ID, P.PD_ITEM ,0 AS REV_NO FROM MCSAPP..PRODUCT P  
                                            UNION 
                                            SELECT P.PJ_ID,P.ITEM  ,MAX(REV_NO) REV_NO FROM MCSAPP..REV_PLAN P 
                                            GROUP BY P.PJ_ID,P.ITEM) P 
				                            WHERE NOT EXISTS
              			                    (SELECT *
              			                    FROM NCR_CHECK NC    
						                    WHERE NC.PC_ID = @TYPE_PC And NC.NC_STATUS < 0
              			                    AND P.PJ_ID = CAST(LEFT(NC.BARCODE,5) AS INT)  AND P.PD_ITEM = CAST(RIGHT(NC.BARCODE,4) AS INT)   AND P.REV_NO = NC.REV_NO)   
			                                ORDER BY PJ_NAME";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mProject()
                        {
                            PJ_ID = string.Format("{0:00000}", (int)dr["PJ_ID"]),
                            PJ_NAME = (string)dr["PJ_NAME"]
                        });
                    }
                }

                connection.Close();
            }

            return results;
        }
        #endregion

        #region List ไอเทม
        public List<mItemPj> PCgetProjectItem(iItemPj input)
        {
            List<mItemPj> results = new List<mItemPj>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT P.PD_ITEM AS ITEM
                                               FROM 
                                                (SELECT P.PJ_ID, P.PD_ITEM ,0 AS REV_NO FROM MCSAPP..PRODUCT P  
                                                 UNION
                                                 SELECT P.PJ_ID,P.ITEM  ,MAX(REV_NO) REV_NO FROM MCSAPP..REV_PLAN P 
                                                 GROUP BY P.PJ_ID,P.ITEM) P 
                                             WHERE NOT EXISTS
                	                            (SELECT *
                			                            FROM NCR_CHECK NC    
                                                   WHERE NC.PC_ID = @TYPE_PC And NC.NC_STATUS < 0
                	                                AND MCSAPP.DBO.SF_BARCODE(P.PJ_ID,P.PD_ITEM) = NC.BARCODE AND P.REV_NO = NC.REV_NO) 
                                             AND P.PJ_ID = @PJ_ID
                                             ORDER BY PD_ITEM";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    command1.Parameters.Add("@PJ_ID", SqlDbType.Int, 5).Value = input.nPj_Id;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mItemPj()
                        {
                            PD_ITEM = dr["ITEM"].ToString()
                        });
                    }
                }

                connection.Close();
            }

            return results;
        }
        #endregion

        #region List Rev No
        public List<mRevNoPlanCheck> PCgetProjectRevno(iRevNoPlanCheck input)
        {
            List<mRevNoPlanCheck> results = new List<mRevNoPlanCheck>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT P.REV_NO,
                                               CASE WHEN P.REV_NO = 0 THEN 'FINAL' 
                                               ELSE CAST(P.REV_NO AS VARCHAR) END AS REV_NAME
                                                 FROM 
                                                  (SELECT P.PJ_ID, P.PD_ITEM ,0 AS REV_NO FROM MCSAPP..PRODUCT P 
                                                   UNION 
                                                   SELECT P.PJ_ID,P.ITEM  ,MAX(REV_NO) REV_NO FROM MCSAPP..REV_PLAN P 
                                                   GROUP BY P.PJ_ID,P.ITEM) P 
                                               WHERE NOT EXISTS
              	                                (SELECT * 
              			                        FROM NCR_CHECK NC    
                                                WHERE NC.PC_ID = @TYPE_PC And NC.NC_STATUS < 0
              	                                AND MCSAPP.DBO.SF_BARCODE(P.PJ_ID,P.PD_ITEM) = NC.BARCODE AND P.REV_NO = NC.REV_NO) 
                                                AND P.PJ_ID = @PJ_ID
                                                AND P.PD_ITEM = @PD_ITEM
                                                ORDER BY REV_NO";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    command1.Parameters.Add("@PJ_ID", SqlDbType.Int, 5).Value = input.nPj_Id;
                    command1.Parameters.Add("@PD_ITEM", SqlDbType.Int, 4).Value = input.nPd_Item;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mRevNoPlanCheck()
                        {
                            REV_NO = (int)dr["REV_NO"],
                            REV_NAME = dr["REV_NAME"].ToString()
                        });
                    }
                }

                connection.Close();
            }
            return results;
        }
        #endregion

        #region List Group Fab & Weld
        public List<mGroupFab> PCgetGroupFab()
        {
            List<mGroupFab> results = new List<mGroupFab>();

            McsQcDataContext db = new McsQcDataContext();

            var queryFabGroup = (from f in db.V301_MainProducts group f by f.F_GRP into Fabgroup orderby Fabgroup.Key select Fabgroup.Key);

            foreach (var items in queryFabGroup)
            {
                results.Add(new mGroupFab()
                {
                    F_GRP = items
                });
            }
            return results;
        }

        public List<mGroupWeld> PCgetGroupWeld()
        {
            List<mGroupWeld> results = new List<mGroupWeld>();

            McsQcDataContext db = new McsQcDataContext();

            var queryWeldGroup = (from w in db.V301_MainProducts group w by w.W_GRP into Weldgroup orderby Weldgroup.Key select Weldgroup.Key);
            foreach (var items in queryWeldGroup)
            {
                results.Add(new mGroupWeld()
                {
                    W_GRP = items
                });
            }
            return results;
        }
        #endregion

        #region List สถานะ
        public List<mStatus> PCgetStatus()
        {
            List<mStatus> results = new List<mStatus>();

            string[] name = new string[] { "0: QC Check", "1: QC Confirm to Repair", "2: PD Repair", "3: QC Recheck", "4: Not Pass", "5: OK" };
            int i = 0;

            foreach (string value in name)
            {
                results.Add(new mStatus()
                {
                    STATUS_ID = (int)i++,
                    STATUS_NAME = (string)value
                });
            }
            return results;
        }
        #endregion

        #region แสดงข้อมูล ncr plan check
        public List<mPjShowListCheck> PCgetProjectShowListCheck(iPjShowListCheck input)
        {
            List<mPjShowListCheck> results = new List<mPjShowListCheck>();

            try
            {
                object[] prm = { input.nTypePc, input.nPj_Id, input.nPd_Item, input.sRev_No, input.sCode, input.sGroupFab, input.sGroupWeld, input.sFab_Date, input.sWeld_Date, input.sNc_Status };

                var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_PLAN_NCR_CHECK", prm);

                while (reader.Read())
                {
                    results.Add(new mPjShowListCheck()
                    {
                        PJ_NAME = reader["PJ_NAME"].ToString(),
                        PD_ITEM = string.Format("{0:0000}", (Int16)reader["PD_ITEM"]),
                        FW_REV = reader["FW_REV"].ToString(),
                        PD_CODE = reader["PD_CODE"].ToString(),
                        PART = reader["PART"].ToString(),
                        F_GRP = reader["F_GRP"].ToString(),
                        F_DATE = reader["F_DATE"].ToString(),
                        W_GRP = reader["W_GRP"].ToString(),
                        W_DATE = reader["W_DATE"].ToString(),
                        OA_GRP = reader["OA_GRP"].ToString(),
                        OA_DATE = reader["OA_DATE"].ToString(),
                        CHECK_NO = reader["CHECK_NO"].ToString(),
                        COUNT_NCR = reader["COUNT_NCR"].ToString(),
                        DESIGN_CHANGE = reader["DESIGN_CHANGE"].ToString(),
                        NCR_STATUS = reader["NCR_STATUS"].ToString(),
                        USERS = reader["USERS"].ToString(),
                        NCR_UPDATE = reader["NCR_UPDATE"].ToString(),
                        PLACE = reader["PLACE"].ToString()
                    });
                }
            }
            catch (Exception)
            {
                throw;
            }
            return results;
        }
        #endregion


        // IN Form NCR------------------------------------------------------------------------------------------------
        #region ListData InFormNcr
        public mListDataForm IFgetListDataForm(iListDataForm input)
        {
            McsQcDataContext db = new McsQcDataContext();

            mListDataForm results = new mListDataForm();
            results.getDwgType = new List<mProjectImage>();
            results.getDirection = new List<mDirection>();

            #region List DWG Type
            if (input.nPt_MainType > 0)
            {
                var queryPjImageT = (from p in db.NCR_FROM_IMGs orderby p.POSITION_ID descending where 1 == 1 && p.PT_GROUP_TYPE_ID == input.nPt_MainType || p.PT_GROUP_TYPE_ID == 0 select p);
                foreach (var items_position in queryPjImageT)
                {
                    results.getDwgType.Add(new mProjectImage()
                    {
                        IMAGE_NAME = items_position.IMG_NAME.ToString(),
                        POSITION_ID = items_position.POSITION_ID.ToString()
                    });
                }
            }
            else
            {
                var queryPjImage = (from p in db.NCR_FROM_IMGs where 1 == 1 select p);
                foreach (var items_position in queryPjImage)
                {
                    results.getDwgType.Add(new mProjectImage()
                    {
                        IMAGE_NAME = items_position.IMG_NAME.ToString(),
                        POSITION_ID = items_position.POSITION_ID.ToString()
                    });
                }
            }
            #endregion

            #region List Direction
            foreach (vt_direction items_dt in db.vt_directions.OrderBy(d => d.drId))
            {
                results.getDirection.Add(new mDirection()
                {
                    DRNAME = items_dt.drName
                });
            }
            #endregion

            return results;
        }
        #endregion

        #region List Rev no
        public List<mRevNoInForm> IFgetProjectRevNo(iRevNoInForm input)
        {
            List<mRevNoInForm> results = new List<mRevNoInForm>();

            int project = 0;
            int items = 0;
            int point = input.sBarcode.IndexOf("-");

            if (point == -1)
            {
                project = 0;
                items = 0;
            }
            else
            {
                project = Convert.ToInt32(input.sBarcode.Substring(0, point));
                items = Convert.ToInt16(input.sBarcode.Substring(point + 1));
            }


            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT P.REV_NO,
                                                       CASE WHEN P.REV_NO = 0 THEN 'FINAL' 
                                                       ELSE CAST(P.REV_NO AS VARCHAR) END AS REV_NAME
                                                         FROM 
                                                          (SELECT P.PJ_ID, P.PD_ITEM ,0 AS REV_NO FROM MCSAPP..PRODUCT P 
                                                           UNION 
                                                           SELECT P.PJ_ID,P.ITEM  ,MAX(REV_NO) REV_NO FROM MCSAPP..REV_PLAN P 
                                                           GROUP BY P.PJ_ID,P.ITEM) P 
                                                       WHERE NOT EXISTS
                      	                                (SELECT * 
                      			                        FROM NCR_CHECK NC    
                                                        WHERE NC.PC_ID = @TYPE_PC And NC.NC_STATUS > 1  AND CHECK_NO = @CHECK_NO
                      	                                AND MCSAPP.DBO.SF_BARCODE(P.PJ_ID,P.PD_ITEM) = NC.BARCODE AND P.REV_NO = NC.REV_NO) 
                                                        AND P.PJ_ID = @PJ_ID
                                                        AND P.PD_ITEM = @PD_ITEM
                                                        ORDER BY REV_NO";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    command1.Parameters.Add("@CHECK_NO", SqlDbType.Int, 2).Value = input.nCheck_no;
                    command1.Parameters.Add("@PJ_ID", SqlDbType.Int, 5).Value = project;
                    command1.Parameters.Add("@PD_ITEM", SqlDbType.Int, 4).Value = items;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mRevNoInForm()
                        {
                            REV_NO = (string)dr["REV_NO"].ToString(),
                            REV_NAME = dr["REV_NAME"].ToString()
                        });
                    }
                }

                connection.Close();
            }
            return results;
        }
        #endregion

        #region get Project Detial
        public mChProjectDetail IFgetProjectDetail(iChProjectDetail input)
        {
            McsAppDataContext db = new McsAppDataContext();
            McsQcDataContext qc = new McsQcDataContext();
            int project = 0;
            int items = 0;
            int point = input.sBarcode.IndexOf("-");

            if (point == -1)
            {
                project = 0;
                items = 0;
            }
            else
            {
                project = Convert.ToInt32(input.sBarcode.Substring(0, point));
                items = Convert.ToInt16(input.sBarcode.Substring(point + 1));
            }

            mChProjectDetail results = new mChProjectDetail();

            try
            {
                if (project != 0)
                {
                    Project items_pj = db.Projects.Where(pj => pj.pj_id == project).FirstOrDefault();

                    if (items_pj != null)
                    {
                        results.PJ_NAME = items_pj.pj_name.ToString() + "-s" + items_pj.pj_sharp + "-Lot" + items_pj.pj_lot;
                    }
                    else
                    {
                        results.PJ_NAME = "====";
                    }

                    NCR_CHECK item_nc = qc.NCR_CHECKs.Where(nc => nc.PC_ID == input.nTypePc && nc.BARCODE == input.sBarcode && nc.REV_NO == input.nRev_no && nc.CHECK_NO == 1).FirstOrDefault();

                    if (item_nc != null)
                    {
                        results.NC_NCR = (int)item_nc.NC_NCR;
                        results.PLACE = (string)item_nc.PLAN_INS;
                    }
                    else
                    {
                        results.NC_NCR = 0;
                        results.PLACE = "";
                    }

                    if (results.PLACE == "")
                    {
                        Product place = db.Products.Where(p => p.pj_id == project && p.pd_item == items).FirstOrDefault();
                        if (place != null)
                        {
                            results.PLACE = (string)place.place;
                        }
                        else
                        {
                            results.PLACE = "";
                        }
                    }
                }

                object[] prm = { input.nTypePc, project, items, input.nRev_no };
                var reader1 = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_CHECK_PRODUCT_DETAIL", prm);

                string pa_state = "";

                if (reader1.HasRows)
                {
                    while (reader1.Read())
                    {
                        pa_state = reader1["PA_STATE"].ToString();
                    }

                    if (pa_state == "1")
                    {
                        var reader2 = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_CHECK_PRODUCT_DETAIL", prm);
                        while (reader2.Read())
                        {
                            results.ERROR = (string)reader2["ERROR"];
                            results.PLAN_DATE = ":: No Plan Date ::";
                            results.PD_CODE = ":: No Code ::";
                            results.PT_MAINTYPE = "0";
                            results.BARCODE = "";
                        }
                    }
                    else
                    {
                        var reader2 = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_CHECK_PRODUCT_DETAIL", prm);
                        while (reader2.Read())
                        {
                            results.PLAN_DATE = Convert.ToDateTime(reader2["PLAN_DATE"]).ToString("yyyy-MM-dd HH:mm");
                            results.PD_CODE = (string)reader2["PD_CODE"];
                            results.PT_MAINTYPE = reader2["PT_MAINTYPE"].ToString();
                            results.BARCODE = (string)reader2["BARCODE"];
                            results.ERROR = "";
                        }
                    }
                }
                else
                {
                    results.PLAN_DATE = "";
                    results.PD_CODE = "";
                    results.PT_MAINTYPE = "";
                    results.BARCODE = "";
                    results.ERROR = "";
                    results.PJ_NAME = "";
                    results.NC_NCR = 0;
                    results.UG_NAME = "";
                    results.PLACE = "";
                }
            }
            catch (Exception)
            {
                throw;
            }
            return results;
        }
        #endregion

        #region List ตำแหน่ง
        public List<mPosition> IFgetPositionList(iPosition input)
        {
            List<mPosition> results = new List<mPosition>();

            McsQcDataContext db = new McsQcDataContext();

            var queryNcId = from nc in db.NCR_CHECK_LISTs where nc.PT_GROUP_TYPE_ID == input.nPt_MainType select nc.NT_ID;
            var queryPosition = from cl in db.NCR_CHECK_LIST_TYPEs where cl.PC_ID == input.nTypePc && queryNcId.Contains(cl.NT_ID) select cl;

            foreach (var items in queryPosition)
            {
                results.Add(new mPosition()
                {
                    NT_NAME = items.NT_NAME,
                    NT_ID = items.NT_ID.ToString()
                });
            }
            return results;
        }
        #endregion

        #region List ความผิดพลาด
        public List<mDescriptions> IFgetDescriptionsNcr(iDescriptions input)
        {
            List<mDescriptions> results = new List<mDescriptions>();

            McsQcDataContext db = new McsQcDataContext();

            var queryDescNce = from cl in db.NCR_CHECK_LISTs
                               join nd in db.NCR_DESCs
                               on cl.DESC_ID equals nd.DESC_ID
                               where cl.PT_GROUP_TYPE_ID == input.nPt_MainType && cl.NT_ID == input.nNt_Id
                               orderby nd.DESCRIPTIONS
                               select new { cl.NCR_NO, nd.DESCRIPTIONS };

            foreach (var items in queryDescNce)
            {
                results.Add(new mDescriptions()
                {
                    DESCRIPTIONS = items.DESCRIPTIONS,
                    NCR_NO = items.NCR_NO.ToString()
                });
            }
            return results;
        }
        #endregion

        public mgetOther IFgetOther(igetOther input)
        {
            mgetOther result = new mgetOther();

            McsQcDataContext db = new McsQcDataContext();

            NCR_DIM_CHECK icheck = db.NCR_DIM_CHECKs.Where(nd => nd.NC_ID == input.sNc_id && nd.NCR_NO == input.sNcr_no).FirstOrDefault();

            if (icheck == null)
            {
                result.OTHER_NO = "1";
            }
            else
            {
                var query = (from nd in db.NCR_DIM_CHECKs
                             where nd.NC_ID == input.sNc_id && nd.NCR_NO == input.sNcr_no
                             select nd.OTHER_NO).Max() + 1;

                result.OTHER_NO = query.ToString();
            }
            return result;
        }


        #region แสดงกรุ๊ปที่รับผิดชอบ
        public mResponse IFgetResponse(iResponse input)
        {
            mResponse results = new mResponse();

            int project = 0;
            int items = 0;
            int point = input.sBarcode.IndexOf("-");

            if (point == -1)
            {
                project = 0;
                items = 0;
            }
            else
            {
                project = Convert.ToInt32(input.sBarcode.Substring(0, point));
                items = Convert.ToInt16(input.sBarcode.Substring(point + 1));
            }

            try
            {
                string group = "";
                string process;

                McsQcDataContext db = new McsQcDataContext();

                NCR_CHECK_LIST icheck = db.NCR_CHECK_LISTs.Where(nc => nc.NCR_NO.Equals(input.sNcr_no)).FirstOrDefault();

                if (icheck == null)
                {
                    group = "";
                    results.DESC_CHK = 0;
                }
                else
                {
                    group = icheck.RESPONS.ToString();
                    results.DESC_CHK = (int)icheck.DESC_CHK;
                }

                if (group != "")
                {
                    process = Position_Ncr_Process(input.sNcr_no);
                    if (Show_Group(project, items, process, 0) != "")
                    {
                        group += " / ";
                    }
                    group += Show_Group(project, items, process, 0);
                }
                else if (group == "")
                {
                    process = Position_Ncr_Process(input.sNcr_no);
                    group += Show_Group(project, items, process, 0);
                }
                results.RESPONSE_GROUP = group;
            }
            catch (Exception)
            {
                throw;
            }
            return results;
        }

        private string Position_Ncr_Process(string sNcr_no)
        {
            string process = "";
            string value_pc = "";

            McsQcDataContext db = new McsQcDataContext();

            foreach (NCR_RESPON items in db.NCR_RESPONs.Where(nc => nc.NCR_NO.Equals(sNcr_no)))
            {
                value_pc = items.PC_ID.ToString();
            }

            if (process != "")
            {
                process += "," + value_pc.ToString();
            }
            else
            {
                process = value_pc.ToString();
            }
            return process;
        }

        private string Show_Group(int nPj_Id, int nPd_Item, string sTypePc, int nRev_no)
        {
            string Group = "";
            string Value_g = "";
            //int i;
            string[] Str = sTypePc.Split(',');
            for (int i = 0; i < Str.Length; i++)
            {
                McsAppDataContext db = new McsAppDataContext();

                if (Str[i].Substring(0, 1) == "5")
                {
                    if (nRev_no == 0)
                    {
                        var query = from GFW in db.SF_GET_GROUP_FAB_WELD(nPj_Id, nPd_Item, nRev_no, Str[i]) select new { group_name = GFW.ToString() };

                        foreach (var item_g in query)
                        {
                            Value_g += item_g.group_name.ToString();
                        }

                        if (Group != "")
                        {
                            Group += " / " + Value_g.ToString();
                        }
                        else
                        {
                            Group += Value_g.ToString();
                        }
                    }
                }
                else if (Str[i].Substring(0, 1) == "1")
                {
                    var query = from GB in db.SF_GET_GROUP_BUILT(nPj_Id, nPd_Item, Str[i]) select new { group_name = GB };

                    foreach (var items in query)
                    {
                        Value_g = items.group_name.ToString();
                    }

                    if (Group != "")
                    {
                        Group += " / " + Value_g.ToString();
                    }
                    else
                    {
                        Group += Value_g.ToString();
                    }
                }
            }
            return Group;
        }
        #endregion

        #region แจ้ง NCR  : แบบมี NCR
        public mResult IFsetAddNcrCheck(iNcrCheckAdd input)
        {
            mResult result = new mResult();

            try
            {
                McsQcDataContext db = new McsQcDataContext();
                McsFg2010DataContext fg = new McsFg2010DataContext();

                var icheck = (from nd in db.NCR_DIM_CHECKs
                              join nc in db.NCR_CHECKs on nd.NC_ID equals nc.NC_ID
                              where nc.BARCODE == input.sBarcode && nc.REV_NO == input.nRev_no && nc.CHECK_NO == input.nCheck_no && nd.NCR_NO == input.sNcr_no && nd.OTHER_NO == input.nOther_no
                              select nd).FirstOrDefault();
                //เช็ค NCR นี้เคยส่งหรือยัง
                if (icheck == null)
                {
                    //เช็คพื้นที่
                    var check_st = (from ss in fg.stock_shelfs where ss.place == input.sPlanIns select ss.place).FirstOrDefault();
                    if (check_st != null)
                    {
                        //เช็ครูปภาพ
                        if (input.Get_Image == true)
                        {
                            string sImage_Name = input.sNc_id + input.sNcr_no + "-" + input.nOther_no + ".jpg";

                            object[] prm = { input.nTypePc, input.sBarcode, input.nRev_no, input.nCheck_no, input.sNcr_no, input.sUser, input.sUser, input.sGroup, sImage_Name, input.sSug, input.sDirection, input.nDwg_type, input.sPlanDate, input.sPlanIns, input.Dwg, input.Actual, 1 };

                            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_SET_NCR_CHECK", prm);

                            while (reader.Read())
                            {
                                result.RESULT = reader["ERROR"].ToString();
                                result.EXCEPTION = "";
                            }
                        }
                        else
                        {
                            object[] prm = { input.nTypePc, input.sBarcode, input.nRev_no, input.nCheck_no, input.sNcr_no, input.sUser, input.sUser, input.sGroup, "No.jpg", input.sSug, input.sDirection, input.nDwg_type, input.sPlanDate, input.sPlanIns, input.Dwg, input.Actual, 1 };

                            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_SET_NCR_CHECK", prm);

                            while (reader.Read())
                            {
                                result.RESULT = reader["ERROR"].ToString();
                                result.EXCEPTION = "";
                            }
                        }
                    }
                    else
                    {
                        result.RESULT = "กรุณาป้อนข้อมูลพื้นที่ให้ถูกต้อง";
                        result.EXCEPTION = "";
                    }
                }
                else
                {
                    result.RESULT = "ความผิดพลาด(NCR)นี้แจ้งไปแล้ว";
                    result.EXCEPTION = "";
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }
        #endregion

        #region แก้ไข NCR
        public mResult IFsetEditNcrCheck(iNcrCheckEdit input)
        {
            mResult result = new mResult();

            DateTime now = DateTime.Now;

            try
            {
                McsQcDataContext db = new McsQcDataContext();
                McsFg2010DataContext fg = new McsFg2010DataContext();

                var check_st = (from ss in fg.stock_shelfs where ss.place == input.sPlace select ss.place).FirstOrDefault();
                if (check_st != null)
                {

                    var query_nc = (from nc in db.NCR_CHECKs
                                    where nc.PC_ID == input.nTypePc && nc.BARCODE == input.sBarcode &&
                                        nc.REV_NO == input.nRev_No && nc.CHECK_NO == input.nCheck_No
                                    select nc).FirstOrDefault();

                    var query_nd = (from nd in db.NCR_DIM_CHECKs
                                    where nd.NCR_NO == input.sNcr_No && nd.OTHER_NO == input.nOther_no &&
                                    (from nc in db.NCR_CHECKs
                                     where nc.BARCODE == input.sBarcode && nc.REV_NO == input.nRev_No && nc.CHECK_NO == input.nCheck_No
                                     select new { nc.NC_ID }).Contains(new { NC_ID = nd.NC_ID })
                                    select nd).FirstOrDefault();


                    if (input.Get_Image == true)
                    {
                        query_nd.IMG_BEFORE = input.sFile;
                        query_nd.USER_UPDATE = input.sUsers;
                        query_nd.TIME_UPDATE = (DateTime)(now);
                        query_nd.SUGGEST = input.sSug;
                        query_nd.DIRECTION = input.sDirection;
                        query_nd.GROUP_NAME = input.sGroup;
                        query_nd.DWG = input.Dwg;
                        query_nd.ACTUAL = input.Actual;
                        query_nc.PLAN_INS = input.sPlace;
                    }
                    else
                    {
                        query_nd.IMG_BEFORE = input.sFile;
                        query_nd.USER_UPDATE = input.sUsers;
                        query_nd.TIME_UPDATE = (DateTime)(now);
                        query_nd.SUGGEST = input.sSug;
                        query_nd.DIRECTION = input.sDirection;
                        query_nd.GROUP_NAME = input.sGroup;
                        query_nd.DWG = input.Dwg;
                        query_nd.ACTUAL = input.Actual;
                        query_nc.PLAN_INS = input.sPlace;
                    }

                    db.SubmitChanges();

                    result.RESULT = "1|แก้ไขข้อมูลเรียบร้อยแล้ว";
                    result.EXCEPTION = "";
                }
                else
                {
                    result.RESULT = "กรุณาป้อนข้อมูลพื้นที่ให้ถูกต้อง";
                    result.EXCEPTION = "";
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถทำการแก้ไขข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }
        #endregion

        #region ลบ NCR
        public mResult IFsetDeleteNcrCheck(iNcrCheckDelete input)
        {
            mResult result = new mResult();

            DateTime now = DateTime.Now;

            try
            {
                McsQcDataContext db = new McsQcDataContext();

                if (input.sImage_Before != "No.jpg")
                {
                    string img = DeleteImage(input.sImage_Before);
                }

                var Del_Ncr = from nd in db.NCR_DIM_CHECKs
                              where nd.NC_ID == input.sNc_Id && nd.NCR_NO == input.sNcr_No && nd.OTHER_NO == input.nOther_no
                              select nd;

                foreach (var items in Del_Ncr)
                {
                    db.NCR_DIM_CHECKs.DeleteOnSubmit(items);
                }

                db.SubmitChanges();

                NCR_DIM_CHECK icheck = db.NCR_DIM_CHECKs.Where(nd => nd.NC_ID == input.sNc_Id).FirstOrDefault();

                if (icheck == null)
                {
                    NCR_CHECK ncr_check = db.NCR_CHECKs.Where(nc => nc.NC_ID == input.sNc_Id).FirstOrDefault();

                    ncr_check.NC_NCR = 0;
                    ncr_check.NC_STATUS = 1;
                    ncr_check.USER_UPDATE = input.sUser;
                    ncr_check.TIME_UPDATE = (DateTime)(now);
                }

                db.SubmitChanges();

                result.RESULT = "ลบรายการแจ้งความผิดพลาด(NCR)เรียบร้อยแล้วค่ะ.";
                result.EXCEPTION = "";

                return result;

            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถลบรายการแจ้งความผิดพลาด(NCR)ได้ค่ะ.";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }
        #endregion

        #region อัพโหลดรูปภาพ
        public mResult uploadImages_QC(Stream fileUpload)
        {
            mResult result = new mResult();

            try
            {
                McsAppDataContext dc = new McsAppDataContext();

                string servPath = "";
                string fileSize = "";

                int point = 0;
                int width = 0;
                int height = 0;

                foreach (PDPC_config items in dc.PDPC_configs)
                {
                    if (items.config_key.Equals("PATH_BEFORE")) servPath = items.config_value;
                    if (items.config_key.Equals("PICTURE")) fileSize = items.config_value;
                }

                if (fileSize != "")
                {
                    point = fileSize.IndexOf("x");
                    width = Convert.ToInt32(fileSize.Substring(0, point));
                    height = Convert.ToInt32(fileSize.Substring(point + 1));
                }

                //------------------------------------------------------------------------------------------------------

                var parser = new MultipartFormDataParser(fileUpload);

                var username = parser.Parameters["username"].Data;
                var barcode = parser.Parameters["barcode"].Data;
                var otherno = parser.Parameters["otherno"].Data;
                var imagename = parser.Parameters["imagename"].Data;

                var file = parser.Files.FirstOrDefault();

                Stream stream = file.Data;

                //------------------------------------------------------------------------------------------------------

                if (!Directory.Exists(servPath)) Directory.CreateDirectory(servPath);

                FileStream targetStream = null;
                Stream sourceStream = stream;

                string fileName = imagename + "-" + otherno + "-X.jpeg";
                string filePath = Path.Combine(servPath, fileName);

                using (targetStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    const int bufferLen = 4096;
                    byte[] buffer = new byte[bufferLen];
                    int count = 0;
                    int totalBytes = 0;

                    while ((count = sourceStream.Read(buffer, 0, bufferLen)) > 0)
                    {
                        totalBytes += count;

                        targetStream.Write(buffer, 0, count);
                    }

                    targetStream.Close();

                    sourceStream.Close();
                }

                resizeImage(filePath, width, height);

                result.RESULT = "อัพโหลดรูปภาพเรียบร้อยแล้ว";
                result.EXCEPTION = "";
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถอัพโหลดรูปภาพได้";
                result.EXCEPTION = ex.Message;
            }

            return result;
        }
        #endregion

        #region Delete Image
        private string DeleteImage(string img_name)
        {
            string result = "";
            string servPath = "";
            try
            {
                McsAppDataContext db = new McsAppDataContext();

                foreach (PDPC_config items in db.PDPC_configs)
                {
                    if (items.config_key.Equals("PATH_BEFORE")) servPath = items.config_value;
                }

                string filePath = Path.Combine(servPath, img_name);

                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                    result = "ลบรูปภาพแล้ว";
                }
                else
                {
                    result = "ไม่มีรูปภาพ";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        #endregion

        #region ส่ง Ncr : แบบไม่มี NCR
        public mResult IFsetNoNcrCheck(iNoNcrCheck input)
        {
            mResult result = new mResult();
            McsFg2010DataContext fg = new McsFg2010DataContext();

            try
            {
                var check_st = (from ss in fg.stock_shelfs where ss.place == input.sPlanIns select ss.place).FirstOrDefault();
                if (check_st != null)
                {
                    if (input.sBarcode != "" && input.sPlanDate != "")
                    {
                        object[] prm = { input.nTypePc, input.sBarcode, input.nRev_no, input.nCheck_no, "NULL", input.sUser, input.sUser, "NULL", "NULL", "NULL", "NULL", input.nDwg_type, input.sPlanDate, input.sPlanIns, null, null, 5 };

                        var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_SET_NCR_CHECK", prm);

                        while (reader.Read())
                        {
                            result.RESULT = reader["ERROR"].ToString();
                            result.EXCEPTION = "";
                        }
                    }
                    else
                    {
                        result.RESULT = "ไม่สามารถบันทึกข้อมูลได้";
                        result.EXCEPTION = "";
                    }
                }
                else
                {
                    result.RESULT = "กรุณาป้อนข้อมูลพื้นที่ให้ถูกต้อง";
                    result.EXCEPTION = "";
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }
        #endregion

        #region แสดงรายการ NCR
        public List<mNcrReport> IFgetNcrReport(iNcrReport input)
        {
            string other_no = "";
            List<mNcrReport> results = new List<mNcrReport>();

            object[] prm = { input.sNc_Id, input.sNc_State, "", "" };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_REPORT_NCR", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    other_no = reader["OTHER_NO"].ToString();
                }

                if (other_no != "")
                {
                    var reader1 = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_REPORT_NCR", prm);

                    while (reader1.Read())
                    {
                        results.Add(new mNcrReport
                        {
                            NCR_NO = reader1["NCR_NO"].ToString(),
                            DIRECTION = reader1["DIRECTION"].ToString(),
                            POSITION = reader1["NT_NAME"].ToString(),
                            DESCRIPTIONS = reader1["DESCRIPTIONS"].ToString(),
                            IMG_BEFORE = reader1["IMG_BEFORE"].ToString(),
                            RESPONSE = reader1["GROUP_NAME"].ToString(),
                            DWG = reader1["DWG"].ToString(),
                            ACTUAL = reader1["ACTUAL"].ToString(),
                            OTHER_NO = (int)reader1["OTHER_NO"],
                            SUGGEST = reader1["SUGGEST"].ToString(),
                            PLACE = reader1["PLACE"].ToString()
                        });
                    }
                }
                else
                {
                    results.Add(new mNcrReport
                    {
                        NCR_NO = "-",
                        DIRECTION = "-",
                        POSITION = "-",
                        DESCRIPTIONS = "-",
                        IMG_BEFORE = "-",
                        RESPONSE = "-",
                        DWG = "-",
                        ACTUAL = "-",
                        OTHER_NO = 0,
                        SUGGEST = "-",
                        PLACE = "",
                    });
                }
            }
            else
            {
                results.Add(new mNcrReport
                {
                    NCR_NO = "-",
                    DIRECTION = "-",
                    POSITION = "-",
                    DESCRIPTIONS = "-",
                    IMG_BEFORE = "-",
                    RESPONSE = "-",
                    DWG = "-",
                    ACTUAL = "-",
                    OTHER_NO = 0,
                    SUGGEST = "-",
                    PLACE = ""
                });
            }
            return results;
        }
        #endregion

        #region แสดง Ncr Type
        public mNcrType IFgetNcrType(iNcrType input)
        {
            mNcrType results = new mNcrType();

            McsQcDataContext db = new McsQcDataContext();

            NCR_CHECK_LIST icheck = db.NCR_CHECK_LISTs.Where(nl => nl.NCR_NO == input.sNcr_No).FirstOrDefault();

            if (icheck == null)
            {
                results.NT_ID = "====";
            }
            else
            {
                results.NT_ID = icheck.NT_ID.ToString();
            }
            return results;
        }
        #endregion

        #region ยืนยันการแจ้ง NCR
        public mResult IFsetConfirmNcr(iConfirmNcr input)
        {
            mResult result = new mResult();

            DateTime now = DateTime.Now;

            try
            {
                McsQcDataContext db = new McsQcDataContext();

                NCR_CHECK items = db.NCR_CHECKs.Where(nc => nc.PC_ID == input.nTypePc && nc.BARCODE == input.sBarcode && nc.REV_NO == input.nRev_No && nc.CHECK_NO == input.nCheck_No).FirstOrDefault();

                if (items != null)
                {
                    items.NC_STATUS = 1;
                    items.DATE_FINISH = (DateTime)(now);
                    items.USER_UPDATE = input.sUser;
                    items.TIME_UPDATE = (DateTime)(now);

                    db.SubmitChanges();

                    result.RESULT = "1|ยืนยันการแจ้งความผิดพลาด(NCR)เรียบร้อยแล้วค่ะ.";
                    result.EXCEPTION = "";

                    return result;
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "2|ยืนยันการแจ้งความผิดพลาด(NCR)ไม่สำเร็จค่ะ.";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }
        #endregion


        // Qc Recheck ----------------------------------------------------------------------------------------------
        #region แสดงข้อมูล NCR Recheck
        public mDataRecheck RCgetDataRecheck(iDataRecheck input)
        {
            mDataRecheck result = new mDataRecheck();

            // '9100001058-0001001"
            int nTypePc = Convert.ToInt32(input.sNc_Id.Substring(0, 5));
            string sBarcode = input.sNc_Id.Substring(5, 10);
            int nRev = Convert.ToInt32(input.sNc_Id.Substring(15, 2));
            int nCheckNo = Convert.ToInt32(input.sNc_Id.Substring(17, 1));

            object[] prm = { input.sNc_Id };

            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_REPORT_NCR_EDIT", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.PJ_NAME = reader["PJ_NAME"].ToString();
                    result.PD_ITEM = (Int16)reader["PD_ITEM"];
                    result.REV_NO = reader["REV_NO"].ToString();
                    result.CHECK_NO = nCheckNo;
                    result.PD_CODE = reader["PD_CODE"].ToString();
                    result.DATE_PLAN = reader["DATE_PLAN"].ToString();
                    result.USERNAME = reader["USERNAME"].ToString();
                    result.DATE_START = reader["DATE_START"].ToString();
                    result.BARCODE = reader["BARCODE"].ToString();
                    result.PT_MAINTYPE = (int)reader["PT_MAINTYPE"];
                }
            }
            else
            {
                result.PJ_NAME = "-";
                result.PD_ITEM = 0;
                result.REV_NO = "-";
                result.CHECK_NO = 0;
                result.PD_CODE = "-";
                result.DATE_PLAN = "-";
                result.USERNAME = "-";
                result.DATE_START = "-";
                result.BARCODE = "-";
                result.PT_MAINTYPE = 0;
            }
            return result;
        }
        #endregion

        #region แสดง Report NCR Recheck
        public List<mNcrReportRC> RCgetNcrReport(iNcrReportRC input)
        {
            List<mNcrReportRC> results = new List<mNcrReportRC>();
            string pc_id = input.sNc_Id.Substring(0, 5);
            string barcode = input.sNc_Id.Substring(5, 10);
            string rev_no = input.sNc_Id.Substring(15, 2);

            object[] prm = { input.sNc_Id, "3", "3", "", pc_id, barcode, rev_no };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_REPORT_NCR", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    results.Add(new mNcrReportRC()
                    {
                        NCR_NO = reader["NCR_NO"].ToString(),
                        NT_NAME = reader["NT_NAME"].ToString(),
                        DIRECTION = reader["DIRECTION"].ToString(),
                        DESCRIPTIONS = reader["DESCRIPTIONS"].ToString(),
                        GROUP_NAME = reader["GROUP_NAME"].ToString(),
                        FULLNAME_PD = reader["FULLNAME_PD"].ToString(),
                        DATE_PD = reader["DATE_PD"].ToString(),
                        DWG = reader["DWG"].ToString(),
                        ACTUAL = reader["ACTUAL"].ToString(),
                        SUGGEST = reader["SUGGEST"].ToString(),
                        OTHER_NO = reader["OTHER_NO"].ToString(),
                        POSITION_ID = reader["POSITION_ID"].ToString(),
                        PD_COMMENT = reader["PD_COMMENT"].ToString(),
                        PD_ACTUAL = reader["PD_ACTUAL"].ToString()
                    });
                }
            }
            else
            {
                results.Add(new mNcrReportRC()
                {
                    NCR_NO = "-",
                    NT_NAME = "",
                    DIRECTION = "",
                    DESCRIPTIONS = "",
                    GROUP_NAME = "",
                    FULLNAME_PD = "",
                    DATE_PD = "",
                    DWG = "",
                    ACTUAL = "",
                    SUGGEST = "",
                    OTHER_NO = "",
                    POSITION_ID = "",
                    PD_COMMENT = "",
                    PD_ACTUAL = ""
                });
            }
            return results;
        }
        #endregion

        #region Update Recheck OK
        public mResult RCsetRecheckOK(iRecheckOK input)
        {
            mResult result = new mResult();

            DateTime now = DateTime.Now;

            try
            {
                McsQcDataContext db = new McsQcDataContext();

                NCR_DIM_CHECK items = db.NCR_DIM_CHECKs.Where(nd => nd.NC_ID == input.sNc_Id && nd.NCR_NO == input.sNcr_No && nd.OTHER_NO == input.nOther_no).FirstOrDefault();

                if (items != null)
                {
                    items.ND_STATE = 5;
                    items.IMG_AFTER = (input.sFile == "") ? "No.jpg" : input.sFile;
                    items.QC_RECHECK = input.sUsers;
                    items.DATE_RECHECK = (DateTime)(now);
                    items.USER_UPDATE = input.sUsers;
                    items.TIME_UPDATE = (DateTime)(now);
                    items.ACTUAL = input.Actual;

                    db.SubmitChanges();

                    result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
                    result.EXCEPTION = "";

                    return result;
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }
        #endregion

        #region Update Recheck Not OK
        public mResult RCsetRecheckNotOK(iRecheckNotOK input)
        {
            mResult result = new mResult();

            DateTime now = DateTime.Now;

            string error = "";

            try
            {
                McsQcDataContext db = new McsQcDataContext();

                object[] prm = { input.nTypePc, input.sBarcode, input.nRev_no, input.nCheck_no + 1, input.sNcr_no, input.sUser, input.sUser, input.sGroup, (input.sFile == "") ? "No.jpg" : input.sFile, input.sSug, input.sDirection, input.nDwg_type, input.sPlanDate, input.sPlanIns, input.Dwg, input.Actual, 1 };
                var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_SET_NCR_CHECK", prm);

                while (reader.Read())
                {
                    error = reader["ERROR"].ToString();
                }

                if (error.Substring(0, 1) == "1")
                {
                    NCR_DIM_CHECK items = db.NCR_DIM_CHECKs.Where(nd => nd.NC_ID == input.sNc_Id && nd.NCR_NO == input.sNcr_no && nd.OTHER_NO == input.nOther_No).FirstOrDefault();

                    if (items != null)
                    {
                        items.ND_STATE = 4;
                        items.QC_RECHECK = input.sUser;
                        items.DATE_RECHECK = (DateTime)(now);
                        items.USER_UPDATE = input.sUser;
                        items.TIME_UPDATE = (DateTime)(now);

                        db.SubmitChanges();

                        result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
                        result.EXCEPTION = "";
                    }
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }
        #endregion

        #region Delete Ncr Recheck
        public mResult RCsetRecheckDel(iRecheckDel input)
        {
            mResult result = new mResult();

            try
            {
                McsQcDataContext db = new McsQcDataContext();

                if (input.sImg_Before == "None.jpg" || input.sImg_Before == null)
                {
                    result.RESULT = "ยังไม่ได้ทำการอัพโหลดรูปภาพ";
                    result.EXCEPTION = "";
                }
                else
                {
                    string img = DeleteImage(input.sImg_Before);

                    if (img == "T")
                    {

                        var Del_Ncr = from nd in db.NCR_DIM_CHECKs
                                      where nd.NC_ID == input.sNc_Id && nd.NCR_NO == input.sNcr_No && nd.OTHER_NO == input.nOther_No
                                      select nd;

                        foreach (var items in Del_Ncr)
                        {
                            db.NCR_DIM_CHECKs.DeleteOnSubmit(items);
                        }

                        db.SubmitChanges();

                        result.RESULT = "ลบภาพความผิดพลาด(NCR)เรียบร้อยแล้ว";
                        result.EXCEPTION = "";

                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถลบข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }
        #endregion


        //Qc Finish Check 
        #region List ชื่อโปรเจค
        public List<mProject> FNgetProjectName(iProject input)
        {
            List<mProject> results = new List<mProject>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT  MCSAPP.DBO.SF_FULLNAME(P.PJ_ID) PJ_NAME,P.PJ_ID 
                                            FROM 
                                            (SELECT P.PJ_ID, P.PD_ITEM ,0 AS REV_NO FROM MCSAPP..PRODUCT P  
                                            UNION 
                                            SELECT P.PJ_ID,P.ITEM  ,MAX(REV_NO) REV_NO FROM MCSAPP..REV_PLAN P 
                                            GROUP BY P.PJ_ID,P.ITEM) P 
				                            WHERE NOT EXISTS
              			                    (SELECT *
              			                    FROM NCR_CHECK NC    
						                    WHERE NC.PC_ID = @TYPE_PC And NC.NC_STATUS = 1
              			                    AND P.PJ_ID = CAST(LEFT(NC.BARCODE,5) AS INT)  AND P.PD_ITEM = CAST(RIGHT(NC.BARCODE,4) AS INT)   AND P.REV_NO = NC.REV_NO)   
			                                ORDER BY PJ_NAME";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mProject()
                        {
                            PJ_ID = string.Format("{0:00000}", (int)dr["PJ_ID"]),
                            PJ_NAME = (string)dr["PJ_NAME"]
                        });
                    }
                }

                connection.Close();
            }

            return results;
        }
        #endregion

        #region List ไอเทม
        public List<mItemPj> FNgetProjectItem(iItemPj input)
        {
            List<mItemPj> results = new List<mItemPj>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT P.PD_ITEM AS ITEM
                                               FROM 
                                                (SELECT P.PJ_ID, P.PD_ITEM ,0 AS REV_NO FROM MCSAPP..PRODUCT P  
                                                 UNION
                                                 SELECT P.PJ_ID,P.ITEM  ,MAX(REV_NO) REV_NO FROM MCSAPP..REV_PLAN P 
                                                 GROUP BY P.PJ_ID,P.ITEM) P 
                                             WHERE EXISTS
                	                            (SELECT *
                			                            FROM NCR_CHECK NC    
                                                   WHERE NC.PC_ID = @TYPE_PC And NC.NC_STATUS = 1
                	                                AND MCSAPP.DBO.SF_BARCODE(P.PJ_ID,P.PD_ITEM) = NC.BARCODE AND P.REV_NO = NC.REV_NO) 
                                             AND P.PJ_ID = @PJ_ID
                                             ORDER BY PD_ITEM";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    command1.Parameters.Add("@PJ_ID", SqlDbType.Int, 5).Value = input.nPj_Id;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mItemPj()
                        {
                            PD_ITEM = dr["ITEM"].ToString()
                        });
                    }
                }

                connection.Close();
            }

            return results;
        }
        #endregion

        #region List Checker
        public List<mChecker> FNgetChecker(iChecker input)
        {
            List<mChecker> results = new List<mChecker>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT USERS,MCSAPP.DBO.SF_GETNAMES(NC.USERS) USERNAME 
                                             FROM NCR_CHECK NC 
                                             WHERE NC.PC_ID = @TYPE_PC
                                             AND NC.NC_STATUS IN (1) AND NC.NC_NCR IN (1)
                                             AND LEN(USERS) = 7
                                             ORDER BY USERNAME";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mChecker()
                        {
                            USERS = (string)dr["USERS"],
                            FULLNAME = (string)dr["USERNAME"]
                        });
                    }
                }

                connection.Close();
            }
            return results;
        }
        #endregion

        #region แสดงรายการ Ncr Report List
        public List<mNcrReportFN> FNgetNcrReport(iNcrReportFN input)
        {
            List<mNcrReportFN> results = new List<mNcrReportFN>();

            object[] prm = { input.nTypePc, 1, input.nPj_Id, input.nPd_Item, input.sUser_Check, "0", input.sDate_Edit, input.sPd_Code, "", "", "" };

            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_REPORT_NCR_LIST", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    results.Add(new mNcrReportFN()
                    {
                        PJ_NAME = reader["PJ_NAME"].ToString(),
                        PD_ITEM = reader["PD_ITEM"].ToString(),
                        REV_NO = reader["REV_NO"].ToString(),
                        CHECK_NO = (int)reader["CHECK_NO"],
                        PD_CODE = reader["PD_CODE"].ToString(),
                        USERNAME = reader["USERNAME"].ToString(),
                        DATE_START = reader["DATE_START"].ToString(),
                        COUNT_NCR = reader["COUNT_NCR"].ToString(),
                        NC_NCR = Check((int)reader["NC_NCR"]),
                        BARCODE = reader["BARCODE"].ToString(),
                        NC_ID = reader["NC_ID"].ToString(),
                        PLACE = reader["PLACE"].ToString()
                    });
                }
            }
            else
            {
                results.Add(new mNcrReportFN()
                {
                    PJ_NAME = "-",
                    PD_ITEM = "-",
                    REV_NO = "-",
                    CHECK_NO = 0,
                    PD_CODE = "-",
                    USERNAME = "-",
                    DATE_START = "-",
                    COUNT_NCR = "-",
                    NC_NCR = "-",
                    BARCODE = "-",
                    NC_ID = "-",
                    PLACE = ""
                });
            }
            return results;
        }

        private string Check(int id)
        {
            string result = "";


            if (id == 0)
            {
                result = "ผ่าน";
            }
            else if (id == 1)
            {
                result = "ไม่ผ่าน";
            }
            else
            {
                result = "ไม่มีผล";
            }
            return result;
        }
        #endregion

        #region Update Qc finish Check
        public mResult FNsetNcrQcCheck(iNcrCheckFN input)
        {
            mResult result = new mResult();
            DateTime now = DateTime.Now;

            try
            {
                McsQcDataContext db = new McsQcDataContext();

                NCR_CHECK icheck = db.NCR_CHECKs.Where(nc => nc.NC_ID == input.sNc_Id).FirstOrDefault();

                if (icheck.NC_NCR >= 1)
                {
                    icheck.NC_STATUS = 2;
                    icheck.USER_QC = input.sUsers;
                    icheck.DATE_QC = (DateTime)(now);
                    icheck.USER_UPDATE = input.sUsers;
                    icheck.TIME_UPDATE = (DateTime)(now);

                    db.SubmitChanges();

                    result.RESULT = "1|ยืนยันข้อมูลเรียบร้อยแล้ว";
                    result.EXCEPTION = "";

                    return result;
                }
                else if (icheck.NC_NCR == 0)
                {
                    icheck.NC_STATUS = 5;
                    icheck.USER_QC = input.sUsers;
                    icheck.DATE_QC = (DateTime)(now);
                    icheck.USER_UPDATE = input.sUsers;
                    icheck.TIME_UPDATE = (DateTime)(now);

                    db.SubmitChanges();

                    result.RESULT = "1|ยืนยันข้อมูลเรียบร้อยแล้ว";
                    result.EXCEPTION = "";

                    return result;
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }
        #endregion

        #endregion

        #region HR
        public mListYearAndUser getUserInGroup(iUserInGroup input)
        {
            mListYearAndUser results = new mListYearAndUser();
            results.getYear = new List<mYear>();
            results.getUserIngroup = new List<mUserInGroup>();

            int[] year = new int[2];
            int thisYear =  Convert.ToInt32(DateTime.Now.ToString("yyyy"));
            int pastYear = thisYear -1;
            year[0] = thisYear;
            year[1] = pastYear;

            foreach (int value in year)
            {
                results.getYear.Add(new mYear
                {
                    YEAR = (int)value
                });
            }

            object[] prm = { input.sUs_Id };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_USER_IN_GROUP_BY_USER", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    results.getUserIngroup.Add(new mUserInGroup()
                    {
                        US_ID = reader["US_ID"].ToString(),
                        FULLNAME = reader["FULLNAME"].ToString()
                    });
                }
            }
            else
            {
                results.getUserIngroup.Add(new mUserInGroup()
                {
                    US_ID = "====",
                    FULLNAME = "===="
                });
            }
            return results;
        }

        public List<mLeaveQuotaByYear> getMasterLeaveQuotaByYear(iLeaveQuotaByYear input)
        {
            List<mLeaveQuotaByYear> results = new List<mLeaveQuotaByYear>();
     
            
            object[] prm = { input.sUs_Id, input.nYear };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsHrMs, "GET_MASTER_LEAVE_QUOTA_BY_YEAR", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    results.Add(new mLeaveQuotaByYear
                    {
                        LEAVE_TYPE_MSG = reader["LEAVE_TYPE_MSG"].ToString(),
                        NUMBER_OF_QUOTA = reader["NUMBER_OF_QUOTA"].ToString(),
                        NUMBER_OF_QUOTA_USAGE = reader["NUMBER_OF_QUOTA_USAGE"].ToString(),
                        LEAVE_REMAIN = (reader["NUMBER_OF_QUOTA"].ToString() == "-") ? 0 : float.Parse(reader["NUMBER_OF_QUOTA"].ToString()) - float.Parse(reader["NUMBER_OF_QUOTA_USAGE"].ToString()),
                        LIFETIMES = (reader["NUMBER_OF_QUOTA"].ToString() == "-") ? "" : reader["LIFETIMES"].ToString()
                    });
                }
            }
            else
            {
                results.Add(new mLeaveQuotaByYear()
                {
                    LEAVE_TYPE_MSG = "-",
                    NUMBER_OF_QUOTA = "-",
                    NUMBER_OF_QUOTA_USAGE = "-",
                    LEAVE_REMAIN = 0,
                    LIFETIMES = "-"
                });
            }

            return results;
        }

        #endregion

        #region FG
        #region Connect
        public mLoginMcsFg CheckLoginUserMcsFg(iLoginMcsFg input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            mLoginMcsFg result = new mLoginMcsFg();

            user iCheck = dc.users.Where(us => us.usID == input.Username && us.usPwd == input.Password).FirstOrDefault();

            if (iCheck == null)
            {
                result.US_ID = "NULL";
            }
            else
            {
                result.US_ID = iCheck.usID;
            }
            return result;
        }

        public List<mUserCheckMenu> CheckUserMenu(iUserCheckMenu input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            List<mUserCheckMenu> result = new List<mUserCheckMenu>();

            var query = from us in dc.users
                        join ua in dc.users_auths on us.usID equals ua.usID
                        join um in dc.users_menus on ua.mnID equals um.mnID
                        where us.usID == input.UserID && us.status == 1
                        orderby ua.mnID
                        select ua.mnID.ToString();

            if (query.FirstOrDefault() != null)
            {
                foreach (string items in query)
                {
                    result.Add(new mUserCheckMenu()
                    {
                        MN_ID = items
                    });
                }
            }
            else
            {
                result.Add(new mUserCheckMenu()
                {
                    MN_ID = "===="
                });
            }
            return result;
        }
        #endregion

        #region Stock Info
        public mGetProductStatus GetProductStatusFG(iBarcode input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            mGetProductStatus result = new mGetProductStatus();

            var query = from pd in dc.products
                        join pt in dc.product_types
                        on pd.type equals pt.type into LeftJoinType
                        from pdt in LeftJoinType.DefaultIfEmpty()
                        join st in dc.stocks on pd.barcode equals st.barcode into LeftJoinBarcode
                        from stpd in LeftJoinBarcode.DefaultIfEmpty()
                        where pd.barcode == input.Barcode
                        select new { pd.project, pdt.typeName, pd.item, pd.sharp, pd.dwg, pd.code, pd.size, pd.length, pd.weight, stpd.place, stpd.no };

            if (query.FirstOrDefault() != null)
            {
                foreach (var items in query)
                {
                    result.PROJECT = items.project.ToString();
                    result.TYPE_NAME = items.typeName.ToString();
                    result.ITEM = items.item.ToString();
                    result.SHARP = items.sharp.ToString();
                    result.DWG = items.dwg.ToString();
                    result.CODE = items.code.ToString();
                    result.SIZE = items.size.ToString();
                    result.LENGTH = items.length.ToString();
                    result.WEIGHT = items.weight.ToString();
                    result.PLACE = (items.place == null) ? "" : items.place.ToString();
                    result.NO = (items.no == null) ? "" : items.no.ToString();
                }
            }
            else
            {
                result.PROJECT = "-";
                result.TYPE_NAME = "-";
                result.ITEM = "-";
                result.SHARP = "-";
                result.DWG = "-";
                result.CODE = "-";
                result.SIZE = "-";
                result.LENGTH = "-";
                result.WEIGHT = "-";
                result.PLACE = "-";
                result.NO = "-";
            }
            return result;
        }

        public List<mGetProductPlaceInStock> GetProductPlaceInStock(iProjectNameAndItem input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            List<mGetProductPlaceInStock> result = new List<mGetProductPlaceInStock>();

            if (input.Item == null)
            {
                var query1 = from pd in dc.products
                             join st in dc.stocks on pd.barcode equals st.barcode
                             join pt in dc.product_types on pd.type equals pt.type
                             where SqlMethods.Like(pd.project, "%" + input.ProjectName + "%")
                             orderby pd.project, pt.typeName, pd.sharp, pd.item
                             select new { pd.project, pt.typeName, pd.sharp, pd.item, st.place, st.no };

                if (query1.FirstOrDefault() != null)
                {
                    foreach (var items in query1)
                    {
                        result.Add(new mGetProductPlaceInStock()
                        {
                            PROJECT = items.project.ToString(),
                            TYPE_NAME = items.typeName.ToString(),
                            SHARP = '#' + items.sharp.ToString(),
                            ITEM = items.item.ToString(),
                            PLACE = items.place.ToString(),
                            NO = items.no.ToString()
                        });
                    }
                }
                else
                {
                    result.Add(new mGetProductPlaceInStock()
                    {
                        PROJECT = "-",
                        TYPE_NAME = "-",
                        SHARP = "-",
                        ITEM = "-",
                        PLACE = "-",
                        NO = "-"
                    });
                }
            }
            else
            {
                var query2 = from pd in dc.products
                             join st in dc.stocks on pd.barcode equals st.barcode
                             join pt in dc.product_types on pd.type equals pt.type
                             where SqlMethods.Like(pd.project, "%" + input.ProjectName + "%") && input.Item == pd.item
                             orderby pd.project, pt.typeName, pd.sharp, pd.item
                             select new { pd.project, pt.typeName, pd.sharp, pd.item, st.place, st.no };

                if (query2.FirstOrDefault() != null)
                {
                    foreach (var items in query2)
                    {
                        result.Add(new mGetProductPlaceInStock()
                        {
                            PROJECT = items.project.ToString(),
                            TYPE_NAME = items.typeName.ToString(),
                            SHARP = '#' + items.sharp.ToString(),
                            ITEM = items.item.ToString(),
                            PLACE = items.place.ToString(),
                            NO = items.no.ToString()
                        });
                    }
                }
                else
                {
                    result.Add(new mGetProductPlaceInStock()
                    {
                        PROJECT = "-",
                        TYPE_NAME = "-",
                        SHARP = "-",
                        ITEM = "-",
                        PLACE = "-",
                        NO = "-"
                    });
                }
            }

            return result;
        }
        #endregion

        #region Stock In
        private int SetProductIntoShelf(string sBarcodeRef, string sBarcode, string sUser)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            string sShelf = "";
            int nNo = 0;

            sBarcodeRef = sBarcodeRef.Trim().ToUpper();
            sBarcode = sBarcode.Trim().ToUpper();

            if (IsBarcode(sBarcodeRef))
            {
                if (GetShelfProductInfoByBarcode(sBarcodeRef, sShelf, nNo))
                {
                    var query = from s in dc.stocks
                                where s.barcode == sBarcodeRef
                                select new { s.place, s.no };

                    foreach (var items in query)
                    {
                        sShelf = items.place;
                        nNo = Int32.Parse(items.no.ToString());
                    }

                    SetProductInsertOnShelf(sBarcode, sShelf, nNo + 1, sUser);
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                if (IsShelfInStock(sBarcodeRef))
                {
                    if (GetShelfProductTotal(sBarcodeRef) == 0)
                    {
                        AddProductIntoShelf(sBarcode, sBarcodeRef, 1, sUser);
                    }
                    else
                    {
                        return -3;
                    }
                }
                else
                {
                    return -2;
                }
            }
            return 1;
        }

        public List<mStockArea> GetStockArea()
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            List<mStockArea> result = new List<mStockArea>();

            foreach (stock_area items in dc.stock_areas.OrderBy(sa => sa.saName))
            {
                result.Add(new mStockArea()
                {
                    SA_ID = items.saId.ToString(),
                    SA_NAME = items.saName.ToString()
                });
            }

            return result;
        }

        public mResult SetUptoTruck(iSetUptoTruck input)
        {
            mResult result = new mResult();
            DateTime now = DateTime.Now;

            try
            {
                McsFg2010DataContext dc = new McsFg2010DataContext();

                if (IsStockInRequest(input.sBarcode) == "False")
                {
                    result.RESULT = "-11|ยังไม่ได้แจ้งงานเข้าคลังสินค้า";
                    result.EXCEPTION = "";

                    return result;
                }

                if (IsCarNo(input.sCarTruck) == false)
                {
                    result.RESULT = "-10|เลขทะเบียนรถผิด";
                    result.EXCEPTION = "";

                    return result;
                }

                if (GetDocNoStockInByBarcode(input.sBarcode) == "False")
                {
                    result.RESULT = "-12|เลขที่เอกสารแจ้งงานเข้าคลังสินค้า ที่พิมพ์ไม่ตรงกับเลขที่เอกสารที่แจ้ง";
                    result.EXCEPTION = "";

                    return result;
                }

                stock_in items = dc.stock_ins.Where(si => si.barcode == input.sBarcode && si.status == 0).FirstOrDefault();

                if (items != null)
                {
                    items.fromFac = Convert.ToInt32(input.sStockArea);
                    items.fromCarTruck = input.sCarTruck;
                    items.fromUser = input.sUserID;
                    items.FromDate = (DateTime)(now);

                    dc.SubmitChanges();

                    result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
                    result.EXCEPTION = "";

                    return result;
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "0|ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }

        public List<mGetCrane> GetCrane()
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            List<mGetCrane> result = new List<mGetCrane>();

            foreach (car_crane items in dc.car_cranes.OrderBy(cc => cc.crane_detail))
            {
                result.Add(new mGetCrane()
                {
                    CRANE_NO = items.crane_no.ToString(),
                    CRANE_DETAIL = items.crane_no + " : " + items.crane_operator.ToString() 
                });
            }

            return result;
        }

        public mResult SetReceiveToStock(iSetReceiveToStock input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            mResult result = new mResult();
            DateTime now = DateTime.Now;

            string sPlace = "";
            string sDocNo = "";
            int nResult = 0;

            if (IsStockInRequest(input.sBarcode) == "False")
            {
                result.RESULT = "-11|ยังไม่ได้แจ้งงานเข้าคลังสินค้า";
                result.EXCEPTION = "";

                return result;
            }

            sDocNo = GetDocNoStockInByBarcode(input.sBarcode);

            if (GetDocNoStockInByBarcode(input.sBarcode) == "")
            {
                result.RESULT = "-12|เลขที่เอกสารแจ้งงานเข้าคลังสินค้า ที่พิมพ์ไม่ตรงกับเลขที่เอกสารที่แจ้ง";
                result.EXCEPTION = "";

                return result;
            }

            nResult = SetProductIntoShelf(input.sBarcodeRef, input.sBarcode, input.sUserID);

            if (nResult == 1)
            {
                try
                {
                        foreach (stock items in dc.stocks.Where(s => s.barcode == input.sBarcode))
                        {
                            sPlace = items.place.ToString();
                        }

                        //Update Stock_in, Product
                        stock_in iStock = dc.stock_ins.Where(si => si.barcode == input.sBarcode).FirstOrDefault();

                        if (iStock != null)
                        {
                            iStock.status = 1;
                            iStock.place = sPlace;
                            iStock.users = input.sUserID;
                            iStock.dates = (DateTime)(now);
                            iStock.destCrane = input.sCrane;

                            dc.SubmitChanges();
                        }

                        product iProduct = dc.products.Where(pd => pd.barcode == input.sBarcode).FirstOrDefault();

                        if (iProduct != null)
                        {
                            iProduct.status = 1;

                            dc.SubmitChanges();
                        }

                        //Update Stock_in_doc
                        var query = from sid in dc.stock_in_docs
                                    where sid.status == 0 && sid.docNo == sDocNo && !dc.stock_ins.Any(si => si.status == 1 && si.docNo == sDocNo)
                                    select sid;

                        if (query.FirstOrDefault() != null)
                        {
                            foreach (var iStockInDoc in query)
                            {
                                iStockInDoc.status = 2;
                                iStockInDoc.dateStart = (DateTime)(now);
                            }

                            dc.SubmitChanges();
                        }

                        var query2 = from sid in dc.stock_in_docs
                                     where sid.status == 2 && sid.docNo == sDocNo && !dc.stock_ins.Any(si => si.status == 0 && si.docNo == sDocNo)
                                     select sid;

                        if (query2.FirstOrDefault() != null)
                        {
                            foreach (var iStockInDoc2 in query2)
                            {
                                iStockInDoc2.status = 1;
                                iStockInDoc2.dateFinish = (DateTime)(now);
                            }

                            dc.SubmitChanges();

                            result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
                            result.EXCEPTION = "";
                        }
                }
                catch (Exception ex)
                {
                    result.RESULT = "0|ไม่สามารถบันทึกข้อมูลได้";
                    result.EXCEPTION = ex.Message;

                    return result;
                }
            }
            else
            {
                result.RESULT = nResult.ToString();
                result.EXCEPTION = "";

                return result;
            }

            result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
            result.EXCEPTION = "";
            return result;
        }

        public mResult SetUpReceive_Rev(iSetUpRecive_Rev input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            mResult result = new mResult();
            DateTime now = DateTime.Now;

            string sDocNo = "";

            try
            {
                if (!IsStockOutRevRequest(input.sBarcode))
                {
                    result.RESULT = "-11|ยังไม่ได้แจ้งงานแก้ไขเข้าคลังสินค้า";
                    result.EXCEPTION = "";

                    return result;
                }

                if (IsCarNo(input.sCarTruck) == false)
                {
                    result.RESULT = "-10|เลขทะเบียนรถผิด";
                    result.EXCEPTION = "";

                    return result;
                }

                sDocNo = GetDocNoStockInRevByBarcode(input.sBarcode);

                if (sDocNo == "")
                {
                    result.RESULT = "-11|เลขที่เอกสารแจ้งงานแก้ไขเข้าคลังสินค้า ที่พิมพ์ไม่ตรงกับเลขที่เอกสารที่แจ้ง";
                    result.EXCEPTION = "";

                    return result;
                }

                stock_in_rev iStockInRev = dc.stock_in_revs.Where(sir => sir.barcode == input.sBarcode && sir.status == 0).FirstOrDefault();

                if (iStockInRev != null)
                {
                    iStockInRev.fromCrane = input.sCrane;
                    iStockInRev.fromCarTruck = input.sCarTruck;
                    iStockInRev.fromUser = input.sUserID;
                    iStockInRev.fromDate = (DateTime)(now);

                    dc.SubmitChanges();

                    result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
                    result.EXCEPTION = "";

                    return result;
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "0|ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            
            return result;
        }

        public mResult SetDownReceive_Rev(iSetDownRecive_Rev input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            mResult result = new mResult();
            DateTime now = DateTime.Now;
            string sDocNo = "";
            string sShelf = "";
            string sPlace = "";
            int nNo = 0;
            int nResult = 0;

            if (IsStockInRevRequest(input.sBarcode) == "False")
            {
                result.RESULT = "-11|ยังไม่ได้แจ้งงานเข้าคลังสินค้า";
                result.EXCEPTION = "";

                return result;
            }

            sDocNo = GetDocNoStockInRevByBarcode(input.sBarcode);

            if (sDocNo == "")
            {
                result.RESULT = "-11|เลขที่เอกสารแจ้งงานแก้ไขเข้าคลังสินค้า ที่พิมพ์ไม่ตรงกับเลขที่เอกสารที่แจ้ง";
                result.EXCEPTION = "";

                return result;
            }


            if (!GetShelfProductInfoByBarcode(input.sBarcode, sShelf, nNo))
            {
                result.RESULT = "-12|ไม่มีข้อมูลบาร์โค้ดในคลังสินค้า";
                result.EXCEPTION = "";

                return result;
            }

            int BarcodeCount = GetShelfProductTotal(input.sBarcodeRef);
            if (BarcodeCount > 0)
            {
                result.RESULT = "-3|ไม่มีชั้นวาง";
                result.EXCEPTION = "";

                return result;
            }

            nResult = SetProductIntoShelf(input.sBarcodeRef, input.sBarcode, input.sUserID);

            if (nResult == 1)
            {
                foreach (stock items in dc.stocks.Where(s => s.barcode == input.sBarcode))
                {
                    sPlace = items.place.ToString();
                }

                try
                {
                    //UPDATE STOCK_IN_REV,PRODUCT
                    stock_in_rev iStockInRevProduct = dc.stock_in_revs.Where(sir => sir.barcode == input.sBarcode && sir.status == 0).FirstOrDefault();

                    if (iStockInRevProduct != null)
                    {
                        iStockInRevProduct.status = 1;
                        iStockInRevProduct.place = sPlace;
                        iStockInRevProduct.users = input.sUserID;
                        iStockInRevProduct.dates = (DateTime)(now);
                        iStockInRevProduct.destCrane = input.sCrane;

                        dc.SubmitChanges();
                    }

                    product iProduct = dc.products.Where(pd => pd.barcode == input.sBarcode).FirstOrDefault();

                    if (iProduct != null)
                    {
                        iProduct.status = 1;

                        dc.SubmitChanges();
                    }

                    //UPDATE STOCK_IN_REV_DOC (START,FINISH)
                    var query1 = from sird in dc.stock_in_rev_docs
                                 where sird.status == 0 && sird.docNo == sDocNo && !dc.stock_in_revs.Any(si => si.status == 1 && si.docNo == sDocNo)
                                 select sird;

                    if (query1.FirstOrDefault() != null)
                    {
                        foreach (var iStockInDocRev in query1)
                        {
                            iStockInDocRev.status = 2;
                            iStockInDocRev.dateStart = (DateTime)(now);
                        }

                        dc.SubmitChanges();
                    }

                    var query2 = from sird in dc.stock_in_rev_docs
                                 where sird.status == 2 && sird.docNo == sDocNo && !dc.stock_in_revs.Any(si => si.status == 0 && si.docNo == sDocNo)
                                 select sird;

                    if (query2.FirstOrDefault() != null)
                    {
                        foreach (var iStockInDocRev in query2)
                        {
                            iStockInDocRev.status = 1;
                            iStockInDocRev.dateStart = (DateTime)(now);
                        }

                        dc.SubmitChanges();
                    }
                }
                catch (Exception ex)
                {
                    result.RESULT = "0|ไม่สามารถบันทึกข้อมูลได้";
                    result.EXCEPTION = ex.Message;

                    return result;
                }
            }
            else
            {
                result.RESULT = nResult.ToString() +"|ไม่สามารถอัพเดทข้อมูลได้";
                result.EXCEPTION = "";

                return result;
            }

            result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
            result.EXCEPTION = "";
            return result;
        }

        public mSummaryByDate GetShowSummaryByDate(iDateInputSummary input)
        {
            mSummaryByDate result = new mSummaryByDate();

            result.GetSummary = new List<mGetSummary>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;

                    if (input.nTypeJob == 0)
                    {
                        command1.CommandText = @"SELECT * FROM (SELECT TOP (100) PERCENT CONVERT(VARCHAR(10),A.DATEREQ,121) AS DATE, 
                                            COUNT(B.BARCODE) AS REQ, COUNT(B.PLACE) AS REC, COUNT(B.BARCODE) - COUNT(B.PLACE) AS REM
                                            FROM STOCK_IN_DOC AS A INNER JOIN STOCK_IN AS B ON A.DOCNO = B.DOCNO
                                            GROUP BY CONVERT(VARCHAR(10),A.DATEREQ,121)
                                            HAVING (CONVERT(VARCHAR(10), A.DATEREQ, 121) BETWEEN @DATE_START AND @DATE_FINISH))AS SUMMARY WHERE REM>0 ORDER BY DATE";
                    }
                    else
                    {
                        command1.CommandText = @"SELECT * FROM (SELECT TOP (100) PERCENT CONVERT(VARCHAR(10),A.DATEREQ,121) AS DATE, 
                                                COUNT(B.BARCODE) AS REQ, COUNT(B.PLACE) AS REC, COUNT(B.BARCODE) - COUNT(B.PLACE) AS REM
                                                 FROM STOCK_IN_REV_DOC AS A INNER JOIN
                                                 STOCK_IN_REV AS B ON A.DOCNO = B.DOCNO
                                                 GROUP BY CONVERT(VARCHAR(10),A.DATEREQ,121)
                                                 HAVING (CONVERT(VARCHAR(10), A.DATEREQ, 121) BETWEEN @DATE_START AND @DATE_FINISH))AS SUMMARY WHERE REM>0 ORDER BY DATE";
                    }
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@DATE_START", SqlDbType.DateTime, 10).Value = input.sDateStart;
                    command1.Parameters.Add("@DATE_FINISH", SqlDbType.DateTime, 10).Value = input.sDateFinish;
                    SqlDataReader dr = command1.ExecuteReader();

                    int total = 0;

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            result.GetSummary.Add(new mGetSummary() 
                            {
                                DATE = dr["Date"].ToString(),
                                REQ = dr["Req"].ToString(),
                                REC = dr["Rec"].ToString(),
                                REM = dr["Rem"].ToString()
                            });

                            total = total + (int)dr["Rem"];

                            result.TOTAL = total;
                        }                      
                    }
                    else
                    {
                        result.GetSummary.Add(new mGetSummary()
                        {
                            DATE = "-",
                            REQ = "-",
                            REC = "-",
                            REM = "-"
                        });
                    }


                }

                connection.Close();
            }
            return result;
        }

        public mRemindByDate GetRemindByDate(iDateInputRemind input)
        {
            mRemindByDate result = new mRemindByDate();
            result.GetRemindByDate = new List<getRemindByDate>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;

                    if (input.nTypeJob == 0)
                    {
                        command1.CommandText = @"SELECT B.BARCODE, C.WGROUP AS WELD, C.LENGTH, C.WEIGHT
                                             FROM STOCK_IN_DOC AS A INNER JOIN
                                             STOCK_IN AS B ON A.DOCNO = B.DOCNO INNER JOIN
                                             PRODUCT AS C ON B.BARCODE = C.BARCODE
                                             WHERE (CONVERT(VARCHAR(10),A.DATEREQ,121) = @DATE) AND (B.PLACE IS NULL)
                                             ORDER BY A.DATEREQ";
                    }
                    else
                    {
                        command1.CommandText = @"SELECT B.BARCODE, C.WGROUP AS WELD, C.LENGTH, C.WEIGHT
                                                FROM STOCK_IN_REV_DOC AS A INNER JOIN
                                                STOCK_IN_REV AS B ON A.DOCNO = B.DOCNO INNER JOIN
                                                PRODUCT AS C ON B.BARCODE = C.BARCODE
                                                WHERE (CONVERT(VARCHAR(10),A.DATEREQ,121) = @DATE) AND (B.PLACE IS NULL)
                                                ORDER BY A.DATEREQ";
                    }
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@DATE", SqlDbType.DateTime, 10).Value = input.sDate;
                    SqlDataReader dr = command1.ExecuteReader();
                    int total = 0;

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            result.GetRemindByDate.Add(new getRemindByDate()
                            {
                                BARCODE = dr["BARCODE"].ToString(),
                                WELD = dr["WELD"].ToString(),
                                LENGTH = dr["LENGTH"].ToString(),
                                WEIGHT = dr["WEIGHT"].ToString(),
                            });

                            total = total + 1;

                            result.TOTAL = total;
                        }
                    }
                    else
                    {
                        result.GetRemindByDate.Add(new getRemindByDate()
                        {
                            BARCODE = "-",
                            WELD = "-",
                            LENGTH = "-",
                            WEIGHT = "-",
                        });
                    }
                }

                connection.Close();
            }

            return result;
        }

        public mGetStockInDaily GetStockInDaily(iDateInputRemind input)
        {
            mGetStockInDaily result = new mGetStockInDaily();
            result.GetStockInDaily = new List<getStockInDaily>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;

                    if (input.nTypeJob == 0)
                    {
                        command1.CommandText = @"SELECT PRODUCT.PROJECT,PRODUCT_TYPE.TYPENAME, PRODUCT.SHARP, PRODUCT.ITEM,
                                                  CONVERT(VARCHAR,CONVERT(MONEY,PRODUCT.LENGTH),1) AS [LENGTH], 
                                                  CAST(PRODUCT.WEIGHT AS  DECIMAL(9,3)) AS WEIGHT ,
                                                  ISNULL(STOCK_AREA.SANAME,'-') AS FROMPLACE, STOCK_IN.FROMCARTRUCK,
                                                  STOCK_IN.DESTCRANE, STOCK_IN.PLACE, 
                                                  STOCK_IN.USERS, LEFT(CONVERT(VARCHAR(10), STOCK_IN.DATES, 108),5) AS DATES 
                                                  FROM PRODUCT_TYPE INNER JOIN 
                                                  PRODUCT ON PRODUCT_TYPE.TYPE = PRODUCT.TYPE RIGHT OUTER JOIN 
                                                  STOCK_AREA RIGHT OUTER JOIN 
                                                  STOCK_IN ON STOCK_AREA.SAID = STOCK_IN.FROMFAC ON PRODUCT.BARCODE = STOCK_IN.BARCODE 
                                                  WHERE (CONVERT(VARCHAR(10), STOCK_IN.DATES, 120) = @DATE) AND STOCK_IN.STATUS =1 
                                                  ORDER BY STOCK_IN.DATES";
                    }
                    else if (input.nTypeJob == 1)
                    {
                        command1.CommandText = @"SELECT product.project, product_type.typeName, product.sharp, product.item,
                                                  convert(varchar,convert(money,product.length),1) as [length], 
                                                  cast(product.weight as  decimal(9,3)) as weight, isnull(stock_in_rev.fromCarTruck,'-') as fromCarTruck  ,
                                                 isnull(stock_in_rev.fromPlace,'-')as fromPlace , stock_in_rev.fromCrane,stock_in_rev.destCrane, 
                                                  stock_in_rev.place, stock_in_rev.users, LEFT(CONVERT(varchar(10), stock_in_rev.dates, 108), 5) AS dates
                                                 FROM product_type INNER JOIN 
                                                  product ON product_type.type = product.type RIGHT OUTER JOIN 
                                                 stock_in_rev ON product.barcode = stock_in_rev.barcode 
                                                  WHERE     (CONVERT(varchar(10), stock_in_rev.dates, 121) = @DATE) AND (stock_in_rev.status = 1) ";
                    }
                    else
                    {
                        command1.CommandText = @"SELECT  STOCK_MOVE.DESTUSER, LEFT(CONVERT(VARCHAR(10), STOCK_MOVE.DESTDATE, 108), 5) AS DATES, 
                                                 PRODUCT.PROJECT, PRODUCT_TYPE.TYPENAME, PRODUCT.SHARP, PRODUCT.ITEM, 
                                                 CONVERT(VARCHAR,CONVERT(MONEY,PRODUCT.LENGTH),1) AS [LENGTH], 
                                                 CAST(PRODUCT.WEIGHT AS  DECIMAL(9,3)) AS WEIGHT, ISNULL(STOCK_MOVE.FROMCARTRUCK,'-') AS FROMCARTRUCK  ,
                                                 ISNULL(STOCK_MOVE.FROMPLACE,'-')AS FROMPLACE ,ISNULL(STOCK_MOVE.FROMCRANE,'-') AS FROMCRANE, 
                                                 ISNULL(STOCK_MOVE.DESTCRANE,'-') AS DESTCRANE, 
                                                 STOCK_MOVE.DESTPLACE AS PLACE 
                                                 FROM PRODUCT_TYPE INNER JOIN 
                                                 PRODUCT ON PRODUCT_TYPE.TYPE = PRODUCT.TYPE RIGHT OUTER JOIN  
                                                 STOCK_MOVE ON PRODUCT.BARCODE = STOCK_MOVE.BARCODE  
                                                 WHERE (CONVERT(VARCHAR(10), STOCK_MOVE.DESTDATE, 121) = @DATE) AND (STOCK_MOVE.STATUS = 1)  
                                                 ORDER BY DATES";
                    }
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@DATE", SqlDbType.DateTime, 10).Value = input.sDate;
                    SqlDataReader dr = command1.ExecuteReader();

                    int total = 0;
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            result.GetStockInDaily.Add(new getStockInDaily()
                            {
                                PROJECT = dr["project"].ToString(),
                                TYPE_NAME    = dr["typeName"].ToString(),     
                                SHARP= dr["sharp"].ToString(),
                                ITEM= dr["item"].ToString(),
                                LENGTH= dr["length"].ToString(),
                                WEIGHT= dr["weight"].ToString(),
                                FROM_PLACE= dr["fromPlace"].ToString(),
                                FROM_CAR_TRUCK= dr["fromCarTruck"].ToString(),
                                DEST_CRANE= dr["destCrane"].ToString(),
                                PLACE= dr["place"].ToString(),
                                USERS= dr["users"].ToString(),
                                DATES= dr["dates"].ToString()
                            });

                            total = total + 1;

                            result.TOTAL = total;
                        }
                    }
                    else
                    {
                        result.GetStockInDaily.Add(new getStockInDaily()
                        {
                            PROJECT = "-",
                            TYPE_NAME = "-",
                            SHARP = "-",
                            ITEM = "-",
                            LENGTH = "-",
                            WEIGHT = "-",
                            FROM_PLACE = "-",
                            FROM_CAR_TRUCK = "-",
                            DEST_CRANE = "-",
                            PLACE = "-",
                            USERS = "-",
                            DATES = "-"
                        });
                    }
                }

                connection.Close();
            }

            return result;
        }

        #endregion

        #region Stock Out
        private string Check_NCR(string sBarcode)
        {
            string result = "";

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsQcConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT  MCSQC.DBO.SF_CHECK_NCR(@BARCODE) as remain";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@BARCODE", SqlDbType.VarChar, 10).Value = sBarcode;
                    SqlDataReader dr = command1.ExecuteReader();

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            result = dr["remain"].ToString();
                        }

                        string[] Temp = result.Split('|');

                        for (int i = 0; i <= result.Length - 1; i++)
                        {
                            result += Temp[i] + System.Environment.NewLine;
                        }
                    }
                    else
                    {
                        return "OK";
                    }
                }

                connection.Close();
            }

            return result;
    }

        private string GetStockOutDocRequest(string sBarcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            string result = "";

            stock_out iTop1 = dc.stock_outs.Where(so => so.barcode == sBarcode && so.status == 0).FirstOrDefault();

            if (iTop1 != null)
            {
                result = iTop1.docNo.ToString();
            }
            else
            {
                result = "";
            }

            return result;
        }

        private bool IsStockOutTripReady(string sDocNo, string sCarNo)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            var query = from ct in dc.car_trips
                        where ct.docNo == sDocNo && ct.carNo == sCarNo && ct.status == 0
                        select ct;

            if (query.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool SetProductMoveOutOnShelf(string sBarcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            if (!IsBarcode(sBarcode))
            {
                return false;
            }

            string place = "";
            int? no = 0;

            foreach (stock iPlace in dc.stocks.Where(s => s.barcode == sBarcode))
            {
                place = iPlace.place.ToString();
                no = iPlace.no;
            }

            stock iStock = dc.stocks.Where(s => s.place == place && s.no > no).FirstOrDefault();

            if (iStock != null)
            {
                iStock.no = no - 1;

                dc.SubmitChanges();
            }

            stock dStock = dc.stocks.Where(s => s.barcode == sBarcode).FirstOrDefault();

            if (dStock != null)
            {
                dc.stocks.DeleteOnSubmit(dStock);
                dc.SubmitChanges();
            }
            else
            {
                return false;
            }

            return true;
        }

        private bool IsStockOutRevRequest(string sBarcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            var iStockOut = from sor in dc.stock_out_revs
                            where sor.barcode == sor.barcode && sor.status == 0
                            select sor;

            if (iStockOut.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private string getDocNoStockOutRevByBarcode(string sBarcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            stock_out_rev iStockOutRev = dc.stock_out_revs.Where(sor => sor.barcode == sBarcode && sor.status == 0).FirstOrDefault();

            if (iStockOutRev != null)
            {
                return iStockOutRev.docNo.ToString();
            }
            else
            {
                return "";
            }
        }

        private int SetProductIntoShelf_NoZero(string sBarcodeRef, string sBarcode, string sUser)
        {
            sBarcodeRef = sBarcodeRef.Trim().ToUpper();
            sBarcode = sBarcode.Trim().ToUpper();

            if (IsShelfInStock(sBarcodeRef))
            {
                AddProductIntoShelf(sBarcode, sBarcodeRef, 0, sUser);
            }
            else
            {
                return -2;
            }
            return 1;
        }

        private string GetStockOutDocNoPrint(string sBarcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            string result = "";

            stock_in iTop1 = dc.stock_ins.Where(si => si.barcode == sBarcode).First();

            if (iTop1 != null)
            {
                result = iTop1.docNo.ToString();
            }
            else
            {
                result = "";
            }

            return result;
        }

        private bool _4IsItemSendPaint(int sPj_id, int sPd_item)
        {
            int total = 0;

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;


                    command1.CommandText = @"SELECT COUNT(*) PAINT FROM (
                                            SELECT PJ_ID,PD_ITEM FROM PAINT_LOAD EXCEPT 
                                            SELECT PJ_ID,PD_ITEM FROM PAINT_ACTION WHERE PS_ID = 1 ) AS G1 
                                            WHERE G1.PJ_ID = @PJ_ID AND G1.PD_ITEM = @ITEM";

                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@PJ_ID", SqlDbType.Int, 5).Value = sPj_id;
                    command1.Parameters.Add("@ITEM", SqlDbType.Int, 4).Value = sPd_item;
                    SqlDataReader dr = command1.ExecuteReader();

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            total = (int)dr["PAINT"];
                        }

                        if (total == 0)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                connection.Close();
            }
            return true;
        }

        public mResult SetOnTruck(iSetOnTruck input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            mResult result = new mResult();
            DateTime now = DateTime.Now;
            string sDocNo = "";
            int? sTripNo = 0;

            int project = 0;
            int items = 0;
            int point = input.sBarcode.IndexOf("-");

            if (point == -1)
            {
                project = 0;
                items = 0;
            }
            else
            {
                project = Convert.ToInt32(input.sBarcode.Substring(0, point));
                items = Convert.ToInt32(input.sBarcode.Substring(point + 1));
            }

            //if (!_4IsItemSendPaint(project, items))
            //{
            //    result.RESULT = "-13|ไม่สามารถบันทึกข้อมูลได้ ชิ้นงานยังไม่ได้ส่งงานสี";
            //    result.EXCEPTION = "";

            //    return result;
            //}

            //string ncr_check = "";
            //ncr_check = Check_NCR(input.sBarcode);

            //if (ncr_check != "OK")
            //{
            //    result.RESULT = "NCR : " + ncr_check;
            //    result.EXCEPTION = "";

            //    return result;
            //}

            sDocNo = GetStockOutDocRequest(input.sBarcode);

            //if (sDocNo == "")
            //{
            //    result.RESULT = "-12|เลขที่เอกสารแจ้งงานเข้าคลังสินค้า ที่พิมพ์ไม่ตรงกับเลขที่เอกสารที่แจ้ง";
            //    result.EXCEPTION = "";

            //    return result;
            //}

            if (!IsCarNo(input.sCarNo))
            {
                result.RESULT = "-10|เลขทะเบียนรถผิด";
                result.EXCEPTION = "";

                return result;
            }

            if (!IsStockOutTripReady(sDocNo, input.sCarNo))
            {
                //add car trip
                int NoCar = 0;
                string IdCart = "";
                

                var iCountNoCar = from ct in dc.car_trips
                               where ct.docNo == sDocNo
                               select ct;

                NoCar = iCountNoCar.Count();

                foreach (car_truck_driver itemIdCard in dc.car_truck_drivers.Where(ctd => ctd.carno == input.sCarNo))
                {
                    IdCart = itemIdCard.idcard;
                }

                try
                {
                    car_trip cartrip = new car_trip()
                    {
                        status = 0,
                        docNo = new string(sDocNo.ToUpper().Take(12).ToArray()),
                        tripNo = NoCar,
                        carNo = new string(input.sCarNo.Take(10).ToArray()),
                        idcard = new string(IdCart.Take(10).ToArray()),
                        users = new string(input.sUserID.Take(10).ToArray()),
                        dates = (DateTime)(now),
                        dateStart = (DateTime)(now)
                    };

                    dc.car_trips.InsertOnSubmit(cartrip);
                }
                catch (Exception ex)
                {
                    result.RESULT = "";
                    result.EXCEPTION = ex.Message;

                    return result;
                }
            }

            if (SetProductMoveOutOnShelf(input.sBarcode))
            {
                //update stock_out,product
                foreach (car_trip tripno in dc.car_trips.Where(ct => ct.docNo == sDocNo && ct.carNo == input.sCarNo && ct.status == 0))
                {
                    sTripNo = tripno.tripNo;
                }

                stock_out iStockOut = dc.stock_outs.Where(so => so.docNo == sDocNo && so.barcode == input.sBarcode && so.status == 0).FirstOrDefault();

                if (iStockOut != null)
                {
                    iStockOut.status = 3;
                    iStockOut.users = input.sUserID;
                    iStockOut.dates = (DateTime)(now);
                    iStockOut.tripNo = sTripNo;
                    iStockOut.fromCrane = input.sCrane;

                    dc.SubmitChanges();
                }

                var iProduct = from pd in dc.products
                               where pd.barcode == input.sBarcode && dc.stock_outs.Any(so => so.barcode == input.sBarcode && so.status == 3)
                               select pd;

                if (iProduct.FirstOrDefault() != null)
                {
                    foreach (var itemsPr in iProduct)
                    {
                        itemsPr.status = 9;
                    }

                    dc.SubmitChanges();
                }

                //update stock_out_doc (start,finish)
                stock_out_doc iStockOutDoc = dc.stock_out_docs.Where(soc => soc.docNo == sDocNo && soc.dateStart == null).FirstOrDefault();

                if (iStockOutDoc != null)
                {
                    iStockOutDoc.status = 2;
                    iStockOutDoc.dateStart = (DateTime)(now);

                    dc.SubmitChanges();
                }

                var iStockOutDoc2 = from sod in dc.stock_out_docs 
                             where sod.status == 2 && sod.docNo == sDocNo && !dc.stock_outs.Any(si => si.status != 1 && si.docNo == sDocNo)
                             select sod;

                if (iStockOutDoc2.FirstOrDefault() != null)
                {
                    foreach (var iSod2 in iStockOutDoc2)
                    {
                        iSod2.status = 1;
                        iSod2.dateFinish = (DateTime)(now);
                    }

                    dc.SubmitChanges();
                }
                
            }

            result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
            result.EXCEPTION = "";

            return result;
        }

        public mResult SetOutCom(iSetOutCom input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            mResult result = new mResult();
            DateTime now = DateTime.Now;

            try
            {
                stock_out iStockOut = dc.stock_outs.Where(so => so.docNo == input.sDocNo && so.barcode == input.sBarcode && so.status == 33).FirstOrDefault();

                if (iStockOut != null)
                {
                    iStockOut.status = 4;
                    iStockOut.secUser = input.sUserID;
                    iStockOut.secDate = (DateTime)(now);

                    dc.SubmitChanges();

                    result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
                    result.EXCEPTION = "";
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "0|ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message ;
            }

            return result;
        }

        public mResult SetOutUpReceive(iSetOutUpReceive input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            mResult result = new mResult();
            DateTime now = DateTime.Now;

            string sDocNo = "";

            if (!IsStockOutRevRequest(input.sBarcode))
            {
                result.RESULT = "-11|ยังไม่ได้แจ้งงานเข้าคลังสินค้า";
                result.EXCEPTION = "";

                return result;
            }

            if (!IsCarNo(input.sCarTruck))
            {
                result.RESULT = "-10|เลขทะเบียนรถผิด";
                result.EXCEPTION = "";

                return result;
            }

            sDocNo = getDocNoStockOutRevByBarcode(input.sBarcode);

            if (sDocNo == "")
            {
                result.RESULT = "-11|Not Found DocNo";
                result.EXCEPTION = "";

                return result;
            }

            stock_out_rev iStockOutRev = dc.stock_out_revs.Where(sor => sor.barcode == input.sBarcode && sor.status == 0).FirstOrDefault();

            if (iStockOutRev != null)
            {
                iStockOutRev.fromCrane = input.sCrane;
                iStockOutRev.fromUser = input.sUserID;
                iStockOutRev.fromDate = (DateTime)(now);
                iStockOutRev.fromCarTruck = input.sCarTruck;

                dc.SubmitChanges();

                result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
                result.EXCEPTION = "";

                return result;
            }

            return result;
        }

        public mResult SetOutDownReceive(iSetOutDownReceive input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            DateTime now = DateTime.Now;

            mResult result = new mResult();
            string sDocNo = "";
            string place = "";
            int nResult = 0;

            if (!IsStockOutRevRequest(input.sBarcode))
            {
                result.RESULT = "-10|ยังไม่ได้แจ้งงานแก้ไขเข้าคลังสินค้า";
                result.EXCEPTION = "";

                return result;
            }

            sDocNo = getDocNoStockOutRevByBarcode(input.sBarcode);

            if (sDocNo == "")
            {
                result.RESULT = "-11|เลขที่เอกสารแจ้งงานแก้ไขเข้าคลังสินค้า ที่พิมพ์ไม่ตรงกับเลขที่เอกสารที่แจ้ง";
                result.EXCEPTION = "";

                return result;
            }

            if (!IsShelfInStock(input.sBarcodeRef))
            {
                result.RESULT = "-12|ชื่อชั้นวางผิด";
                result.EXCEPTION = "";

                return result;
            }

            if (SetProductMoveOutOnShelf(input.sBarcode))
            {
                nResult = SetProductIntoShelf_NoZero(input.sBarcodeRef, input.sBarcode, input.sUserID);

                if (nResult == 1)
                {
                    foreach (stock iStock in dc.stocks.Where(s => s.barcode == input.sBarcode))
                    {
                        place = iStock.place.ToString();
                    }

                    try
                    {
                        //update stock_out_rev,product
                        stock_out_rev iStockOutRev = dc.stock_out_revs.Where(sor => sor.barcode == input.sBarcode && sor.status == 0).FirstOrDefault();

                        if (iStockOutRev != null)
                        {
                            iStockOutRev.status = 1;
                            iStockOutRev.place = place;
                            iStockOutRev.users = input.sUserID;
                            iStockOutRev.dates = (DateTime)(now);
                            iStockOutRev.destCrane = input.sCrane;

                            dc.SubmitChanges();
                        }

                        product iProduct = dc.products.Where(pd => pd.barcode == input.sBarcode).FirstOrDefault();

                        if (iProduct != null)
                        {
                            iProduct.status = 2;

                            dc.SubmitChanges();                          
                        }

                        //update stock_out_rev_doc (start,finish)
                        var query1 = from sord in dc.stock_out_rev_docs
                                     where sord.status == 0 && sord.docNo == sDocNo && !dc.stock_out_revs.Any(si => si.status == 1 && si.docNo == sDocNo)
                                     select sord;

                        if (query1.FirstOrDefault() != null)
                        {
                            foreach (var iStockOutRevDoc in query1)
                            {
                                iStockOutRevDoc.status = 2;
                                iStockOutRevDoc.dateStart = (DateTime)(now);
                            }

                            dc.SubmitChanges();
                        }

                        var query2 = from sord in dc.stock_out_rev_docs
                                     where sord.status == 2 && sord.docNo == sDocNo && !dc.stock_out_revs.Any(si => si.status == 0 && si.docNo == sDocNo)
                                     select sord;

                        if (query2.FirstOrDefault() != null)
                        {
                            foreach (var iStockOutRevDoc2 in query1)
                            {
                                iStockOutRevDoc2.status = 1;
                                iStockOutRevDoc2.dateFinish = (DateTime)(now);
                            }

                            dc.SubmitChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        result.RESULT = "-13|ไม่สามารถอัพเดทข้อมูลได้";
                        result.EXCEPTION = ex.Message;

                        return result;
                    }
                }
            }
            else
            {
                result.RESULT = "-14|บันทึกข้อมูลลำดับชิ้นงานภายในชั้นวางไม่สาเร็จ";
                result.EXCEPTION = "";

                return result;
            }

            result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
            result.EXCEPTION = "";

            return result;
        }

        public mGetSummaryByDateStockOut GetSummaryByDateStockOut(iGetSummaryByDate_SO input)
        {
            mGetSummaryByDateStockOut result = new mGetSummaryByDateStockOut();
            result.GetSummaryByDate_SO = new List<GetSummaryByDateStockOut>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;

                    if (input.nTypeJob == 0)
                    {
                        command1.CommandText = @"SELECT * FROM (SELECT TOP (100) PERCENT CONVERT(VARCHAR(10),A.DATEREQ,121) AS DATE, COUNT(B.BARCODE) AS REQ, COUNT(B.USERS)                                                 AS REC, COUNT(B.BARCODE) - COUNT(B.USERS) AS REM
                                              FROM STOCK_OUT_DOC AS A INNER JOIN
                                              STOCK_OUT AS B ON A.DOCNO = B.DOCNO
                                              GROUP BY CONVERT(VARCHAR(10),A.DATEREQ,121)
                                              HAVING (CONVERT(VARCHAR(10), A.DATEREQ, 121) BETWEEN @DATE_START AND @DATE_FINISH))AS SUMMARY WHERE REM>0 ORDER BY DATE";
                    }
                    else
                    {
                        command1.CommandText = @"SELECT * FROM (SELECT TOP (100) PERCENT CONVERT(VARCHAR(10),A.DATEREQ,121) AS DATE, COUNT(B.BARCODE) AS REQ, COUNT(B.PLACE)                                                 AS REC, COUNT(B.BARCODE) - COUNT(B.PLACE) AS REM
                                               FROM STOCK_OUT_REV_DOC AS A INNER JOIN
                                               STOCK_OUT_REV AS B ON A.DOCNO = B.DOCNO
                                               GROUP BY CONVERT(VARCHAR(10),A.DATEREQ,121)
                                               HAVING (CONVERT(VARCHAR(10), A.DATEREQ, 121) BETWEEN @DATE_START AND @DATE_FINISH))AS SUMMARY WHERE REM>0 ORDER BY DATE";
                    }
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@DATE_START", SqlDbType.DateTime, 10).Value = input.sDateStart;
                    command1.Parameters.Add("@DATE_FINISH", SqlDbType.DateTime, 10).Value = input.sDateFinish;
                    SqlDataReader dr = command1.ExecuteReader();

                    int total = 0;
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            result.GetSummaryByDate_SO.Add(new GetSummaryByDateStockOut()
                            {
                                DATE = dr["Date"].ToString(),
                                REQ = dr["Req"].ToString(),
                                REC = dr["Rec"].ToString(),
                                REM = dr["Rem"].ToString()
                            });
                        }

                        total = total + (int)dr["Rem"];

                        result.TOTAL = total;
                    }
                    else
                    {
                        result.GetSummaryByDate_SO.Add(new GetSummaryByDateStockOut()
                        {
                            DATE = "-",
                            REQ = "-",
                            REC = "-",
                            REM = "-"
                        });
                    }
                }

                connection.Close();
            }
            return result;
        }

        public mGetRemindByDateStockOut GetRemindByDateStockOut(iGetRemindDate input)
        {
            mGetRemindByDateStockOut result = new mGetRemindByDateStockOut();
            result.GetRemindByDate_SO = new List<GetRemindByDateStockOut>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;

                    if (input.nTypeJob == 0)
                    {
                        command1.CommandText = @"SELECT B.BARCODE, C.WGROUP AS WELD, C.LENGTH, C.WEIGHT
                                                FROM STOCK_OUT_DOC AS A INNER JOIN
                                                STOCK_OUT AS B ON A.DOCNO = B.DOCNO INNER JOIN
                                                PRODUCT AS C ON B.BARCODE = C.BARCODE
                                                WHERE (CONVERT(VARCHAR(10),A.DATEREQ,121) = @DATE) AND (B.USERS IS NULL)
                                                ORDER BY A.DATEREQ";
                    }
                    else
                    {
                        command1.CommandText = @"SELECT B.BARCODE, C.WGROUP AS WELD, C.LENGTH, C.WEIGHT
		                                        FROM STOCK_OUT_REV_DOC AS A INNER JOIN
                                                STOCK_OUT_REV AS B ON A.DOCNO = B.DOCNO INNER JOIN
                                                PRODUCT AS C ON B.BARCODE = C.BARCODE
                                                WHERE (CONVERT(VARCHAR(10),A.DATEREQ,121) = @DATE) AND (B.PLACE IS NULL)
                                                ORDER BY A.DATEREQ";
                    }
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@DATE", SqlDbType.DateTime, 10).Value = input.sDate;
                    SqlDataReader dr = command1.ExecuteReader();

                    int total = 0;
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            result.GetRemindByDate_SO.Add(new GetRemindByDateStockOut()
                            {
                                BARCODE = dr["BARCODE"].ToString(),
                                WELD = dr["WELD"].ToString(),
                                LENGTH = dr["LENGTH"].ToString(),
                                WEIGTH = dr["WEIGHT"].ToString(),
                            });

                            total = total + 1;
                            result.TOTAL = total;
                        }
                    }
                    else
                    {
                        result.GetRemindByDate_SO.Add(new GetRemindByDateStockOut()
                        {
                            BARCODE = "-",
                            WELD = "-",
                            LENGTH = "-",
                            WEIGTH = "-",
                        });
                    }
                }

                connection.Close();
            }

            return result;
        }

        public mGetStockOutDaily GetStockOutDaily(iGetRemindDate input)
        {
            mGetStockOutDaily result = new mGetStockOutDaily();
            result.GetStockOutDaily_SO = new List<GetStockOutDaily>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;

                    if (input.nTypeJob == 0)
                    {
                        command1.CommandText = @"SELECT LEFT(CONVERT(VARCHAR(10), STOCK_OUT.DATES, 108), 5) AS DATES, STOCK_OUT.USERS, PRODUCT.PROJECT, 
                                                 PRODUCT_TYPE.TYPENAME, PRODUCT.SHARP, PRODUCT.ITEM, 
                                                 CONVERT(VARCHAR, CONVERT(MONEY, PRODUCT.LENGTH), 1) AS LENGTH, CAST(PRODUCT.WEIGHT AS DECIMAL(9, 3)) AS WEIGHT,
                                                 ISNULL(STOCK_OUT.FROMCRANE, N'-') AS FROMCRANE, STOCK_OUT.PLACE AS FROMPLACE, CAR_TRIP.CARNO AS FROMCARTRUCK 
                                                 FROM STOCK_OUT INNER JOIN 
                                                 CAR_TRIP ON STOCK_OUT.DOCNO = CAR_TRIP.DOCNO AND STOCK_OUT.TRIPNO = CAR_TRIP.TRIPNO LEFT OUTER JOIN
                                                 PRODUCT_TYPE INNER JOIN
                                                 PRODUCT ON PRODUCT_TYPE.TYPE = PRODUCT.TYPE ON STOCK_OUT.BARCODE = PRODUCT.BARCODE 
                                                 WHERE (CONVERT(VARCHAR(10), STOCK_OUT.DATES, 121) = @DATE) AND (PRODUCT.STATUS = 9) 
                                                 ORDER BY DATES;";
                    }
                    else if (input.nTypeJob == 1)
                    {
                        command1.CommandText = @"SELECT LEFT(CONVERT(VARCHAR(10), STOCK_OUT_REV.DATES, 108), 5) AS DATES, STOCK_OUT_REV.USERS,
                                                PRODUCT.PROJECT, PRODUCT_TYPE.TYPENAME, PRODUCT.SHARP, PRODUCT.ITEM, 
                                                CONVERT(VARCHAR,CONVERT(MONEY,PRODUCT.LENGTH),1) AS [LENGTH],  
                                                CAST(PRODUCT.WEIGHT AS  DECIMAL(9,3)) AS WEIGHT, ISNULL(STOCK_OUT_REV.FROMCARTRUCK,'-') AS FROMCARTRUCK  ,  
                                                ISNULL(STOCK_OUT_REV.FROMCRANE,'-') AS FROMCRANE, 
                                                 ISNULL(STOCK_OUT_REV.DESTCRANE,'-') AS DESTCRANE, STOCK_OUT_REV.PLACE 
                                                 FROM PRODUCT_TYPE INNER JOIN  
                                                 PRODUCT ON PRODUCT_TYPE.TYPE = PRODUCT.TYPE RIGHT OUTER JOIN  
                                                 STOCK_OUT_REV ON PRODUCT.BARCODE = STOCK_OUT_REV.BARCODE 
                                                 WHERE     (CONVERT(VARCHAR(10), STOCK_OUT_REV.DATES, 121) = @DATE) AND (STOCK_OUT_REV.STATUS = 1)  
                                                 ORDER BY DATES ;";
                    }

                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@DATE", SqlDbType.DateTime, 10).Value = input.sDate;
                    SqlDataReader dr = command1.ExecuteReader();

                    int total = 0;
                    if (dr.HasRows && input.nTypeJob == 0)
                    {
                        while (dr.Read())
                        {
                            result.GetStockOutDaily_SO.Add(new GetStockOutDaily()
                            {
                                DATES = dr["dates"].ToString(),
                                USERS = dr["users"].ToString(),
                                PROJECT = dr["project"].ToString(),
                                TYPE_NAME = dr["typeName"].ToString(),
                                SHARP = dr["sharp"].ToString(),
                                ITEM = dr["item"].ToString(),
                                LENGTH = dr["length"].ToString(),
                                WEIGHT = dr["weight"].ToString(),
                                FROM_CRANE = dr["fromCrane"].ToString(),
                                FROM_PLACE = dr["fromPlace"].ToString(),
                                FROM_CAR_TRUCK = dr["fromCarTruck"].ToString(),
                                DESC_CRANE = "-"
                            });

                            total = total + 1;
                            result.TOTAL = total;
                        }
                    }
                    else if (dr.HasRows && input.nTypeJob == 1)
                    {
                        while (dr.Read())
                        {
                            result.GetStockOutDaily_SO.Add(new GetStockOutDaily()
                            {
                                DATES = dr["dates"].ToString(),
                                USERS = dr["users"].ToString(),
                                PROJECT = dr["project"].ToString(),
                                TYPE_NAME = dr["typeName"].ToString(),
                                SHARP = dr["sharp"].ToString(),
                                ITEM = dr["item"].ToString(),
                                LENGTH = dr["length"].ToString(),
                                WEIGHT = dr["weight"].ToString(),
                                FROM_CRANE = dr["fromCrane"].ToString(),
                                FROM_PLACE = dr["Place"].ToString(),
                                FROM_CAR_TRUCK = dr["fromCarTruck"].ToString(),
                                DESC_CRANE = dr["destcrane"].ToString()
                            });
                        }
                    }
                    else
                    {
                        result.GetStockOutDaily_SO.Add(new GetStockOutDaily() 
                        {
                            DATES = "-",
                            USERS = "-",
                            PROJECT = "-",
                            TYPE_NAME = "-",
                            SHARP = "-",
                            ITEM = "-",
                            LENGTH = "-",
                            WEIGHT = "-",
                            FROM_CRANE = "-",
                            FROM_PLACE = "-",
                            FROM_CAR_TRUCK = "-",
                            DESC_CRANE = "-"
                        });
                    }
                }

                connection.Close();
            }

            return result;
        }

        public mResult PrintDocOnTruckNoDo(iPrintDocOnTruckNoDo input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            mResult result = new mResult();

            string sDocNo = "";

            if (!IsCarNo(input.sCarNo))
            {
                result.RESULT  = "-10|ทะเบียนรถผิด";
                result.EXCEPTION = "";

                return result;
            }

            sDocNo = GetStockOutDocNoPrint(input.sBarcode);

            //set print Queue
            var iQuery = from ct in dc.car_trips 
                         join sod in dc.stock_out_docs 
                         on ct.docNo  equals sod.docNo 
                         where ct.docNo == sDocNo && ct.carNo == input.sCarNo && ct.status ==0
                         select new { ct.docNo, sod.shipNo, ct.tripNo};


            return result;
        }
  

        #endregion

        #region FN
        #region FN Stock In
        private string IsStockInRequest(string Barcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            string result = "";

            var query = (from si in dc.stock_ins
                         where si.barcode == Barcode && si.status == 0
                         select si);

            if (query.Count() > 0)
            {
                result = "True";
            }
            else
            {
                result = "False";
            }

            return result; ;
        }

        private string IsStockInRevRequest(string Barcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            string result = "";

            var query = (from sir in dc.stock_in_revs
                         where sir.barcode == Barcode && sir.status == 0
                         select sir);

            if (query.Count() > 0)
            {
                foreach (var iStockRev in query)
                {
                    result = iStockRev.docNo.ToString();
                }
            }
            else
            {
                result = "False";
            }

            return result; ;
        }

        private bool IsCarNo(string CarNo)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            var query = from ctd in dc.car_truck_drivers
                        where ctd.carno == CarNo && ctd.status == 1
                        select ctd;

            if (query.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private string GetDocNoStockInByBarcode(string Barcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            string result = "";

            stock_in iTop1 = dc.stock_ins.Where(si => si.barcode == Barcode && si.status == 0).First();

            if (iTop1 != null)
            {
                result = iTop1.docNo.ToString();
            }
            else
            {
                result = "";
            }

            return result;
        }

        private string GetDocNoStockInRevByBarcode(string sBarcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            string result = "";

            var query = from sir in dc.stock_in_revs.Where(s => s.barcode == sBarcode && s.status == 0)
                        select sir.docNo;

            if (query.First() != null)
            {
                foreach (var iDocNo in query)
                {
                    result = iDocNo.ToString();
                }
            }
            else
            {
                result = "";
            }

            return result;
        }
        #endregion

        #region Stock
        private bool IsProductInStock(string sBarcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            bool result = true;

            var query = from s in dc.stocks
                        where s.barcode == sBarcode
                        select s;

            if (query.Count() > 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }

            return result;
        }
        #endregion

        #region Shelf
        private bool IsShelfInStock(string sShelf)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            var query = from ss in dc.stock_shelfs
                        where ss.place == sShelf
                        select ss;

            if (query.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool GetShelfProductInfoByBarcode(string sBarcode,  string sShelf, int nNo)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            bool bResult = true;

            var query = from s in dc.stocks
                        where s.barcode == sBarcode
                        select new { s.place, s.no };

            foreach (var items in query)
            {
                sShelf = items.place;
                nNo = Int32.Parse(items.no.ToString());
            }

            if (query.Count() == 0)
            {
                bResult = false;
            }
            else if (nNo >= 0)
            {
                bResult = true;
            }

            return bResult;
        }

        private int GetShelfProductTotal(string sShelf)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            int nCount = 0;

            var iCount = from s in dc.stocks
                         where s.place == sShelf && s.no > 0
                         select s.barcode;

            nCount = iCount.Count();

            return nCount;
        }

        private bool SetProductInsertOnShelf(string sBarcode, string sShelf, int nNo, string sUser)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            DateTime now = DateTime.Now;

            if (!IsBarcode(sBarcode))
                return false;

            try
            {
                if (IsProductInStock(sBarcode))
                {
                    stock iStockUpdateNo = dc.stocks.Where(s => s.place == sShelf && s.no >= nNo).FirstOrDefault();

                    if (iStockUpdateNo != null)
                    {
                        iStockUpdateNo.no = iStockUpdateNo.no + 1;

                        dc.SubmitChanges();
                    }

                    stock iStockUpdatePlace = dc.stocks.Where(s => s.barcode == sBarcode).FirstOrDefault();

                    if (iStockUpdatePlace != null)
                    {
                        iStockUpdatePlace.place = sShelf;
                        iStockUpdatePlace.no = nNo;
                        iStockUpdatePlace.users = sUser;
                        iStockUpdatePlace.dates = (DateTime)(now);

                        dc.SubmitChanges();
                    }

                    return true;
                }
                else
                {
                    stock iStockUpdateNo = dc.stocks.Where(s => s.place == sShelf && s.no >= nNo).FirstOrDefault();

                    if (iStockUpdateNo != null)
                    {
                        iStockUpdateNo.no = iStockUpdateNo.no + 1;

                        dc.SubmitChanges();
                    }

                        stock dtStock = new stock()
                        {
                            status = 1,
                            barcode = new string(sBarcode.Take(20).ToArray()),
                            place = new string(sShelf.ToUpper().Take(10).ToArray()),
                            no = nNo,
                            users = new string(sUser.ToUpper().Take(7).ToArray()),
                            dates = (DateTime)(now),
                            ts = (DateTime)(now)
                        };

                        dc.stocks.InsertOnSubmit(dtStock);
                        dc.SubmitChanges();
                    }
                    return true;
                }      
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool AddProductIntoShelf(string sBarcode, string sShelf, int nNo, string sUser)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            DateTime now = DateTime.Now;

            stock iStockInsert = dc.stocks.FirstOrDefault();
            try
            {
                    stock dtStock = new stock()
                    {
                        status = 1,
                        barcode = new string(sBarcode.Take(20).ToArray()),
                        place = new string(sShelf.ToUpper().Take(10).ToArray()),
                        no = nNo,
                        users = new string(sUser.ToUpper().Take(7).ToArray()),
                        dates = (DateTime)(now),
                        ts = (DateTime)(now)
                    };

                    dc.stocks.InsertOnSubmit(dtStock);
                    dc.SubmitChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion

        #region Is Barcode
        private bool IsBarcode(string sBarcode)
        {
            if (IsBarcodeNew(sBarcode))
                return true;
            if (IsBarcodeOld(sBarcode))
                return true;
            return false;
        }

        private bool IsBarcodeOld(string sBarcode)
        {
            sBarcode = sBarcode.ToUpper().Trim();
            if (sBarcode.Length != 12)
                return false;

            if (sBarcode.Substring(7, 1) != "-")
                return false;

            string[] barcode = sBarcode.Split('-');
            if (barcode.Length != 2)
                return false;
            try
            {
                if (int.Parse(barcode[0]) > 0)
                    return false;
            }
            catch (Exception e)
            {
                try
                {
                    if (int.Parse(barcode[1]) == 0)
                        return false;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return true;
        }

        private bool IsBarcodeNew(string sBarcode)
        {
            sBarcode = sBarcode.ToUpper().Trim();
            if (sBarcode.Length != 10)
                return false;

            if (sBarcode.Substring(5, 1) != "-")
                return false;

            string[] barcode = sBarcode.Split('-');
            if (barcode.Length != 2)
                return false;
            try
            {
                if (int.Parse(barcode[0]) == 0)
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            try
            {
                if (int.Parse(barcode[1]) == 0)
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        #endregion
        #endregion

        #endregion
    }
}
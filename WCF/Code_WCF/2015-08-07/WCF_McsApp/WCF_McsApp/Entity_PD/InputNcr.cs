﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class InputNcrBarcodePC
    {
        [DataMember]
        public string sBarcode { get; set; }

        [DataMember]
        public int nType_PC { get; set; }
    }

    [DataContract]
    public class InputNcrPD
    {
        [DataMember]
        public string sBarcode { get; set; }
    }

    [DataContract]
    public class InputNcrReport
    {
        [DataMember]
        public string sBarcode { get; set; }

        [DataMember]
        public int nRev_No { get; set; }

        [DataMember]
        public int nCheck_No { get; set; }

        [DataMember]
        public int nPC_ID { get; set; }

        [DataMember]
        public string sPlace { get; set; } 

        [DataMember]
        public string sUS_ID { get; set; }
    }

    [DataContract]
    public class InputNcrPassNcr
    {
        [DataMember]
        public string sNC_ID { get; set; }

        [DataMember]
        public string sNcr_No { get; set; }

        [DataMember]
        public int nOther_No { get; set; }

        [DataMember]
        public string sUS_ID { get; set; }

        [DataMember]
        public bool getImage { get; set; }

        [DataMember]
        public string sPd_Comment { get; set; }

        [DataMember]
        public int sPd_Actual { get; set; }
    }
}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class InputReviseGroup
    {
        [DataMember]
        public string sUG_Name { get; set; }
    }

    [DataContract]
    public class InputRevisePJ
    {
        [DataMember]
        public int nPJ_ID { get; set; }

        [DataMember]
        public int nPd_Item { get; set; }
    }

    [DataContract]
    public class InputReviseDT
    {
        [DataMember]
        public int nPJ_ID { get; set; }

        [DataMember]
        public int nPd_Item { get; set; }

        [DataMember]
        public int nRev_no { get; set; }

        [DataMember]
        public int nProcess { get; set; }
    }

    [DataContract]
    public class InputGroupRevise
    {
        [DataMember]
        public string nUG_ID { get; set; }

        [DataMember]
        public string sUG_Name { get; set; }


    }

    [DataContract]
    public class InputReviseActual
    {
        [DataMember]
        public int nPJ_ID { get; set; }

        [DataMember]
        public int nPd_Item { get; set; }

        [DataMember]
        public int nRev_no { get; set; }

        [DataMember]
        public int nProcess { get; set; }

        [DataMember]
        public string sUserActual { get; set; }

        [DataMember]
        public decimal nActual_QTY { get; set; }

        [DataMember]
        public string sUserID { get; set; }

        [DataMember]
        public int QTY { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
#region GetRevisePlan
    [DataContract]
    public class GetGruopRevise
    {
        [DataMember]
        public string UG_NAME { get; set; }
    }

    [DataContract]
    public class GetRevisePlanGroup
    {
        [DataMember]
        public List<GetRevisePlan> getRevise { get; set; }

        [DataMember]
        public int TOTAL { get; set; }
    }

    [DataContract]
    public class GetRevisePlan
    {
        [DataMember]
        public Int16 PD_ITEM { get; set; }

        [DataMember]
        public string MP_SNAME { get; set; }

        [DataMember]
        public string PT_ITEMTYPE { get; set; }

        [DataMember]
        public Int16 PJ_SHARP { get; set; }

        [DataMember]
        public string UG_NAME { get; set; }

        [DataMember]
        public string REVDATE { get; set; }
    }
#endregion

    #region GetReviseRev
    [DataContract]
    public class GetReviseRev
    {
        [DataMember]
        public List<GetRevno> getRevno { get; set; }

        [DataMember]
        public List<GetProcessRev> getProcessRev { get; set; } 
    }

    [DataContract]
    public class GetRevno
    {
        [DataMember]
        public int REV_NO { get; set; }
    }

    [DataContract]
    public class GetProcessRev
    {
        [DataMember]
        public string PROCESS_NAME { get; set; }

        [DataMember]
        public int PC_ID { get; set; }
    }
    #endregion

    #region GetReviseDetail
    [DataContract]
    public class GetReviseDetail
    {
        [DataMember]
        public int PJ_ID { get; set; }

        [DataMember]
        public string PROJECT { get; set; }

        [DataMember]
        public Int16 ITEM { get; set; }

        [DataMember]
        public int REV_NO { get; set; }

        [DataMember]
        public string DESIGN_CHANGE { get; set; }

        [DataMember]
        public int PC_ID { get; set; }

        [DataMember]
        public string PROCESS { get; set; }

        [DataMember]
        public string TYPES { get; set; }

        [DataMember]
        public string PLAN_GROUP { get; set; }

        [DataMember]
        public int PLAN_ID { get; set; }

        [DataMember]
        public string PLAN_DATE { get; set; }

        [DataMember]
        public int PLAN { get; set; }

        [DataMember]
        public int ACTUAL { get; set; }
    }



#endregion

#region GetReviseGroup
    [DataContract]
    public class GetReviseGroup
    {
        [DataMember]
        public string US_ID { get; set; }

        [DataMember]
        public string FULLNAME { get; set; }

        //[DataMember]
        //public int ACTUAL { get; set; }
    }
#endregion
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Web;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class ResultStatus
    {
        [DataMember]
        public string RESULT { get; set; }
    }
}

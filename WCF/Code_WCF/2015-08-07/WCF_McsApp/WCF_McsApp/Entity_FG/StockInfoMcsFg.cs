﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class iBarcode
    {
        [DataMember]
        public string Barcode { get; set; }
    }

    public class mGetProductStatus
    {
        [DataMember]
        public string PROJECT { get; set; }

        [DataMember]
        public string TYPE_NAME { get; set; }

        [DataMember]
        public string ITEM { get; set; }

        [DataMember]
        public string SHARP { get; set; }

        [DataMember]
        public string DWG { get; set; }

        [DataMember]
        public string CODE { get; set; }

        [DataMember]
        public string SIZE { get; set; }

        [DataMember]
        public string LENGTH { get; set; }

        [DataMember]
        public string WEIGHT { get; set; }

        [DataMember]
        public string PLACE { get; set; }

        [DataMember]
        public string NO { get; set; }
    }

    public class iProjectNameAndItem
    {
        [DataMember]
        public string ProjectName { get; set; }

        [DataMember]
        public int? Item { get; set; }
    }

    public class mGetProductPlaceInStock
    {
        [DataMember]
        public string PROJECT { get; set; }

        [DataMember]
        public string TYPE_NAME { get; set; }

        [DataMember]
        public string SHARP { get; set; }

        [DataMember]
        public string ITEM { get; set; }

        [DataMember]
        public string PLACE { get; set; }

        [DataMember]
        public string NO { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.IO;
using System.ComponentModel;

namespace WCF_McsApp
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.

    [ServiceContract]
    public interface IServices
    {
        #region PD
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetConfig")]
        [Description("PD - Config Show Data")]
        List<mConfig> Getconfig();  //mConfig อยู่ใน Userinfo

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SetCreateDivice")]
        [Description("PD -  Creat Log Divice Mobile")]
        mResult CreateDivice(SetDevice input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "Login")]
        [Description("PD - Check Login")]
        ResultUser CheckUsernamePassword(InputLogin input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "CheckUserAuthen")]
        [Description("PD - Check User Authen")]
        mResult CheckUserAuthen(CheckAuthen input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "CheckGroupAuthen")]
        [Description("PD - Check Group Authen")]
        mResult CheckGroupAuthen(CheckAuthen input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "SetUserPassword")]
        string SetUsernamePassword(SetUserPass input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetProductStatus")]
        [Description("PD - Process Product Status Show Data")]
        GetProductInfo GetProductStatus(InputBarcode input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetProductProcess")]
        [Description("PD - Process Product Send Show Process Name")]
        List<GetProcessPD> GetProductProcess();

        [OperationContract]
        [WebInvoke(Method = "POST",ResponseFormat = WebMessageFormat.Json,RequestFormat = WebMessageFormat.Json,UriTemplate = "GetProductSend")]
        [Description("PD - Process Product Send Show Process Name")]
        GetProductSend GetProductSend(InputBarcodeType input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetBuiltProductStep1")]
        [Description("PD - Process Built Beam & Box Show Data Part 1")]
        BuiltDetailProject GetBuiltProjectDetails(InputBuiltProject input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetBuiltProductStep2")]
        [Description("PD - Process Built Beam & Box Show Data Part 2")]
        GetBuiltStatus GetbuiltStatus(InputCheckBuilt input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SetBuiltActual")]
        [Description("PD - Process Built Beam & Box Save Data To Database")]
        ResultStatus setBuiltActualSend(InputBuiltActualSend input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetFabDetail_1")]
        [Description("PD - Process Fab Show Data Part 1")]
        FabGetProcess GetFabProcessDetail(InputFabBarcode input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetFabDetail_2")]
        [Description("PD - Process Fab Show Data Part 2")]
        FabGetProject GetFabProjectDetail(InputFabBarcodePC input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetFabDetail_3")]
        [Description("PD - Process Fab Show Data Part 3")]
        List<FabGetGroupUser> GetFabGroupUser(InputFabGroup input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "SetFabActualSend")]
        [Description("PD - Process Fab Save Data To Database")]
        ResultStatus SetFabActual(InputFabActualSend input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetWeldDetail_1")]
        [Description("PD - Process Weld Show Data Part 1")]
        WeldGetProcess GetWeldProcessDetail(InputWeldBarcode input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetWeldDetail_2")]
        [Description("PD - Process Weld Show Data Part 2")]
        WeldGetProject GetWeldProjectDetail(InputWeldBarcodePC input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetWeldDetail_3")]
        [Description("PD - Process Weld Show Data Part 3")]
        List<WeldGetGroupUser> GetWeldGroupUser(InputWeldGroup input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "SetWeldActualSend")]
        [Description("PD - Process Weld Save Data To Database")]
        ResultStatus SetWeldActual(InputWeldActualSend input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetOtherDetail_1")]
        [Description("PD - Process Other Show Data Part 1")]
        GetListPcRev GetOtherProcess(InputOtherStep1 input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetOtherDetail_2")]
        [Description("PD - Process Other Show Data Part 2")]
        OtherGetProcessDetail GetOtherProcessDetail(InputOtherStep2 input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "SetOtherActualSend")]
        [Description("PD - Process Other Save Data To Database")]
        ResultStatus SetOtherActual(InputOtherActualSend input);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetRevisePlan_1")]
        [Description("PD - Process Plan Revise Show List Group Name")]
        List<GetGruopRevise> GetGroupReviseInfo();

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetRevisePlan_2")]
        [Description("PD - Process Plan Revise Show Data")]
        GetRevisePlanGroup GetReviseInfo(InputReviseGroup input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetReviseProject")]
        [Description("PD - Process Revise Fab & Weld Show Data Part 1")]
        GetReviseRev GetreviseRevNo(InputRevisePJ input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetReviseDetail")]
        [Description("PD - Process Revise Fab & Weld Show Data Part 2")]
        GetReviseDetail GetreviseDetail(InputReviseDT input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetReviseGroup")]
        [Description("PD - Process Revise Fab & Weld Show Data Part 3")]
        List<GetReviseGroup> GetreviseGroup(InputGroupRevise input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SetReviseActual")]
        [Description("PD - Process Revise Fab & Weld Save Data To Database")]
        ResultStatus SetREviseActual(InputReviseActual input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetNcrRevCheckList")]
        [Description("PD - Process Ncr Show Data Part 1")]
        NcrGetRevCheckList GetNcrRevCheckList(InputNcrBarcodePC input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetNcrProduct")]
        [Description("PD - Process NcrShow Data Part 2")]
        NcrProduct GetDimProduct(InputNcrPD input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetNcrReport")]
        [Description("PD - Process Ncr Show Data Part 3")]
        List<NcrGetReport> GetNcrReport(InputNcrReport input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SetNcrPassNcr")]
        [Description("PD - Process Ncr Save Data To Database")]
        ResultStatus SetNcrSend(InputNcrPassNcr input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SetQADimension")]
        [Description("PD - Process Ncr Request Save Data To Database")]
        ResultStatus SetNCR_Request(InputNcrReport input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadImages_PD")]
        [Description("PD -  Upload Images To Folder PD")]
        mResult uploadImages_PD(Stream fileUpload);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetLocationList")]
        [Description("PD - Process Ncr Show Data List Location")]
        List<GetLocationList> getLocationList(InputLocation input);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "PD_GetProcessUt")]
        [Description("PD - Process UT")]
        List<getProcessUt> PD_GetProcessUt();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "PD_UTRequest")]
        [Description("PD - UT Request")]
        setRequestReport PD_UTRequest(getRequestReport input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "PD_ReportUTRequest")]
        [Description("PD - Report UT Request")]
        List<setReportUTRequest> PD_ReportUTRequest(getReportUTRequest input);
	#endregion

        #region Qc
        //PC : QC PLAN CHECK
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "PCgetProjectName")]
        [Description("QC - Process Plan/Summary Status Show List Project Name ")]
        List<mProject> PCgetProjectName(iProject input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "PCgetProjectItem")]
        [Description("QC - Process Plan/Summary Status Show List Item ")]
        List<mItemPj> PCgetProjectItem(iItemPj input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "PCgetProjectRevno")]
        [Description("QC - Process Plan/Summary Status Show List Revise")]
        List<mRevNoPlanCheck> PCgetProjectRevno(iRevNoPlanCheck input);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "PCgetGroupFab")]
        [Description("QC - Process Plan/Summary Status Show List Group Fab")]
        List<mGroupFab> PCgetGroupFab();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "PCgetGroupWeld")]
        [Description("QC - Process Plan/Summary Status Show List Group Weld")]
        List<mGroupWeld> PCgetGroupWeld();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "PCgetStatus")]
        [Description("QC - Process Plan/Summary Status Show List Status")]
        List<mStatus> PCgetStatus();

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "PCgetProjectShowListCheck")]
        [Description("QC - Process Plan/Summary Status Show Data Report")]
        List<mPjShowListCheck> PCgetProjectShowListCheck(iPjShowListCheck input);

        //IF : IN FORM NCR
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "IFgetListDataForm")]
        [Description("QC - Process Inform Ncr Show List DWG Type & Direction")]
        mListDataForm IFgetListDataForm(iListDataForm input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFgetProjectRevNo")]
        [Description("QC - Process Inform Ncr Show List Revise")]
        List<mRevNoInForm> IFgetProjectRevNo(iRevNoInForm input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFgetProjectDetail")]
        [Description("QC - Process Inform Ncr Show Detail Of project")]
        mChProjectDetail IFgetProjectDetail(iChProjectDetail input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFgetPositionList")]
        [Description("QC - Process Inform Ncr Show List Positions")]
        List<mPosition> IFgetPositionList(iPosition input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFgetDescriptionsNcr")]
        [Description("QC - Process Inform Ncr Show List Descriptions")]
        List<mDescriptions> IFgetDescriptionsNcr(iDescriptions input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFgetResponse")]
        [Description("QC - Process Inform Ncr Show Data Response")]
        mResponse IFgetResponse(iResponse input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFsetAddNcrCheck")]
        [Description("QC - Process Inform Ncr Save Data To Database : Add Ncr")]
        mResult IFsetAddNcrCheck(iNcrCheckAdd input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFsetNoNcrCheck")]
        [Description("QC - Process Inform Ncr Save Data To Database : No Ncr")]
        mResult IFsetNoNcrCheck(iNoNcrCheck input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFgetNcrReport")]
        [Description("QC - Process Inform Ncr Show Data Ncr Report")]
        List<mNcrReport> IFgetNcrReport(iNcrReport input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFgetNcrType")]
        mNcrType IFgetNcrType(iNcrType input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFsetEditNcrCheck")]
        [Description("QC - Process Inform Ncr Save Data To Database : Edit Ncr")]
        mResult IFsetEditNcrCheck(iNcrCheckEdit input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFsetDeleteNcrCheck")]
        [Description("QC - Process Inform Ncr Save Data To Database : Delete Ncr")]
        mResult IFsetDeleteNcrCheck(iNcrCheckDelete input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFsetConfirmNcr")]
        [Description("QC - Process Inform Ncr Save Data To Database : Confirm Ncr")]
        mResult IFsetConfirmNcr(iConfirmNcr input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFgetOther")]
        [Description("QC - Process Inform Ncr Show Other Of Ncr")]
        mgetOther IFgetOther(igetOther input);

        //RC : Qc Recheck
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "RCgetDataRecheck")]
        [Description("QC - Process Recheck Show Data Part 1")]
        mDataRecheck RCgetDataRecheck(iDataRecheck input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "RCgetNcrReport")]
        [Description("QC - Process Recheck Show Data Report")]
        List<mNcrReportRC> RCgetNcrReport(iNcrReportRC input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "RCsetRecheckOK")]
        [Description("QC - Process Inform Ncr Save Data To Database : Recheck OK")]
        mResult RCsetRecheckOK(iRecheckOK input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "RCsetRecheckNotOK")]
        [Description("QC - Process Inform Ncr Save Data To Database : Recheck Not OK")]
        mResult RCsetRecheckNotOK(iRecheckNotOK input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "RCsetRecheckDel")]
        mResult RCsetRecheckDel(iRecheckDel input);

        //FN : QC Final
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "FNgetProjectName")]
        [Description("QC - Process Confirm Show List Project Name")]
        List<mProject> FNgetProjectName(iProject input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "FNgetProjectItem")]
        [Description("QC - Process Confirm Show List Item")]
        List<mItemPj> FNgetProjectItem(iItemPj input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "FNgetChecker")]
        [Description("QC - Process Confirm Show List User Check")]
        List<mChecker> FNgetChecker(iChecker input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "FNgetNcrReport")]
        [Description("QC - Process Confirm Show Data Report Ncr")]
        List<mNcrReportFN> FNgetNcrReport(iNcrReportFN input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "FNsetNcrQcCheck")]
        [Description("QC - Process Confirm Ncr Save Data To Database : Confirm Ncr")]
        mResult FNsetNcrQcCheck(iNcrCheckFN input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadImages_QC")]
        [Description("QC - Upload Images To Folder QC")]
        mResult uploadImages_QC(Stream fileUpload);
        #endregion

        #region HR
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetUserInGroup")]
        [Description("HR - Show List User In Group")]
        mListYearAndUser getUserInGroup(iUserInGroup input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetMasterLeaveQuotaByYear")]
        [Description("HR - Show Data Leave Quota")]
        List<mLeaveQuotaByYear> getMasterLeaveQuotaByYear(iLeaveQuotaByYear input);
        #endregion

        #region FG
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgCheckLoginUser")]
        [Description("FG - Login")]
        mLoginMcsFg CheckLoginUserMcsFg(iLoginMcsFg input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgCheckUserMenu")]
        [Description("FG - User Menu")]
        List<mUserCheckMenu> CheckUserMenu(iUserCheckMenu input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgGetProductStatus")]
        [Description("FG - Stock Info Get Product Status")]
        mGetProductStatus GetProductStatusFG(iBarcode input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgGetProductPlaceInStock")]
        [Description("FG - Stock Info Get Product Place In Stock")]
        List<mGetProductPlaceInStock> GetProductPlaceInStock(iProjectNameAndItem input);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgGetStockArea")]
        [Description("FG - Stock In Get Stock Area")]
        List<mStockArea> GetStockArea();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgSetUptoTruck")]
        [Description("FG - Stock In Set Up To Trauck")]
        mResult SetUptoTruck(iSetUptoTruck input);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgGetCrane")]
        [Description("FG - Stock In Get Crane")]
        List<mGetCrane> GetCrane();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgSetReceiveToStock")]
        [Description("FG - Stock In Set Receive To Stock")]
        mResult SetReceiveToStock(iSetReceiveToStock input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgSetUpReceive")]
        [Description("FG - Stock In Revise Set Up Receive")]
        mResult SetUpReceive_Rev(iSetUpRecive_Rev input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgSetDownReceive")]
        [Description("FG - Stock In Revise Set Down Receive")]
        mResult SetDownReceive_Rev(iSetDownRecive_Rev input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgGetShowSummaryByDate")]
        [Description("FG - Stock In Summary By Date")]
        mSummaryByDate GetShowSummaryByDate(iDateInputSummary input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgGetRemindByDate")]
        [Description("FG - Stock In Remine By Date")]
        mRemindByDate GetRemindByDate(iDateInputRemind input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgGetStockInDaily")]
        [Description("FG - Stock In Dairy")]
        mGetStockInDaily GetStockInDaily(iDateInputRemind input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat=WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate="McsFgStockOut_SetOnTruck")]
        [Description("FG - Stock Out Set On Truck")]
        mResult SetOnTruck(iSetOnTruck input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStockOut_SetOutCom")]
        [Description("FG - Stock Out Set Out Com")]
        mResult SetOutCom(iSetOutCom input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStockOut_SetUpReceive")]
        [Description("FG - Stock Out Revise Set Up Receive")]
        mResult SetOutUpReceive(iSetOutUpReceive input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStockOut_SetDownReceive")]
        [Description("FG - Stock Out Revise Set Down Receive")]
        mResult SetOutDownReceive(iSetOutDownReceive input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStockOut_GetSummaryByDate")]
        [Description("FG - Stock Out Get Summary By Date")]
        mGetSummaryByDateStockOut GetSummaryByDateStockOut(iGetSummaryByDate_SO input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStockOut_GetRemindByDate")]
        [Description("FG - Stock Out Remine By Date")]
        mGetRemindByDateStockOut GetRemindByDateStockOut(iGetRemindDate input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStockOut_GetDaily")]
        [Description("FG - Stock Out Dairy")]
        mGetStockOutDaily GetStockOutDaily(iGetRemindDate input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStockOut_PrintDocOnTruckNoDoc")]
        [Description("FG - Stock Out Print Document")]
        mResult PrintDocOnTruckNoDo(iPrintDocOnTruckNoDo input);

        #endregion
    }


    //-----------------------------------------------------------------

    [DataContract]
    public class mResult
    {
        [DataMember]
        public string RESULT { get; set; }

        [DataMember]
        public string EXCEPTION { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class iDataRecheck
    {
        [DataMember]
        public string sNc_Id { get; set; }
    }

    [DataContract]
    public class mDataRecheck
    {
        [DataMember]
        public string PJ_NAME { get; set; }

        [DataMember]
        public Int16 PD_ITEM { get; set; }

        [DataMember]
        public string REV_NO { get; set; }

        [DataMember]
        public int CHECK_NO { get; set; }

        [DataMember]
        public string PD_CODE { get; set; }

        [DataMember]
        public string DATE_PLAN { get; set; }

        [DataMember]
        public string USERNAME { get; set; }

        [DataMember]
        public string DATE_START { get; set; }

        [DataMember]
        public string BARCODE { get; set; }

        [DataMember]
        public int PT_MAINTYPE { get; set; }
    }

    [DataContract]
    public class iNcrReportRC
    {
        [DataMember]
        public string sNc_Id { get; set; }
    }

    [DataContract]
    public class mNcrReportRC
    {
        [DataMember]
        public string NCR_NO { get; set; }

        [DataMember]
        public string NT_NAME { get; set; }

        [DataMember]
        public string DIRECTION { get; set; }

        [DataMember]
        public string DESCRIPTIONS { get; set; }

        [DataMember]
        public string GROUP_NAME { get; set; }

        [DataMember]
        public string FULLNAME_PD { get; set; }

        [DataMember]
        public string DATE_PD { get; set; }

        [DataMember]
        public string DWG { get; set; }

        [DataMember]
        public string ACTUAL { get; set; }

        [DataMember]
        public string SUGGEST { get; set; }

        [DataMember]
        public string OTHER_NO { get; set; }

        [DataMember]
        public string POSITION_ID { get; set; }

        [DataMember]
        public string PD_COMMENT { get; set; }

        [DataMember]
        public string PD_ACTUAL { get; set; }
    }

    [DataContract]
    public class iRecheckOK
    {
        [DataMember]
        public string sNc_Id { get; set; }

        [DataMember]
        public string sNcr_No { get; set; }

        [DataMember]
        public int nOther_no { get; set; }

        [DataMember]
        public string sUsers { get; set; }

        [DataMember]
        public string sFile { get; set; }

        [DataMember]
        public int? Actual { get; set; }
    }

    [DataContract]
    public class iRecheckNotOK
    {
        [DataMember]
        public int nTypePc { get; set; }

        [DataMember]
        public string sBarcode { get; set; }

        [DataMember]
        public int nRev_no { get; set; }

        [DataMember]
        public int nCheck_no { get; set; }

        [DataMember]
        public string sNcr_no { get; set; }

        [DataMember]
        public string sUser { get; set; }

        [DataMember]
        public string sGroup { get; set; }

        [DataMember]
        public string sFile { get; set; }

        [DataMember]
        public string sSug { get; set; }

        [DataMember]
        public string sDirection { get; set; }

        [DataMember]
        public int nDwg_type { get; set; }

        [DataMember]
        public string sPlanDate { get; set; }

        [DataMember]
        public string sPlanIns { get; set; }

        [DataMember]
        public int? Dwg { get; set; }

        [DataMember]
        public int? Actual { get; set; }

        [DataMember]
        public string sNc_Id { get; set; }

        [DataMember]
        public int nOther_No { get; set; }
    }

    [DataContract]
    public class iRecheckDel
    {
        [DataMember]
        public string sNc_Id { get; set; }

        [DataMember]
        public string sNcr_No { get; set; }

        [DataMember]
        public int nOther_No { get; set; }

        [DataMember]
        public string sUsers { get; set; }

        [DataMember]
        public string sImg_Before { get; set; }
    }
}

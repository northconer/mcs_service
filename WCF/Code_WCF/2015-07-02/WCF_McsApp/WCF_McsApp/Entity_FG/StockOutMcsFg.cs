﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class iSetOnTruck
    {
        [DataMember]
        public string sCrane { get; set; }

        [DataMember]
        public string sCarNo { get; set; }

        [DataMember]
        public string sBarcode { get; set; }

        [DataMember]
        public string sUserID { get; set; }
    }

    [DataContract]
    public class iSetOutCom
    {
        [DataMember]
        public string sDocNo { get; set; }

        [DataMember]
        public string sBarcode { get; set; }

        [DataMember]
        public string sUserID { get; set; }

    }

    [DataContract]
    public class iGetSummaryByDate_SO
    {
        [DataMember]
        public string sDateStart { get; set; }

        [DataMember]
        public string sDateFinish { get; set; }

        [DataMember]
        public int nTypeJob { get; set; }
    }

    [DataContract]
    public class mGetSummaryByDateStockOut
    {
        [DataMember]
        public List<GetSummaryByDateStockOut> GetSummaryByDate_SO { get; set; }

        [DataMember]
        public int TOTAL { get; set; }
    }

    [DataContract]
    public class GetSummaryByDateStockOut
    {
        [DataMember]
        public string DATE { get; set; }

        [DataMember]
        public string REQ { get; set; }

        [DataMember]
        public string REC { get; set; }

        [DataMember]
        public string REM { get; set; }
    }

    [DataContract]
    public class iGetRemindDate
    {
        [DataMember]
        public string sDate { get; set; }

        [DataMember]
        public int nTypeJob { get; set; }
    }

    [DataContract]
    public class mGetRemindByDateStockOut
    {
        [DataMember]
        public List<GetRemindByDateStockOut> GetRemindByDate_SO { get; set; }

        [DataMember]
        public int TOTAL { get; set; }
    }

    [DataContract]
    public class GetRemindByDateStockOut
    {
        [DataMember]
        public string BARCODE { get; set; }

        [DataMember]
        public string WELD { get; set; }

        [DataMember]
        public string LENGTH { get; set; }

        [DataMember]
        public string WEIGTH { get; set; }
    }

    [DataContract]
    public class mGetStockOutDaily
    {
        [DataMember]
        public List<GetStockOutDaily> GetStockOutDaily_SO { get; set; }

        [DataMember]
        public int TOTAL { get; set; }
    }

    [DataContract]
    public class GetStockOutDaily
    {
        [DataMember]
        public string DATES { get; set; }

        [DataMember]
        public string USERS { get; set; }

        [DataMember]
        public string PROJECT { get; set; }

        [DataMember]
        public string TYPE_NAME { get; set; }

        [DataMember]
        public string SHARP { get; set; }

        [DataMember]
        public string ITEM { get; set; }

        [DataMember]
        public string LENGTH { get; set; }

        [DataMember]
        public string WEIGHT { get; set; }

        [DataMember]
        public string FROM_CRANE { get; set; }

        [DataMember]
        public string FROM_PLACE { get; set; }

        [DataMember]
        public string FROM_CAR_TRUCK { get; set; }

        [DataMember]
        public string DESC_CRANE { get; set; }
    }

    [DataContract]
    public class iSetOutUpReceive
    {
        [DataMember]
        public string sCrane { get; set; }

        [DataMember]
        public string sCarTruck { get; set; }

        [DataMember]
        public string sBarcode { get; set; }

        [DataMember]
        public string sUserID { get; set; }
    }

    [DataContract]
    public class iSetOutDownReceive
    {
        [DataMember]
        public string sCrane { get; set; }

        [DataMember]
        public string sBarcodeRef { get; set; }

        [DataMember]
        public string sBarcode { get; set; }

        [DataMember]
        public string sUserID { get; set; }
    }

    [DataContract]
    public class iPrintDocOnTruckNoDo
    {
        [DataMember]
        public string sBarcode { get; set; }

        [DataMember]
        public string sCarNo { get; set; }

        [DataMember]
        public string sUser { get; set; }
    }
}

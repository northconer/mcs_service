﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class mShelfName
    {
        [DataMember]
        public string PLACE { get; set; }
    }

    [DataContract]
    public class iShelfName
    {
        [DataMember]
        public string PLACE { get; set; }
    }


    [DataContract]
    public class mProductOnShelf
    {
        [DataMember]
        public List<getProductOnShelf> GetProductOnShelf { get; set; }

        [DataMember]
        public int TOTAL { get; set; }
    }

    [DataContract]
    public class getProductOnShelf
    {
        [DataMember]
        public string NO { get; set; }

        [DataMember]
        public string BARCODE { get; set; }

        [DataMember]
        public string LENGTH { get; set; }

        [DataMember]
        public string WEIGHT { get; set; }
      
    }

    [DataContract]
    public class igetShowdataSwap
    {
        [DataMember]
        public string BARCODE { get; set; }
    }
    [DataContract]
    public class mgetShowdataSwap
    {
        [DataMember]
        public string PROJECT { get; set; }

        [DataMember]
        public string No { get; set; }

        [DataMember]
        public string TYPE { get; set; }

        [DataMember]
        public string SHARP { get; set; }

        [DataMember]
        public string SHELF { get; set; }

        [DataMember]
        public string ITEM { get; set; }
    }

    [DataContract]
    public class iupdatebarcodeSwap
    {
        [DataMember]
        public string NEWBARCODE { get; set; }

        [DataMember]
        public string OLDBARCODE { get; set; }

        [DataMember]
        public string USERID { get; set; }
    }

    [DataContract]
    public class iNumberSort
    {
        [DataMember]
        public string PLACE { get; set; }
    }

    [DataContract]
    public class iNumberUpdate
    {
        [DataMember]
        public int NUMBER { get; set; }

        [DataMember]
        public string BARCODE { get; set; }

        [DataMember]
        public string SHELF { get; set; }
    }

    [DataContract]
    public class iStockOut
    {
        [DataMember]
        public string CRANE { get; set; }

        [DataMember]
        public string CARTRUCK { get; set; }

        [DataMember]
        public string BARCODE { get; set; }

        [DataMember]
        public string USER { get; set; }
    }

    [DataContract]
    public class iStockIn
    {
        [DataMember]
        public string CRANE { get; set; }

        [DataMember]
        public string BARCODE { get; set; }

        [DataMember]
        public string USER { get; set; }

        [DataMember]
        public string SHELF { get; set; }       
    }

    [DataContract]
    public class iBarcodeSort
    {
        [DataMember]
        public string Barcode { get; set; }
    }

    public class mBarcodeSort
    {
        [DataMember]
        public string PROJECT { get; set; }

        [DataMember]
        public string TYPE_NAME { get; set; }

        [DataMember]
        public string ITEM { get; set; }

        [DataMember]
        public string LENGTH { get; set; }

        [DataMember]
        public string WEIGHT { get; set; }

        [DataMember]
        public string PLACE { get; set; }

        [DataMember]
        public string NO { get; set; }
    }
}


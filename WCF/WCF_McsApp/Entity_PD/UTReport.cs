﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;

namespace WCF_McsApp
{

    #region Request Report
    [DataContract]
    public class getRequestReport
    {
        [DataMember]
        public int pj_id { get; set; }

        [DataMember]
        public int pd_item { get; set; }

        [DataMember]
        public string item_process { get; set; }

        [DataMember]
        public string user_request { get; set; }

        [DataMember]
        public string place { get; set; }
    }

    [DataContract]
    public class setRequestReport
    {
        [DataMember]
        public string query { get; set; }
    }
    #endregion

    #region Report UT Request
    [DataContract]
    public class getReportUTRequest
    {
        [DataMember]
        public string pj_id { get; set; }

        [DataMember]
        public string pd_item { get; set; }

        [DataMember]
        public string item_process { get; set; }
    }

    [DataContract]
    public class setReportUTRequest
    {
        [DataMember]
        public string pj_id { get; set; }

        [DataMember]
        public string pj_name { get; set; }

        [DataMember]
        public string pd_item { get; set; }

        [DataMember]
        public string pd_code { get; set; }

        [DataMember]
        public string pd_qty { get; set; }

        [DataMember]
        public string pd_dwg { get; set; }

        [DataMember]
        public string pd_size { get; set; }

        [DataMember]
        public string place { get; set; }

        [DataMember]
        public string barcode { get; set; }

        [DataMember]
        public string pa_no { get; set; }

        [DataMember]
        public string item_process { get; set; }

        [DataMember]
        public string pc_name { get; set; }

        [DataMember]
        public string user_request { get; set; }

        [DataMember]
        public string request_name { get; set; }

        [DataMember]
        public string request_date { get; set; }

        [DataMember]
        public string ug_name { get; set; }

        [DataMember]
        public string ut_status { get; set; }
    }

    [DataContract]
    public class getProcessUt
    {
        [DataMember]
        public string process_id { get; set; }

        [DataMember]
        public string process_name { get; set; }
    }
    #endregion

}
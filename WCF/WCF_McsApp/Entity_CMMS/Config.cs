﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;
using System.Collections;

namespace WCF_McsApp
{
    [DataContract]
    public class SetConfig
    {
        [DataMember]
        public string config_key { get; set; }

        [DataMember]
        public string config_value { get; set; }
    }

    [DataContract]
    public class Result
    {
        [DataMember]
        public Boolean result { get; set; }

        [DataMember]
        public string exception { get; set; }
    }

    [DataContract]
    public class queryResult
    {
        [DataMember]
        public string result { get; set; }

        [DataMember]
        public string message { get; set; }
    }

    [DataContract]
    public class GetLogin
    {
        [DataMember]
        public string user_name { get; set; }

        [DataMember]
        public string user_pwd { get; set; }
    }

    [DataContract]
    public class SetLogin
    {
        [DataMember]
        public string emp_code { get; set; }
    }

    [DataContract]
    public class getEmployeeByEmpCode
    {
        [DataMember]
        public string emp_code { get; set; }
    }

    [DataContract]
    public class setEmployeeByEmpCode
    {
        [DataMember]
        public String emp_code { get; set; }

        [DataMember]
        public string prefix { get; set; }

        [DataMember]
        public string full_name { get; set; }

        [DataMember]
        public string sup_dept_code { get; set; }

        [DataMember]
        public string sup_dept_nme { get; set; }

        [DataMember]
        public string dept_code { get; set; }

        [DataMember]
        public string dept_nme { get; set; }

        [DataMember]
        public string under { get; set; }
    }

    [DataContract]
    public class emailNotification
    {
        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string date { get; set; }

        [DataMember]
        public string priority { get; set; }

        [DataMember]
        public string as_id { get; set; }

        [DataMember]
        public string as_name { get; set; }

        [DataMember]
        public string user_report { get; set; }

        [DataMember]
        public string rp_detail { get; set; }

    }

    [DataContract]
    public class GetEmailContract
    {
        [DataMember]
        public string as_type { get; set; }
    }

    //[DataContract]
    //public class SetEmailContract
    //{
    //    [DataMember]
    //    public string receiver { get; set; }

    //    [DataMember]
    //    public string cc { get; set; }
    //}

}
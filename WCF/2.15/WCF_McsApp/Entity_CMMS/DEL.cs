﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;
using System.Collections;

namespace WCF_McsApp
{
    #region DEL Request
    [DataContract]
    public class DEL_GetRequest
    {
        [DataMember]
        public string rq_id { get; set; }

        [DataMember]
        public string user_update { get; set; }
    }
    #endregion
}
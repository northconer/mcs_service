﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;
using System.Collections;

namespace WCF_McsApp
{

    #region DDL AssetType
    [DataContract]
    public class SetAssetType
    {
        [DataMember]
        public string asset_id { get; set; }

        [DataMember]
        public string asset_name { get; set; }
    }

    [DataContract]
    public class GetAssetType
    {
        [DataMember]
        public int asset_id { get; set; }
    }
    #endregion

    #region DDL AssetStatus
    [DataContract]
    public class GetAssetStatus
    {
        [DataMember]
        public int asset_id { get; set; }
    }

    [DataContract]
    public class SetAssetStatus
    {
        [DataMember]
        public string asset_id { get; set; }

        [DataMember]
        public string asset_name { get; set; }
    }
    #endregion

    #region DDL Evaluate
    [DataContract]
    public class GetEvaluate
    {
        [DataMember]
        public int evaluate_id { get; set; }
    }

    [DataContract]
    public class SetEvaluate
    {
        [DataMember]
        public string evaluate_id { get; set; }

        [DataMember]
        public string evaluate_name { get; set; }
    }
    #endregion

    #region DDL Priority
    [DataContract]
    public class GetPriority
    {
        [DataMember]
        public int priority_id { get; set; }
    }

    [DataContract]
    public class SetPriority
    {
        [DataMember]
        public string priority_id { get; set; }

        [DataMember]
        public string priority_name { get; set; }
    }
    #endregion

    #region DDL RepairCause
    [DataContract]
    public class DDL_GetRepairCause
    {
        [DataMember]
        public int all { get; set; }
    }

    [DataContract]
    public class DDL_SetRepairCause
    {
        [DataMember]
        public int cause_id { get; set; }

        [DataMember]
        public string cause_name { get; set; }
    }
    #endregion

    #region DDL RepairStatus
    [DataContract]
    public class GetRepairStatus
    {
        [DataMember]
        public int repairStatus_id { get; set; }
    }

    [DataContract]
    public class SetRepairStatus
    {
        [DataMember]
        public string repairStatus_id { get; set; }

        [DataMember]
        public string repairStatus_name { get; set; }
    }
    #endregion

    #region DDL Team
    [DataContract]
    public class GetTeam
    {
        [DataMember]
        public int team_id { get; set; }
    }

    [DataContract]
    public class SetTeam
    {
        [DataMember]
        public string team_id { get; set; }

        [DataMember]
        public string team_name { get; set; }
    }
    #endregion

    #region DDL WorkClass
    [DataContract]
    public class GetWorkClass
    {
        [DataMember]
        public int workClass_id { get; set; }
    }

    [DataContract]
    public class SetWorkClass
    {
        [DataMember]
        public string workClass_id { get; set; }

        [DataMember]
        public string workClass_name { get; set; }
    }
    #endregion

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Collections;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class WeldGetProcess
    {
        [DataMember]
        public List<GetProcessWeld> getProcessWeld { get; set; }
    }

    [DataContract]
    public class GetProcessWeld
    {
        [DataMember]
        public int PC_ID { get; set; }

        [DataMember]
        public string PC_NAME { get; set; }
    }

    [DataContract]
    public class WeldGetProject
    {
        [DataMember]
        public int PJ_ID { get; set; }

        [DataMember]
        public Int16 PD_ITEM { get; set; }

        [DataMember]
        public string PJ_NAME { get; set; }

        [DataMember]
        public string PT_NAME { get; set; }

        [DataMember]
        public string UG_NAME { get; set; }

        [DataMember]
        public string PLANDATE { get; set; }

        [DataMember]
        public int PC_ID { get; set; }

        [DataMember]
        public string PC_NAME { get; set; }

        [DataMember]
        public int WELD_PLAN { get; set; }

        [DataMember]
        public int WELD_ACTUAL { get; set; }
    }


    [DataContract]
    public class WeldGetGroupUser
    {
        [DataMember]
        public string US_ID { get; set; }

        [DataMember]
        public string FULLNAME { get; set; }
    }

}


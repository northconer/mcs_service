﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class iProject
    {
        [DataMember]
        public int nTypePc { get; set; }
    }

    [DataContract]
    public class mProject
    {
        [DataMember]
        public string PJ_NAME { get; set; }

        [DataMember]
        public string PJ_ID { get; set; }
    }

    [DataContract]
    public class iItemPj
    {
        [DataMember]
        public int nTypePc { get; set; }

        [DataMember]
        public int nPj_Id { get; set; }
    }

    [DataContract]
    public class mItemPj
    {
        [DataMember]
        public string PD_ITEM { get; set; }
    }

    [DataContract]
    public class iRevNoPlanCheck
    {
        [DataMember]
        public int nTypePc { get; set; }

        [DataMember]
        public int nPj_Id { get; set; }

        [DataMember]
        public int nPd_Item { get; set; }
    }

    [DataContract]
    public class mRevNoPlanCheck
    {
        [DataMember]
        public int REV_NO { get; set; }

        [DataMember]
        public string REV_NAME { get; set; }
    }

    [DataContract]
    public class mGroupFab
    {
        [DataMember]
        public string F_GRP { get; set; }
    }

    [DataContract]
    public class mGroupWeld
    {
        [DataMember]
        public string W_GRP { get; set; }
    }

    [DataContract]
    public class mStatus
    {
        [DataMember]
        public int STATUS_ID { get; set; }

        [DataMember]
        public string STATUS_NAME { get; set; }
    }

    //List Detail Plan Check

    [DataContract]
    public class iPjShowListCheck
    {
        [DataMember]
        public int nTypePc { get; set; }

        [DataMember]
        public int nPj_Id { get; set; }

        [DataMember]
        public int nPd_Item { get; set; }

        [DataMember]
        public string sRev_No { get; set; }

        [DataMember]
        public string sCode { get; set; }

        [DataMember]
        public string sGroupFab { get; set; }

        [DataMember]
        public string sGroupWeld { get; set; }

        [DataMember]
        public string sFab_Date { get; set; }

        [DataMember]
        public string sWeld_Date { get; set; }

        [DataMember]
        public string sNc_Status { get; set; }
    }

    [DataContract]
    public class mPjShowListCheck
    {
        [DataMember]
        public string PJ_NAME { get; set; }

        [DataMember]
        public string PD_ITEM { get; set; }

        [DataMember]
        public string FW_REV { get; set; }

        [DataMember]
        public string PD_CODE { get; set; }

        [DataMember]
        public string PART { get; set; }

        [DataMember]
        public string F_GRP { get; set; }

        [DataMember]
        public string F_DATE { get; set; }

        [DataMember]
        public string W_GRP { get; set; }

        [DataMember]
        public string W_DATE { get; set; }

        [DataMember]
        public string OA_GRP { get; set; }

        [DataMember]
        public string OA_DATE { get; set; }

        [DataMember]
        public string CHECK_NO { get; set; }

        [DataMember]
        public string COUNT_NCR { get; set; }

        [DataMember]
        public string DESIGN_CHANGE { get; set; }

        [DataMember]
        public string NCR_STATUS { get; set; }

        [DataMember]
        public string USERS { get; set; }

        [DataMember]
        public string NCR_UPDATE { get; set; }
    }
}

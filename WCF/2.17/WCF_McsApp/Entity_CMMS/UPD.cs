﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;
using System.Collections;

namespace WCF_McsApp
{

    #region UPD DocumentByRpId
    [DataContract]
    public class UPD_DocumentByRpId
    {
        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string as_id { get; set; }

        [DataMember]
        public string user_update { get; set; }
    }
    #endregion

    #region UPD DocumentByUploadImgFail
    [DataContract]
    public class UPD_DocumentByUploadImgFail
    {
        [DataMember]
        public string doc_id { get; set; }
    }
    #endregion

    #region UPD Repair
    [DataContract]
    public class UPD_GetRepair
    {
        [DataMember]
        public int priority { get; set; }

        [DataMember]
        public string user_rp { get; set; }

        [DataMember]
        public string request_date { get; set; }

        [DataMember]
        public string wc_id { get; set; }

        [DataMember]
        public string rp_detial { get; set; }

        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string as_id { get; set; }
    }
    #endregion

    #region UPD RepairByAdminCancel
    [DataContract]
    public class UPD_GetRepairByAdminCancel
    {
        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string user_update { get; set; }

        [DataMember]
        public string remark { get; set; }
    }
    #endregion

    #region UPD RepairByAssign
    [DataContract]
    public class UPD_GetRepairByAssign
    {
        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string team_id { get; set; }

        [DataMember]
        public string assign_to { get; set; }

        [DataMember]
        public string user_update { get; set; }

        [DataMember]
        public string remark { get; set; }
    }
    #endregion

    #region UPD RepairByAssignNone
    [DataContract]
    public class UPD_GetRepairByAssignNone
    {
        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string user_update { get; set; }

        [DataMember]
        public string remark { get; set; }
    }
    #endregion

    #region UPD RepairByEvaluate
    [DataContract]
    public class UPD_GetRepairByEvaluate
    {
        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string user_commit { get; set; }

        [DataMember]
        public string user_comment { get; set; }

        [DataMember]
        public int rate { get; set; }
    }
    #endregion

    #region UPD RepairByOperatorOpen
    [DataContract]
    public class UPD_GetRepairByOperatorOpen
    {
        [DataMember]
        public string rp_id { get; set; }
    }
    #endregion

    #region UPD RepairByOperatorRequest
    [DataContract]
    public class UPD_GetRepairByOperatorRequest
    {
        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string user_update { get; set; }
    }
    #endregion

    #region UPD RepairByOperatorSent
    [DataContract]
    public class UPD_GetRepairByOperatorSent
    {
        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string operator_sent { get; set; }

        [DataMember]
        public string symptom { get; set; }

        [DataMember]
        public string start_date { get; set; }

        [DataMember]
        public string finish_date { get; set; }

        [DataMember]
        public int cause_id { get; set; }

        [DataMember]
        public string cause_comment { get; set; }

        [DataMember]
        public string protection { get; set; }
    }
    #endregion

    #region UPD RepairByUploadFailed
    [DataContract]
    public class UPD_GetRepairByUploadFailed
    {
        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string exception { get; set; }
    }
    #endregion

    #region UPD RepairByUserCancel
    [DataContract]
    public class UPD_GetRepairByUserCancel
    {
        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string user_update { get; set; }
    }
    #endregion

}
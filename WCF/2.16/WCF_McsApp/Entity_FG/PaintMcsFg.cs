﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class iPaintDataShowBarcode
    {
        [DataMember]
        public int pj_id { get; set; }

        [DataMember]
        public int pd_item { get; set; }
    }
    //"Project", "Item", "Type", "Code", "Dwg", "Inspection", "Seq", "subSeq", "Line", "Paint", "Date_Finish", "Status", "Contractor"
    
    [DataContract]
    public class mPaintDataShow
    {
        [DataMember]
        public string  PROJECT { get; set; }

        [DataMember]
        public string CODE { get; set; }
        
        [DataMember]
        public string DWG { get; set; }

        [DataMember]
        public string TYPE { get; set; }

        [DataMember]
        public string ITEM { get; set; }

        [DataMember]
        public string PAINT { get; set; }

        [DataMember]
        public string SEND_IN { get; set; }

        [DataMember]
        public string STATUS { get; set; }

        [DataMember]
        public string CONTRACTOR { get; set; }

        [DataMember]
        public string SEQ { get; set; }

        [DataMember]
        public string SUB_SEQ { get; set; }

        [DataMember]
        public string LINE { get; set; }

        [DataMember]
        public string INSPECTION { get; set; }

        [DataMember]
        public string DATE_FINISH { get; set; }
    }

    [DataContract]
    public class iPaintDataReport
    {
        [DataMember]
        public string PJ_ID { get; set; }

        [DataMember]
        public string PJ_ITEM { get; set; }

    }
    [DataContract]
        public class mPaintDataReport
    {
        [DataMember]
        public string PROJECT { get; set; }

        [DataMember]
        public string ITEM { get; set; }

        [DataMember]
        public string TYPE { get; set; }

        [DataMember]
        public string CODE { get; set; }

        [DataMember]
        public string PAINT { get; set; }

        [DataMember]
        public string STATUS { get; set; }

        [DataMember]
        public string CONTRACTOR { get; set; }

        [DataMember]
        public string DATE_FINISH { get; set; }

    }

  
    
    [DataContract]
    public class mProjectPaint
    {
        [DataMember]
        public string PJ_ID { get; set; }

        [DataMember]
        public string PJ_NAME { get; set; }
    }

    [DataContract]
    public class igetItem
    {
        [DataMember]
        public int PJ_ID { get; set; }
    }
    [DataContract]
    public class mItemPaint
    {
        [DataMember]
        public int item { get; set; }
    }

 

    [DataContract]
    public class iInsertPaint
    {
        [DataMember]
        public int PJ_ID   { get; set; }
        [DataMember]
        public int PD_ITEM { get; set; }
        [DataMember]
        public string US_ID   { get; set; }

    }

}
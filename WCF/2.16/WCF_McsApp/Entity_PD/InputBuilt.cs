﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class InputBuiltProject
    {
        [DataMember]
        public string Users { get; set; }

        [DataMember]
        public int nPJ_ID { get; set; }

        [DataMember]
        public int nPd_Item { get; set; }
    }


    [DataContract]
    public class InputCheckBuilt
    {
        [DataMember]
        public int nPJ_ID { get; set; }

        [DataMember]
        public int nPd_Item { get; set; }

        [DataMember]
        public int nItem_no { get; set; }

        [DataMember]
        public int nBuilt_Type { get; set; }

        [DataMember]
        public int nProcess { get; set; }

    }

    [DataContract]
    public class InputBuiltActualSend
    {
        [DataMember]
        public int nPJ_ID { get; set; }

        [DataMember]
        public int nPd_Item { get; set; }

        [DataMember]
        public int nItem_no { get; set; }

        [DataMember]
        public int nBuilt_Type { get; set; }

        [DataMember]
        public int nProcess { get; set; }

        [DataMember]
        public string sUser_work { get; set; }

        [DataMember]
        public string sPlace { get; set; }

        [DataMember]
        public string sUser_id { get; set; }

        [DataMember]
        public int nResult { get; set; }
    }

    [DataContract]
    public class InputBarcode
    {
        [DataMember]
        public int nPJ_ID { get; set; }

        [DataMember]
        public int nPd_Item { get; set; }
    }

    [DataContract]
    public class InputBarcodeType
    {
        [DataMember]
        public int nPJ_ID { get; set; }

        [DataMember]
        public int nPd_Item { get; set; }

        [DataMember]
        public int Type { get; set; }
    }

    #region CheckLocationBarcode
    [DataContract]
    public class GetCheckLocationBarcode
    {
        [DataMember]
        public string barcode { get; set; }
    }
    #endregion

    #region CheckUserGroupBuilt
    [DataContract]
    public class GetCheckUserGroupBuilt
    {
        [DataMember]
        public string ugName { get; set; }

        [DataMember]
        public string userId { get; set; }

    }
    #endregion
}

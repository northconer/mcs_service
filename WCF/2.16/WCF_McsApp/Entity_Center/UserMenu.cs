﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WCF_McsApp
{

    [DataContract]
    public class LogApplication
    {
        [DataMember]
        public int mn_id { get; set; }

        [DataMember]
        public string log_user { get; set; }

        [DataMember]
        public string log_local_ip { get; set; }

        [DataMember]
        public string log_remote_ip { get; set; }

        [DataMember]
        public string log_device_emei { get; set; }

        [DataMember]
        public string log_device_brand { get; set; }

        [DataMember]
        public string log_device_model { get; set; }

        [DataMember]
        public string log_device_version { get; set; }

        [DataMember]
        public string log_device_mac { get; set; }

        [DataMember]
        public string log_device_number { get; set; }
    }

    [DataContract]
    public class UserMenu
    {
        [DataMember]
        public int mn_id { get; set; }

        [DataMember]
        public string mn_file { get; set; }
    }

    [DataContract]
    public class Newupdate
    {
        [DataMember]
        public string version { get; set; }
    }
}
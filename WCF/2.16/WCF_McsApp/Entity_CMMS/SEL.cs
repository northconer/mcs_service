﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;
using System.Collections;

namespace WCF_McsApp
{

    #region SEL Asset
    [DataContract]
    public class SEL_GetAsset
    {
        [DataMember]
        public string as_id { get; set; }

        [DataMember]
        public string as_name { get; set; }

        [DataMember]
        public string as_type_id { get; set; }

        [DataMember]
        public string location_id { get; set; }

        [DataMember]
        public string division_code { get; set; }

        [DataMember]
        public string ven_id { get; set; }
    }

    [DataContract]
    public class SEL_SetAsset
    {
        [DataMember]
        public string as_id { get; set; }

        [DataMember]
        public string as_name_eng { get; set; }

        [DataMember]
        public string as_name_thai { get; set; }

        [DataMember]
        public string price { get; set; }

        [DataMember]
        public string ac_code { get; set; }

        [DataMember]
        public string as_type_id { get; set; }

        [DataMember]
        public string as_type_name { get; set; }

        [DataMember]
        public string location_id { get; set; }

        [DataMember]
        public string location_detail { get; set; }

        [DataMember]
        public string division_code { get; set; }

        [DataMember]
        public string emp_code { get; set; }

        [DataMember]
        public string full_name { get; set; }

        [DataMember]
        public string manufacturer { get; set; }

        [DataMember]
        public string sn { get; set; }

        [DataMember]
        public string model { get; set; }

        [DataMember]
        public string wi { get; set; }

        [DataMember]
        public string img { get; set; }

        [DataMember]
        public string as_detail { get; set; }

        [DataMember]
        public string po_number { get; set; }

        [DataMember]
        public string po_date { get; set; }

        [DataMember]
        public string delivery_date { get; set; }

        [DataMember]
        public string install_date { get; set; }

        [DataMember]
        public string expire_date { get; set; }

        [DataMember]
        public string lot_number { get; set; }

        [DataMember]
        public string ven_id { get; set; }

        [DataMember]
        public string ven_name { get; set; }

        //[DataMember]
        //public string ven_lot_number { get; set; }

        [DataMember]
        public string ven_sn { get; set; }

        [DataMember]
        public string ven_expire_date { get; set; }

        [DataMember]
        public string location_ac { get; set; }

        [DataMember]
        public string admin_update { get; set; }

        [DataMember]
        public string time_update { get; set; }

        [DataMember]
        public string as_status_name { get; set; }
    }
    #endregion

    #region SEL AssetByType
    [DataContract]
    public class SEL_GetAssetByType
    {
        [DataMember]
        public string as_type_id { get; set; }

        [DataMember]
        public string search { get; set; }
    }

    [DataContract]
    public class SEL_SetAssetByType
    {
        [DataMember]
        public string as_id { get; set; }

        [DataMember]
        public string as_name_eng { get; set; }

        [DataMember]
        public string as_name_thai { get; set; }

        [DataMember]
        public string as_type_id { get; set; }

        [DataMember]
        public string as_type_name { get; set; }

        [DataMember]
        public string location_id { get; set; }

        [DataMember]
        public string location_detail { get; set; }

        [DataMember]
        public string img { get; set; }

        [DataMember]
        public string as_status_name { get; set; }
    }
    #endregion

    #region SEL AssetType
    [DataContract]
    public class SEL_GetAssetType
    {
        [DataMember]
        public string as_type_id { get; set; }

        [DataMember]
        public string as_type_name { get; set; }
    }

    [DataContract]
    public class SEL_SetAssetType
    {
        [DataMember]
        public string as_type_id { get; set; }

        [DataMember]
        public string as_type_name { get; set; }
    }
    #endregion

    #region SEL Component
    [DataContract]
    public class SEL_GetComponent
    {
        [DataMember]
        public string cp_item { get; set; }

        [DataMember]
        public string cp_item_name { get; set; }

        [DataMember]
        public string cp_group { get; set; }

        [DataMember]
        public string cp_type { get; set; }

        [DataMember]
        public string ven_id { get; set; }

        [DataMember]
        public string location_id { get; set; }

        [DataMember]
        public string is_active { get; set; }
    }

    [DataContract]
    public class SEL_SetComponent
    {
        [DataMember]
        public string cp_item { get; set; }

        [DataMember]
        public string cp_item_name { get; set; }

        [DataMember]
        public string cp_type_id { get; set; }

        [DataMember]
        public string cp_type_name { get; set; }

        [DataMember]
        public string cp_gp_id { get; set; }

        [DataMember]
        public string cp_gp_name { get; set; }

        [DataMember]
        public string as_type_id { get; set; }

        [DataMember]
        public string as_type_name { get; set; }

        [DataMember]
        public string brand { get; set; }

        [DataMember]
        public string model { get; set; }

        [DataMember]
        public string size { get; set; }

        [DataMember]
        public string price_unit { get; set; }

        [DataMember]
        public string unit_type { get; set; }

        [DataMember]
        public string unit_type_name { get; set; }

        [DataMember]
        public string total_qty { get; set; }

        [DataMember]
        public string used_qty { get; set; }

        [DataMember]
        public string remain_qty { get; set; }

        [DataMember]
        public string po_number { get; set; }

        [DataMember]
        public string po_date { get; set; }

        [DataMember]
        public string delivery_date { get; set; }

        [DataMember]
        public string expire_date { get; set; }

        [DataMember]
        public string ven_id { get; set; }

        [DataMember]
        public string img { get; set; }

        [DataMember]
        public string detail { get; set; }

        [DataMember]
        public string is_active { get; set; }

        [DataMember]
        public string admin_update { get; set; }

        [DataMember]
        public string time_update { get; set; }

        [DataMember]
        public string location_ac_code { get; set; }

        [DataMember]
        public string location_id { get; set; }

        [DataMember]
        public string ias_active_desc { get; set; }
    }
    #endregion

    #region SEL Location
    [DataContract]
    public class SEL_GetLocation
    {
        [DataMember]
        public string location_id { get; set; }

        [DataMember]
        public string location_ac_code { get; set; }

        [DataMember]
        public string location_detail { get; set; }
    }

    [DataContract]
    public class SEL_SetLocation
    {
        [DataMember]
        public string location_id { get; set; }

        [DataMember]
        public string location_ac_code { get; set; }

        [DataMember]
        public string location_detail { get; set; }
    }
    #endregion

    #region SEL OperatorById
    [DataContract]
    public class SEL_GetOperatorById
    {
        [DataMember]
        public string team_id { get; set; }

        [DataMember]
        public string search { get; set; }
    }

    [DataContract]
    public class SEL_SetOperatorById
    {
        [DataMember]
        public string emp_code { get; set; }

        [DataMember]
        public string full_name { get; set; }

        [DataMember]
        public string emp_pic { get; set; }
    }
    #endregion

    #region SEL Repair
    [DataContract]
    public class SEL_SetRepair
    {
        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string as_id { get; set; }

        [DataMember]
        public string priority { get; set; }

        [DataMember]
        public string priority_desc { get; set; }

        [DataMember]
        public string user_rp { get; set; }

        [DataMember]
        public string full_name { get; set; }

        [DataMember]
        public string rp_date { get; set; }

        [DataMember]
        public string wc_id { get; set; }

        [DataMember]
        public string rp_detail { get; set; }

        [DataMember]
        public string rp_status_id { get; set; }

        [DataMember]
        public string rp_status_name { get; set; }
    }
    #endregion

    #region SEL RepairByAdd
    [DataContract]
    public class SEL_GetRepairByAdd
    {
        [DataMember]
        public string start_date { get; set; }

        [DataMember]
        public string end_date { get; set; }

        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public int rp_status_id { get; set; }

        [DataMember]
        public string user_login { get; set; }
    }

    [DataContract]
    public class SEL_SetRepairByAdd
    {
        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string as_id { get; set; }

        [DataMember]
        public string as_name_eng { get; set; }

        [DataMember]
        public int priority { get; set; }

        [DataMember]
        public string priority_desc { get; set; }

        [DataMember]
        public string user_rp { get; set; }

        [DataMember]
        public string full_name { get; set; }

        [DataMember]
        public string rp_date { get; set; }

        [DataMember]
        public string wc_id { get; set; }

        [DataMember]
        public string rp_detail { get; set; }

        [DataMember]
        public int rp_status_id { get; set; }

        [DataMember]
        public string rp_status_name { get; set; }

        [DataMember]
        public string assign_to { get; set; }

        [DataMember]
        public string sent_date { get; set; }
    }
    #endregion

    #region SEL RepairById
    [DataContract]
    public class SEL_GetRepairById
    {
        [DataMember]
        public string rp_id { get; set; }
    }

    [DataContract]
    public class SEL_SetRepairById
    {
        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string as_type_id { get; set; }

        [DataMember]
        public string as_type_name { get; set; }

        [DataMember]
        public string as_id { get; set; }

        [DataMember]
        public string as_name_thai { get; set; }

        [DataMember]
        public int priority { get; set; }

        [DataMember]
        public string priority_desc { get; set; }

        [DataMember]
        public string user_rp { get; set; }

        [DataMember]
        public string full_name { get; set; }

        [DataMember]
        public string rp_date { get; set; }

        [DataMember]
        public string request_date { get; set; }

        [DataMember]
        public string wc_id { get; set; }

        [DataMember]
        public string wc_name { get; set; }

        [DataMember]
        public string rp_detail { get; set; }

        [DataMember]
        public int rp_status_id { get; set; }

        [DataMember]
        public string rp_status_name { get; set; }

        [DataMember]
        public string team_id { get; set; }

        [DataMember]
        public string assign_to { get; set; }

        [DataMember]
        public string assign_date { get; set; }

        [DataMember]
        public string assign_name { get; set; }

        [DataMember]
        public string start_date { get; set; }

        [DataMember]
        public string finish_date { get; set; }

        [DataMember]
        public string operator_sent { get; set; }

        [DataMember]
        public string operator_name { get; set; }

        [DataMember]
        public string sent_date { get; set; }

        [DataMember]
        public string user_commit { get; set; }

        [DataMember]
        public string user_commit_name { get; set; }

        [DataMember]
        public string commit_date { get; set; }

        [DataMember]
        public string user_comment { get; set; }

        [DataMember]
        public string rate { get; set; }

        [DataMember]
        public string rate_desc { get; set; }

        [DataMember]
        public string as_status_desc { get; set; }

        [DataMember]
        public string cause_id { get; set; }

        [DataMember]
        public string cause_desc { get; set; }

        [DataMember]
        public string cause_comment { get; set; }

        [DataMember]
        public string protection { get; set; }

        [DataMember]
        public string location_id { get; set; }

        [DataMember]
        public string location_detail { get; set; }

    }
    #endregion

    #region SEL RepairByUserForAssign
    [DataContract]
    public class SEL_GetRepairByUserForAssign
    {
        [DataMember]
        public string start_date { get; set; }

        [DataMember]
        public string end_date { get; set; }

        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public int rp_status_id { get; set; }

    }

    [DataContract]
    public class SEL_SetRepairByUserForAssign
    {
        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string as_id { get; set; }

        [DataMember]
        public string as_name_eng { get; set; }

        [DataMember]
        public int priority { get; set; }

        [DataMember]
        public string priority_desc { get; set; }

        [DataMember]
        public string user_rp { get; set; }

        [DataMember]
        public string full_name { get; set; }

        [DataMember]
        public string rp_date { get; set; }

        [DataMember]
        public string wc_id { get; set; }

        [DataMember]
        public string rp_detail { get; set; }

        [DataMember]
        public int rp_status_id { get; set; }

        [DataMember]
        public string rp_status_name { get; set; }

        [DataMember]
        public string assign_to { get; set; }

        [DataMember]
        public string sent_date { get; set; }

    }
    #endregion

    #region SEL RepairByUserForEvaluate
    [DataContract]
    public class SEL_GetRepairByUserForEvaluate
    {
        [DataMember]
        public string start_date { get; set; }

        [DataMember]
        public string end_date { get; set; }

        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string user_login { get; set; }
    }

    [DataContract]
    public class SEL_SetRepairByUserForEvaluate
    {
        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string as_id { get; set; }

        [DataMember]
        public string as_name_eng { get; set; }

        [DataMember]
        public int priority { get; set; }

        [DataMember]
        public string priority_desc { get; set; }

        [DataMember]
        public string user_rp { get; set; }

        [DataMember]
        public string full_name { get; set; }

        [DataMember]
        public string rp_date { get; set; }

        [DataMember]
        public string wc_id { get; set; }

        [DataMember]
        public string rp_detail { get; set; }

        [DataMember]
        public int rp_status_id { get; set; }

        [DataMember]
        public string rp_status_name { get; set; }

        [DataMember]
        public string assign_to { get; set; }

        [DataMember]
        public string sent_date { get; set; }
    }
    #endregion

    #region SEL RepairByWorkList
    [DataContract]
    public class SEL_GetRepairByWorkList
    {
        [DataMember]
        public string start_date { get; set; }

        [DataMember]
        public string end_date { get; set; }

        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public int rp_status_id { get; set; }

        [DataMember]
        public string user_login { get; set; }
    }

    [DataContract]
    public class SEL_SetRepairByWorkList
    {
        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string as_id { get; set; }

        [DataMember]
        public string as_name_eng { get; set; }

        [DataMember]
        public string location_id { get; set; }

        [DataMember]
        public int priority { get; set; }

        [DataMember]
        public string priority_desc { get; set; }

        [DataMember]
        public string user_rp { get; set; }

        [DataMember]
        public string full_name { get; set; }

        [DataMember]
        public string rp_date { get; set; }

        [DataMember]
        public string wc_id { get; set; }

        [DataMember]
        public string rp_detail { get; set; }

        [DataMember]
        public int rp_status_id { get; set; }

        [DataMember]
        public string rp_status_name { get; set; }

        [DataMember]
        public string assign_to { get; set; }

        [DataMember]
        public string sent_date { get; set; }
    }
    #endregion

    #region SEL RepairWorkListByRpId
    [DataContract]
    public class SEL_GetRepairWorkListByRpId
    {
        [DataMember]
        public string start_date { get; set; }

        [DataMember]
        public string end_date { get; set; }

        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string rp_status_id { get; set; }

        [DataMember]
        public string user_login { get; set; }
    }
    [DataContract]
    public class SEL_SetRepairWorkListByRpId
    {
        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string as_id { get; set; }

        [DataMember]
        public string as_name_eng { get; set; }

        [DataMember]
        public string location_id { get; set; }

        [DataMember]
        public int priority { get; set; }

        [DataMember]
        public string priority_desc { get; set; }

        [DataMember]
        public string user_rp { get; set; }

        [DataMember]
        public string full_name { get; set; }

        [DataMember]
        public string rp_date { get; set; }

        [DataMember]
        public string wc_id { get; set; }

        [DataMember]
        public string rp_detail { get; set; }

        [DataMember]
        public int rp_status_id { get; set; }

        [DataMember]
        public string rp_status_name { get; set; }

        [DataMember]
        public string assign_to { get; set; }

        [DataMember]
        public string sent_date { get; set; }
    }
    #endregion

    #region SEL RequestByRepair
    [DataContract]
    public class SEL_GetRequestByRepair
    {
        [DataMember]
        public string rp_id { get; set; }
    }

    [DataContract]
    public class SEL_SetRequestByRepair
    {
        [DataMember]
        public string cp_item { get; set; }

        [DataMember]
        public string cp_item_name { get; set; }

        [DataMember]
        public int issue { get; set; }

        [DataMember]
        public string user_create { get; set; }

        [DataMember]
        public string full_name { get; set; }

        [DataMember]
        public string create_date { get; set; }

        [DataMember]
        public string rq_id { get; set; }
    }
    #endregion

}
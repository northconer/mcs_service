﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data;
using System.Web.DataAccess;
using System.Data.SqlClient;
using System.Collections;
using System.Web.Script.Serialization;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using HttpMultipartParser;
using System.Web.Configuration;
using System.Net.Mail;
using System.Data.Linq.SqlClient;
using System.Runtime.InteropServices;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace WCF_McsApp
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class Service1 : IServices
    {
        string ConnMcsApp = ConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString;
        string ConnMcsQc = ConfigurationManager.ConnectionStrings["McsQCConnectionString"].ConnectionString;
        string ConnMcsHrMs = ConfigurationManager.ConnectionStrings["McsHrMsConnectionString"].ConnectionString;

        string ConnMcsAppCenter =ConfigurationManager.ConnectionStrings["McsAppCenterConnectionString"].ConnectionString;

        string ConnMcsPdFg = ConfigurationManager.ConnectionStrings["McsFG2010ConnectionString"].ConnectionString;

        #region LOG

        public void LogLogin(LogApplication input)
        {
            //object[] obj = { 4, input.mn_id, input.log_user, input.log_local_ip, input.log_remote_ip, input.log_device_emei, input.log_device_brand, input.log_device_model, input.log_device_version, input.log_device_mac, input.log_device_number };
            //var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsAppCenter, "SP_INS_LOG_APPLICATION", obj);
            try
            {
                using (
                    SqlConnection connect =
                        new SqlConnection(
                            WebConfigurationManager.ConnectionStrings["McsAppCenterConnectionString"].ConnectionString))
                {
                    connect.Open();

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = connect;
                        cmd.CommandText = "SP_INS_LOG_APPLICATION";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@UA_ID", SqlDbType.Int).Value = 3;
                        cmd.Parameters.Add("@MN_ID", SqlDbType.Int).Value = input.mn_id;
                        cmd.Parameters.Add("@LOG_USER", SqlDbType.VarChar, 7).Value = input.log_user;
                        cmd.Parameters.Add("@LOG_LOCAL_IP", SqlDbType.VarChar, 15).Value = input.log_local_ip;
                        cmd.Parameters.Add("@LOG_REMOTE_IP", SqlDbType.VarChar, 15).Value = input.log_remote_ip;
                        cmd.Parameters.Add("@LOG_DEVICE_EMEI", SqlDbType.VarChar, 50).Value = input.log_device_emei;
                        cmd.Parameters.Add("@LOG_DEVICE_BRAND", SqlDbType.VarChar, 50).Value = input.log_device_brand;
                        cmd.Parameters.Add("@LOG_DEVICE_MODEL", SqlDbType.VarChar, 50).Value = input.log_device_model;
                        cmd.Parameters.Add("@LOG_DEVICE_VERSION", SqlDbType.VarChar, 50).Value =
                            input.log_device_version;
                        cmd.Parameters.Add("@LOG_DEVICE_MAC", SqlDbType.VarChar, 50).Value = input.log_device_mac;
                        cmd.Parameters.Add("@LOG_DEVICE_NUMBER", SqlDbType.VarChar, 50).Value =
                            (input.log_device_number == null) ? "" : input.log_device_number;
                        cmd.ExecuteNonQuery();
                    }

                    connect.Close();

                }
            }
            catch (Exception)
            {

            }
        }

        public List<UserMenu> GetUserMenu()
        {
            mcsappcenterDataContext context = new mcsappcenterDataContext();
            List<UserMenu> results = new List<UserMenu>();

            foreach (USER_MENU item in context.USER_MENUs.Where(s => s.MN_URL.Equals("Android")))
            {
                UserMenu result = new UserMenu();

                result.mn_id = (int) (item.MN_ID);
                result.mn_file = item.MN_FILE_NAME;

                results.Add(result);
            }

            return results;
        }

        public mResult GetNewupdate(Newupdate input)
        {
            mResult result=new mResult();
            result.RESULT = "**เพิ่มเมนูระบบขนส่ง**";
            result.EXCEPTION = "";
            return result;
        }

        #endregion

        #region PD

        #region เก็บค่า config และ log ผู้ใช้งานเข้าสู่ระบบ McsPdPc

        public List<mConfig> Getconfig()
        {
            McsAppDataContext dc = new McsAppDataContext();

            List<mConfig> results = new List<mConfig>();

            foreach (PDPC_config item in dc.PDPC_configs.OrderBy(s => s.config_key))
            {
                results.Add(new mConfig
                {
                    CONFIG_KEY = item.config_key,
                    CONFIG_VALUE = item.config_value
                });
            }
            return results;
        }

        public mResult CreateDevice(SetDevice input)
        {
            mResult result = new mResult();
            DateTime now = DateTime.Now;

            try
            {
                McsAppDataContext dc = new McsAppDataContext();

                PDPC_device icheck = dc.PDPC_devices.Where(s => s.device_id == input.Device_id).FirstOrDefault();

                if (icheck == null)
                {
                    PDPC_device device = new PDPC_device()
                    {
                        device_id = new string(input.Device_id.Take(20).ToArray()),
                        device_brand = new string(input.Device_brand.ToUpper().Take(10).ToArray()),
                        device_model = new string(input.Device_model.ToUpper().Take(10).ToArray()),
                        device_version = new string(input.Device_version.Take(20).ToArray()),
                        device_imei = new string(input.Device_imei.Take(20).ToArray()),
                        device_time = (DateTime) (now),
                        device_status = input.Device_status,
                        device_track = input.Device_track,
                        device_ip = new string(input.Device_ip.Take(20).ToArray())
                    };

                    dc.PDPC_devices.InsertOnSubmit(device);
                }
                else
                {
                    var cust = (from c in dc.PDPC_devices where c.device_id == input.Device_id select c).First();
                    cust.device_brand = input.Device_brand;
                    cust.device_model = input.Device_model;
                    cust.device_version = input.Device_version;
                    cust.device_imei = input.Device_imei;
                    cust.device_time = (DateTime) (now);
                    cust.device_status = input.Device_status;
                    cust.device_track = input.Device_track;
                    cust.device_ip = input.Device_ip;

                    //icheck.device_id = new string(input.Device_id.Take(20).ToArray());
                    //icheck.device_brand = new string(input.Device_brand.ToUpper().Take(10).ToArray());
                    //icheck.device_model = new string(input.Device_model.ToUpper().Take(10).ToArray());
                    //icheck.device_version = new string(input.Device_version.Take(20).ToArray());
                    //icheck.device_imei = new string(input.Device_imei.Take(20).ToArray());
                    //icheck.device_time = (DateTime)(now);
                    //icheck.device_status = input.Device_status;
                    //icheck.device_track = new string(input.Device_track.Take(20).ToArray());
                    //icheck.device_ip = new string(input.Device_ip.Take(20).ToArray());
                }

                dc.SubmitChanges();

                result.RESULT = "true";
                result.EXCEPTION = "";

                return result;
            }
            catch (Exception ex)
            {
                result.RESULT = "false";
                result.EXCEPTION = ex.Message;

                return result;
            }
        }

        #endregion

        #region Login ป้อน User and Password

        public ResultUser CheckUsernamePassword(InputLogin input)
        {
            ResultUser UserInfo = new ResultUser();

            string check = "";

            object[] prm = {input.Username, input.Password, 0};

            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_LOGIN", prm);

            while (reader.Read())
            {
                check = reader["RESULT"].ToString();
            }

            if (check == "T")
            {
                using (
                    SqlConnection connection =
                        new SqlConnection(
                            WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
                {
                    connection.Open();

                    using (SqlCommand command1 = new SqlCommand())
                    {
                        command1.Connection = connection;
                        command1.CommandText =
                            "SELECT US_ID ,US_FNAME + ' ' + US_LNAME AS US_FNAME FROM McsAppCenter.dbo.USERS WHERE US_ID=@US_ID";
                        command1.CommandType = CommandType.Text;
                        command1.Parameters.Add("@US_ID", SqlDbType.VarChar, 7).Value = input.Username;

                        SqlDataReader dr = command1.ExecuteReader();

                        while (dr.Read())
                        {
                            UserInfo.US_ID = dr["US_ID"].ToString();
                            UserInfo.US_FNAME = dr["US_FNAME"].ToString();
                        }
                    }

                    connection.Close();
                }
            }
            else
            {
                UserInfo.US_ID = null;
                UserInfo.US_FNAME = null;
            }
            return UserInfo;
        }

        //แก้ไขรหัสผ่าน
        public string SetUsernamePassword(SetUserPass input)
        {
            object[] prm = {input.Username, input.OldPassword, input.NewPassword};

            string returnMessage = "";
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SET_UPDATE_CHANGE_PASSWORD", prm);
            while (reader.Read())
            {
                returnMessage = (string) reader["RETURN_MESSAGE"];
            }

            return returnMessage;
        }

        #endregion

        public mResult CheckUserAuthen(CheckAuthen input)
        {
            mResult result = new mResult();

            mcsappcenterDataContext db = new mcsappcenterDataContext();

            int ua_auth = 0;
            string subfilename = input.sMn_File_Name.Substring(6);

            if (subfilename == "status")
            {
                ua_auth = 1;
            }
            else if (subfilename == "inform_ncr" || subfilename == "recheck_ncr")
            {
                ua_auth = 2;
            }
            else if (subfilename == "confirm_ncr")
            {
                ua_auth = 4;
            }

            V306_Group us_check =
                db.V306_Groups.Where(
                        ua => ua.US_ID == input.sUsername && ua.MN_FILE_NAME == input.sMn_File_Name && ua.UA_AUTH >= ua_auth)
                    .FirstOrDefault();

            if (us_check == null)
            {
                result.RESULT = "false";
                result.EXCEPTION = "";
            }
            else
            {
                result.RESULT = "true";
                result.EXCEPTION = "";
            }
            return result;
        }

        public mResult CheckGroupAuthen(CheckAuthen input)
        {
            mResult result = new mResult();

            mcsappcenterDataContext db = new mcsappcenterDataContext();


            V306_Group gu_check =
                db.V306_Groups.Where(ua => ua.US_ID == input.sUsername && ua.MN_FILE_NAME == input.sMn_File_Name)
                    .FirstOrDefault();

            if (gu_check == null)
            {
                result.RESULT = "false";
                result.EXCEPTION = "";
            }
            else
            {
                result.RESULT = "true";
                result.EXCEPTION = "";
            }
            return result;
        }

        public Result CheckMTDep(SetLogin input)
        {
            Result result = new Result();

            CMMSDataContext context = new CMMSDataContext();
            MST_EMP emp =
                context.MST_EMPs.Where(ip => ip.EMP_CODE == input.emp_code && ip.DEPT_CODE == "O28" && ip.IS_ACTIVE == 1)
                    .FirstOrDefault();

            if (emp == null)
            {
                OPERATOR_LABOUR ol =
                    context.OPERATOR_LABOURs.Where(ip => ip.EMP_CODE == input.emp_code && ip.IS_ACTIVE == 1)
                        .FirstOrDefault();
                if (ol == null)
                {
                    result.result = false;
                    result.exception = "";
                }
                else
                {
                    result.result = true;
                    result.exception = "";
                }
            }
            else
            {
                result.result = true;
                result.exception = "";
            }

            return result;
        }

        public Result CheckLocationBarcode(GetCheckLocationBarcode input)
        {
            Result result = new Result();

            McsFg2010DataContext fg = new McsFg2010DataContext();
            stock_shelf ss = fg.stock_shelfs.Where(ip => ip.place == input.barcode).FirstOrDefault();

            if (ss == null)
            {
                result.result = false;
                result.exception = "";
            }
            else
            {
                result.result = true;
                result.exception = "";
            }

            return result;
        }

        public Result CheckUserGroupBuilt(GetCheckUserGroupBuilt input)
        {
            Result result = new Result();

            using (
                SqlConnection connection =
                    new SqlConnection(
                        WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText =
                        "SELECT US_ID FROM USER_MEMBER M JOIN USER_GROUP G ON M.UG_ID = G.UG_ID WHERE G.UG_NAME = @UG_NAME AND M.US_ID = @US_ID";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add("@UG_NAME", SqlDbType.VarChar, 7).Value = input.ugName;
                    cmd.Parameters.Add("@US_ID", SqlDbType.VarChar, 7).Value = input.userId;

                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result.result = true;
                        result.exception = "";
                    }
                }

                connection.Close();
            }

            return result;
        }

        #region SP_GET_PRODUCT_STATUS เช็คสถานะชิ้นงาน

        public GetProductInfo GetProductStatus(InputBarcode input)
        {
            GetProductInfo GetProductStatus = new GetProductInfo();
            GetProductStatus.getProductStatus = new List<GetProductStatus>();

            object[] prm = {input.nPJ_ID, input.nPd_Item};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_PRODUCT_STATUS", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    GetProductStatus.FPROJ = (string) reader["FPROJ"];
                    GetProductStatus.TYPE = (string) reader["TYPE"];
                    GetProductStatus.SPROJ = (string) reader["SPROJ"];
                    GetProductStatus.SHARP = (Int16) reader["SHARP"];
                    GetProductStatus.ITEM = (Int16) reader["ITEM"];
                    GetProductStatus.CODE = (string) reader["CODE"];
                    GetProductStatus.DWG = (string) reader["DWG"];
                    GetProductStatus.SIZE = reader["SIZE"].ToString();
                    GetProductStatus.LENGTH = (decimal) reader["LENGTH"];
                    GetProductStatus.WEIGHT = (decimal) reader["WEIGHT"];
                }
            }
            else
            {
                GetProductStatus.FPROJ = "-";
                GetProductStatus.TYPE = "-";
                GetProductStatus.SPROJ = "-";
                GetProductStatus.SHARP = 0;
                GetProductStatus.ITEM = 0;
                GetProductStatus.CODE = "-";
                GetProductStatus.DWG = "-";
                GetProductStatus.SIZE = "-";
                GetProductStatus.LENGTH = 0;
                GetProductStatus.WEIGHT = 0;
            }


            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        GetProductStatus.getProductStatus.Add(new GetProductStatus
                        {
                            PC_NAME = (string) reader["PC_NAME"],
                            PA_ACTUAL_DATE = (string) reader["PA_ACTUAL_DATE"],
                            ANAME = (string) reader["ANAME"]
                        });
                    }

                }
                else
                {
                    GetProductStatus.getProductStatus.Add(new GetProductStatus
                    {
                        PC_NAME = "-",
                        PA_ACTUAL_DATE = "-",
                        ANAME = "-"
                    });
                }
            }
            return GetProductStatus;
        }

        #endregion

        #region GET_PRODUCT_SEND เช็คสถานะส่งงาน

        public List<GetProcessPD> GetProductProcess()
        {
            return new List<GetProcessPD>
            {
                new GetProcessPD {TYPE_ID = 1, TYPE_NAME = "Built Box"},
                new GetProcessPD {TYPE_ID = 2, TYPE_NAME = "Built Beam"},
                new GetProcessPD {TYPE_ID = 3, TYPE_NAME = "Fab"},
                new GetProcessPD {TYPE_ID = 4, TYPE_NAME = "Weld"}
            };
        }

        public GetProductSend GetProductSend(InputBarcodeType input)
        {
            GetProductSend GetProductSend = new GetProductSend();
            GetProductSend.getListProductSend = new List<GetListProductSend>();

            object[] prm = {input.nPJ_ID, input.nPd_Item, input.Type};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_PRODUCT_SEND", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    GetProductSend.PJ_NAME = (string) reader["pj_name"];
                }
            }
            else
            {
                GetProductSend.PJ_NAME = "-";
            }

            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        GetProductSend.getListProductSend.Add(new GetListProductSend
                        {
                            PC_NAME = (string) reader["PC_NAME"],
                            TIMES = (string) reader["TIMES"],
                            NAMES = (string) reader["NAMES"],
                            ACTUAL = (int) reader["ACTUAL"]
                        });
                    }
                }
                else
                {
                    GetProductSend.getListProductSend.Add(new GetListProductSend
                    {
                        PC_NAME = "-",
                        TIMES = "-",
                        NAMES = "-",
                        ACTUAL = 0
                    });
                }
            }

            return GetProductSend;
        }

        #endregion

        #region GET_BUILT_PRODUCT_DETAIL_STEP_1

        public BuiltDetailProject GetBuiltProjectDetails(InputBuiltProject input)
        {
            BuiltDetailProject BuiltInfo = new BuiltDetailProject();

            BuiltInfo.getItem = new List<GetItem>();

            BuiltInfo.getBuiltType = new List<GetBuiltType>();

            BuiltInfo.getProcess = new List<GetProcess>();

            BuiltInfo.PC_ID = new int();

            object[] prm = {input.nPJ_ID, input.nPd_Item, input.Users};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_M_GET_BUILT_PRODUCT_DETAIL_STEP_1", prm);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    BuiltInfo.PJ_ID = (int) reader["PJ_ID"];
                    BuiltInfo.PROJECT = (string) reader["PROJECT"];
                    BuiltInfo.PD_ITEM = (Int16) reader["PD_ITEM"];
                    BuiltInfo.PD_WEIGHT = (decimal) reader["PD_WEIGHT"];
                }
            }
            else
            {
                BuiltInfo.PJ_ID = 0;
                BuiltInfo.PROJECT = "-";
                BuiltInfo.PD_ITEM = 0;
                BuiltInfo.PD_WEIGHT = 0;
            }

            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        BuiltInfo.getItem.Add(new GetItem
                        {
                            ITEM_NO = (Int16) reader["ITEM_NO"],
                            ITEM_NO_NAME = (Int16) reader["ITEM_NO_NAME"]
                        });
                    }
                }
                else
                {
                    BuiltInfo.getItem.Add(new GetItem
                    {
                        ITEM_NO = 0,
                        ITEM_NO_NAME = 0
                    });
                }
            }

            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        BuiltInfo.getBuiltType.Add(new GetBuiltType
                        {
                            BUILT_TYPE_ID = (Int16) reader["BUILT_TYPE_ID"],
                            BUILT_TYPE_NAME = (string) reader["BUILT_TYPE_NAME"]
                        });
                    }
                }
                else
                {
                    BuiltInfo.getBuiltType.Add(new GetBuiltType
                    {
                        BUILT_TYPE_ID = 0,
                        BUILT_TYPE_NAME = "-"
                    });
                }
            }

            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        BuiltInfo.getProcess.Add(new GetProcess
                        {
                            PC_ID = (int) reader["PC_ID"],
                            PC_NAME = (string) reader["PC_NAME"]
                        });
                    }
                }
                else
                {
                    BuiltInfo.getProcess.Add(new GetProcess
                    {
                        PC_ID = 0,
                        PC_NAME = "-"
                    });
                }
            }
            if (reader.NextResult())
            {
                while (reader.Read())
                {
                    BuiltInfo.PC_ID = (int) reader["PC_ID"];
                }
            }
            return BuiltInfo;
        }

        #endregion

        #region GET_BUILT_PRODUCT_DETAIL_STEP_2

        public GetBuiltStatus GetbuiltStatus(InputCheckBuilt input)
        {
            GetBuiltStatus getBuiltStatus = new GetBuiltStatus();

            object[] prm = {input.nPJ_ID, input.nPd_Item, input.nItem_no, input.nBuilt_Type, input.nProcess};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_M_GET_BUILT_PRODUCT_DETAIL_STEP_2", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    getBuiltStatus.PC_ID = (int) reader["PC_ID"];
                    getBuiltStatus.PC_NAME = (string) reader["PC_NAME"];
                    getBuiltStatus.UG_NAME = (string) reader["UG_NAME"];
                    getBuiltStatus.US_FNAME = (string) reader["US_FNAME"];
                    getBuiltStatus.PA_PLAN_DATE = (string) reader["PA_PLAN_DATE"];
                    getBuiltStatus.PA_ACTUAL_DATE = (string) reader["PA_ACTUAL_DATE"];
                }
            }
            else
            {
                getBuiltStatus.PC_ID = 0;
                getBuiltStatus.PC_NAME = "-";
                getBuiltStatus.UG_NAME = "-";
                getBuiltStatus.US_FNAME = "-";
                getBuiltStatus.PA_PLAN_DATE = "-";
                getBuiltStatus.PA_ACTUAL_DATE = "-";
            }
            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        getBuiltStatus.PC_ID2 = (int) reader["PC_ID"];
                        getBuiltStatus.PC_NAME2 = (string) reader["PC_NAME"];
                        getBuiltStatus.UG_NAME2 = (string) reader["UG_NAME"];
                        getBuiltStatus.PA_PLAN_DATE2 = Convert.ToDateTime(reader["PA_PLAN_DATE"]).ToString("yyyy-MM-dd");
                    }
                }
                else
                {
                    getBuiltStatus.PC_ID2 = 0;
                    getBuiltStatus.PC_NAME2 = "-";
                    getBuiltStatus.UG_NAME2 = "-";
                    getBuiltStatus.PA_PLAN_DATE2 = "-";
                }
            }
            return getBuiltStatus;
        }

        #endregion

        #region SP_CHECK_SET_BUILT_ACTUAL ส่งงาน built

        public ResultStatus setBuiltActualSend(InputBuiltActualSend input)
        {
            ResultStatus ResultStatus = new ResultStatus();
            object[] prm =
            {
                input.nPJ_ID, input.nPd_Item, input.nItem_no, input.nBuilt_Type, input.nProcess,
                input.sUser_work, input.sPlace, input.sUser_id, input.nResult
            };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_SET_BUILT_SENT", prm);
            while (reader.Read())
            {
                ResultStatus.RESULT = (string) reader["ERROR"];
            }
            return ResultStatus;
        }

        #endregion

        //Actual parts on built up beam process

        #region CHECK_BARCODE (Built Up Part Actual)

        public int PD_CheckBarcodePart(CheckBarcode input)
        {
            int result = 0;

            McsAppDataContext mcsapp = new McsAppDataContext();
            CUT_BUILT_PART cbp = mcsapp.CUT_BUILT_PARTs.Where(ip => ip.BARCODE_PART == input.barcode).FirstOrDefault();

            if (cbp == null)
            {
                result = 0;
            }
            else
            {
                if (input.process == 72001 || input.process == 73001)
                {
                    if (cbp.CUT_USER == null)
                    {
                        result = 1;
                    }
                    else
                    {
                        result = 2;
                    }
                }
                else if (input.process == 72002 || input.process == 73002)
                {
                    if (cbp.TAPER_USER == null)
                    {
                        result = 1;
                    }
                    else
                    {
                        result = 2;
                    }
                }
                else if (input.process == 12001 || input.process == 11002 || input.process == 11003)
                {
                    if (cbp.BUILT_USER == null)
                    {
                        result = 1;
                    }
                    else
                    {
                        result = 2;
                    }
                }
                else
                {
                    result = 0;
                }
            }

            return result;
        }

        #endregion

        #region GET_BARCODE_PART_ACTUAL (Built Up Part Actual)

        public List<SetBarcodePartActual> PD_BarcodePartActual(GetBarcodePartActual input)
        {
            List<SetBarcodePartActual> list = new List<SetBarcodePartActual>();

            object[] obj = {input.barcode, input.item_no, input.built_type_id};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_M_GET_BARCODE_PART_BUILT_BEAM", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SetBarcodePartActual()
                    {
                        barcode_part = reader["BARCODE"].ToString(),
                        qty = (int) reader["QTY"],
                        actual = (int) reader["ACTUAL"]
                    });
                }
            }

            return list;
        }

        #endregion

        #region GET_BARCODE_PART_ACTUAL (Built Box)

        public List<SetBarcodePartActual> PD_BarcodePartSkinPlate(GetBarcodePartActualBox input)
        {
            List<SetBarcodePartActual> list = new List<SetBarcodePartActual>();

            object[] obj = { input.barcode, input.item_no, input.built_type_id, input.pc_id  };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_M_GET_BARCODE_PART_BUILT_BOX", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SetBarcodePartActual()
                    {
                        barcode_part = reader["BARCODE"].ToString(),
                        qty = (int)reader["QTY"],
                        actual = (int)reader["ACTUAL"]
                    });
                }
            }

            return list;
        }

        public List<SetBarcodePartDiaphragm> PD_BarcodePartDiaphragm(GetBarcodePartActualBox input)
        {
            List<SetBarcodePartDiaphragm> list = new List<SetBarcodePartDiaphragm>();

            object[] obj = { input.barcode, input.item_no, input.built_type_id, input.pc_id  };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_M_GET_BARCODE_PART_BUILT_BOX", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SetBarcodePartDiaphragm()
                    {
                        built_code = reader["BUILT_CODE"].ToString(),
                        cutting_plan   = reader["CUTTING_PLAN"].ToString(),
                        qty = (int)reader["QTY"],
                        actual = (int)reader["ACTUAL"]
                    });
                }
            }

            return list;
        }

        #endregion

        #region GET_SYMBOL (Built Up Part Actual)

        //public List<SetSymbol> PD_Symbol(GetSymbol input)
        //{
        //    List<SetSymbol> list = new List<SetSymbol>();

        //    object[] obj = { input.process, input.barcode, input.item_no, input.built_type_id };
        //    var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_M_GET_SYMBOL_TYPE", obj);

        //    if (reader.HasRows)
        //    {
        //        while (reader.Read())
        //        {
        //            list.Add(new SetSymbol()
        //            {
        //                symbol_id = reader["SYMBOL_TYPE_ID"].ToString(),
        //                symbol_name = reader["SYMBOL_TYPE_NAME"].ToString()
        //            });
        //        }
        //    }

        //    return list;
        //}

        #endregion

        #region ActualPart

        public SetActualPart PD_ActualPart(GetActualPart input)
        {
            SetActualPart result = new SetActualPart();

            object[] obj =
            {
                input.barcode_part, input.barcode, input.item_no, input.built_type_id,input.pc_id, input.symbol,
                input.user
            };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_M_SET_BUILT_PART_ACTUAL", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.result = reader["RESULT"].ToString();
                    result.message = reader["MESSAGE"].ToString();
                }
            }

            return result;
        }

        #endregion

        //====================================

        #region SP_GET_FAB_PRODUCT_DETAIL_1

        public FabGetProcess GetFabProcessDetail(InputFabBarcode input)
        {
            FabGetProcess FabGetProcess = new FabGetProcess();
            FabGetProcess.getProcessFab = new List<GetProcessFab>();

            McsAppDataContext dc = new McsAppDataContext();

            var query = (from f in dc.FAB_SUMs
                where f.pc_id != 81000 && f.pj_id == input.nPJ_ID && f.pd_item == input.nPd_Item
                orderby f.pc_id
                select new {PC_ID = f.pc_id, PC_NAME = dc.sf_ProcessName(f.pc_id.ToString())}).FirstOrDefault();

            if (query == null)
            {
                FabGetProcess.getProcessFab.Add(new GetProcessFab
                {
                    PC_ID = 0,
                    PC_NAME = "-"
                });
            }
            else
            {
                var fab_pc = (from f in dc.FAB_SUMs
                    where f.pc_id != 81000 && f.pj_id == input.nPJ_ID && f.pd_item == input.nPd_Item
                    select new {PC_ID = f.pc_id, PC_NAME = dc.sf_ProcessName(f.pc_id.ToString())});

                foreach (var pc_fab in fab_pc)
                {
                    FabGetProcess.getProcessFab.Add(new GetProcessFab
                    {
                        PC_ID = (int) pc_fab.PC_ID,
                        PC_NAME = (string) pc_fab.PC_NAME
                    });
                }
            }
            return FabGetProcess;
        }

        #endregion

        #region SP_GET_FAB_PRODUCT_DETAIL_2

        public FabGetProject GetFabProjectDetail(InputFabBarcodePC input)
        {
            FabGetProject FabGetProject = new FabGetProject();

            object[] prm = {input.nPJ_ID, input.nPd_Item, input.nProcess};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_FAB_PRODUCT_DETAIL_2", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    FabGetProject.PJ_ID = (int) reader["PJ_ID"];
                    FabGetProject.PD_ITEM = (Int16) reader["PD_ITEM"];
                    FabGetProject.PJ_NAME = (string) reader["PJ_NAME"];
                    FabGetProject.PT_NAME = (string) reader["PT_NAME"];
                    FabGetProject.UG_NAME = (string) reader["UG_NAME"];
                    FabGetProject.PLANDATE = (string) reader["PLANDATE"];
                    FabGetProject.PC_ID = (int) reader["PC_ID"];
                    FabGetProject.PC_NAME = (string) reader["PC_NAME"];
                    FabGetProject.FAB_PLAN = (int) reader["FAB_PLAN"];
                    FabGetProject.FAB_ACTUAL = (int) reader["FAB_ACTUAL"];
                }
            }
            else
            {
                FabGetProject.PJ_ID = 0;
                FabGetProject.PD_ITEM = 0;
                FabGetProject.PJ_NAME = "-";
                FabGetProject.PT_NAME = "-";
                FabGetProject.UG_NAME = "-";
                FabGetProject.PLANDATE = "-";
                FabGetProject.PC_ID = 0;
                FabGetProject.PC_NAME = "-";
                FabGetProject.FAB_PLAN = 0;
                FabGetProject.FAB_ACTUAL = 0;
            }
            return FabGetProject;
        }

        #endregion

        #region SP_GET_FAB_PRODUCT_DETAIL_3

        public List<FabGetGroupUser> GetFabGroupUser(InputFabGroup input)
        {
            object[] prm = {input.sUG_Name};
            List<FabGetGroupUser> listFabGroupUser = new List<FabGetGroupUser>();
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_FAB_PRODUCT_DETAIL_3", prm);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    listFabGroupUser.Add(new FabGetGroupUser
                    {
                        US_ID = (string) reader["US_ID"],
                        FULLNAME = reader["FULLNAME"].ToString()
                    });
                }
            }
            else
            {
                listFabGroupUser.Add(new FabGetGroupUser
                {
                    US_ID = "-",
                    FULLNAME = "-"
                });
            }
            return listFabGroupUser;
        }

        #endregion

        #region SP_SET_FAB_SENT_REPORT ส่งงาน Fab

        public ResultStatus SetFabActual(InputFabActualSend input)
        {
            ResultStatus REsultstatus = new ResultStatus();

            object[] prm =
            {
                input.nPJ_ID, input.nPd_Item, input.nProcess, input.nVal, input.sUserActual, input.sPlace,
                input.sUserID, input.Result
            };

            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_SET_FAB_SENT_REPORT", prm);

            while (reader.Read())
            {
                REsultstatus.RESULT = (string) reader["ERROR"];
            }
            return REsultstatus;
        }

        #endregion


        #region SP_GET_WELD_PRODUCT_DETAIL_1

        public WeldGetProcess GetWeldProcessDetail(InputWeldBarcode input)
        {
            WeldGetProcess WeldGetProcess = new WeldGetProcess();
            WeldGetProcess.getProcessWeld = new List<GetProcessWeld>();

            McsAppDataContext dc = new McsAppDataContext();

            var query = (from w in dc.WELD_SUMs
                where w.pc_id != 82000 && w.pj_id == input.nPJ_ID && w.pd_item == input.nPd_Item
                orderby w.pc_id
                select new {PC_ID = w.pc_id, PC_NAME = dc.sf_ProcessName(w.pc_id.ToString())}).FirstOrDefault();

            if (query == null)
            {
                WeldGetProcess.getProcessWeld.Add(new GetProcessWeld
                {
                    PC_ID = 0,
                    PC_NAME = "-"
                });
            }
            else
            {
                var weld_pc = (from w in dc.WELD_SUMs
                    where w.pc_id != 82000 && w.pj_id == input.nPJ_ID && w.pd_item == input.nPd_Item
                    orderby w.pc_id
                    select new {PC_ID = w.pc_id, PC_NAME = dc.sf_ProcessName(w.pc_id.ToString())});

                foreach (var pc_weld in weld_pc)
                {
                    WeldGetProcess.getProcessWeld.Add(new GetProcessWeld
                    {
                        PC_ID = (int) pc_weld.PC_ID,
                        PC_NAME = (string) pc_weld.PC_NAME
                    });
                }
            }
            return WeldGetProcess;
        }

        #endregion

        #region SP_GET_WELD_PRODUCT_DETAIL_2

        public WeldGetProject GetWeldProjectDetail(InputWeldBarcodePC input)
        {
            WeldGetProject WeldGetProject = new WeldGetProject();

            object[] prm = {input.nPJ_ID, input.nPd_Item, input.nProcessID};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_WELD_PRODUCT_DETAIL_2", prm);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    WeldGetProject.PJ_ID = (int) reader["PJ_ID"];
                    WeldGetProject.PD_ITEM = (Int16) reader["PD_ITEM"];
                    WeldGetProject.PJ_NAME = (string) reader["PJ_NAME"];
                    WeldGetProject.PT_NAME = (string) reader["PT_NAME"];
                    WeldGetProject.UG_NAME = (string) reader["UG_NAME"];
                    WeldGetProject.PLANDATE = (string) reader["PLANDATE"];
                    WeldGetProject.PC_ID = (int) reader["PC_ID"];
                    WeldGetProject.PC_NAME = (string) reader["PC_NAME"];
                    WeldGetProject.WELD_PLAN = (int) reader["WELD_PLAN"];
                    WeldGetProject.WELD_ACTUAL = (int) reader["WELD_ACTUAL"];
                }
            }
            else
            {
                WeldGetProject.PJ_ID = 0;
                WeldGetProject.PD_ITEM = 0;
                WeldGetProject.PJ_NAME = "-";
                WeldGetProject.PT_NAME = "-";
                WeldGetProject.UG_NAME = "-";
                WeldGetProject.PLANDATE = "-";
                WeldGetProject.PC_ID = 0;
                WeldGetProject.PC_NAME = "-";
                WeldGetProject.WELD_PLAN = 0;
                WeldGetProject.WELD_ACTUAL = 0;
            }
            return WeldGetProject;
        }

        #endregion

        #region SP_GET_WELD_PRODUCT_DETAIL_3

        public List<WeldGetGroupUser> GetWeldGroupUser(InputWeldGroup input)
        {
            object[] prm = {input.sUG_Name};
            List<WeldGetGroupUser> listWeltGroupUser = new List<WeldGetGroupUser>();
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_WELD_PRODUCT_DETAIL_3", prm);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    listWeltGroupUser.Add(new WeldGetGroupUser
                    {
                        US_ID = (string) reader["US_ID"],
                        FULLNAME = (string) reader["FULLNAME"]
                    });
                }
            }
            else
            {
                listWeltGroupUser.Add(new WeldGetGroupUser
                {
                    US_ID = "-",
                    FULLNAME = "-"
                });
            }
            return listWeltGroupUser;
        }

        #endregion

        #region SP_SET_WELD_SENT_REPORT ส่งงาน Weld

        public ResultStatus SetWeldActual(InputWeldActualSend input)
        {
            ResultStatus ResultSTatus = new ResultStatus();

            object[] prm =
            {
                input.nPJ_ID, input.nPd_Item, input.nProcess, input.nVal, input.sUserActual, input.sPlace,
                input.sUserID, input.Result
            };

            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_SET_WELD_SENT_REPORT", prm);
            while (reader.Read())
            {
                ResultSTatus.RESULT = (string) reader["ERROR"];
            }
            return ResultSTatus;
        }

        #endregion


        #region SP_M_GET_OTHER_PROCESS_DETAIL_STEP_1

        public GetListPcRev GetOtherProcess(InputOtherStep1 input)
        {
            GetListPcRev oTherGet = new GetListPcRev();
            oTherGet.getListPC = new List<GetOtherProcess>();
            oTherGet.getListRev = new List<GetOtherRev>();

            object[] prm = {input.nPJ_ID, input.nPd_Item, input.sUserID};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_M_GET_OTHER_PROCESS_DETAIL_STEP_1", prm);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    oTherGet.getListPC.Add(new GetOtherProcess
                    {
                        PC_ID = (int) reader["PC_ID"],
                        PC_NAME = (string) reader["PC_NAME"]
                    });
                }
            }
            else
            {
                oTherGet.getListPC.Add(new GetOtherProcess
                {
                    PC_ID = 0,
                    PC_NAME = "-"
                });
            }

            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        oTherGet.getListRev.Add(new GetOtherRev
                        {
                            PD_REV = (Int16) reader["PD_REV"],
                            PD_REV_NAME = (Int16) reader["PD_REV_NAME"]
                        });
                    }
                }
                else
                {
                    oTherGet.getListRev.Add(new GetOtherRev
                    {
                        PD_REV = 0,
                        PD_REV_NAME = 0
                    });
                }
            }

            if (reader.Read())
            {
                while (reader.Read())
                {
                    oTherGet.PC_ID = (int) reader["PC_ID"];
                    oTherGet.PD_REV = (Int16) reader["PD_REV"];
                }
            }
            return oTherGet;
        }

        #endregion

        #region SP_M_GET_OTHER_PROCESS_DETAIL_STEP_2

        public OtherGetProcessDetail GetOtherProcessDetail(InputOtherStep2 input)
        {
            OtherGetProcessDetail OtherGEtProcess = new OtherGetProcessDetail();
            OtherGEtProcess.getOtherUser = new List<GetOtherUser>();

            object[] prm = {input.nPJ_ID, input.nPd_Item, input.nPd_Rev, input.nProcess};

            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_M_GET_OTHER_PROCESS_DETAIL_STEP_2", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    OtherGEtProcess.PJ_ID = (int) reader["PJ_ID"];
                    OtherGEtProcess.PROJECT = (string) reader["PROJECT"];
                    OtherGEtProcess.PD_ITEM = (Int16) reader["PD_ITEM"];
                    OtherGEtProcess.PD_REV = (Int16) reader["PD_REV"];
                    OtherGEtProcess.UG_NAME = (string) reader["UG_NAME"];
                    OtherGEtProcess.PA_PLAN_DATE = (string) reader["PA_PLAN_DATE"];
                    OtherGEtProcess.PA_STATUS = (string) reader["PA_STATUS"];
                    OtherGEtProcess.PA_STATE = (Int16) reader["PA_STATE"];
                }
            }
            else
            {
                OtherGEtProcess.PJ_ID = 0;
                OtherGEtProcess.PROJECT = "-";
                OtherGEtProcess.PD_ITEM = 0;
                OtherGEtProcess.PD_REV = 0;
                OtherGEtProcess.UG_NAME = "-";
                OtherGEtProcess.PA_PLAN_DATE = "-";
                OtherGEtProcess.PA_STATUS = "-";
                OtherGEtProcess.PA_STATE = 0;
            }
            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        OtherGEtProcess.getOtherUser.Add(new GetOtherUser
                        {
                            US_ID = (string) reader["US_ID"],
                            FULLNAME = (string) reader["FULLNAME"]
                        });
                    }
                }
                else
                {
                    OtherGEtProcess.getOtherUser.Add(new GetOtherUser
                    {
                        US_ID = "-",
                        FULLNAME = "-"
                    });
                }

            }
            return OtherGEtProcess;
        }

        #endregion

        #region SP_SET_OTHER_ACTUAL_SEND ส่งงานอื่นๆ

        public ResultStatus SetOtherActual(InputOtherActualSend input)
        {
            ResultStatus reSultsTatus = new ResultStatus();

            object[] prm =
            {
                input.nPJ_ID, input.nPd_Item, input.nPd_Rev, input.nProcess, input.sUserID, input.sPlace,
                input.sUser_Actual, input.nResult
            };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_SET_OTHER_SENT", prm);
            while (reader.Read())
            {
                reSultsTatus.RESULT = (string) reader["RESULT"];
            }
            return reSultsTatus;
        }

        #endregion


        #region SP_GET_REVISE_PLAN_GROUP  //แสดงกรุ๊ป

        public List<GetGruopRevise> GetGroupReviseInfo()
        {
            McsAppDataContext dc = new McsAppDataContext();

            List<GetGruopRevise> GroupData = new List<GetGruopRevise>();

            var item = ((from Fab in dc.V301_FabRevs select Fab.ug_name)
                .Union(from Weld in dc.V301_WeldRevs select Weld.ug_name));
            foreach (string items in item)
            {
                GroupData.Add(new GetGruopRevise()
                {
                    UG_NAME = items
                });
            }
            return GroupData;
        }

        public GetRevisePlanGroup GetReviseInfo(InputReviseGroup input) //แสดงแผนงานประกอบ - เชื่อม
        {
            McsAppDataContext dc = new McsAppDataContext();

            GetRevisePlanGroup getRevisePlan = new GetRevisePlanGroup();
            getRevisePlan.getRevise = new List<GetRevisePlan>();

            var FabWeld = ((from f in dc.V301_FabRevs
                    where f.ug_name == input.sUG_Name
                    select
                    new {f.pj_id, f.pd_item, f.mp_sname, f.pt_ItemType, f.pj_sharp, f.ug_name, RevDate = f.revDate})
                .Union(from w in dc.V301_WeldRevs
                    where w.ug_name == input.sUG_Name
                    select
                    new {w.pj_id, w.pd_item, w.mp_sname, w.pt_ItemType, w.pj_sharp, w.ug_name, RevDate = w.revDateWeld}));

            foreach (var items in FabWeld)
            {
                getRevisePlan.getRevise.Add(new GetRevisePlan()
                {
                    PD_ITEM = items.pd_item,
                    MP_SNAME = items.mp_sname,
                    PT_ITEMTYPE = items.pt_ItemType,
                    PJ_SHARP = (Int16) items.pj_sharp,
                    UG_NAME = items.ug_name,
                    REVDATE = items.RevDate
                });
            }

            var total = FabWeld.Count();

            {
                getRevisePlan.TOTAL = total;
            }
            return getRevisePlan;
        }

        #endregion


        #region SP_GET_REVISE_REV_NO

        public GetReviseRev GetreviseRevNo(InputRevisePJ input)
        {
            GetReviseRev GetreviseREv = new GetReviseRev();
            GetreviseREv.getRevno = new List<GetRevno>();
            GetreviseREv.getProcessRev = new List<GetProcessRev>();

            McsAppDataContext dc = new McsAppDataContext();

            REV_PLAN icheck =
                dc.REV_PLANs.Where(r => r.PJ_ID == input.nPJ_ID && r.ITEM == input.nPd_Item).FirstOrDefault();

            if (icheck != null)
            {
                var query_rev =
                (from r in dc.REV_PLANs
                    where r.PJ_ID == input.nPJ_ID && r.ITEM == input.nPd_Item
                    orderby r.REV_NO
                    select r.REV_NO).Distinct();
                foreach (var items in query_rev)
                {
                    GetreviseREv.getRevno.Add(new GetRevno
                    {
                        REV_NO = (int) items
                    });
                }
            }
            else
            {
                GetreviseREv.getRevno.Add(new GetRevno
                {
                    REV_NO = 0
                });
            }

            REV_PROCESS icheck_pc = dc.REV_PROCESSes.OrderBy(pc => pc.PC_ID).FirstOrDefault();

            if (icheck_pc != null)
            {
                var query_pc = (from p in dc.REV_PROCESSes orderby p.PC_ID select new {p.PC_NAME, p.PC_ID}).Distinct();
                foreach (var item in query_pc)
                {
                    GetreviseREv.getProcessRev.Add(new GetProcessRev
                    {
                        PROCESS_NAME = item.PC_NAME.ToString(),
                        PC_ID = (int) item.PC_ID
                    });
                }
            }
            else
            {
                GetreviseREv.getProcessRev.Add(new GetProcessRev
                {
                    PROCESS_NAME = "-",
                    PC_ID = 0
                });
            }
            return GetreviseREv;
        }

        #endregion

        #region SP_GET_REVISE_PLAN_DETAIL

        public GetReviseDetail GetreviseDetail(InputReviseDT input)
        {
            GetReviseDetail GetReviseFab = new GetReviseDetail();

            object[] prm = {input.nPJ_ID, input.nPd_Item, input.nRev_no, input.nProcess};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_REVISE_PLAN_DETAIL", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    GetReviseFab.PJ_ID = (int) reader["PJ_ID"];
                    GetReviseFab.PROJECT = (string) reader["PROJECT"];
                    GetReviseFab.ITEM = (Int16) reader["ITEM"];
                    GetReviseFab.REV_NO = (int) reader["REV_NO"];
                    GetReviseFab.DESIGN_CHANGE = (string) reader["DESIGN_CHANGE"];
                    GetReviseFab.PC_ID = (int) reader["PC_ID"];
                    GetReviseFab.PROCESS = (string) reader["PROCESS"];
                    GetReviseFab.TYPES = (string) reader["TYPES"];
                    GetReviseFab.PLAN_GROUP = (string) reader["PLAN_GROUP"];
                    GetReviseFab.PLAN_ID = (int) reader["PLAN_ID"];
                    GetReviseFab.PLAN_DATE = (string) reader["PLAN_DATE"];
                    GetReviseFab.PLAN = (int) reader["PLAN"];
                }

                if (reader.NextResult())
                {

                    if (reader.NextResult())
                    {
                        if (reader.NextResult())
                        {
                            while (reader.Read())
                            {
                                GetReviseFab.ACTUAL = (int) reader["ACTUAL"];
                            }
                        }
                    }
                }
            }
            else
            {
                GetReviseFab.PJ_ID = 0;
                GetReviseFab.PROJECT = "-";
                GetReviseFab.ITEM = 0;
                GetReviseFab.REV_NO = 0;
                GetReviseFab.DESIGN_CHANGE = "-";
                GetReviseFab.PC_ID = 0;
                GetReviseFab.PROCESS = "-";
                GetReviseFab.TYPES = "-";
                GetReviseFab.PLAN_GROUP = "-";
                GetReviseFab.PLAN_ID = 0;
                GetReviseFab.PLAN_DATE = "-";
                GetReviseFab.PLAN = 0;
            }
            return GetReviseFab;
        }

        #endregion

        #region SP_GET_USER_IN_GROUP

        public List<GetReviseGroup> GetreviseGroup(InputGroupRevise input)
        {
            List<GetReviseGroup> GetReviseGroup = new List<GetReviseGroup>();

            object[] prm = {input.nUG_ID, input.sUG_Name};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_USER_IN_GROUP", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    GetReviseGroup.Add(new GetReviseGroup
                    {
                        US_ID = (string) reader["US_ID"],
                        FULLNAME = (string) reader["FULLNAME"]
                    });
                }
            }
            else
            {
                GetReviseGroup.Add(new GetReviseGroup
                {
                    US_ID = "-",
                    FULLNAME = "-"
                });
            }
            return GetReviseGroup;
        }

        #endregion

        #region SP_SET_REVISE_ACTUAL

        public ResultStatus SetREviseActual(InputReviseActual input)
        {
            ResultStatus SetReviseActual = new ResultStatus();
            int check = 0;

            object[] prm = {input.sUserID, input.nPJ_ID, input.nPd_Item, input.nRev_no, input.nProcess};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_CHECK_GROUP", prm);
            while (reader.Read())
            {
                check = (int) reader["RESULT"];
            }

            if (check > 0)
            {
                object[] prm2 =
                {
                    input.nPJ_ID, input.nPd_Item, input.nRev_no, input.nProcess, input.sUserActual,
                    input.nActual_QTY, input.sUserID, input.QTY
                };
                var reader2 = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_SET_REVISE_ACTUAL", prm2);
                while (reader2.Read())
                {
                    SetReviseActual.RESULT = (string) reader2["RESULT"];
                }
            }
            else
            {
                SetReviseActual.RESULT = "ไม่มีสิทธ์ส่งงานข้ามกรุ๊ป";
            }

            return SetReviseActual;
        }

        #endregion


        #region Get NCR REV_NO CHECK_NO LIST ลิสต์ Rev_no และ check_no

        public NcrGetRevCheckList GetNcrRevCheckList(InputNcrBarcodePC input)
        {
            NcrGetRevCheckList GetNcrRevCheckList = new NcrGetRevCheckList();
            GetNcrRevCheckList.GetRevNo = new List<GetlistRevNo>();
            GetNcrRevCheckList.GetCheckNo = new List<GetlistCheckNo>();

            object[] prm = {input.sBarcode, input.nType_PC};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_NCR_REV_CHECK_NO_LIST", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    GetNcrRevCheckList.GetRevNo.Add(new GetlistRevNo
                    {
                        REV_NO = (int) reader["REV_NO"],
                        REV_NAME = (string) reader["REV_NAME"]
                    });
                }
            }
            else
            {
                GetNcrRevCheckList.GetRevNo.Add(new GetlistRevNo
                {
                    REV_NO = 0,
                    REV_NAME = "-"
                });
            }
            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        GetNcrRevCheckList.GetCheckNo.Add(new GetlistCheckNo
                        {
                            CHECK_NO = ((int) reader["CHECK_NO"] <= 0) ? 1 : (int) reader["CHECK_NO"],
                            CHECK_NAME = ((int) reader["CHECK_NAME"] <= 0) ? 1 : (int) reader["CHECK_NAME"]
                        });
                    }
                }
                else
                {
                    GetNcrRevCheckList.GetCheckNo.Add(new GetlistCheckNo
                    {
                        CHECK_NO = 1,
                        CHECK_NAME = 1
                    });
                }


            }
            return GetNcrRevCheckList;
        }

        #endregion

        #region SP_GET_DIM_PRODUCT   NCR แสดงรายละเอียดโปรดักส์

        public NcrProduct GetDimProduct(InputNcrPD input)
        {
            NcrProduct GetNcrProduct = new NcrProduct();

            McsAppDataContext dc = new McsAppDataContext();

            int project = 0;
            int items = 0;
            int point = input.sBarcode.IndexOf("-");

            if (point == -1)
            {
                project = 0;
                items = 0;
            }
            else
            {
                project = Convert.ToInt32(input.sBarcode.Substring(0, point));
                items = Convert.ToInt16(input.sBarcode.Substring(point + 1));
            }

            var query_dp = (from pd in dc.Products
                    join pt in dc.Product_Types on pd.pt_id equals pt.pt_id
                    where pd.pj_id == project && pd.pd_item == items
                    select
                    new {pj_name = dc.sf_sname(project.ToString()), pt.pt_ItemType, pd.pd_item, pd.pd_dwg, pd.pd_code})
                .FirstOrDefault();

            if (query_dp != null)
            {
                GetNcrProduct.PROJECT = (string) query_dp.pj_name;
                GetNcrProduct.PT_ITEMTYPE = (string) query_dp.pt_ItemType;
                GetNcrProduct.PD_ITEM = (Int16) query_dp.pd_item;
                GetNcrProduct.PD_DWG = (string) query_dp.pd_dwg;
                GetNcrProduct.PD_CODE = (string) query_dp.pd_code;
            }
            else
            {
                GetNcrProduct.PROJECT = "-";
                GetNcrProduct.PT_ITEMTYPE = "-";
                GetNcrProduct.PD_ITEM = 0;
                GetNcrProduct.PD_DWG = "-";
                GetNcrProduct.PD_CODE = "-";
            }
            return GetNcrProduct;
        }

        #endregion

        #region GET NCR REPORT DESC ลิสต์รายการ ncr

        public List<NcrGetReport> GetNcrReport(InputNcrReport input)
        {
            List<NcrGetReport> NcrGetReport = new List<NcrGetReport>();
            string sNc_id = input.nPC_ID + input.sBarcode + "0" + input.nRev_No + input.nCheck_No;
            string sNc_state = "2";
            string sNd_state = "1";
            string sGroup_name = "";

            object[] us_id = {input.sUS_ID};
            var group_name = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "sp_Group", us_id);
            while (group_name.Read())
            {
                sGroup_name = (string) group_name["ug_name"];
            }

            object[] prm = {sNc_id, sNc_state, sNd_state, sGroup_name, input.nPC_ID, input.sBarcode, input.nRev_No};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_REPORT_NCR", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    NcrGetReport.Add(new NcrGetReport
                    {
                        GROUP_NAME = (string) reader["GROUP_NAME"],
                        NCR_NO = (string) reader["NCR_NO"],
                        NT_NAME = (string) reader["NT_NAME"],
                        DESCRIPTIONS = (string) reader["DESCRIPTIONS"],
                        NC_ID = (string) reader["NC_ID"],
                        OTHER_NO = (int) reader["OTHER_NO"],
                        IMG_BEFORE = (string) reader["IMG_BEFORE"]
                    });
                }
            }
            else
            {

                NcrGetReport.Add(new NcrGetReport
                {
                    GROUP_NAME = "-",
                    NCR_NO = "-",
                    NT_NAME = "-",
                    DESCRIPTIONS = "-",
                    NC_ID = "-",
                    OTHER_NO = 0,
                    IMG_BEFORE = "No.jpg"
                });
            }
            return NcrGetReport;
        }

        #endregion

        #region ส่งงาน  NCR

        public ResultStatus SetNcrSend(InputNcrPassNcr input)
        {
            DateTime now = DateTime.Now;
            McsQcDataContext dc = new McsQcDataContext();
            ResultStatus Result = new ResultStatus();

            NCR_DIM_CHECK items =
                dc.NCR_DIM_CHECKs.Where(
                        nc => nc.NC_ID == input.sNC_ID && nc.NCR_NO == input.sNcr_No && nc.OTHER_NO == input.nOther_No)
                    .FirstOrDefault();

            try
            {
                if (input.getImage == false)
                {
                    items.ND_STATE = 3;
                    items.IMG_AFTER = "No.jpg";
                    items.USERS = input.sUS_ID;
                    items.TIMES = (DateTime) (now);
                    items.USER_UPDATE = input.sUS_ID;
                    items.TIME_UPDATE = (DateTime) (now);
                    items.PD_COMMENT = input.sPd_Comment;
                    items.PD_ACTUAL = input.sPd_Actual;
                    dc.SubmitChanges();

                    Result.RESULT = "T";

                    return Result;
                }
                else
                {
                    string img_before = "";

                    var img_name =
                        from ncr in
                        dc.NCR_DIM_CHECKs.Where(
                            nc =>
                                nc.NC_ID == input.sNC_ID && nc.NCR_NO == input.sNcr_No &&
                                nc.OTHER_NO == input.nOther_No)
                        select ncr.IMG_BEFORE;
                    foreach (string item in img_name)
                    {
                        img_before = item;
                    }

                    if (img_before == "No.jpg")
                    {
                        items.ND_STATE = 3;
                        items.IMG_AFTER = input.sNC_ID + input.sNcr_No + "-" + input.nOther_No + ".jpg";
                        items.USERS = input.sUS_ID;
                        items.TIMES = (DateTime) (now);
                        items.USER_UPDATE = input.sUS_ID;
                        items.TIME_UPDATE = (DateTime) (now);
                        items.PD_COMMENT = input.sPd_Comment;
                        items.PD_ACTUAL = input.sPd_Actual;
                        dc.SubmitChanges();

                        Result.RESULT = "T";

                        return Result;
                    }
                    else
                    {
                        items.ND_STATE = 3;
                        items.IMG_AFTER = img_before;
                        items.USERS = input.sUS_ID;
                        items.TIMES = (DateTime) (now);
                        items.USER_UPDATE = input.sUS_ID;
                        items.TIME_UPDATE = (DateTime) (now);
                        items.PD_COMMENT = input.sPd_Comment;
                        items.PD_ACTUAL = input.sPd_Actual;
                        dc.SubmitChanges();

                        Result.RESULT = "T";

                        return Result;
                    }
                }
            }
            catch (Exception ex)
            {
                Result.RESULT = ex.Message;

                return Result;
            }
        }

        #endregion


        #region  แจ้งตรวจ Dimension

        public ResultStatus SetNCR_Request(InputNcrReport input)
        {
            ResultStatus Result = new ResultStatus();

            object[] prm = {input.nPC_ID, input.sBarcode, input.nRev_No, input.nCheck_No, input.sPlace, input.sUS_ID};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_SET_QA_DIMENSION_AD", prm);

            while (reader.Read())
            {
                Result.RESULT = (string) reader["RESULT"];
            }
            return Result;
        }

        #endregion

        public mResult uploadImages_PD(Stream fileUpload)
        {
            mResult result = new mResult();

            try
            {
                McsAppDataContext dc = new McsAppDataContext();

                string servPath = "";
                string fileSize = "";

                int point = 0;
                int width = 0;
                int height = 0;

                foreach (PDPC_config items in dc.PDPC_configs)
                {
                    if (items.config_key.Equals("PATH_AFTER")) servPath = items.config_value;
                    if (items.config_key.Equals("PICTURE")) fileSize = items.config_value;
                }

                if (fileSize != "")
                {
                    point = fileSize.IndexOf("x");
                    width = Convert.ToInt32(fileSize.Substring(0, point));
                    height = Convert.ToInt32(fileSize.Substring(point + 1));
                }

                //------------------------------------------------------------------------------------------------------

                var parser = new MultipartFormDataParser(fileUpload);

                var username = parser.Parameters["username"].Data;
                var barcode = parser.Parameters["barcode"].Data;
                var otherno = parser.Parameters["otherno"].Data;
                var imagename = parser.Parameters["imagename"].Data;

                var file = parser.Files.FirstOrDefault();

                Stream stream = file.Data;

                //------------------------------------------------------------------------------------------------------

                if (!Directory.Exists(servPath)) Directory.CreateDirectory(servPath);

                FileStream targetStream = null;
                Stream sourceStream = stream;

                string fileName = imagename + "-" + otherno + "-X.jpeg";
                string filePath = Path.Combine(servPath, fileName);

                using (targetStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    const int bufferLen = 4096;
                    byte[] buffer = new byte[bufferLen];
                    int count = 0;
                    int totalBytes = 0;

                    while ((count = sourceStream.Read(buffer, 0, bufferLen)) > 0)
                    {
                        totalBytes += count;

                        targetStream.Write(buffer, 0, count);
                    }

                    targetStream.Close();

                    sourceStream.Close();
                }

                resizeImage(filePath, width, height);

                result.RESULT = "true";
                result.EXCEPTION = "";
            }
            catch (Exception ex)
            {
                result.RESULT = "false";
                result.EXCEPTION = ex.Message;
            }

            return result;
        }


        private byte[] StreamToByte(Stream stream)
        {
            byte[] buffer = new byte[16*1024];

            using (MemoryStream ms = new MemoryStream())
            {
                int read;

                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                return ms.ToArray();
            }
        }


        private void resizeImage(string path, int width, int height)
        {
            Bitmap tmp = new Bitmap(path);
            Bitmap bmp = new Bitmap(tmp, width, height);

            bmp.Save(path.Substring(0, path.Length - 7) + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

            tmp.Dispose();
            bmp.Dispose();

            tmp = null;
            bmp = null;

            File.Delete(path);
        }

        #region พื้นที่

        public List<GetLocationList> getLocationList(InputLocation input)
        {
            List<GetLocationList> GetLocationList = new List<GetLocationList>();

            object[] prm = {input.sPlace};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_LOCATION_LIST", prm);

            while (reader.Read())
            {
                GetLocationList.Add(new GetLocationList
                {
                    PLACE = (string) reader["PLACE"],
                    PLACE_NAME = (string) reader["PLACE_NAME"]
                });
            }
            return GetLocationList;
        }

        #endregion

        #region UT Request

        public setRequestReport PD_UTRequest(getRequestReport input)
        {
            setRequestReport result = new setRequestReport();

            object[] obj = {input.pj_id, input.pd_item, input.item_process, input.user_request, input.place};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_SET_UT_REQUEST_REPORT", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.query = reader["ERROR"].ToString();
                }
            }

            return result;
        }

        #endregion

        #region Report UT Request

        public List<getProcessUt> PD_GetProcessUt()
        {
            return new List<getProcessUt>
            {
                new getProcessUt {process_id = "G", process_name = "GMAW"},
                new getProcessUt {process_id = "S", process_name = "SAW"},
                new getProcessUt {process_id = "E", process_name = "ESW"},
            };
        }

        public List<setReportUTRequest> PD_ReportUTRequest(getReportUTRequest input)
        {
            List<setReportUTRequest> list = new List<setReportUTRequest>();

            object[] obj = {input.pj_id, input.pd_item, input.item_process, 1};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_REPORT_UT_REQUEST", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new setReportUTRequest
                    {
                        pj_id = reader["PJ_ID"].ToString(),
                        pj_name = reader["PJ_NAME"].ToString(),
                        pd_item = reader["PD_ITEM"].ToString(),
                        pd_code = reader["PD_CODE"].ToString(),
                        pd_qty = reader["PD_QTY"].ToString(),
                        pd_dwg = reader["PD_DWG"].ToString(),
                        pd_size = reader["PD_SIZE"].ToString(),
                        place = reader["PLACE"].ToString(),
                        barcode = reader["BARCODE"].ToString(),
                        pa_no = reader["PA_NO"].ToString(),
                        item_process = reader["ITEM_PROCESS"].ToString(),
                        pc_name = reader["PC_NAME"].ToString(),
                        user_request = reader["USER_REQUEST"].ToString(),
                        request_name = reader["REQUEST_NAME"].ToString(),
                        request_date = reader["REQUEST_DATE"].ToString(),
                        ug_name = reader["UG_NAME"].ToString(),
                        ut_status = reader["UT_STATUS"].ToString()
                    });
                }
            }

            return list;
        }

        #endregion

        #endregion

        #region QC

        // Qc Plan Check----------------------------------------------------------------------------------------------

        #region List ชื่อโปรเจค

        public List<mProject> PCgetProjectName(iProject input)
        {
            List<mProject> results = new List<mProject>();

            using (
                SqlConnection connection =
                    new SqlConnection(
                        WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT  MCSAPP.DBO.SF_FULLNAME(P.PJ_ID) PJ_NAME,P.PJ_ID 
                                            FROM 
                                            (SELECT P.PJ_ID, P.PD_ITEM ,0 AS REV_NO FROM MCSAPP..PRODUCT P  
                                            UNION 
                                            SELECT P.PJ_ID,P.ITEM  ,MAX(REV_NO) REV_NO FROM MCSAPP..REV_PLAN P 
                                            GROUP BY P.PJ_ID,P.ITEM) P 
				                            WHERE NOT EXISTS
              			                    (SELECT *
              			                    FROM NCR_CHECK NC    
						                    WHERE NC.PC_ID = @TYPE_PC And NC.NC_STATUS < 0
              			                    AND P.PJ_ID = CAST(LEFT(NC.BARCODE,5) AS INT)  AND P.PD_ITEM = CAST(RIGHT(NC.BARCODE,4) AS INT)   AND P.REV_NO = NC.REV_NO)   
			                                ORDER BY PJ_NAME";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mProject()
                        {
                            PJ_ID = string.Format("{0:00000}", (int) dr["PJ_ID"]),
                            PJ_NAME = (string) dr["PJ_NAME"]
                        });
                    }
                }

                connection.Close();
            }

            return results;
        }

        #endregion

        #region List ไอเทม

        public List<mItemPj> PCgetProjectItem(iItemPj input)
        {
            List<mItemPj> results = new List<mItemPj>();

            using (
                SqlConnection connection =
                    new SqlConnection(
                        WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT P.PD_ITEM AS ITEM
                                               FROM 
                                                (SELECT P.PJ_ID, P.PD_ITEM ,0 AS REV_NO FROM MCSAPP..PRODUCT P  
                                                 UNION
                                                 SELECT P.PJ_ID,P.ITEM  ,MAX(REV_NO) REV_NO FROM MCSAPP..REV_PLAN P 
                                                 GROUP BY P.PJ_ID,P.ITEM) P 
                                             WHERE NOT EXISTS
                	                            (SELECT *
                			                            FROM NCR_CHECK NC    
                                                   WHERE NC.PC_ID = @TYPE_PC And NC.NC_STATUS < 0
                	                                AND MCSAPP.DBO.SF_BARCODE(P.PJ_ID,P.PD_ITEM) = NC.BARCODE AND P.REV_NO = NC.REV_NO) 
                                             AND P.PJ_ID = @PJ_ID
                                             ORDER BY PD_ITEM";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    command1.Parameters.Add("@PJ_ID", SqlDbType.Int, 5).Value = input.nPj_Id;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mItemPj()
                        {
                            PD_ITEM = dr["ITEM"].ToString()
                        });
                    }
                }

                connection.Close();
            }

            return results;
        }

        #endregion

        #region List Rev No

        public List<mRevNoPlanCheck> PCgetProjectRevno(iRevNoPlanCheck input)
        {
            List<mRevNoPlanCheck> results = new List<mRevNoPlanCheck>();

            using (
                SqlConnection connection =
                    new SqlConnection(
                        WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT P.REV_NO,
                                               CASE WHEN P.REV_NO = 0 THEN 'FINAL' 
                                               ELSE CAST(P.REV_NO AS VARCHAR) END AS REV_NAME
                                                 FROM 
                                                  (SELECT P.PJ_ID, P.PD_ITEM ,0 AS REV_NO FROM MCSAPP..PRODUCT P 
                                                   UNION 
                                                   SELECT P.PJ_ID,P.ITEM  ,MAX(REV_NO) REV_NO FROM MCSAPP..REV_PLAN P 
                                                   GROUP BY P.PJ_ID,P.ITEM) P 
                                               WHERE NOT EXISTS
              	                                (SELECT * 
              			                        FROM NCR_CHECK NC    
                                                WHERE NC.PC_ID = @TYPE_PC And NC.NC_STATUS < 0
              	                                AND MCSAPP.DBO.SF_BARCODE(P.PJ_ID,P.PD_ITEM) = NC.BARCODE AND P.REV_NO = NC.REV_NO) 
                                                AND P.PJ_ID = @PJ_ID
                                                AND P.PD_ITEM = @PD_ITEM
                                                ORDER BY REV_NO";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    command1.Parameters.Add("@PJ_ID", SqlDbType.Int, 5).Value = input.nPj_Id;
                    command1.Parameters.Add("@PD_ITEM", SqlDbType.Int, 4).Value = input.nPd_Item;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mRevNoPlanCheck()
                        {
                            REV_NO = (int) dr["REV_NO"],
                            REV_NAME = dr["REV_NAME"].ToString()
                        });
                    }
                }

                connection.Close();
            }
            return results;
        }

        #endregion

        #region List Group Fab & Weld

        public List<mGroupFab> PCgetGroupFab()
        {
            List<mGroupFab> results = new List<mGroupFab>();

            McsQcDataContext db = new McsQcDataContext();

            var queryFabGroup =
            (from f in db.V301_MainProducts
                group f by f.F_GRP
                into Fabgroup
                orderby Fabgroup.Key
                select Fabgroup.Key);

            foreach (var items in queryFabGroup)
            {
                results.Add(new mGroupFab()
                {
                    F_GRP = items
                });
            }
            return results;
        }

        public List<mGroupWeld> PCgetGroupWeld()
        {
            List<mGroupWeld> results = new List<mGroupWeld>();

            McsQcDataContext db = new McsQcDataContext();

            var queryWeldGroup =
            (from w in db.V301_MainProducts
                group w by w.W_GRP
                into Weldgroup
                orderby Weldgroup.Key
                select Weldgroup.Key);
            foreach (var items in queryWeldGroup)
            {
                results.Add(new mGroupWeld()
                {
                    W_GRP = items
                });
            }
            return results;
        }

        #endregion

        #region List สถานะ

        public List<mStatus> PCgetStatus()
        {
            List<mStatus> results = new List<mStatus>();

            string[] name = new string[]
                {"0: QC Check", "1: QC Confirm to Repair", "2: PD Repair", "3: QC Recheck", "4: Not Pass", "5: OK"};
            int i = 0;

            foreach (string value in name)
            {
                results.Add(new mStatus()
                {
                    STATUS_ID = (int) i++,
                    STATUS_NAME = (string) value
                });
            }
            return results;
        }

        #endregion

        #region แสดงข้อมูล ncr plan check

        public List<mPjShowListCheck> PCgetProjectShowListCheck(iPjShowListCheck input)
        {
            List<mPjShowListCheck> results = new List<mPjShowListCheck>();

            try
            {
                object[] prm =
                {
                    input.nTypePc, input.nPj_Id, input.nPd_Item, input.sRev_No, input.sCode, input.sGroupFab,
                    input.sGroupWeld, input.sFab_Date, input.sWeld_Date, input.sNc_Status
                };

                var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_PLAN_NCR_CHECK", prm);

                while (reader.Read())
                {
                    results.Add(new mPjShowListCheck()
                    {
                        PJ_NAME = reader["PJ_NAME"].ToString(),
                        PD_ITEM = string.Format("{0:0000}", (Int16) reader["PD_ITEM"]),
                        FW_REV = reader["FW_REV"].ToString(),
                        PD_CODE = reader["PD_CODE"].ToString(),
                        PART = reader["PART"].ToString(),
                        F_GRP = reader["F_GRP"].ToString(),
                        F_DATE = reader["F_DATE"].ToString(),
                        W_GRP = reader["W_GRP"].ToString(),
                        W_DATE = reader["W_DATE"].ToString(),
                        OA_GRP = reader["OA_GRP"].ToString(),
                        OA_DATE = reader["OA_DATE"].ToString(),
                        CHECK_NO = reader["CHECK_NO"].ToString(),
                        COUNT_NCR = reader["COUNT_NCR"].ToString(),
                        DESIGN_CHANGE = reader["DESIGN_CHANGE"].ToString(),
                        NCR_STATUS = reader["NCR_STATUS"].ToString(),
                        USERS = reader["USERS"].ToString(),
                        NCR_UPDATE = reader["NCR_UPDATE"].ToString(),
                        PLACE = reader["PLACE"].ToString()
                    });
                }
            }
            catch (Exception)
            {
                throw;
            }
            return results;
        }

        #endregion


        // IN Form NCR------------------------------------------------------------------------------------------------

        #region ListData InFormNcr

        public mListDataForm IFgetListDataForm(iListDataForm input)
        {
            McsQcDataContext db = new McsQcDataContext();

            mListDataForm results = new mListDataForm();
            results.getDwgType = new List<mProjectImage>();
            results.getDirection = new List<mDirection>();

            #region List DWG Type

            if (input.nPt_MainType > 0)
            {
                var queryPjImageT = (from p in db.NCR_FROM_IMGs
                    orderby p.POSITION_ID descending
                    where 1 == 1 && p.PT_GROUP_TYPE_ID == input.nPt_MainType || p.PT_GROUP_TYPE_ID == 0
                    select p);
                foreach (var items_position in queryPjImageT)
                {
                    results.getDwgType.Add(new mProjectImage()
                    {
                        IMAGE_NAME = items_position.IMG_NAME.ToString(),
                        POSITION_ID = items_position.POSITION_ID.ToString()
                    });
                }
            }
            else
            {
                var queryPjImage = (from p in db.NCR_FROM_IMGs where 1 == 1 select p);
                foreach (var items_position in queryPjImage)
                {
                    results.getDwgType.Add(new mProjectImage()
                    {
                        IMAGE_NAME = items_position.IMG_NAME.ToString(),
                        POSITION_ID = items_position.POSITION_ID.ToString()
                    });
                }
            }

            #endregion

            #region List Direction

            foreach (vt_direction items_dt in db.vt_directions.OrderBy(d => d.drId))
            {
                results.getDirection.Add(new mDirection()
                {
                    DRNAME = items_dt.drName
                });
            }

            #endregion

            return results;
        }

        #endregion

        #region List Rev no

        public List<mRevNoInForm> IFgetProjectRevNo(iRevNoInForm input)
        {
            List<mRevNoInForm> results = new List<mRevNoInForm>();

            int project = 0;
            int items = 0;
            int point = input.sBarcode.IndexOf("-");

            if (point == -1)
            {
                project = 0;
                items = 0;
            }
            else
            {
                project = Convert.ToInt32(input.sBarcode.Substring(0, point));
                items = Convert.ToInt16(input.sBarcode.Substring(point + 1));
            }


            using (
                SqlConnection connection =
                    new SqlConnection(
                        WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT P.REV_NO,
                                                       CASE WHEN P.REV_NO = 0 THEN 'FINAL' 
                                                       ELSE CAST(P.REV_NO AS VARCHAR) END AS REV_NAME
                                                         FROM 
                                                          (SELECT P.PJ_ID, P.PD_ITEM ,0 AS REV_NO FROM MCSAPP..PRODUCT P 
                                                           UNION 
                                                           SELECT P.PJ_ID,P.ITEM  ,MAX(REV_NO) REV_NO FROM MCSAPP..REV_PLAN P 
                                                           GROUP BY P.PJ_ID,P.ITEM) P 
                                                       WHERE NOT EXISTS
                      	                                (SELECT * 
                      			                        FROM NCR_CHECK NC    
                                                        WHERE NC.PC_ID = @TYPE_PC And NC.NC_STATUS > 1  AND CHECK_NO = @CHECK_NO
                      	                                AND MCSAPP.DBO.SF_BARCODE(P.PJ_ID,P.PD_ITEM) = NC.BARCODE AND P.REV_NO = NC.REV_NO) 
                                                        AND P.PJ_ID = @PJ_ID
                                                        AND P.PD_ITEM = @PD_ITEM
                                                        ORDER BY REV_NO";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    command1.Parameters.Add("@CHECK_NO", SqlDbType.Int, 2).Value = input.nCheck_no;
                    command1.Parameters.Add("@PJ_ID", SqlDbType.Int, 5).Value = project;
                    command1.Parameters.Add("@PD_ITEM", SqlDbType.Int, 4).Value = items;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mRevNoInForm()
                        {
                            REV_NO = (string) dr["REV_NO"].ToString(),
                            REV_NAME = dr["REV_NAME"].ToString()
                        });
                    }
                }

                connection.Close();
            }
            return results;
        }

        #endregion

        #region get Project Detial

        public mChProjectDetail IFgetProjectDetail(iChProjectDetail input)
        {
            McsAppDataContext db = new McsAppDataContext();
            McsQcDataContext qc = new McsQcDataContext();
            int project = 0;
            int items = 0;
            int point = input.sBarcode.IndexOf("-");

            if (point == -1)
            {
                project = 0;
                items = 0;
            }
            else
            {
                project = Convert.ToInt32(input.sBarcode.Substring(0, point));
                items = Convert.ToInt16(input.sBarcode.Substring(point + 1));
            }

            mChProjectDetail results = new mChProjectDetail();

            try
            {
                if (project != 0)
                {
                    Project items_pj = db.Projects.Where(pj => pj.pj_id == project).FirstOrDefault();

                    if (items_pj != null)
                    {
                        results.PJ_NAME = items_pj.pj_name.ToString() + "-s" + items_pj.pj_sharp + "-Lot" +
                                          items_pj.pj_lot;
                    }
                    else
                    {
                        results.PJ_NAME = "====";
                    }

                    NCR_CHECK item_nc =
                        qc.NCR_CHECKs.Where(
                            nc =>
                                nc.PC_ID == input.nTypePc && nc.BARCODE == input.sBarcode &&
                                nc.REV_NO == input.nRev_no && nc.CHECK_NO == 1).FirstOrDefault();

                    if (item_nc != null)
                    {
                        results.NC_NCR = (int) item_nc.NC_NCR;
                        results.PLACE = (string) item_nc.PLAN_INS;
                    }
                    else
                    {
                        results.NC_NCR = 0;
                        results.PLACE = "";
                    }

                    if (results.PLACE == "")
                    {
                        Product place =
                            db.Products.Where(p => p.pj_id == project && p.pd_item == items).FirstOrDefault();
                        if (place != null)
                        {
                            results.PLACE = (string) place.place;
                        }
                        else
                        {
                            results.PLACE = "";
                        }
                    }
                }

                object[] prm = {input.nTypePc, project, items, input.nRev_no};
                var reader1 = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_CHECK_PRODUCT_DETAIL", prm);

                string pa_state = "";

                if (reader1.HasRows)
                {
                    while (reader1.Read())
                    {
                        pa_state = reader1["PA_STATE"].ToString();
                    }

                    if (pa_state == "1")
                    {
                        var reader2 = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_CHECK_PRODUCT_DETAIL", prm);
                        while (reader2.Read())
                        {
                            results.ERROR = (string) reader2["ERROR"];
                            results.PLAN_DATE = ":: No Plan Date ::";
                            results.PD_CODE = ":: No Code ::";
                            results.PT_MAINTYPE = "0";
                            results.BARCODE = "";
                        }
                    }
                    else
                    {
                        var reader2 = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_CHECK_PRODUCT_DETAIL", prm);
                        while (reader2.Read())
                        {
                            results.PLAN_DATE = Convert.ToDateTime(reader2["PLAN_DATE"]).ToString("yyyy-MM-dd HH:mm");
                            results.PD_CODE = (string) reader2["PD_CODE"];
                            results.PT_MAINTYPE = reader2["PT_MAINTYPE"].ToString();
                            results.BARCODE = (string) reader2["BARCODE"];
                            results.ERROR = "";
                        }
                    }
                }
                else
                {
                    results.PLAN_DATE = "";
                    results.PD_CODE = "";
                    results.PT_MAINTYPE = "";
                    results.BARCODE = "";
                    results.ERROR = "";
                    results.PJ_NAME = "";
                    results.NC_NCR = 0;
                    results.UG_NAME = "";
                    results.PLACE = "";
                }
            }
            catch (Exception)
            {
                throw;
            }
            return results;
        }

        #endregion

        #region List ตำแหน่ง

        public List<mPosition> IFgetPositionList(iPosition input)
        {
            List<mPosition> results = new List<mPosition>();

            McsQcDataContext db = new McsQcDataContext();

            var queryNcId = from nc in db.NCR_CHECK_LISTs
                where nc.PT_GROUP_TYPE_ID == input.nPt_MainType
                select nc.NT_ID;
            var queryPosition = from cl in db.NCR_CHECK_LIST_TYPEs
                where cl.PC_ID == input.nTypePc && queryNcId.Contains(cl.NT_ID)
                select cl;

            foreach (var items in queryPosition)
            {
                results.Add(new mPosition()
                {
                    NT_NAME = items.NT_NAME,
                    NT_ID = items.NT_ID.ToString()
                });
            }
            return results;
        }

        #endregion

        #region List ความผิดพลาด

        public List<mDescriptions> IFgetDescriptionsNcr(iDescriptions input)
        {
            List<mDescriptions> results = new List<mDescriptions>();

            McsQcDataContext db = new McsQcDataContext();

            var queryDescNce = from cl in db.NCR_CHECK_LISTs
                join nd in db.NCR_DESCs
                on cl.DESC_ID equals nd.DESC_ID
                where cl.PT_GROUP_TYPE_ID == input.nPt_MainType && cl.NT_ID == input.nNt_Id
                orderby nd.DESCRIPTIONS
                select new {cl.NCR_NO, nd.DESCRIPTIONS};

            foreach (var items in queryDescNce)
            {
                results.Add(new mDescriptions()
                {
                    DESCRIPTIONS = items.DESCRIPTIONS,
                    NCR_NO = items.NCR_NO.ToString()
                });
            }
            return results;
        }

        #endregion

        public mgetOther IFgetOther(igetOther input)
        {
            mgetOther result = new mgetOther();

            McsQcDataContext db = new McsQcDataContext();

            NCR_DIM_CHECK icheck =
                db.NCR_DIM_CHECKs.Where(nd => nd.NC_ID == input.sNc_id && nd.NCR_NO == input.sNcr_no).FirstOrDefault();

            if (icheck == null)
            {
                result.OTHER_NO = "1";
            }
            else
            {
                var query = (from nd in db.NCR_DIM_CHECKs
                                where nd.NC_ID == input.sNc_id && nd.NCR_NO == input.sNcr_no
                                select nd.OTHER_NO).Max() + 1;

                result.OTHER_NO = query.ToString();
            }
            return result;
        }


        #region แสดงกรุ๊ปที่รับผิดชอบ

        public mResponse IFgetResponse(iResponse input)
        {
            mResponse results = new mResponse();

            int project = 0;
            int items = 0;
            int point = input.sBarcode.IndexOf("-");

            if (point == -1)
            {
                project = 0;
                items = 0;
            }
            else
            {
                project = Convert.ToInt32(input.sBarcode.Substring(0, point));
                items = Convert.ToInt16(input.sBarcode.Substring(point + 1));
            }

            try
            {
                string group = "";
                string process;

                McsQcDataContext db = new McsQcDataContext();

                NCR_CHECK_LIST icheck = db.NCR_CHECK_LISTs.Where(nc => nc.NCR_NO.Equals(input.sNcr_no)).FirstOrDefault();

                if (icheck == null)
                {
                    group = "";
                    results.DESC_CHK = 0;
                }
                else
                {
                    group = icheck.RESPONS.ToString();
                    results.DESC_CHK = (int) icheck.DESC_CHK;
                }

                if (group != "")
                {
                    process = Position_Ncr_Process(input.sNcr_no);
                    if (Show_Group(project, items, process, 0) != "")
                    {
                        group += " / ";
                    }
                    group += Show_Group(project, items, process, 0);
                }
                else if (group == "")
                {
                    process = Position_Ncr_Process(input.sNcr_no);
                    group += Show_Group(project, items, process, 0);
                }
                results.RESPONSE_GROUP = group;
            }
            catch (Exception)
            {
                throw;
            }
            return results;
        }

        private string Position_Ncr_Process(string sNcr_no)
        {
            string process = "";
            string value_pc = "";

            McsQcDataContext db = new McsQcDataContext();

            foreach (NCR_RESPON items in db.NCR_RESPONs.Where(nc => nc.NCR_NO.Equals(sNcr_no)))
            {
                value_pc = items.PC_ID.ToString();
            }

            if (process != "")
            {
                process += "," + value_pc.ToString();
            }
            else
            {
                process = value_pc.ToString();
            }
            return process;
        }

        private string Show_Group(int nPj_Id, int nPd_Item, string sTypePc, int nRev_no)
        {
            string Group = "";
            string Value_g = "";
            //int i;
            string[] Str = sTypePc.Split(',');
            for (int i = 0; i < Str.Length; i++)
            {
                McsAppDataContext db = new McsAppDataContext();

                if (Str[i].Substring(0, 1) == "5")
                {
                    if (nRev_no == 0)
                    {
                        var query = from GFW in db.SF_GET_GROUP_FAB_WELD(nPj_Id, nPd_Item, nRev_no, Str[i])
                            select new {group_name = GFW.ToString()};

                        foreach (var item_g in query)
                        {
                            Value_g += item_g.group_name.ToString();
                        }

                        if (Group != "")
                        {
                            Group += " / " + Value_g.ToString();
                        }
                        else
                        {
                            Group += Value_g.ToString();
                        }
                    }
                }
                else if (Str[i].Substring(0, 1) == "1")
                {
                    var query = from GB in db.SF_GET_GROUP_BUILT(nPj_Id, nPd_Item, Str[i]) select new {group_name = GB};

                    foreach (var items in query)
                    {
                        Value_g = items.group_name.ToString();
                    }

                    if (Group != "")
                    {
                        Group += " / " + Value_g.ToString();
                    }
                    else
                    {
                        Group += Value_g.ToString();
                    }
                }
            }
            return Group;
        }

        #endregion

        #region แจ้ง NCR  : แบบมี NCR

        public mResult IFsetAddNcrCheck(iNcrCheckAdd input)
        {
            mResult result = new mResult();

            try
            {
                McsQcDataContext db = new McsQcDataContext();
                McsFg2010DataContext fg = new McsFg2010DataContext();

                var icheck = (from nd in db.NCR_DIM_CHECKs
                    join nc in db.NCR_CHECKs on nd.NC_ID equals nc.NC_ID
                    where
                    nc.BARCODE == input.sBarcode && nc.REV_NO == input.nRev_no && nc.CHECK_NO == input.nCheck_no &&
                    nd.NCR_NO == input.sNcr_no && nd.OTHER_NO == input.nOther_no
                    select nd).FirstOrDefault();
                //เช็ค NCR นี้เคยส่งหรือยัง
                if (icheck == null)
                {
                    //เช็คพื้นที่
                    var check_st =
                        (from ss in fg.stock_shelfs where ss.place == input.sPlanIns select ss.place).FirstOrDefault();
                    if (check_st != null)
                    {
                        //เช็ครูปภาพ
                        if (input.Get_Image == true)
                        {
                            string sImage_Name = input.sNc_id + input.sNcr_no + "-" + input.nOther_no + ".jpg";

                            object[] prm =
                            {
                                input.nTypePc, input.sBarcode, input.nRev_no, input.nCheck_no, input.sNcr_no,
                                input.sUser, input.sUser, input.sGroup, sImage_Name, input.sSug, input.sDirection,
                                input.nDwg_type, input.sPlanDate, input.sPlanIns, input.Dwg, input.Actual, 1
                            };

                            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_SET_NCR_CHECK", prm);

                            while (reader.Read())
                            {
                                result.RESULT = reader["ERROR"].ToString();
                                result.EXCEPTION = "";
                            }
                        }
                        else
                        {
                            object[] prm =
                            {
                                input.nTypePc, input.sBarcode, input.nRev_no, input.nCheck_no, input.sNcr_no,
                                input.sUser, input.sUser, input.sGroup, "No.jpg", input.sSug, input.sDirection,
                                input.nDwg_type, input.sPlanDate, input.sPlanIns, input.Dwg, input.Actual, 1
                            };

                            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_SET_NCR_CHECK", prm);

                            while (reader.Read())
                            {
                                result.RESULT = reader["ERROR"].ToString();
                                result.EXCEPTION = "";
                            }
                        }
                    }
                    else
                    {
                        result.RESULT = "กรุณาป้อนข้อมูลพื้นที่ให้ถูกต้อง";
                        result.EXCEPTION = "";
                    }
                }
                else
                {
                    result.RESULT = "ความผิดพลาด(NCR)นี้แจ้งไปแล้ว";
                    result.EXCEPTION = "";
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }

        #endregion

        #region แก้ไข NCR

        public mResult IFsetEditNcrCheck(iNcrCheckEdit input)
        {
            mResult result = new mResult();

            DateTime now = DateTime.Now;

            try
            {
                McsQcDataContext db = new McsQcDataContext();
                McsFg2010DataContext fg = new McsFg2010DataContext();

                var check_st =
                    (from ss in fg.stock_shelfs where ss.place == input.sPlace select ss.place).FirstOrDefault();
                if (check_st != null)
                {

                    var query_nc = (from nc in db.NCR_CHECKs
                        where nc.PC_ID == input.nTypePc && nc.BARCODE == input.sBarcode &&
                              nc.REV_NO == input.nRev_No && nc.CHECK_NO == input.nCheck_No
                        select nc).FirstOrDefault();

                    var query_nd = (from nd in db.NCR_DIM_CHECKs
                        where nd.NCR_NO == input.sNcr_No && nd.OTHER_NO == input.nOther_no &&
                              (from nc in db.NCR_CHECKs
                                  where
                                  nc.BARCODE == input.sBarcode && nc.REV_NO == input.nRev_No &&
                                  nc.CHECK_NO == input.nCheck_No
                                  select new {nc.NC_ID}).Contains(new {NC_ID = nd.NC_ID})
                        select nd).FirstOrDefault();


                    if (input.Get_Image == true)
                    {
                        query_nd.IMG_BEFORE = input.sFile;
                        query_nd.USER_UPDATE = input.sUsers;
                        query_nd.TIME_UPDATE = (DateTime) (now);
                        query_nd.SUGGEST = input.sSug;
                        query_nd.DIRECTION = input.sDirection;
                        query_nd.GROUP_NAME = input.sGroup;
                        query_nd.DWG = input.Dwg;
                        query_nd.ACTUAL = input.Actual;
                        query_nc.PLAN_INS = input.sPlace;
                    }
                    else
                    {
                        query_nd.IMG_BEFORE = input.sFile;
                        query_nd.USER_UPDATE = input.sUsers;
                        query_nd.TIME_UPDATE = (DateTime) (now);
                        query_nd.SUGGEST = input.sSug;
                        query_nd.DIRECTION = input.sDirection;
                        query_nd.GROUP_NAME = input.sGroup;
                        query_nd.DWG = input.Dwg;
                        query_nd.ACTUAL = input.Actual;
                        query_nc.PLAN_INS = input.sPlace;
                    }

                    db.SubmitChanges();

                    result.RESULT = "1|แก้ไขข้อมูลเรียบร้อยแล้ว";
                    result.EXCEPTION = "";
                }
                else
                {
                    result.RESULT = "กรุณาป้อนข้อมูลพื้นที่ให้ถูกต้อง";
                    result.EXCEPTION = "";
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถทำการแก้ไขข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }

        #endregion

        #region ลบ NCR

        public mResult IFsetDeleteNcrCheck(iNcrCheckDelete input)
        {
            mResult result = new mResult();

            DateTime now = DateTime.Now;

            try
            {
                McsQcDataContext db = new McsQcDataContext();

                if (input.sImage_Before != "No.jpg")
                {
                    string img = DeleteImage(input.sImage_Before);
                }

                var Del_Ncr = from nd in db.NCR_DIM_CHECKs
                    where nd.NC_ID == input.sNc_Id && nd.NCR_NO == input.sNcr_No && nd.OTHER_NO == input.nOther_no
                    select nd;

                foreach (var items in Del_Ncr)
                {
                    db.NCR_DIM_CHECKs.DeleteOnSubmit(items);
                }

                db.SubmitChanges();

                NCR_DIM_CHECK icheck = db.NCR_DIM_CHECKs.Where(nd => nd.NC_ID == input.sNc_Id).FirstOrDefault();

                if (icheck == null)
                {
                    NCR_CHECK ncr_check = db.NCR_CHECKs.Where(nc => nc.NC_ID == input.sNc_Id).FirstOrDefault();

                    ncr_check.NC_NCR = 0;
                    ncr_check.NC_STATUS = 1;
                    ncr_check.USER_UPDATE = input.sUser;
                    ncr_check.TIME_UPDATE = (DateTime) (now);
                }

                db.SubmitChanges();

                result.RESULT = "ลบรายการแจ้งความผิดพลาด(NCR)เรียบร้อยแล้วค่ะ.";
                result.EXCEPTION = "";

                return result;

            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถลบรายการแจ้งความผิดพลาด(NCR)ได้ค่ะ.";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }

        #endregion

        #region อัพโหลดรูปภาพ

        public mResult uploadImages_QC(Stream fileUpload)
        {
            mResult result = new mResult();

            try
            {
                McsAppDataContext dc = new McsAppDataContext();

                string servPath = "";
                string fileSize = "";

                int point = 0;
                int width = 0;
                int height = 0;

                foreach (PDPC_config items in dc.PDPC_configs)
                {
                    if (items.config_key.Equals("PATH_BEFORE")) servPath = items.config_value;
                    if (items.config_key.Equals("PICTURE")) fileSize = items.config_value;
                }

                if (fileSize != "")
                {
                    point = fileSize.IndexOf("x");
                    width = Convert.ToInt32(fileSize.Substring(0, point));
                    height = Convert.ToInt32(fileSize.Substring(point + 1));
                }

                //------------------------------------------------------------------------------------------------------

                var parser = new MultipartFormDataParser(fileUpload);

                var username = parser.Parameters["username"].Data;
                var barcode = parser.Parameters["barcode"].Data;
                var otherno = parser.Parameters["otherno"].Data;
                var imagename = parser.Parameters["imagename"].Data;

                var file = parser.Files.FirstOrDefault();

                Stream stream = file.Data;

                //------------------------------------------------------------------------------------------------------

                if (!Directory.Exists(servPath)) Directory.CreateDirectory(servPath);

                FileStream targetStream = null;
                Stream sourceStream = stream;

                string fileName = imagename + "-" + otherno + "-X.jpeg";
                string filePath = Path.Combine(servPath, fileName);

                using (targetStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    const int bufferLen = 4096;
                    byte[] buffer = new byte[bufferLen];
                    int count = 0;
                    int totalBytes = 0;

                    while ((count = sourceStream.Read(buffer, 0, bufferLen)) > 0)
                    {
                        totalBytes += count;

                        targetStream.Write(buffer, 0, count);
                    }

                    targetStream.Close();

                    sourceStream.Close();
                }

                resizeImage(filePath, width, height);

                result.RESULT = "อัพโหลดรูปภาพเรียบร้อยแล้ว";
                result.EXCEPTION = "";
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถอัพโหลดรูปภาพได้";
                result.EXCEPTION = ex.Message;
            }

            return result;
        }

        #endregion

        #region Delete Image

        private string DeleteImage(string img_name)
        {
            string result = "";
            string servPath = "";
            try
            {
                McsAppDataContext db = new McsAppDataContext();

                foreach (PDPC_config items in db.PDPC_configs)
                {
                    if (items.config_key.Equals("PATH_BEFORE")) servPath = items.config_value;
                }

                string filePath = Path.Combine(servPath, img_name);

                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                    result = "ลบรูปภาพแล้ว";
                }
                else
                {
                    result = "ไม่มีรูปภาพ";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }

        #endregion

        #region ส่ง Ncr : แบบไม่มี NCR

        public mResult IFsetNoNcrCheck(iNoNcrCheck input)
        {
            mResult result = new mResult();
            McsFg2010DataContext fg = new McsFg2010DataContext();

            try
            {
                var check_st =
                    (from ss in fg.stock_shelfs where ss.place == input.sPlanIns select ss.place).FirstOrDefault();
                if (check_st != null)
                {
                    if (input.sBarcode != "" && input.sPlanDate != "")
                    {
                        object[] prm =
                        {
                            input.nTypePc, input.sBarcode, input.nRev_no, input.nCheck_no, "NULL",
                            input.sUser, input.sUser, "NULL", "NULL", "NULL", "NULL", input.nDwg_type, input.sPlanDate,
                            input.sPlanIns, null, null, 5
                        };

                        var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_SET_NCR_CHECK", prm);

                        while (reader.Read())
                        {
                            result.RESULT = reader["ERROR"].ToString();
                            result.EXCEPTION = "";
                        }
                    }
                    else
                    {
                        result.RESULT = "ไม่สามารถบันทึกข้อมูลได้";
                        result.EXCEPTION = "";
                    }
                }
                else
                {
                    result.RESULT = "กรุณาป้อนข้อมูลพื้นที่ให้ถูกต้อง";
                    result.EXCEPTION = "";
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }

        #endregion

        #region แสดงรายการ NCR

        public List<mNcrReport> IFgetNcrReport(iNcrReport input)
        {
            string other_no = "";
            List<mNcrReport> results = new List<mNcrReport>();

            object[] prm = {input.sNc_Id, input.sNc_State, "", "", "", "", ""};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_REPORT_NCR", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    other_no = reader["OTHER_NO"].ToString();
                }

                if (other_no != "")
                {
                    var reader1 = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_REPORT_NCR", prm);

                    while (reader1.Read())
                    {
                        results.Add(new mNcrReport
                        {
                            NCR_NO = reader1["NCR_NO"].ToString(),
                            DIRECTION = reader1["DIRECTION"].ToString(),
                            POSITION = reader1["NT_NAME"].ToString(),
                            DESCRIPTIONS = reader1["DESCRIPTIONS"].ToString(),
                            IMG_BEFORE = reader1["IMG_BEFORE"].ToString(),
                            RESPONSE = reader1["GROUP_NAME"].ToString(),
                            DWG = reader1["DWG"].ToString(),
                            ACTUAL = reader1["ACTUAL"].ToString(),
                            OTHER_NO = (int) reader1["OTHER_NO"],
                            SUGGEST = reader1["SUGGEST"].ToString(),
                            PLACE = reader1["PLACE"].ToString()
                        });
                    }
                }
                else
                {
                    results.Add(new mNcrReport
                    {
                        NCR_NO = "-",
                        DIRECTION = "-",
                        POSITION = "-",
                        DESCRIPTIONS = "-",
                        IMG_BEFORE = "-",
                        RESPONSE = "-",
                        DWG = "-",
                        ACTUAL = "-",
                        OTHER_NO = 0,
                        SUGGEST = "-",
                        PLACE = "",
                    });
                }
            }
            else
            {
                results.Add(new mNcrReport
                {
                    NCR_NO = "-",
                    DIRECTION = "-",
                    POSITION = "-",
                    DESCRIPTIONS = "-",
                    IMG_BEFORE = "-",
                    RESPONSE = "-",
                    DWG = "-",
                    ACTUAL = "-",
                    OTHER_NO = 0,
                    SUGGEST = "-",
                    PLACE = ""
                });
            }
            return results;
        }

        #endregion

        #region แสดง Ncr Type

        public mNcrType IFgetNcrType(iNcrType input)
        {
            mNcrType results = new mNcrType();

            McsQcDataContext db = new McsQcDataContext();

            NCR_CHECK_LIST icheck = db.NCR_CHECK_LISTs.Where(nl => nl.NCR_NO == input.sNcr_No).FirstOrDefault();

            if (icheck == null)
            {
                results.NT_ID = "====";
            }
            else
            {
                results.NT_ID = icheck.NT_ID.ToString();
            }
            return results;
        }

        #endregion

        #region ยืนยันการแจ้ง NCR

        public mResult IFsetConfirmNcr(iConfirmNcr input)
        {
            mResult result = new mResult();

            DateTime now = DateTime.Now;

            try
            {
                McsQcDataContext db = new McsQcDataContext();

                NCR_CHECK items =
                    db.NCR_CHECKs.Where(
                        nc =>
                            nc.PC_ID == input.nTypePc && nc.BARCODE == input.sBarcode && nc.REV_NO == input.nRev_No &&
                            nc.CHECK_NO == input.nCheck_No).FirstOrDefault();

                if (items != null)
                {
                    items.NC_STATUS = 1;
                    items.DATE_FINISH = (DateTime) (now);
                    items.USER_UPDATE = input.sUser;
                    items.TIME_UPDATE = (DateTime) (now);

                    db.SubmitChanges();

                    result.RESULT = "1|ยืนยันการแจ้งความผิดพลาด(NCR)เรียบร้อยแล้วค่ะ.";
                    result.EXCEPTION = "";

                    return result;
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "2|ยืนยันการแจ้งความผิดพลาด(NCR)ไม่สำเร็จค่ะ.";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }

        #endregion


        // Qc Recheck ----------------------------------------------------------------------------------------------

        #region แสดงข้อมูล NCR Recheck

        public mDataRecheck RCgetDataRecheck(iDataRecheck input)
        {
            mDataRecheck result = new mDataRecheck();

            // '9100001058-0001001"
            int nTypePc = Convert.ToInt32(input.sNc_Id.Substring(0, 5));
            string sBarcode = input.sNc_Id.Substring(5, 10);
            int nRev = Convert.ToInt32(input.sNc_Id.Substring(15, 2));
            int nCheckNo = Convert.ToInt32(input.sNc_Id.Substring(17, 1));

            object[] prm = {input.sNc_Id};

            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_REPORT_NCR_EDIT", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.PJ_NAME = reader["PJ_NAME"].ToString();
                    result.PD_ITEM = (Int16) reader["PD_ITEM"];
                    result.REV_NO = reader["REV_NO"].ToString();
                    result.CHECK_NO = nCheckNo;
                    result.PD_CODE = reader["PD_CODE"].ToString();
                    result.DATE_PLAN = reader["DATE_PLAN"].ToString();
                    result.USERNAME = reader["USERNAME"].ToString();
                    result.DATE_START = reader["DATE_START"].ToString();
                    result.BARCODE = reader["BARCODE"].ToString();
                    result.PT_MAINTYPE = (int) reader["PT_MAINTYPE"];
                }
            }
            else
            {
                result.PJ_NAME = "-";
                result.PD_ITEM = 0;
                result.REV_NO = "-";
                result.CHECK_NO = 0;
                result.PD_CODE = "-";
                result.DATE_PLAN = "-";
                result.USERNAME = "-";
                result.DATE_START = "-";
                result.BARCODE = "-";
                result.PT_MAINTYPE = 0;
            }
            return result;
        }

        #endregion

        #region แสดง Report NCR Recheck

        public List<mNcrReportRC> RCgetNcrReport(iNcrReportRC input)
        {
            List<mNcrReportRC> results = new List<mNcrReportRC>();
            string pc_id = input.sNc_Id.Substring(0, 5);
            string barcode = input.sNc_Id.Substring(5, 10);
            string rev_no = input.sNc_Id.Substring(15, 2);

            object[] prm = {input.sNc_Id, "3", "3", "", pc_id, barcode, rev_no};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_REPORT_NCR", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    results.Add(new mNcrReportRC()
                    {
                        NCR_NO = reader["NCR_NO"].ToString(),
                        NT_NAME = reader["NT_NAME"].ToString(),
                        DIRECTION = reader["DIRECTION"].ToString(),
                        DESCRIPTIONS = reader["DESCRIPTIONS"].ToString(),
                        GROUP_NAME = reader["GROUP_NAME"].ToString(),
                        FULLNAME_PD = reader["FULLNAME_PD"].ToString(),
                        DATE_PD = reader["DATE_PD"].ToString(),
                        DWG = reader["DWG"].ToString(),
                        ACTUAL = reader["ACTUAL"].ToString(),
                        SUGGEST = reader["SUGGEST"].ToString(),
                        OTHER_NO = reader["OTHER_NO"].ToString(),
                        POSITION_ID = reader["POSITION_ID"].ToString(),
                        PD_COMMENT = reader["PD_COMMENT"].ToString(),
                        PD_ACTUAL = reader["PD_ACTUAL"].ToString()
                    });
                }
            }
            else
            {
                results.Add(new mNcrReportRC()
                {
                    NCR_NO = "-",
                    NT_NAME = "",
                    DIRECTION = "",
                    DESCRIPTIONS = "",
                    GROUP_NAME = "",
                    FULLNAME_PD = "",
                    DATE_PD = "",
                    DWG = "",
                    ACTUAL = "",
                    SUGGEST = "",
                    OTHER_NO = "",
                    POSITION_ID = "",
                    PD_COMMENT = "",
                    PD_ACTUAL = ""
                });
            }
            return results;
        }

        #endregion

        #region Update Recheck OK

        public mResult RCsetRecheckOK(iRecheckOK input)
        {
            mResult result = new mResult();

            DateTime now = DateTime.Now;

            try
            {
                McsQcDataContext db = new McsQcDataContext();

                NCR_DIM_CHECK items =
                    db.NCR_DIM_CHECKs.Where(
                            nd => nd.NC_ID == input.sNc_Id && nd.NCR_NO == input.sNcr_No && nd.OTHER_NO == input.nOther_no)
                        .FirstOrDefault();

                if (items != null)
                {
                    items.ND_STATE = 5;
                    items.IMG_AFTER = (input.sFile == "") ? "No.jpg" : input.sFile;
                    items.QC_RECHECK = input.sUsers;
                    items.DATE_RECHECK = (DateTime) (now);
                    items.USER_UPDATE = input.sUsers;
                    items.TIME_UPDATE = (DateTime) (now);
                    items.ACTUAL = input.Actual;

                    db.SubmitChanges();

                    result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
                    result.EXCEPTION = "";

                    return result;
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }

        #endregion

        #region Update Recheck Not OK

        public mResult RCsetRecheckNotOK(iRecheckNotOK input)
        {
            mResult result = new mResult();

            DateTime now = DateTime.Now;

            string error = "";

            try
            {
                McsQcDataContext db = new McsQcDataContext();

                object[] prm =
                {
                    input.nTypePc, input.sBarcode, input.nRev_no, input.nCheck_no + 1, input.sNcr_no,
                    input.sUser, input.sUser, input.sGroup, (input.sFile == "") ? "No.jpg" : input.sFile, input.sSug,
                    input.sDirection, input.nDwg_type, input.sPlanDate, input.sPlanIns, input.Dwg, input.Actual, 1
                };
                var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_SET_NCR_CHECK", prm);

                while (reader.Read())
                {
                    error = reader["ERROR"].ToString();
                }

                if (error.Substring(0, 1) == "1")
                {
                    NCR_DIM_CHECK items =
                        db.NCR_DIM_CHECKs.Where(
                            nd =>
                                nd.NC_ID == input.sNc_Id && nd.NCR_NO == input.sNcr_no &&
                                nd.OTHER_NO == input.nOther_No).FirstOrDefault();

                    if (items != null)
                    {
                        items.ND_STATE = 4;
                        items.QC_RECHECK = input.sUser;
                        items.DATE_RECHECK = (DateTime) (now);
                        items.USER_UPDATE = input.sUser;
                        items.TIME_UPDATE = (DateTime) (now);

                        db.SubmitChanges();

                        result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
                        result.EXCEPTION = "";
                    }
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }

        #endregion

        #region Delete Ncr Recheck

        public mResult RCsetRecheckDel(iRecheckDel input)
        {
            mResult result = new mResult();

            try
            {
                McsQcDataContext db = new McsQcDataContext();

                if (input.sImg_Before == "None.jpg" || input.sImg_Before == null)
                {
                    result.RESULT = "ยังไม่ได้ทำการอัพโหลดรูปภาพ";
                    result.EXCEPTION = "";
                }
                else
                {
                    string img = DeleteImage(input.sImg_Before);

                    if (img == "T")
                    {

                        var Del_Ncr = from nd in db.NCR_DIM_CHECKs
                            where
                            nd.NC_ID == input.sNc_Id && nd.NCR_NO == input.sNcr_No && nd.OTHER_NO == input.nOther_No
                            select nd;

                        foreach (var items in Del_Ncr)
                        {
                            db.NCR_DIM_CHECKs.DeleteOnSubmit(items);
                        }

                        db.SubmitChanges();

                        result.RESULT = "ลบภาพความผิดพลาด(NCR)เรียบร้อยแล้ว";
                        result.EXCEPTION = "";

                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถลบข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }

        #endregion


        //Qc Finish Check 

        #region List ชื่อโปรเจค

        public List<mProject> FNgetProjectName(iProject input)
        {
            List<mProject> results = new List<mProject>();

            using (
                SqlConnection connection =
                    new SqlConnection(
                        WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT  MCSAPP.DBO.SF_FULLNAME(P.PJ_ID) PJ_NAME,P.PJ_ID 
                                            FROM 
                                            (SELECT P.PJ_ID, P.PD_ITEM ,0 AS REV_NO FROM MCSAPP..PRODUCT P  
                                            UNION 
                                            SELECT P.PJ_ID,P.ITEM  ,MAX(REV_NO) REV_NO FROM MCSAPP..REV_PLAN P 
                                            GROUP BY P.PJ_ID,P.ITEM) P 
				                            WHERE NOT EXISTS
              			                    (SELECT *
              			                    FROM NCR_CHECK NC    
						                    WHERE NC.PC_ID = @TYPE_PC And NC.NC_STATUS = 1
              			                    AND P.PJ_ID = CAST(LEFT(NC.BARCODE,5) AS INT)  AND P.PD_ITEM = CAST(RIGHT(NC.BARCODE,4) AS INT)   AND P.REV_NO = NC.REV_NO)   
			                                ORDER BY PJ_NAME";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mProject()
                        {
                            PJ_ID = string.Format("{0:00000}", (int) dr["PJ_ID"]),
                            PJ_NAME = (string) dr["PJ_NAME"]
                        });
                    }
                }

                connection.Close();
            }

            return results;
        }

        #endregion

        #region List ไอเทม

        public List<mItemPj> FNgetProjectItem(iItemPj input)
        {
            List<mItemPj> results = new List<mItemPj>();

            using (
                SqlConnection connection =
                    new SqlConnection(
                        WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT P.PD_ITEM AS ITEM
                                               FROM 
                                                (SELECT P.PJ_ID, P.PD_ITEM ,0 AS REV_NO FROM MCSAPP..PRODUCT P  
                                                 UNION
                                                 SELECT P.PJ_ID,P.ITEM  ,MAX(REV_NO) REV_NO FROM MCSAPP..REV_PLAN P 
                                                 GROUP BY P.PJ_ID,P.ITEM) P 
                                             WHERE EXISTS
                	                            (SELECT *
                			                            FROM NCR_CHECK NC    
                                                   WHERE NC.PC_ID = @TYPE_PC And NC.NC_STATUS = 1
                	                                AND MCSAPP.DBO.SF_BARCODE(P.PJ_ID,P.PD_ITEM) = NC.BARCODE AND P.REV_NO = NC.REV_NO) 
                                             AND P.PJ_ID = @PJ_ID
                                             ORDER BY PD_ITEM";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    command1.Parameters.Add("@PJ_ID", SqlDbType.Int, 5).Value = input.nPj_Id;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mItemPj()
                        {
                            PD_ITEM = dr["ITEM"].ToString()
                        });
                    }
                }

                connection.Close();
            }

            return results;
        }

        #endregion

        #region List Checker

        public List<mChecker> FNgetChecker(iChecker input)
        {
            List<mChecker> results = new List<mChecker>();

            using (
                SqlConnection connection =
                    new SqlConnection(
                        WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT USERS,MCSAPP.DBO.SF_GETNAMES(NC.USERS) USERNAME 
                                             FROM NCR_CHECK NC 
                                             WHERE NC.PC_ID = @TYPE_PC
                                             AND NC.NC_STATUS IN (1) AND NC.NC_NCR IN (1)
                                             AND LEN(USERS) = 7
                                             ORDER BY USERNAME";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mChecker()
                        {
                            USERS = (string) dr["USERS"],
                            FULLNAME = (string) dr["USERNAME"]
                        });
                    }
                }

                connection.Close();
            }
            return results;
        }

        #endregion

        #region แสดงรายการ Ncr Report List

        public List<mNcrReportFN> FNgetNcrReport(iNcrReportFN input)
        {
            List<mNcrReportFN> results = new List<mNcrReportFN>();

            object[] prm =
            {
                input.nTypePc, 1, input.nPj_Id, input.nPd_Item, input.sUser_Check, "0", input.sDate_Edit,
                input.sPd_Code, "", "", ""
            };

            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_REPORT_NCR_LIST", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    results.Add(new mNcrReportFN()
                    {
                        PJ_NAME = reader["PJ_NAME"].ToString(),
                        PD_ITEM = reader["PD_ITEM"].ToString(),
                        REV_NO = reader["REV_NO"].ToString(),
                        CHECK_NO = (int) reader["CHECK_NO"],
                        PD_CODE = reader["PD_CODE"].ToString(),
                        USERNAME = reader["USERNAME"].ToString(),
                        DATE_START = reader["DATE_START"].ToString(),
                        COUNT_NCR = reader["COUNT_NCR"].ToString(),
                        NC_NCR = Check((int) reader["NC_NCR"]),
                        BARCODE = reader["BARCODE"].ToString(),
                        NC_ID = reader["NC_ID"].ToString(),
                        PLACE = reader["PLACE"].ToString()
                    });
                }
            }
            else
            {
                results.Add(new mNcrReportFN()
                {
                    PJ_NAME = "-",
                    PD_ITEM = "-",
                    REV_NO = "-",
                    CHECK_NO = 0,
                    PD_CODE = "-",
                    USERNAME = "-",
                    DATE_START = "-",
                    COUNT_NCR = "-",
                    NC_NCR = "-",
                    BARCODE = "-",
                    NC_ID = "-",
                    PLACE = ""
                });
            }
            return results;
        }

        private string Check(int id)
        {
            string result = "";


            if (id == 0)
            {
                result = "ผ่าน";
            }
            else if (id == 1)
            {
                result = "ไม่ผ่าน";
            }
            else
            {
                result = "ไม่มีผล";
            }
            return result;
        }

        #endregion

        #region Update Qc finish Check

        public mResult FNsetNcrQcCheck(iNcrCheckFN input)
        {
            mResult result = new mResult();
            DateTime now = DateTime.Now;

            try
            {
                McsQcDataContext db = new McsQcDataContext();

                NCR_CHECK icheck = db.NCR_CHECKs.Where(nc => nc.NC_ID == input.sNc_Id).FirstOrDefault();

                if (icheck.NC_NCR >= 1)
                {
                    icheck.NC_STATUS = 2;
                    icheck.USER_QC = input.sUsers;
                    icheck.DATE_QC = (DateTime) (now);
                    icheck.USER_UPDATE = input.sUsers;
                    icheck.TIME_UPDATE = (DateTime) (now);

                    db.SubmitChanges();

                    result.RESULT = "1|ยืนยันข้อมูลเรียบร้อยแล้ว";
                    result.EXCEPTION = "";

                    return result;
                }
                else if (icheck.NC_NCR == 0)
                {
                    icheck.NC_STATUS = 5;
                    icheck.USER_QC = input.sUsers;
                    icheck.DATE_QC = (DateTime) (now);
                    icheck.USER_UPDATE = input.sUsers;
                    icheck.TIME_UPDATE = (DateTime) (now);

                    db.SubmitChanges();

                    result.RESULT = "1|ยืนยันข้อมูลเรียบร้อยแล้ว";
                    result.EXCEPTION = "";

                    return result;
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }

        #endregion

        #endregion

        #region FG

        #region Connect

        public mLoginMcsFg CheckLoginUserMcsFg(iLoginMcsFg input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            mLoginMcsFg result = new mLoginMcsFg();

            user iCheck = dc.users.Where(us => us.usID == input.Username && us.usPwd == input.Password).FirstOrDefault();

            if (iCheck == null)
            {
                result.US_ID = "NULL";
            }
            else
            {
                result.US_ID = iCheck.usID;
            }
            return result;
        }

        public List<mUserCheckMenu> CheckUserMenu(iUserCheckMenu input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            List<mUserCheckMenu> result = new List<mUserCheckMenu>();

            var query = from us in dc.users
                join ua in dc.users_auths on us.usID equals ua.usID
                join um in dc.users_menus on ua.mnID equals um.mnID
                where us.usID == input.UserID && us.status == 1
                orderby ua.mnID
                select ua.mnID.ToString();

            if (query.FirstOrDefault() != null)
            {
                foreach (string items in query)
                {
                    result.Add(new mUserCheckMenu()
                    {
                        MN_ID = items
                    });
                }
            }
            else
            {
                result.Add(new mUserCheckMenu()
                {
                    MN_ID = "===="
                });
            }
            return result;
        }

        #endregion

        #region Stock

        public List<mShelfName> GetShelfName()
        {
            List<mShelfName> result = new List<mShelfName>();

            McsFg2010DataContext dc = new McsFg2010DataContext();

            foreach (stock_shelf items in dc.stock_shelfs.OrderBy(s => s.place))
            {
                result.Add(new mShelfName()
                {
                    PLACE = items.place.ToString()
                });
            }

            return result;
        }

        public mProductOnShelf GetProductOnShelf(iShelfName input)
        {
            mProductOnShelf result = new mProductOnShelf();
            result.GetProductOnShelf = new List<getProductOnShelf>();
            McsFg2010DataContext dc = new McsFg2010DataContext();

            var query = from a in dc.stocks
                join b in dc.products on a.barcode equals b.barcode
                where a.place == input.PLACE
                orderby a.no
                select new {a.no, a.barcode, b.length, b.weight};
            int total = 0;
            if (query.FirstOrDefault() != null)
            {
                foreach (var items in query)
                {

                    result.GetProductOnShelf.Add(new getProductOnShelf()
                    {
                        NO = items.no.ToString(),
                        BARCODE = items.barcode,
                        LENGTH = items.length.ToString(),
                        WEIGHT = items.weight.ToString()
                    });

                    total = total + 1;
                    result.TOTAL = total;
                }

            }
            else
            {
                result.GetProductOnShelf.Add(new getProductOnShelf()
                {
                    NO = "-",
                    BARCODE = "-",
                    LENGTH = "-",
                    WEIGHT = "-"
                });
            }
            return result;
        }

        public List<mgetShowdataSwap> GetDataSwap(igetShowdataSwap input)
        {
            List<mgetShowdataSwap> result = new List<mgetShowdataSwap>();
            McsFg2010DataContext McsPdFg = new McsFg2010DataContext();
            string shelf = "";

            if (IsBarcode(input.BARCODE))
            {
                if (GetShelfProductInfoByBarcode(input.BARCODE, "", 0))
                {
                    var query = from s in McsPdFg.stocks
                        where s.barcode == input.BARCODE
                        select new {s.place, s.no};
                    if (query.Count() >= 0)
                    {
                        foreach (var item in query)
                        {
                            shelf = item.place.ToString();
                        }
                    }
                }
            }

            if (shelf != "")
            {
                var product = McsPdFg.products;
                var stocks = McsPdFg.stocks;
                var product_type = McsPdFg.product_types;
                var query = from pd in product
                    join st in stocks on pd.barcode equals st.barcode
                    join pt in product_type on pd.type equals pt.type
                    where st.place == shelf
                    orderby st.no
                    select new {pd.project, st.no, pd.type, pd.sharp, st.place, pd.item, st.barcode};

                if (query.FirstOrDefault() != null)
                {
                    foreach (var item in query)
                    {
                        result.Add(new mgetShowdataSwap()
                        {
                            PROJECT = item.project.ToString(),
                            No = item.no.ToString(),
                            TYPE = item.type.ToString(),
                            SHARP = '#' + item.sharp.ToString(),
                            SHELF = item.place.ToString(),
                            ITEM = item.item.ToString(),
                        });
                    }
                    //    if (item.barcode == input.BARCODE)
                    //    {
                    //        result.Remove(new mgetShowdataSwap()
                    //        {
                    //            PROJECT = item.project.ToString(),
                    //            No = item.no.ToString(),
                    //            TYPE = item.type.ToString(),
                    //            SHARP = '#' + item.sharp.ToString(),
                    //            SHELF = item.place.ToString(),
                    //            ITEM = item.item.ToString(),
                    //        });

                    //    }

                    //    if (item.barcode == input.BARCODE)
                    //    {
                    //        result.Add(new mgetShowdataSwap()
                    //        {
                    //            PROJECT = item.project.ToString(),
                    //            No = item.no.ToString(),
                    //            TYPE = item.type.ToString(),
                    //            SHARP = '#' + item.sharp.ToString(),
                    //            SHELF = item.place.ToString(),
                    //            ITEM = item.item.ToString()+"****",
                    //        });

                    //    }
                    //}

                }
                else
                {
                    result.Add(new mgetShowdataSwap()
                    {
                        PROJECT = "-",
                        No = "-",
                        TYPE = "-",
                        SHARP = "-",
                        SHELF = "-",
                        ITEM = "-"
                    });
                }
            }
            else
            {
                result.Add(new mgetShowdataSwap()
                {
                    PROJECT = "-",
                    No = "-",
                    TYPE = "-",
                    SHARP = "-",
                    SHELF = "-",
                    ITEM = "-"

                });
            }
            return result;
        }

        public mResult SetUpdateSwap(iupdatebarcodeSwap input)
        {
            McsAppDataContext McsData = new McsAppDataContext();
            McsFg2010DataContext McsFg = new McsFg2010DataContext();
            mResult result = new mResult();
            DateTime now = DateTime.Now;
            string newbarcode = "";
            string newnumber = "";
            string oldnumber = "";
            string olebarcode = "";
            if (IsBarcode(input.NEWBARCODE))
            {
                if (GetShelfProductInfoByBarcode(input.NEWBARCODE, "", 0))
                {
                    var query = from s in McsFg.stocks
                        where s.barcode == input.NEWBARCODE
                        select new {s.place, s.no};
                    if (query.Count() >= 0)
                    {
                        foreach (var item in query)
                        {
                            newbarcode = item.place.ToString();
                            newnumber = item.no.ToString();
                        }
                    }
                }
            }

            if (IsBarcode(input.OLDBARCODE))
            {
                if (GetShelfProductInfoByBarcode(input.OLDBARCODE, "", 0))
                {
                    var query = from s in McsFg.stocks
                        where s.barcode == input.OLDBARCODE
                        select new {s.place, s.no};
                    if (query.Count() >= 0)
                    {
                        foreach (var item in query)
                        {
                            olebarcode = item.place.ToString();
                            oldnumber = item.no.ToString();
                        }
                    }
                }
            }

            try
            {

                stock icheck =
                    McsFg.stocks.Where(s => s.barcode == input.NEWBARCODE && s.barcode == input.OLDBARCODE)
                        .FirstOrDefault();
                if (input.NEWBARCODE != input.OLDBARCODE)
                {
                    if (newbarcode == olebarcode)
                    {

                        stock iupdateold = McsFg.stocks.Where(no => no.barcode == input.OLDBARCODE).FirstOrDefault();
                        iupdateold.no = int.Parse(newnumber);
                        iupdateold.users = input.USERID;
                        iupdateold.dates = (DateTime) (now);
                        McsFg.SubmitChanges();
                        stock iupdatenew = McsFg.stocks.Where(no => no.barcode == input.NEWBARCODE).FirstOrDefault();
                        iupdatenew.no = int.Parse(oldnumber);
                        iupdatenew.users = input.USERID;
                        iupdatenew.dates = (DateTime) (now);

                        McsFg.SubmitChanges();
                        result.RESULT = "1|สลับตำแหน่งเรียบร้อย";
                        result.EXCEPTION = "";
                        return result;
                    }
                    //    else if(newbarcode =="" && olebarcode=="" )
                    //{
                    //    result.RESULT = "3|ไม่่สามารถบันทึกได้";
                    //    result.EXCEPTION = "";
                    //    return result;
                    //}
                    else
                    {
                        result.RESULT = "2|ชิ้นงานไม่ได้อยู่ในชั้นวางเดียวกัน";
                        result.EXCEPTION = "";
                        return result;
                    }
                }
                else
                {
                    result.RESULT = "3|ไม่่สามารถบันทึกได้";
                    result.EXCEPTION = "";
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "0|Error";
                result.EXCEPTION = ex.Message;
                return result;
            }
        }

        public mResult SetNumberSort(iNumberSort input)
        {
            mResult result = new mResult();
            using (
                SqlConnection connection =
                    new SqlConnection(
                        WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
            {
                connection.Open();
                try
                {
                    using (SqlCommand command1 = new SqlCommand())
                    {

                        command1.Connection = connection;
                        command1.CommandText = @"insert into stock_arrange_bak select * from stock where place=@PLACE";
                        command1.CommandText = @" update stock set no=0, status=0 where place= @PLACE";
                        command1.Parameters.Add("@PLACE", SqlDbType.NVarChar, 10).Value = input.PLACE;
                        command1.ExecuteNonQuery();

                        result.RESULT = "1|OK";
                    }
                }

                catch (Exception e)
                {
                    result.RESULT = "0|Error";
                    result.EXCEPTION = e.Message;
                    return result;
                }
                connection.Close();
            }
            return result;
        }

        public mResult SetNumberUpdate(iNumberUpdate input)
        {

            mResult result = new mResult();
            McsAppDataContext mcsapp = new McsAppDataContext();
            McsFg2010DataContext mcsfg = new McsFg2010DataContext();
            string shelf = "";
            if (IsBarcode(input.BARCODE))
            {
                if (GetShelfProductInfoByBarcode(input.BARCODE, "", 0))
                {
                    var query = from s in mcsfg.stocks
                        where s.barcode == input.BARCODE
                        select new {s.place, s.no};
                    if (query.Count() >= 0)
                    {
                        foreach (var item in query)
                        {
                            shelf = item.place.ToString();

                        }
                    }
                }
            }
            if (shelf == input.SHELF)
            {
                using (
                    SqlConnection connection =
                        new SqlConnection(
                            WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
                {
                    connection.Open();
                    try
                    {
                        using (SqlCommand command1 = new SqlCommand())
                        {

                            command1.Connection = connection;
                            command1.CommandText = @" update stock set no=@input, status=1 where ( barcode=@barcode )  ";
                            command1.Parameters.Add("@input", SqlDbType.NVarChar, 10).Value = input.NUMBER;
                            command1.Parameters.Add("@barcode", SqlDbType.NVarChar, 10).Value = input.BARCODE;
                            command1.ExecuteNonQuery();

                            result.RESULT = "1|OK";
                        }
                    }

                    catch (Exception e)
                    {
                        result.RESULT = "0|Error";
                        result.EXCEPTION = e.Message;
                        return result;
                    }
                    connection.Close();
                }
            }
            else
            {
                result.RESULT = "2|NotShelfSam";
            }

            return result;
        }

        public mResult StockOut(iStockOut input)
        {
            DateTime now = DateTime.Now;
            mResult result = new mResult();
            McsFg2010DataContext fg = new McsFg2010DataContext();
            string place = "";
            string number = "";
            string docno = "";
            int no = 0;
            string Error = "";
            int status = 0;
            var query3 = from si in fg.products
                where si.barcode == input.BARCODE
                select new {si.status};
            foreach (var item in query3)
            {
                status = (int) item.status;
            }

            Error = CheckStatus(input.BARCODE);
            if (status != 3)
            {
                result.RESULT = "00|" + Error;
                return result;
            }

            docno = getDocNoStockMoveOutByBarcode(input.BARCODE);
            if (!IsMoveOutRequest(input.BARCODE))
            {
                result.RESULT = "10|ชิ้นงานไม่ได้แจ้งย้ายออก";
                return result;
            }
            if (!IsCarNo(input.CARTRUCK))
            {
                result.RESULT = "14|เลขทะเบียนรถผิด";
                return result;
            }
            if (GetShelfProductInfoByBarcode(input.BARCODE, "", 0))
            {
                var query = from s in fg.stocks
                    where s.barcode == input.BARCODE
                    select new {s.place, s.no};
                if (query.Count() >= 0)
                {
                    foreach (var item in query)
                    {
                        place = item.place.ToString();
                        number = item.no.ToString();
                    }
                    no = int.Parse(number);
                }
            }
            if (docno == "")
            {
                result.RESULT = "11|ไม่พบเลขที่เอกสาร";
                result.EXCEPTION = "";
                return result;
            }
            if (!GetShelfProductInfoByBarcode(input.BARCODE, place, no))
            {
                result.RESULT = "12|ไม่พบชิ้นงานบนชั้นวาง";
                return result;
            }
            try
            {
                if (SetProductMoveOutOnShelf(input.BARCODE))
                {
                    stock_move stm =
                        fg.stock_moves.Where(s => s.docNo == docno && s.barcode == input.BARCODE && s.status == 0)
                            .FirstOrDefault();
                    if (stm != null)
                    {
                        stm.status = short.Parse("2");
                        stm.fromPlace = place;
                        stm.fromUser = input.USER;
                        stm.fromDate = (DateTime) (now);
                        stm.fromCrane = input.CRANE;
                        stm.fromCarTruck = input.CARTRUCK;
                        fg.SubmitChanges();
                        result.RESULT = "55|บันทึกสำเร็จ";
                    }
                    else
                    {
                        result.RESULT = "0|ไม่่สามารถบันทึกได้";
                        return result;
                    }

                    using (
                        SqlConnection connection =
                            new SqlConnection(
                                WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString)
                    )
                    {
                        connection.Open();
                        try
                        {
                            using (SqlCommand command1 = new SqlCommand())
                            {
                                command1.Connection = connection;
                                command1.CommandText =
                                    @"update stock_move_doc set status=2, dateStart=getdate() where status=0 and docNo=@docno and not exists  (select * from stock_move where status=0 and docNo=@docno);";
                                command1.Parameters.Add("@docno", SqlDbType.NVarChar, 15).Value = docno;
                                command1.ExecuteNonQuery();
                            }
                            result.RESULT = "55|บันทึกสำเร็จ";

                        }

                        catch (Exception e)
                        {
                            result.RESULT = "0|ไม่สามารถอัปเดท stock_move_doc ได้";
                            result.EXCEPTION = e.Message;
                            return result;
                        }

                        connection.Close();
                    }
                }
                else
                {
                    result.RESULT = "0|ไม่สามารถย้ายชิ้นงานออกได้";
                    return result;

                }
            }
            catch
            {
                result.RESULT = "0|ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = "error";
                return result;
            }

            return result;

        }

        public mResult StockIn(iStockIn input)
        {
            McsFg2010DataContext fg = new McsFg2010DataContext();
            mResult result = new mResult();
            DateTime now = DateTime.Now;
            string docno = "";
            int nresult = 0;
            string Error = "";
            int status = 0;
            string sShelf = "";
            string sBarcodeRef = input.SHELF.Trim().ToUpper();
            string sBarcode = input.BARCODE.Trim().ToUpper();
            string sUser = input.USER;
            var query3 = from si in fg.stock_moves
                where si.barcode == input.BARCODE
                select new {si.status};
            foreach (var item in query3)
            {
                status = (int) item.status;
            }

            Error = CheckStatus(input.BARCODE);
            if (status != 2)
            {
                result.RESULT = "00|" + Error;
                return result;
            }


            docno = GetDocNoStockMoveInByBarcode(input.BARCODE);
            if (!IsMoveInRequest(input.BARCODE))
            {
                result.RESULT = "10|ชิ้นงานไม่ได้แจ้งย้ายเข้า";
                return result;
            }
            if (docno == "")
            {
                result.RESULT = "11|ไม่พบเลขที่เอกสาร";
                return result;
            }

            var query = from s in fg.stocks
                where s.barcode == input.SHELF
                select new {s.place};
            if (query.FirstOrDefault() != null)
            {
                foreach (var items in query)
                {
                    sShelf = items.place;
                }
            }
            stock_move checkplace =
                fg.stock_moves.Where(c => c.fromPlace == sShelf && c.barcode == input.BARCODE).FirstOrDefault();
            if (checkplace != null)
            {
                result.RESULT = "9|ชิ้นงานอยู่ในชั้นวางเดียวกัน";
                return result;
            }

            nresult = SetProductIntoShelf(sBarcodeRef, sBarcode, sUser);
            if (nresult == 1)
            {
                stock_move stm =
                    fg.stock_moves.Where(s => s.docNo == docno && s.barcode == input.BARCODE && s.status == 2)
                        .FirstOrDefault();
                if (stm != null)
                {
                    stm.status = short.Parse("1");
                    stm.destPlace = sShelf.ToString();
                    stm.destUser = input.USER;
                    stm.destDate = (DateTime) (now);
                    stm.destCrane = input.CRANE;
                    fg.SubmitChanges();
                }
                else
                {
                    result.RESULT = "0|ไม่สามารถบันทึกข้อมูลได้";
                    return result;
                }

                using (
                    SqlConnection connection =
                        new SqlConnection(
                            WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
                {
                    connection.Open();
                    try
                    {
                        using (SqlCommand command1 = new SqlCommand())
                        {

                            command1.Connection = connection;
                            command1.CommandText =
                                @"update stock_move_doc set status=1, dateFinish=getdate() where status=2 and docNo=@docno and not exists (select * from stock_move where status=2 and docNo=@docno) ";
                            command1.Parameters.Add("@docno", SqlDbType.NVarChar, 15).Value = docno;
                            command1.ExecuteNonQuery();
                        }
                    }

                    catch (Exception e)
                    {
                        result.RESULT = "0|Error";
                        result.EXCEPTION = e.Message;
                        return result;
                    }
                    connection.Close();
                }

            }
            else
            {
                result.RESULT = nresult.ToString();
                result.EXCEPTION = "";
                return result;
            }
            result.RESULT = "1|บันทึกข้อมูลสำเร็จ";
            return result;
        }

        public mBarcodeSort BarcodeSort(iBarcodeSort input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            mBarcodeSort result = new mBarcodeSort();
            var query = from pd in dc.products
                join pt in dc.product_types
                on pd.type equals pt.type into LeftJoinType
                from pdt in LeftJoinType.DefaultIfEmpty()
                join st in dc.stocks on pd.barcode equals st.barcode into LeftJoinBarcode
                from stpd in LeftJoinBarcode.DefaultIfEmpty()
                where pd.barcode == input.Barcode
                select
                new
                {
                    pd.project,
                    pdt.typeName,
                    pd.item,
                    pd.sharp,
                    pd.dwg,
                    pd.code,
                    pd.size,
                    pd.length,
                    pd.weight,
                    stpd.place,
                    stpd.no
                };

            if (query.FirstOrDefault() != null)
            {
                foreach (var items in query)
                {
                    result.PROJECT = items.project.ToString();
                    result.TYPE_NAME = items.typeName.ToString();
                    result.ITEM = items.item.ToString();
                    result.LENGTH = items.length.ToString();
                    result.WEIGHT = items.weight.ToString();
                    result.PLACE = (items.place == null) ? "" : items.place.ToString();
                    result.NO = (items.no == null) ? "" : items.no.ToString();
                }
            }
            else
            {
                result.PROJECT = "-";
                result.TYPE_NAME = "-";
                result.ITEM = "-";
                result.LENGTH = "-";
                result.WEIGHT = "-";
                result.PLACE = "-";
                result.NO = "-";
            }
            return result;
        }


        #endregion

        #region Stock Info

        public mGetProductStatus GetProductStatusFG(iBarcode input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            mGetProductStatus result = new mGetProductStatus();

            var query = from pd in dc.products
                join pt in dc.product_types
                on pd.type equals pt.type into LeftJoinType
                from pdt in LeftJoinType.DefaultIfEmpty()
                join st in dc.stocks on pd.barcode equals st.barcode into LeftJoinBarcode
                from stpd in LeftJoinBarcode.DefaultIfEmpty()
                where pd.barcode == input.Barcode
                select
                new
                {
                    pd.project,
                    pdt.typeName,
                    pd.item,
                    pd.sharp,
                    pd.dwg,
                    pd.code,
                    pd.size,
                    pd.length,
                    pd.weight,
                    stpd.place,
                    stpd.no
                };

            if (query.FirstOrDefault() != null)
            {
                foreach (var items in query)
                {
                    result.PROJECT = items.project.ToString();
                    result.TYPE_NAME = items.typeName.ToString();
                    result.ITEM = items.item.ToString();
                    result.SHARP = items.sharp.ToString();
                    result.DWG = items.dwg.ToString();
                    result.CODE = items.code.ToString();
                    result.SIZE = items.size.ToString();
                    result.LENGTH = items.length.ToString();
                    result.WEIGHT = items.weight.ToString();
                    result.PLACE = (items.place == null) ? "" : items.place.ToString();
                    result.NO = (items.no == null) ? "" : items.no.ToString();
                }

            }
            else
            {
                result.PROJECT = "-";
                result.TYPE_NAME = "-";
                result.ITEM = "-";
                result.SHARP = "-";
                result.DWG = "-";
                result.CODE = "-";
                result.SIZE = "-";
                result.LENGTH = "-";
                result.WEIGHT = "-";
                result.PLACE = "-";
                result.NO = "-";
            }

            
            return result;
        }

        public List<mGetProductPlaceInStock> GetProductPlaceInStock(iProjectNameAndItem input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            List<mGetProductPlaceInStock> result = new List<mGetProductPlaceInStock>();

            if (input.Item == null)
            {
                var query1 = from pd in dc.products
                             join st in dc.stocks on pd.barcode equals st.barcode
                             join pt in dc.product_types on pd.type equals pt.type
                             where SqlMethods.Like(pd.project, "%" + input.ProjectName + "%")
                             orderby pd.project, pt.typeName, pd.sharp, pd.item
                             select new { pd.project, pt.typeName, pd.sharp, pd.item, st.place, st.no };

                if (query1.FirstOrDefault() != null)
                {
                    foreach (var items in query1)
                    {
                        result.Add(new mGetProductPlaceInStock()
                        {
                            PROJECT = items.project.ToString(),
                            TYPE_NAME = items.typeName.ToString(),
                            SHARP = '#' + items.sharp.ToString(),
                            ITEM = items.item.ToString(),
                            PLACE = items.place.ToString(),
                            NO = items.no.ToString()
                        });
                    }
                }
                else
                {
                    result.Add(new mGetProductPlaceInStock()
                    {
                        PROJECT = "-",
                        TYPE_NAME = "-",
                        SHARP = "-",
                        ITEM = "-",
                        PLACE = "-",
                        NO = "-"
                    });
                }
            }
            else
            {
                var query2 = from pd in dc.products
                             join st in dc.stocks on pd.barcode equals st.barcode
                             join pt in dc.product_types on pd.type equals pt.type
                             where SqlMethods.Like(pd.project, "%" + input.ProjectName + "%") && input.Item == pd.item
                             orderby pd.project, pt.typeName, pd.sharp, pd.item
                             select new { pd.project, pt.typeName, pd.sharp, pd.item, st.place, st.no };

                if (query2.FirstOrDefault() != null)
                {
                    foreach (var items in query2)
                    {
                        result.Add(new mGetProductPlaceInStock()
                        {
                            PROJECT = items.project.ToString(),
                            TYPE_NAME = items.typeName.ToString(),
                            SHARP = '#' + items.sharp.ToString(),
                            ITEM = items.item.ToString(),
                            PLACE = items.place.ToString(),
                            NO = items.no.ToString()
                        });
                    }
                }
                else
                {
                    result.Add(new mGetProductPlaceInStock()
                    {
                        PROJECT = "-",
                        TYPE_NAME = "-",
                        SHARP = "-",
                        ITEM = "-",
                        PLACE = "-",
                        NO = "-"
                    });
                }
            }

            return result;
        }

        #endregion

        #region Stock In
        private int SetProductIntoShelf(string sBarcodeRef, string sBarcode, string sUser)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            string sShelf = "";
            int nNo = 0;

            sBarcodeRef = sBarcodeRef.Trim().ToUpper();
            sBarcode = sBarcode.Trim().ToUpper();

            if (IsBarcode(sBarcodeRef))
            {
                if (GetShelfProductInfoByBarcode(sBarcodeRef, sShelf, nNo))
                {
                    var query = from s in dc.stocks
                                where s.barcode == sBarcodeRef
                                select new { s.place, s.no };

                    foreach (var items in query)
                    {
                        sShelf = items.place;
                        nNo = Int32.Parse(items.no.ToString());
                    }
                  
                    SetProductInsertOnShelf(sBarcode, sShelf, nNo + 1, sUser);
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                if (IsShelfInStock(sBarcodeRef))
                {
                    if (GetShelfProductTotal(sBarcodeRef) == 0)
                    {
                        AddProductIntoShelf(sBarcode, sBarcodeRef, 1, sUser);
                    }
                    else
                    {
                        return -3;
                    }
                }
                else
                {
                    return -2;
                }
            }
            return 1;
        }

        public List<mStockArea> GetStockArea()
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            List<mStockArea> result = new List<mStockArea>();

            foreach (stock_area items in dc.stock_areas.OrderBy(sa => sa.saName))
            {
                result.Add(new mStockArea()
                {
                    SA_ID = items.saId.ToString(),
                    SA_NAME = items.saName.ToString()
                });
            }

            return result;
        }

        public mResult SetUptoTruck(iSetUptoTruck input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            mResult result = new mResult();
            DateTime now = DateTime.Now;
            string Error = "";
            int status = 0;
            var query3 = from si in dc.products
                         where si.barcode == input.sBarcode
                         select new { si.status };
            foreach (var item in query3)
            {
                status = (int)item.status;
            }

            Error = CheckStatus(input.sBarcode);
            if (status != 5)
            {
                result.RESULT = "00|" + Error;
                return result;
            }
            try
            {
                if (IsStockInRequest(input.sBarcode) == "False")
                {
                    result.RESULT = "-11|ยังไม่ได้แจ้งงานเข้าคลังสินค้า";
                    result.EXCEPTION = "";

                    return result;
                }

                if (IsCarNo(input.sCarTruck) == false)
                {
                    result.RESULT = "-10|เลขทะเบียนรถผิด";
                    result.EXCEPTION = "";

                    return result;
                }

                if (GetDocNoStockInByBarcode(input.sBarcode) == "False")
                {
                    result.RESULT = "-12|เลขที่เอกสารแจ้งงานเข้าคลังสินค้า ที่พิมพ์ไม่ตรงกับเลขที่เอกสารที่แจ้ง";
                    result.EXCEPTION = "";

                    return result;
                }

                stock_in items = dc.stock_ins.Where(si => si.barcode == input.sBarcode && si.status == 0).FirstOrDefault();

                if (items != null && items.fromCarTruck == null)
                {
                    items.fromFac = Convert.ToInt32(input.sStockArea);
                    items.fromCarTruck = input.sCarTruck;
                    items.fromUser = input.sUserID;
                    items.FromDate = (DateTime)(now);

                    dc.SubmitChanges();

                    result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
                    result.EXCEPTION = "";
                    return result;
                }
                else
                {
                    result.RESULT = "2|ชิ้นงานอยู่บนรถเทรนเลอร์";
                    result.EXCEPTION = "";

                    return result;
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "0|ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }

        public List<mGetCrane> GetCrane()
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            List<mGetCrane> result = new List<mGetCrane>();

            foreach (car_crane items in dc.car_cranes.OrderBy(cc => cc.crane_detail))
            {
                result.Add(new mGetCrane()
                {
                    CRANE_NO = items.crane_no.ToString(),
                    CRANE_DETAIL = items.crane_no + " : " + items.crane_operator.ToString()
                });
            }

            return result;
        }

        public mResult SetReceiveToStock(iSetReceiveToStock input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            mResult result = new mResult();
            DateTime now = DateTime.Now;

            string sPlace = "";
            string sDocNo = "";
            int nResult = 0;
            int status=0;
            string Error = "";
            var query3 = from si in dc.products
                         where si.barcode == input.sBarcode
                         select new { si.status };
            foreach (var item in query3)
            {
                status = (int)item.status;
            }

            Error = CheckStatus(input.sBarcode);
            if (status != 5)
            {
                result.RESULT = "00|" + Error;
                return result;
            }

            var query1 = from si in dc.stock_ins
                         where si.barcode == input.sBarcode
                         select new {si.status};
            foreach(var item in query1)
            {
                status = (int)item.status;
            }

            if(status==1)
            {
          
                result.RESULT = "5|ชิ้นงานอยู่ในคลังอยู่แล้ว";
                return result;
            }

            if (IsStockInRequest(input.sBarcode) == "False")
            {
                result.RESULT = "-11|ยังไม่ได้แจ้งงานเข้าคลังสินค้า";
                result.EXCEPTION = "";
                return result;
            }

            sDocNo = GetDocNoStockInByBarcode(input.sBarcode);

            if (GetDocNoStockInByBarcode(input.sBarcode) == "")
            {
                result.RESULT = "-12|เลขที่เอกสารแจ้งงานเข้าคลังสินค้า ที่พิมพ์ไม่ตรงกับเลขที่เอกสารที่แจ้ง";
                result.EXCEPTION = "";
                return result;
            }

            nResult = SetProductIntoShelf(input.sBarcodeRef, input.sBarcode, input.sUserID);

            if (nResult == 1)
            {
                try
                {
                    foreach (stock items in dc.stocks.Where(s => s.barcode == input.sBarcode))
                    {
                        sPlace = items.place.ToString();
                    }

                    //Update Stock_in, Product
                    stock_in iStock = dc.stock_ins.Where(si => si.barcode == input.sBarcode).FirstOrDefault();

                    if (iStock != null)
                    {
                        iStock.status = 1;
                        iStock.place = sPlace;
                        iStock.users = input.sUserID;
                        iStock.dates = (DateTime)(now);
                        iStock.destCrane = input.sCrane;

                        dc.SubmitChanges();
                    }
                   

                    product iProduct = dc.products.Where(pd => pd.barcode == input.sBarcode).FirstOrDefault();

                    if (iProduct != null)
                    {
                        iProduct.status = 1;

                        dc.SubmitChanges();
                    }

                    //Update Stock_in_doc
                    //var query = from sid in dc.stock_in_docs
                    //            where sid.status == 0 && sid.docNo == sDocNo && !dc.stock_ins.Any(si => si.status == 0 && si.docNo == sDocNo)
                    //            select sid;

                    //if (query.FirstOrDefault() != null)
                    //{
                    //    foreach (var iStockInDoc in query)
                    //    {
                    //        iStockInDoc.status = 2;
                    //        iStockInDoc.dateStart = (DateTime)(now);
                    //    }

                    //    dc.SubmitChanges();
                    //}

                    using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
                    {
                        connection.Open();
                        try
                        {
                            using (SqlCommand command1 = new SqlCommand())
                            {
                                command1.Connection = connection;
                                command1.CommandText = @"update stock_in_doc set status=2, dateStart=getdate() where status=0 and docNo=@docno and not exists
                                                                                    (select * from stock_in where status=0 and docNo=@docno) ";
                                command1.Parameters.Add("@docno", SqlDbType.NVarChar, 15).Value = sDocNo;
                                command1.ExecuteNonQuery();
                            }
                        }
                        catch (Exception e)
                        {
                            result.RESULT = "0|Error";
                            result.EXCEPTION = e.Message;
                            return result;
                        }
                        connection.Close();
                    }

                    var query2 = from sid in dc.stock_in_docs
                                 where sid.status == 2 && sid.docNo == sDocNo && !dc.stock_ins.Any(si => si.status == 0 && si.docNo == sDocNo)
                                 select sid;

                    if (query2.FirstOrDefault() != null)
                    {
                        foreach (var iStockInDoc2 in query2)
                        {
                            iStockInDoc2.status = 1;
                            iStockInDoc2.dateFinish = (DateTime)(now);
                        }

                        dc.SubmitChanges();
                    }
                }
                catch (Exception ex)
                {
                    result.RESULT = "0|ไม่สามารถบันทึกข้อมูลได้";
                    result.EXCEPTION = ex.Message;

                    return result;
                }
            }
            else
            {
                result.RESULT = nResult.ToString();
                result.EXCEPTION = "";

                return result;
            }

            result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
            result.EXCEPTION = "";
            return result;
        }

        public mResult SetUpReceive_Rev(iSetUpRecive_Rev input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            mResult result = new mResult();
            DateTime now = DateTime.Now;

            string sDocNo = "";
            int status = 0;
            string Error = "";
            var query3 = from si in dc.products
                         where si.barcode == input.sBarcode
                         select new { si.status };
            foreach (var item in query3)
            {
                status = (int)item.status;
            }

            Error = CheckStatus(input.sBarcode);
            if (status != 6)
            {
                result.RESULT = "00|" + Error;
                return result;
            }
            
                if (!IsStockOutRevRequest(input.sBarcode))
                {
                    result.RESULT = "-10|ยังไม่ได้แจ้งงานแก้ไขเข้าคลังสินค้า";
                    result.EXCEPTION = "";

                    return result;
                }

                if (IsCarNo(input.sCarTruck) == false)
                {
                    result.RESULT = "-18|เลขทะเบียนรถผิด";
                    result.EXCEPTION = "";

                    return result;
                }

                sDocNo = GetDocNoStockInRevByBarcode(input.sBarcode);

                if (sDocNo == "")
                {
                    result.RESULT = "-11|เลขที่เอกสารแจ้งงานแก้ไขเข้าคลังสินค้า ที่พิมพ์ไม่ตรงกับเลขที่เอกสารที่แจ้ง";
                    result.EXCEPTION = "";

                    return result;
                }
                try
                {
                stock_in_rev iStockInRev = dc.stock_in_revs.Where(sir => sir.barcode == input.sBarcode && sir.status == 0).FirstOrDefault();

                if (iStockInRev != null && iStockInRev.fromCarTruck==null)
                {
                    iStockInRev.fromCrane = input.sCrane;
                    iStockInRev.fromCarTruck = input.sCarTruck;
                    iStockInRev.fromUser = input.sUserID;
                    iStockInRev.fromDate = (DateTime)(now);

                    dc.SubmitChanges();

                    result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
                    result.EXCEPTION = "";
                    return result;
                }
                else
                {
                    result.RESULT = "2|ชิ้นงานอยู่บนรถเทรนเลอร์";
                    result.EXCEPTION = "";

                    return result;
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "0|ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
                return result;
            }

        }

        public mResult SetDownReceive_Rev(iSetDownRecive_Rev input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            mResult result = new mResult();
            DateTime now = DateTime.Now;
            string sDocNo = "";
            string sShelf = "";
            string sPlace = "";
            int nNo = 0;
            int nResult = 0;
            string Error = "";
            int status = 0;
            var query3 = from si in dc.products
                         where si.barcode == input.sBarcode
                         select new { si.status };
            foreach (var item in query3)
            {
                status = (int)item.status;
            }

            Error = CheckStatus(input.sBarcode);
            if (status != 6)
            {
                result.RESULT = "00|" + Error;
                return result;
            }

            if (IsStockInRevRequest(input.sBarcode) == "False")
            {
                result.RESULT = "-15|ยังไม่ได้แจ้งงานเข้าคลังสินค้า";
                result.EXCEPTION = "";
                return result;
            }
            sDocNo = GetDocNoStockInRevByBarcode(input.sBarcode);
            if (sDocNo == "")
            {
                result.RESULT = "-11|เลขที่เอกสารแจ้งงานแก้ไขเข้าคลังสินค้า ที่พิมพ์ไม่ตรงกับเลขที่เอกสารที่แจ้ง";
                result.EXCEPTION = "";
                return result;
            }
            if (!GetShelfProductInfoByBarcode(input.sBarcode, sShelf, nNo))
            {
                result.RESULT = "-12|ไม่พบชั้นวางบารโค็ค";
                result.EXCEPTION = "";
                return result;
            }

            int BarcodeCount = GetShelfProductTotal(input.sBarcodeRef);
            if (BarcodeCount > 0)
            {
                result.RESULT = "-3|ไม่มีชั้นวาง";
                result.EXCEPTION = "";
                return result;
            }
           // sDocNo = GetDocNoStockInRevByBarcode(input.sBarcode);
            //if (SetProductMoveOutOnShelf(input.sBarcode))
            //{
            foreach (stock items in dc.stocks.Where(s => s.barcode == input.sBarcodeRef))
            {
                sPlace = items.place.ToString();
            }
            stock_in_rev checkplace = dc.stock_in_revs.Where(c => c.fromPlace == sPlace && c.barcode==input.sBarcode).FirstOrDefault();
            if (checkplace != null)
            {
                result.RESULT = "9|ชิ้นงานอยู่ในชั้นวางเดียวกัน";
                return result;
            }
                nResult = SetProductIntoShelf(input.sBarcodeRef, input.sBarcode, input.sUserID);
                if (nResult == 1)
                {
                    foreach (stock items in dc.stocks.Where(s => s.barcode == input.sBarcode))
                    {
                        sPlace = items.place.ToString();
                        nNo =(int)items.no;                      
                    }

                    
                    //try
                    //{
                        //UPDATE STOCK_IN_REV,PRODUCT
                        stock_in_rev iStockInRevProduct = dc.stock_in_revs.Where(sir => sir.barcode == input.sBarcode && sir.status == 0).FirstOrDefault();

                        if (iStockInRevProduct != null)
                        {
                            iStockInRevProduct.status = 1;
                            iStockInRevProduct.place = sPlace;
                            iStockInRevProduct.users = input.sUserID;
                            iStockInRevProduct.dates = (DateTime)(now);
                            iStockInRevProduct.destCrane = input.sCrane;
                            dc.SubmitChanges();
                        }
                        //up status product ==1
                        product iProduct = dc.products.Where(pd => pd.barcode == input.sBarcode).FirstOrDefault();

                        if (iProduct != null)
                        {
                            iProduct.status = 1;

                            dc.SubmitChanges();
                        }
                        //UPDATE STOCK_IN_REV_DOC (START,FINISH)
                        using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
                        {
                            connection.Open();
                            try
                            {
                                using (SqlCommand command1 = new SqlCommand())
                                {
                                    command1.Connection = connection;
                                    command1.CommandText = @"update stock_in_rev_doc set status=2, dateStart=getdate() where status=0 and docNo=@docno and not exists
                                                                                    (select * from stock_in_rev where status=0 and docNo=@docno) ";
                                    command1.Parameters.Add("@docno", SqlDbType.NVarChar, 15).Value = sDocNo;
                                    command1.ExecuteNonQuery();
                                }
                            }
                            catch (Exception e)
                            {
                                result.RESULT = "0|Error";
                                result.EXCEPTION = e.Message;
                                return result;
                            }
                            connection.Close();
                        }

                        //var query1 = from sird in dc.stock_in_rev_docs
                        //             where sird.status == 0 && sird.docNo == sDocNo && !dc.stock_in_revs.Any(si => si.status == 0 && si.docNo == sDocNo)
                        //             select sird;

                        //if (query1.FirstOrDefault() != null)
                        //{
                        //    foreach (var iStockInDocRev in query1)
                        //    {
                        //        iStockInDocRev.status = 2;
                        //        iStockInDocRev.dateStart = (DateTime)(now);
                        //    }

                        //    dc.SubmitChanges();
                        //}

                        var query2 = from sird in dc.stock_in_rev_docs
                                     where sird.status == 2 && sird.docNo == sDocNo && !dc.stock_in_revs.Any(si => si.status == 0 && si.docNo == sDocNo)
                                     select sird;

                        if (query2.FirstOrDefault() != null)
                        {
                            foreach (var iStockInDocRev in query2)
                            {
                                iStockInDocRev.status = 1;
                                iStockInDocRev.dateFinish = (DateTime)(now);
                            }

                            dc.SubmitChanges();
                        }
                    //}
                    //catch (Exception ex)
                    //{
                    //    result.RESULT = "2|ไม่สามารถบันทึกข้อมูลได้";
                    //    result.EXCEPTION = ex.Message;
                    //    return result;
                    //}
                }
                else
                {
                    result.RESULT = nResult.ToString();
                    result.EXCEPTION = "";
                    return result;
                }
            //}
            //else
            //{
            //    result.RESULT = "-14|บันทึกข้อมูลลำดับชิ้นงานภายในชั้นวางไม่สาเร็จ";
            //    result.EXCEPTION = "";
            //    return result;
            //}
            result.RESULT = "1|บันทึกสำเร็จ";
            result.EXCEPTION = "OK";
            return result;
        }

        public mSummaryByDate GetShowSummaryByDate(iDateInputSummary input)
        {
            mSummaryByDate result = new mSummaryByDate();

            result.GetSummary = new List<mGetSummary>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;

                    if (input.nTypeJob == 0)
                    {
                        command1.CommandText = @"SELECT * FROM (SELECT TOP (100) PERCENT CONVERT(VARCHAR(10),A.DATEREQ,121) AS DATE, 
                                            COUNT(B.BARCODE) AS REQ, COUNT(B.PLACE) AS REC, COUNT(B.BARCODE) - COUNT(B.PLACE) AS REM
                                            FROM STOCK_IN_DOC AS A INNER JOIN STOCK_IN AS B ON A.DOCNO = B.DOCNO
                                            GROUP BY CONVERT(VARCHAR(10),A.DATEREQ,121)
                                            HAVING (CONVERT(VARCHAR(10), A.DATEREQ, 121) BETWEEN @DATE_START AND @DATE_FINISH))AS SUMMARY WHERE REM>0 ORDER BY DATE";
                    }
                    else
                    {
                        command1.CommandText = @"SELECT * FROM (SELECT TOP (100) PERCENT CONVERT(VARCHAR(10),A.DATEREQ,121) AS DATE, 
                                                COUNT(B.BARCODE) AS REQ, COUNT(B.PLACE) AS REC, COUNT(B.BARCODE) - COUNT(B.PLACE) AS REM
                                                 FROM STOCK_IN_REV_DOC AS A INNER JOIN
                                                 STOCK_IN_REV AS B ON A.DOCNO = B.DOCNO
                                                 GROUP BY CONVERT(VARCHAR(10),A.DATEREQ,121)
                                                 HAVING (CONVERT(VARCHAR(10), A.DATEREQ, 121) BETWEEN @DATE_START AND @DATE_FINISH))AS SUMMARY WHERE REM>0 ORDER BY DATE";
                    }
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@DATE_START", SqlDbType.DateTime, 10).Value = input.sDateStart;
                    command1.Parameters.Add("@DATE_FINISH", SqlDbType.DateTime, 10).Value = input.sDateFinish;
                    SqlDataReader dr = command1.ExecuteReader();

                    int total = 0;

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            result.GetSummary.Add(new mGetSummary()
                            {
                                DATE = dr["Date"].ToString(),
                                REQ = dr["Req"].ToString(),
                                REC = dr["Rec"].ToString(),
                                REM = dr["Rem"].ToString()
                            });

                            total = total + (int)dr["Rem"];

                            result.TOTAL = total;
                        }
                    }
                    else
                    {
                        result.GetSummary.Add(new mGetSummary()
                        {
                            DATE = "-",
                            REQ = "-",
                            REC = "-",
                            REM = "-"
                        });
                    }


                }

                connection.Close();
            }
            return result;
        }

        public mRemindByDate GetRemindByDate(iDateInputRemind input)
        {
            mRemindByDate result = new mRemindByDate();
            result.GetRemindByDate = new List<getRemindByDate>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;

                    if (input.nTypeJob == 0)
                    {
                        command1.CommandText = @"SELECT B.BARCODE, C.WGROUP AS WELD, C.LENGTH, C.WEIGHT
                                             FROM STOCK_IN_DOC AS A INNER JOIN
                                             STOCK_IN AS B ON A.DOCNO = B.DOCNO INNER JOIN
                                             PRODUCT AS C ON B.BARCODE = C.BARCODE
                                             WHERE (CONVERT(VARCHAR(10),A.DATEREQ,121) = @DATE) AND (B.PLACE IS NULL)
                                             ORDER BY A.DATEREQ";
                    }
                    else
                    {
                        command1.CommandText = @"SELECT B.BARCODE, C.WGROUP AS WELD, C.LENGTH, C.WEIGHT
                                                FROM STOCK_IN_REV_DOC AS A INNER JOIN
                                                STOCK_IN_REV AS B ON A.DOCNO = B.DOCNO INNER JOIN
                                                PRODUCT AS C ON B.BARCODE = C.BARCODE
                                                WHERE (CONVERT(VARCHAR(10),A.DATEREQ,121) = @DATE) AND (B.PLACE IS NULL)
                                                ORDER BY A.DATEREQ";
                    }
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@DATE", SqlDbType.DateTime, 10).Value = input.sDate;
                    SqlDataReader dr = command1.ExecuteReader();
                    int total = 0;

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            result.GetRemindByDate.Add(new getRemindByDate()
                            {
                                BARCODE = dr["BARCODE"].ToString(),
                                WELD = dr["WELD"].ToString(),
                                LENGTH = dr["LENGTH"].ToString(),
                                WEIGHT = dr["WEIGHT"].ToString(),
                            });

                            total = total + 1;

                            result.TOTAL = total;
                        }
                    }
                    else
                    {
                        result.GetRemindByDate.Add(new getRemindByDate()
                        {
                            BARCODE = "-",
                            WELD = "-",
                            LENGTH = "-",
                            WEIGHT = "-",
                        });
                    }
                }

                connection.Close();
            }

            return result;
        }

        public mGetStockInDaily GetStockInDaily(iDateInputRemind input)
        {
            mGetStockInDaily result = new mGetStockInDaily();
            result.GetStockInDaily = new List<getStockInDaily>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;

                    if (input.nTypeJob == 0)
                    {
                        command1.CommandText = @"SELECT PRODUCT.PROJECT,PRODUCT_TYPE.TYPENAME, PRODUCT.SHARP, PRODUCT.ITEM,
                                                  CONVERT(VARCHAR,CONVERT(MONEY,PRODUCT.LENGTH),1) AS [LENGTH], 
                                                  CAST(PRODUCT.WEIGHT AS  DECIMAL(9,3)) AS WEIGHT ,
                                                  ISNULL(STOCK_AREA.SANAME,'-') AS FROMPLACE, STOCK_IN.FROMCARTRUCK,
                                                  STOCK_IN.DESTCRANE, STOCK_IN.PLACE, 
                                                  STOCK_IN.USERS, LEFT(CONVERT(VARCHAR(10), STOCK_IN.DATES, 108),5) AS DATES 
                                                  FROM PRODUCT_TYPE INNER JOIN 
                                                  PRODUCT ON PRODUCT_TYPE.TYPE = PRODUCT.TYPE RIGHT OUTER JOIN 
                                                  STOCK_AREA RIGHT OUTER JOIN 
                                                  STOCK_IN ON STOCK_AREA.SAID = STOCK_IN.FROMFAC ON PRODUCT.BARCODE = STOCK_IN.BARCODE 
                                                  WHERE (CONVERT(VARCHAR(10), STOCK_IN.DATES, 120) = @DATE) AND STOCK_IN.STATUS =1 
                                                  ORDER BY STOCK_IN.DATES";
                    }
                    else if (input.nTypeJob == 1)
                    {
                        command1.CommandText = @"SELECT product.project, product_type.typeName, product.sharp, product.item,
                                                  convert(varchar,convert(money,product.length),1) as [length], 
                                                  cast(product.weight as  decimal(9,3)) as weight, isnull(stock_in_rev.fromCarTruck,'-') as fromCarTruck  ,
                                                 isnull(stock_in_rev.fromPlace,'-')as fromPlace , stock_in_rev.fromCrane,stock_in_rev.destCrane, 
                                                  stock_in_rev.place, stock_in_rev.users, LEFT(CONVERT(varchar(10), stock_in_rev.dates, 108), 5) AS dates
                                                 FROM product_type INNER JOIN 
                                                  product ON product_type.type = product.type RIGHT OUTER JOIN 
                                                 stock_in_rev ON product.barcode = stock_in_rev.barcode 
                                                  WHERE     (CONVERT(varchar(10), stock_in_rev.dates, 121) = @DATE) AND (stock_in_rev.status = 1) ";
                    }
                    else
                    {
                        command1.CommandText = @"SELECT  STOCK_MOVE.DESTUSER AS users, LEFT(CONVERT(VARCHAR(10), STOCK_MOVE.DESTDATE, 108), 5) AS DATES, 
                                                 PRODUCT.PROJECT, PRODUCT_TYPE.TYPENAME, PRODUCT.SHARP, PRODUCT.ITEM, 
                                                 CONVERT(VARCHAR,CONVERT(MONEY,PRODUCT.LENGTH),1) AS [LENGTH], 
                                                 CAST(PRODUCT.WEIGHT AS  DECIMAL(9,3)) AS WEIGHT, ISNULL(STOCK_MOVE.FROMCARTRUCK,'-') AS FROMCARTRUCK  ,
                                                 ISNULL(STOCK_MOVE.FROMPLACE,'-')AS FROMPLACE ,ISNULL(STOCK_MOVE.FROMCRANE,'-') AS FROMCRANE, 
                                                 ISNULL(STOCK_MOVE.DESTCRANE,'-') AS DESTCRANE, 
                                                 STOCK_MOVE.DESTPLACE AS PLACE 
                                                 FROM PRODUCT_TYPE INNER JOIN 
                                                 PRODUCT ON PRODUCT_TYPE.TYPE = PRODUCT.TYPE RIGHT OUTER JOIN  
                                                 STOCK_MOVE ON PRODUCT.BARCODE = STOCK_MOVE.BARCODE  
                                                 WHERE (CONVERT(VARCHAR(10), STOCK_MOVE.DESTDATE, 121) = @DATE) AND (STOCK_MOVE.STATUS = 1)  
                                                 ORDER BY DATES";
                    }
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@DATE", SqlDbType.DateTime, 10).Value = input.sDate;
                    SqlDataReader dr = command1.ExecuteReader();

                    int total = 0;
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            result.GetStockInDaily.Add(new getStockInDaily()
                            {
                                PROJECT = dr["project"].ToString(),
                                TYPE_NAME = dr["typeName"].ToString(),
                                SHARP = dr["sharp"].ToString(),
                                ITEM = dr["item"].ToString(),
                                LENGTH = dr["length"].ToString(),
                                WEIGHT = dr["weight"].ToString(),
                                FROM_PLACE = dr["fromPlace"].ToString(),
                                FROM_CAR_TRUCK = dr["fromCarTruck"].ToString(),
                                DEST_CRANE = dr["destCrane"].ToString(),
                                PLACE = dr["place"].ToString(),
                                USERS = dr["users"].ToString(),
                                DATES = dr["dates"].ToString()
                            });

                            total = total + 1;

                            result.TOTAL = total;
                        }
                    }
                    else
                    {
                        result.GetStockInDaily.Add(new getStockInDaily()
                        {
                            PROJECT = "-",
                            TYPE_NAME = "-",
                            SHARP = "-",
                            ITEM = "-",
                            LENGTH = "-",
                            WEIGHT = "-",
                            FROM_PLACE = "-",
                            FROM_CAR_TRUCK = "-",
                            DEST_CRANE = "-",
                            PLACE = "-",
                            USERS = "-",
                            DATES = "-"
                        });
                    }
                }

                connection.Close();
            }

            return result;
        }

        #endregion

        #region Stock Out
        private string Check_NCR(string sBarcode)
        {
            string result = "";

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsQcConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT  MCSQC.DBO.SF_CHECK_NCR(@BARCODE) as remain";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@BARCODE", SqlDbType.VarChar, 10).Value = sBarcode;
                    SqlDataReader dr = command1.ExecuteReader();

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            result = dr["remain"].ToString();
                        }
                        if(result != "OK")
                        {
                         string[] Temp = result.Split('|');

                            //for (int i = 0; i <= result.Length; i++)
                            //{
                            result += Temp[0] + System.Environment.NewLine;
                            //}
                        }                     
                    }
                    else
                    {
                        return "OK";
                    }
                }

                connection.Close();
            }

            return result;
        }

        private string GetStockOutDocRequest(string sBarcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            string result = "";

            stock_out iTop1 = dc.stock_outs.Where(so => so.barcode == sBarcode && so.status == 0).FirstOrDefault();

            if (iTop1 != null)
            {
                result = iTop1.docNo.ToString();
            }
            else
            {
                result = "";
            }

            return result;
        }

        private string getDocNoStockMoveOutByBarcode(string sBarcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            string result = "";

            stock_move iTop1 = dc.stock_moves.Where(so => so.barcode == sBarcode && so.status == 0).FirstOrDefault();

            if (iTop1 != null)
            {
                result = iTop1.docNo.ToString();
            }
            else
            {
                result = "";
            }

            return result;
        }

        private bool IsMoveOutRequest(string sBarcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            var query = from sm in dc.stock_moves
                        where sm.barcode == sBarcode && sm.status == 0
                        select sm;
            if (query.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private bool IsStockOutTripReady(string sDocNo, string sCarNo)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            var query = from ct in dc.car_trips
                        where ct.docNo == sDocNo && ct.carNo == sCarNo && ct.status == 0
                        select ct;

            if (query.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool SetProductMoveOutOnShelf(string sBarcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            if (!IsBarcode(sBarcode))
            {
                return false;
            }

            string place = "";
            int? no = 0;

            foreach (stock iPlace in dc.stocks.Where(s => s.barcode == sBarcode))
            {
                place = iPlace.place.ToString();
                no = iPlace.no;
            }

            stock iStock = dc.stocks.Where(s => s.place == place && s.no > no).FirstOrDefault();

            if (iStock != null)
            {
                iStock.no = no - 1;

                dc.SubmitChanges();
            }

            stock dStock = dc.stocks.Where(s => s.barcode == sBarcode).FirstOrDefault();

            if (dStock != null)
            {
                dc.stocks.DeleteOnSubmit(dStock);
                dc.SubmitChanges();
            }
            else
            {
                return false;
            }

            return true;
        }

        private bool IsStockOutRevRequest(string sBarcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            var iStockOut = from sor in dc.stock_out_revs
                            where sor.barcode == sor.barcode && sor.status == 0
                            select sor;

            if (iStockOut.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private string CheckStatus(string sBarcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            string result = "";
            int status = 0;
            var check = from pd in dc.products
                            where pd.barcode == sBarcode
                            select new { pd.status };
            foreach(var item in check)
            {
                status = item.status;
            }
            if(status==0)
            {
                result = "0|ชิ้นงานยังอยู่่ในขั้นตอนการผลิต";

            }
            else if(status==1)
            {
                result = "1|ชิ้นงานอยู่ในคลัง";
            }
            else if(status==2)
            {
                result = "2|ชิ้นงานแก้ไขอยู่ในโรงงาน";
            }
            else if (status == 3)
            {
                result = "3|ย้ายชิ้นงานออกจากคลัง";
            }
            else if (status == 5)
            {
                result = "5|ชิ้นงานแจ้งเข้าคลัง";
            }
            else if (status == 6)
            {
                result = "6|ชิ้นงานแก้ไขแจ้งเข้าคลัง";
            }
            else if (status == 7)
            {
                result = "7|แจ้งชิ้นงานออกท่าเรือ";
            }
            else if (status == 8)
            {
                result = "8|ชิ้นงานแก้ไขออกจากคลัง";
            }
            else if (status == 9)
            {
                result = "9|ชิ้นงานไปท่าเรือ";
            }
            else
            {
                return result = "";
            }
            return result;       
        }

        private string getDocNoStockOutRevByBarcode(string sBarcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            stock_out_rev iStockOutRev = dc.stock_out_revs.Where(sor => sor.barcode == sBarcode && sor.status == 0).FirstOrDefault();

            if (iStockOutRev != null)
            {
                return iStockOutRev.docNo.ToString();
            }
            else
            {
                return "";
            }
        }

        private int SetProductIntoShelf_NoZero(string sBarcodeRef, string sBarcode, string sUser)
        {
            sBarcodeRef = sBarcodeRef.Trim().ToUpper();
            sBarcode = sBarcode.Trim().ToUpper();

            if (IsShelfInStock(sBarcodeRef))
            {
                AddProductIntoShelf(sBarcode, sBarcodeRef, 0, sUser);
            }
            else
            {
                return -2;
            }
            return 1;
        }

        private string GetStockOutDocNoPrint(string sBarcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            string result = "";
            //เปลี่ยนdc.stock_ins เป็นstock stock_out
           stock_out iTop1 = dc.stock_outs.Where(si => si.barcode == sBarcode).First();

            if (iTop1 != null)
            {
                result = iTop1.docNo.ToString();
            }
            else
            {
                result = "";
            }

            return result;
        }

        public mResult SetOnTruck(iSetOnTruck input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            mResult result = new mResult();
            DateTime now = DateTime.Now;
            string sDocNo = "";
            int? sTripNo = 0;

            int project = 0;
            int items = 0;
            int point = input.sBarcode.IndexOf("-");
            int status = 0;
            string Error = "";
            var query3 = from si in dc.products
                         where si.barcode == input.sBarcode
                         select new { si.status };
            foreach (var item in query3)
            {
                status = (int)item.status;
            }

            Error = CheckStatus(input.sBarcode);
            if (status != 7)
            {
                result.RESULT = "00|" + Error;
                return result;
            }

            if (point == -1)
            {
                project = 0;
                items = 0;
            }
            else
            {
                project = int.Parse(input.sBarcode.Substring(0, point));
                items = int.Parse(input.sBarcode.Substring(point + 1));
            }

            if (!_4IsItemSendPaint(project, items))
            {
                result.RESULT = "-13|ไม่สามารถบันทึกข้อมูลได้ ชิ้นงานยังไม่ได้ส่งงานสี";
                result.EXCEPTION = "";

                return result;
            }

            string ncr_check = "";
            ncr_check = Check_NCR(input.sBarcode);

            if (ncr_check != "OK")
            {
                result.RESULT = "9|NCR : " + ncr_check;
                result.EXCEPTION = "";
                return result;
            }

            sDocNo = GetStockOutDocRequest(input.sBarcode);

            if (sDocNo == "")
            {
                result.RESULT = "-12|เลขที่เอกสารแจ้งงานเข้าคลังสินค้า ที่พิมพ์ไม่ตรงกับเลขที่เอกสารที่แจ้ง";
                result.EXCEPTION = "";

                return result;
            }

            if (!IsCarNo(input.sCarNo))
            {
                result.RESULT = "-10|เลขทะเบียนรถผิด";
                result.EXCEPTION = "";

                return result;
            }

            if (!IsStockOutTripReady(sDocNo, input.sCarNo))
            {
                //add car trip
                int NoCar = 0;
                string IdCart = "";


                var iCountNoCar = from ct in dc.car_trips
                                  where ct.docNo == sDocNo
                                  select ct;

                NoCar = iCountNoCar.Count();

                foreach (car_truck_driver itemIdCard in dc.car_truck_drivers.Where(ctd => ctd.carno == input.sCarNo))
                {
                    IdCart = itemIdCard.idcard;
                }
                
                try
                {
                    car_trip cartrip = new car_trip()
                    {
                        status = 0,
                        docNo = new string(sDocNo.ToUpper().Take(12).ToArray()),
                        tripNo = NoCar,
                        carNo = new string(input.sCarNo.Take(10).ToArray()),
                        idcard = new string(IdCart.Take(10).ToArray()),
                        users = new string(input.sUserID.Take(10).ToArray()),
                        dates = (DateTime)(now),
                        dateStart = (DateTime)(now)
                        
                    };

                    dc.car_trips.InsertOnSubmit(cartrip);
                    dc.SubmitChanges();
                }
                catch (Exception ex)
                {
                    result.RESULT = "";
                    result.EXCEPTION = ex.Message;
                    return result;
                }
            }

            if (SetProductMoveOutOnShelf(input.sBarcode))
            {
                //update stock_out,product
                foreach (car_trip tripno in dc.car_trips.Where(ct => ct.docNo == sDocNo && ct.carNo == input.sCarNo && ct.status == 0))
                {
                    sTripNo = tripno.tripNo;
                }

                stock_out iStockOut = dc.stock_outs.Where(so => so.docNo == sDocNo && so.barcode == input.sBarcode && so.status == 0).FirstOrDefault();

                if (iStockOut != null)
                {
                    iStockOut.status = 3;
                    iStockOut.users = input.sUserID;
                    iStockOut.dates = (DateTime)(now);
                    iStockOut.tripNo = sTripNo;
                    iStockOut.fromCrane = input.sCrane;

                    dc.SubmitChanges();
                }

                var iProduct = from pd in dc.products
                               where pd.barcode == input.sBarcode && dc.stock_outs.Any(so => so.barcode == input.sBarcode && so.status == 3)
                               select pd;

                if (iProduct.FirstOrDefault() != null)
                {
                    foreach (var itemsPr in iProduct)
                    {
                        itemsPr.status = 9;
                    }

                    dc.SubmitChanges();
                }

                //update stock_out_doc (start,finish)
                stock_out_doc iStockOutDoc = dc.stock_out_docs.Where(soc => soc.docNo == sDocNo && soc.dateStart == null).FirstOrDefault();

                if (iStockOutDoc != null)
                {
                    iStockOutDoc.status = 2;
                    iStockOutDoc.dateStart = (DateTime)(now);

                    dc.SubmitChanges();
                }

                var iStockOutDoc2 = from sod in dc.stock_out_docs
                                    where sod.status == 2 && sod.docNo == sDocNo && !dc.stock_outs.Any(si => si.status != 1 && si.docNo == sDocNo)
                                    select sod;

                if (iStockOutDoc2.FirstOrDefault() != null)
                {
                    foreach (var iSod2 in iStockOutDoc2)
                    {
                        iSod2.status = 1;
                        iSod2.dateFinish = (DateTime)(now);
                    }

                    dc.SubmitChanges();
                }

            }

            result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
            result.EXCEPTION = "";

            return result;
        }

        public mResult SetOutCom(iSetOutCom input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            mResult result = new mResult();
            DateTime now = DateTime.Now;
            
            string sDocNo = "";
            string doc = "";
            string ship = "";
            int trip = 0;
            var query = from s in dc.stock_out_prints
                        where s.status == 0
                        select new { s.doc, s.ship, s.trip };
            if (query.FirstOrDefault() != null)
            {
                foreach (var item in query)
                {
                    doc = item.doc.ToString();
                    ship = item.ship.ToString();
                    trip = item.trip;
                }
            }
            sDocNo = GetStockOutDocNoPrint(input.sBarcode);
            try
            {
                stock_out iStockOut = dc.stock_outs.Where(so => so.docNo == input.sDocNo && so.barcode == input.sBarcode && so.status == 3).FirstOrDefault();

                if (iStockOut != null)
                {
                    iStockOut.status = 4;
                    iStockOut.secUser = input.sUserID;
                    iStockOut.secDate = (DateTime)(now);

                    dc.SubmitChanges();

                    //==============================================================================================
                    string path = System.Web.Configuration.WebConfigurationManager.AppSettings["pathFileUpdate"].ToString();
                    try
                    {
                        if (File.Exists("Data.xml") == false)
                        {
                            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
                            XmlDocument xml = new XmlDocument();
                            xml.Load(path);
                            int idcount = xml.SelectNodes("Datas/Data").Count;
                            idcount++;
                            XDocument xDocument = XDocument.Load(path);
                            XElement root = xDocument.Element("Datas");
                            IEnumerable<XElement> rows = root.Descendants("Data");
                            XElement firstRow = rows.First();
                            firstRow.AddBeforeSelf(
                                new XElement("Data",
                                    new XAttribute("ID", idcount),
                                    new XElement("docNo", sDocNo),
                                    new XElement("shipNo", ship),
                                    new XElement("tripNo", trip),
                                    new XElement("userc", input.sUserID),
                                    new XElement("timec", DateTime.Now),
                                    new XElement("isPrint", "1"),
                                    new XElement("timep", DateTime.Now)));
                            xDocument.Save(path);
                        }
                        else
                        {
                            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
                            XmlDocument xml = new XmlDocument();
                            xml.Load(path);
                            int idcount = xml.SelectNodes("Datas/Data").Count;
                            idcount++;
                            //  XmlNodeList xmlNodeList = xml.SelectNodes("Datas/Data");

                            xmlWriterSettings.Indent = true;
                            xmlWriterSettings.NewLineOnAttributes = true;
                            using (XmlWriter xmlWriter = XmlWriter.Create(path, xmlWriterSettings))
                            {
                                xmlWriter.WriteStartDocument();
                                xmlWriter.WriteStartElement("Datas");
                                xmlWriter.WriteStartElement("Data");
                                xmlWriter.WriteAttributeString("ID", idcount.ToString());
                                xmlWriter.WriteElementString("docNo", sDocNo);
                                xmlWriter.WriteElementString("shipNo", ship);
                                xmlWriter.WriteElementString("tripNo", trip.ToString());
                                xmlWriter.WriteElementString("userc", input.sUserID);
                                xmlWriter.WriteElementString("timec", DateTime.Now.ToString());
                                xmlWriter.WriteElementString("isPrint", "1");
                                xmlWriter.WriteElementString("timep", DateTime.Now.ToString());
                                xmlWriter.WriteEndElement();
                                xmlWriter.WriteEndElement();
                                xmlWriter.WriteEndDocument();
                                xmlWriter.Flush();
                                xmlWriter.Close();
                            }
                        }
                    }
                    catch (Exception)
                    {
                        {
                            result.RESULT = "0|Error";
                            return result;
                        }
                    }
                    //==============================================================================================
                    result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
                    result.EXCEPTION = "";
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "0|ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }

            return result;
        }

        public mResult SetOutUpReceive(iSetOutUpReceive input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            mResult result = new mResult();
            DateTime now = DateTime.Now;
            string Error = "";
            int status = 0;
            string sDocNo = "";
            var query3 = from si in dc.products
                         where si.barcode == input.sBarcode
                         select new { si.status };
            foreach (var item in query3)
            {
                status = (int)item.status;
            }

            Error = CheckStatus(input.sBarcode);
            if (status != 8)
            {
                result.RESULT = "00|" + Error;
                return result;
            }

            if (!IsStockOutRevRequest(input.sBarcode))
            {
                result.RESULT = "11|ยังไม่ได้แจ้งงานออกไปแก้ไข";
                result.EXCEPTION = "";

                return result;
            }

            if (!IsCarNo(input.sCarTruck))
            {
                result.RESULT = "-10|เลขทะเบียนรถผิด";
                result.EXCEPTION = "";

                return result;
            }

            sDocNo = getDocNoStockOutRevByBarcode(input.sBarcode);

            if (sDocNo == "")
            {
                result.RESULT = "-11|Not Found DocNo";
                result.EXCEPTION = "";

                return result;
            }
            stock_out_rev iStockOutRev = dc.stock_out_revs.Where(sor => sor.barcode == input.sBarcode && sor.status == 0).FirstOrDefault();

            if (iStockOutRev != null && iStockOutRev.fromCarTruck ==null)
            {
                iStockOutRev.fromCrane = input.sCrane;
                iStockOutRev.fromUser = input.sUserID;
                iStockOutRev.fromDate = (DateTime)(now);
                iStockOutRev.fromCarTruck = input.sCarTruck;

                dc.SubmitChanges();

                result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
                result.EXCEPTION = "";

                return result;
            }
            else
            {
                result.RESULT = "2|ชิ้นงานอยู่บนรถเทรนเลอร์";
                result.EXCEPTION = "";

                return result;
            }
        }

        public mResult SetOutDownReceive(iSetOutDownReceive input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            DateTime now = DateTime.Now;
            mResult result = new mResult();
            string sDocNo = "";
            string place = "";
            int nResult = 0;
            string Error = "";
            int status = 0;
            var query3 = from si in dc.products
                         where si.barcode == input.sBarcode
                         select new { si.status };
            foreach (var item in query3)
            {
                status = (int)item.status;
            }

            Error = CheckStatus(input.sBarcode);
            if(status != 8)
            {
                result.RESULT = "00|" + Error;
                return result;
            }

            if (!IsStockOutRevRequest(input.sBarcode))
            {
                result.RESULT = "-10|ยังไม่ได้แจ้งงาน Revise ออกคลังสินค้า";
                result.EXCEPTION = "error";

                return result;
            }

            sDocNo = getDocNoStockOutRevByBarcode(input.sBarcode);

            if (sDocNo == "")
            {
                result.RESULT = "-11|ยังไม่แจ้งงานแก้ไขเข้าคลัง";
                result.EXCEPTION = "error";

                return result;
            }

            if (!IsShelfInStock(input.sBarcodeRef))
            {
                result.RESULT = "-3|ชื่อชั้นวางผิด";
                result.EXCEPTION = "error";

                return result;
            }

            //int BarcodeCount = GetShelfProductTotal(input.sBarcodeRef);
            //if (BarcodeCount > 0)
            //{
            //    result.RESULT = "-3|ไม่มีชั้นวาง";
            //    result.EXCEPTION = "";
            //    return result;
            //}

            if (SetProductMoveOutOnShelf(input.sBarcode))
            {
                nResult = SetProductIntoShelf_NoZero(input.sBarcodeRef, input.sBarcode, input.sUserID);

                if (nResult == 1)
                {
                    foreach (stock iStock in dc.stocks.Where(s => s.barcode == input.sBarcode))
                    {
                        place = iStock.place.ToString();
                    }

                    try
                    {
                        //update stock_out_rev,product
                        stock_out_rev iStockOutRev = dc.stock_out_revs.Where(sor => sor.barcode == input.sBarcode && sor.status == 0).FirstOrDefault();

                        if (iStockOutRev != null)
                        {
                            iStockOutRev.status = 1;
                            iStockOutRev.place = place;
                            iStockOutRev.users = input.sUserID;
                            iStockOutRev.dates = (DateTime)(now);
                            iStockOutRev.destCrane = input.sCrane;

                            dc.SubmitChanges();
                        }

                        product iProduct = dc.products.Where(pd => pd.barcode == input.sBarcode).FirstOrDefault();

                        if (iProduct != null)
                        {
                            iProduct.status = 2;

                            dc.SubmitChanges();
                        }
                        //sDocNo = GetDocNoStockInRevByBarcode(input.sBarcode);
                        using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
                        {
                            connection.Open();
                            try
                            {
                                using (SqlCommand command1 = new SqlCommand())
                                {
                                    command1.Connection = connection;
                                    command1.CommandText = @"update stock_out_rev_doc set status=2, dateStart=getdate() where status=0 and docNo=@docno and not exists
                                                                                    (select * from stock_out_rev where status=0 and docNo=@docno) ";
                                    command1.Parameters.Add("@docno", SqlDbType.NVarChar, 15).Value = sDocNo;
                                    command1.ExecuteNonQuery();
                                }
                            }
                            catch (Exception e)
                            {
                                result.RESULT = "0|Error";
                                result.EXCEPTION = e.Message;
                                return result;
                            }
                            connection.Close();
                        }

                    //    update stock_out_rev_doc (start,finish)
                        //var query1 = from sord in dc.stock_out_rev_docs
                        //             where sord.status == 0 && sord.docNo == sDocNo && !dc.stock_out_revs.Any(si => si.status == 0 && si.docNo == sDocNo)
                        //             select sord;

                        //if (query1.FirstOrDefault() != null)
                        //{
                        //    foreach (var iStockOutRevDoc in query1)
                        //    {
                        //        iStockOutRevDoc.status = 2;
                        //        iStockOutRevDoc.dateStart = (DateTime)(now);
                        //    }

                        //    dc.SubmitChanges();
                        //}

                        var query2 = from sord in dc.stock_out_rev_docs
                                     where sord.status == 2 && sord.docNo == sDocNo && !dc.stock_out_revs.Any(si => si.status == 0 && si.docNo == sDocNo)
                                     select sord;

                        if (query2.FirstOrDefault() != null)
                        {
                            foreach (var iStockOutRevDoc2 in query2)
                            {
                                iStockOutRevDoc2.status = 1;
                                iStockOutRevDoc2.dateFinish = (DateTime)(now);
                            }

                            dc.SubmitChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        result.RESULT = "-10|ยังไม่ได้แจ้งงานแก้ไขออกจากคลังสินค้า";
                        result.EXCEPTION = ex.Message;

                        return result;
                    }
                }
            }
            else
            {
                result.RESULT = "-14|บันทึกข้อมูลลำดับชิ้นงานภายในชั้นวางไม่สาเร็จ";
                result.EXCEPTION = "error";

                return result;
            }

            result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
            result.EXCEPTION = "OK";
            return result;
        }

        public mGetSummaryByDateStockOut GetSummaryByDateStockOut(iGetSummaryByDate_SO input)
        {
            mGetSummaryByDateStockOut result = new mGetSummaryByDateStockOut();
            result.GetSummaryByDate_SO = new List<GetSummaryByDateStockOut>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;

                    if (input.nTypeJob == 0)
                    {
                        command1.CommandText = @"SELECT * FROM (SELECT TOP (100) PERCENT CONVERT(VARCHAR(10),A.DATEREQ,121) AS DATE, COUNT(B.BARCODE) AS REQ, COUNT(B.USERS)                                                 AS REC, COUNT(B.BARCODE) - COUNT(B.USERS) AS REM
                                              FROM STOCK_OUT_DOC AS A INNER JOIN
                                              STOCK_OUT AS B ON A.DOCNO = B.DOCNO
                                              GROUP BY CONVERT(VARCHAR(10),A.DATEREQ,121)
                                              HAVING (CONVERT(VARCHAR(10), A.DATEREQ, 121) BETWEEN @DATE_START AND @DATE_FINISH))AS SUMMARY WHERE REM>0 ORDER BY DATE";
                    }
                    else
                    {
                        command1.CommandText = @"SELECT * FROM (SELECT TOP (100) PERCENT CONVERT(VARCHAR(10),A.DATEREQ,121) AS DATE, COUNT(B.BARCODE) AS REQ, COUNT(B.PLACE)                                                 AS REC, COUNT(B.BARCODE) - COUNT(B.PLACE) AS REM
                                               FROM STOCK_OUT_REV_DOC AS A INNER JOIN
                                               STOCK_OUT_REV AS B ON A.DOCNO = B.DOCNO
                                               GROUP BY CONVERT(VARCHAR(10),A.DATEREQ,121)
                                               HAVING (CONVERT(VARCHAR(10), A.DATEREQ, 121) BETWEEN @DATE_START AND @DATE_FINISH))AS SUMMARY WHERE REM>0 ORDER BY DATE";
                    }
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@DATE_START", SqlDbType.DateTime, 10).Value = input.sDateStart;
                    command1.Parameters.Add("@DATE_FINISH", SqlDbType.DateTime, 10).Value = input.sDateFinish;
                    SqlDataReader dr = command1.ExecuteReader();

                    int total = 0;
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            result.GetSummaryByDate_SO.Add(new GetSummaryByDateStockOut()
                            {
                                DATE = dr["Date"].ToString(),
                                REQ = dr["Req"].ToString(),
                                REC = dr["Rec"].ToString(),
                                REM = dr["Rem"].ToString()
                            });
                            total = total + (int)dr["Rem"];
                            result.TOTAL = total;
                        }

                       // total = total + (int)dr["Rem"];
                        
                    }
                    else
                    {
                        result.GetSummaryByDate_SO.Add(new GetSummaryByDateStockOut()
                        {
                            DATE = "-",
                            REQ = "-",
                            REC = "-",
                            REM = "-"
                        });
                    }
                  //  total = total + (int)dr["Rem"];
                   // result.TOTAL = total;
                }

                connection.Close();
            }

            return result;
        }

        public mGetRemindByDateStockOut GetRemindByDateStockOut(iGetRemindDate input)
        {
            mGetRemindByDateStockOut result = new mGetRemindByDateStockOut();
            result.GetRemindByDate_SO = new List<GetRemindByDateStockOut>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;

                    if (input.nTypeJob == 0)
                    {
                        command1.CommandText = @"SELECT B.BARCODE, C.WGROUP AS WELD, C.LENGTH, C.WEIGHT
                                                FROM STOCK_OUT_DOC AS A INNER JOIN
                                                STOCK_OUT AS B ON A.DOCNO = B.DOCNO INNER JOIN
                                                PRODUCT AS C ON B.BARCODE = C.BARCODE
                                                WHERE (CONVERT(VARCHAR(10),A.DATEREQ,121) = @DATE) AND (B.USERS IS NULL)
                                                ORDER BY A.DATEREQ";
                    }
                    else
                    {
                        command1.CommandText = @"SELECT B.BARCODE, C.WGROUP AS WELD, C.LENGTH, C.WEIGHT
		                                        FROM STOCK_OUT_REV_DOC AS A INNER JOIN
                                                STOCK_OUT_REV AS B ON A.DOCNO = B.DOCNO INNER JOIN
                                                PRODUCT AS C ON B.BARCODE = C.BARCODE
                                                WHERE (CONVERT(VARCHAR(10),A.DATEREQ,121) = @DATE) AND (B.PLACE IS NULL)
                                                ORDER BY A.DATEREQ";
                    }
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@DATE", SqlDbType.DateTime, 10).Value = input.sDate;
                    SqlDataReader dr = command1.ExecuteReader();

                    int total = 0;
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            result.GetRemindByDate_SO.Add(new GetRemindByDateStockOut()
                            {
                                BARCODE = dr["BARCODE"].ToString(),
                                WELD = dr["WELD"].ToString(),
                                LENGTH = dr["LENGTH"].ToString(),
                                WEIGTH = dr["WEIGHT"].ToString(),
                            });

                            total = total + 1;
                            result.TOTAL = total;
                        }
                    }
                    else
                    {
                        result.GetRemindByDate_SO.Add(new GetRemindByDateStockOut()
                        {
                            BARCODE = "-",
                            WELD = "-",
                            LENGTH = "-",
                            WEIGTH = "-",
                        });
                    }
                }

                connection.Close();
            }

            return result;
        }

        public mGetStockOutDaily GetStockOutDaily(iGetRemindDate input)
        {
            mGetStockOutDaily result = new mGetStockOutDaily();
            result.GetStockOutDaily_SO = new List<GetStockOutDaily>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;

                    if (input.nTypeJob == 0)
                    {
                        command1.CommandText = @"SELECT LEFT(CONVERT(VARCHAR(10), STOCK_OUT.DATES, 108), 5) AS DATES, STOCK_OUT.USERS, PRODUCT.PROJECT, 
                                                 PRODUCT_TYPE.TYPENAME, PRODUCT.SHARP, PRODUCT.ITEM, 
                                                 CONVERT(VARCHAR, CONVERT(MONEY, PRODUCT.LENGTH), 1) AS LENGTH, CAST(PRODUCT.WEIGHT AS DECIMAL(9, 3)) AS WEIGHT,
                                                 ISNULL(STOCK_OUT.FROMCRANE, N'-') AS FROMCRANE, STOCK_OUT.PLACE AS FROMPLACE, CAR_TRIP.CARNO AS FROMCARTRUCK 
                                                 FROM STOCK_OUT INNER JOIN 
                                                 CAR_TRIP ON STOCK_OUT.DOCNO = CAR_TRIP.DOCNO AND STOCK_OUT.TRIPNO = CAR_TRIP.TRIPNO LEFT OUTER JOIN
                                                 PRODUCT_TYPE INNER JOIN
                                                 PRODUCT ON PRODUCT_TYPE.TYPE = PRODUCT.TYPE ON STOCK_OUT.BARCODE = PRODUCT.BARCODE 
                                                 WHERE (CONVERT(VARCHAR(10), STOCK_OUT.DATES, 121) = @DATE) AND (PRODUCT.STATUS = 9) 
                                                 ORDER BY DATES;";
                    }
                    else if (input.nTypeJob == 1)
                    {
                        command1.CommandText = @"SELECT LEFT(CONVERT(VARCHAR(10), STOCK_OUT_REV.DATES, 108), 5) AS DATES, STOCK_OUT_REV.USERS,
                                                PRODUCT.PROJECT, PRODUCT_TYPE.TYPENAME, PRODUCT.SHARP, PRODUCT.ITEM, 
                                                CONVERT(VARCHAR,CONVERT(MONEY,PRODUCT.LENGTH),1) AS [LENGTH],  
                                                CAST(PRODUCT.WEIGHT AS  DECIMAL(9,3)) AS WEIGHT, ISNULL(STOCK_OUT_REV.FROMCARTRUCK,'-') AS FROMCARTRUCK  ,  
                                                ISNULL(STOCK_OUT_REV.FROMCRANE,'-') AS FROMCRANE, 
                                                 ISNULL(STOCK_OUT_REV.DESTCRANE,'-') AS DESTCRANE, STOCK_OUT_REV.PLACE 
                                                 FROM PRODUCT_TYPE INNER JOIN  
                                                 PRODUCT ON PRODUCT_TYPE.TYPE = PRODUCT.TYPE RIGHT OUTER JOIN  
                                                 STOCK_OUT_REV ON PRODUCT.BARCODE = STOCK_OUT_REV.BARCODE 
                                                 WHERE     (CONVERT(VARCHAR(10), STOCK_OUT_REV.DATES, 121) = @DATE) AND (STOCK_OUT_REV.STATUS = 1)  
                                                 ORDER BY DATES ;";
                    }

                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@DATE", SqlDbType.DateTime, 10).Value = input.sDate;
                    SqlDataReader dr = command1.ExecuteReader();

                    int total = 0;
                    if (dr.HasRows && input.nTypeJob == 0)
                    {
                        while (dr.Read())
                        {
                            result.GetStockOutDaily_SO.Add(new GetStockOutDaily()
                            {
                                DATES = dr["dates"].ToString(),
                                USERS = dr["users"].ToString(),
                                PROJECT = dr["project"].ToString(),
                                TYPE_NAME = dr["typeName"].ToString(),
                                SHARP = dr["sharp"].ToString(),
                                ITEM = dr["item"].ToString(),
                                LENGTH = dr["length"].ToString(),
                                WEIGHT = dr["weight"].ToString(),
                                FROM_CRANE = dr["fromCrane"].ToString(),
                                FROM_PLACE = dr["fromPlace"].ToString(),
                                FROM_CAR_TRUCK = dr["fromCarTruck"].ToString(),
                                DESC_CRANE = "-"
                            });

                            total = total + 1;
                            result.TOTAL = total;
                        }
                    }
                    else if (dr.HasRows && input.nTypeJob == 1)
                    {
                        while (dr.Read())
                        {
                            result.GetStockOutDaily_SO.Add(new GetStockOutDaily()
                            {
                                DATES = dr["dates"].ToString(),
                                USERS = dr["users"].ToString(),
                                PROJECT = dr["project"].ToString(),
                                TYPE_NAME = dr["typeName"].ToString(),
                                SHARP = dr["sharp"].ToString(),
                                ITEM = dr["item"].ToString(),
                                LENGTH = dr["length"].ToString(),
                                WEIGHT = dr["weight"].ToString(),
                                FROM_CRANE = dr["fromCrane"].ToString(),
                                FROM_PLACE = dr["Place"].ToString(),
                                FROM_CAR_TRUCK = dr["fromCarTruck"].ToString(),
                                DESC_CRANE = dr["destcrane"].ToString()
                            });
                            total = total + 1;
                            result.TOTAL = total;
                        }
                    }
                    else
                    {
                        result.GetStockOutDaily_SO.Add(new GetStockOutDaily()
                        {
                            DATES = "-",
                            USERS = "-",
                            PROJECT = "-",
                            TYPE_NAME = "-",
                            SHARP = "-",
                            ITEM = "-",
                            LENGTH = "-",
                            WEIGHT = "-",
                            FROM_CRANE = "-",
                            FROM_PLACE = "-",
                            FROM_CAR_TRUCK = "-",
                            DESC_CRANE = "-"
                        });
                    }
                }

                connection.Close();
            }

            return result;
        }
        
        public mResult PrintDocOnTruckNoDo(iPrintDocOnTruckNoDo input)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            mResult result = new mResult();
            string sDocNo = "";
            string doc = "";
            string ship = "";
            int trip = 0;
            if (!IsCarNo(input.sCarNo))
            {
                result.RESULT = "-10|ทะเบียนรถผิด";
                result.EXCEPTION = "";
                return result;
            }

            sDocNo = GetStockOutDocNoPrint(input.sBarcode);
            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
            {
                connection.Open();
                try
                {
                    using (SqlCommand command1 = new SqlCommand())
                    {
                        command1.Connection = connection;
                        command1.CommandText = @"insert into stock_out_print(doc,ship,trip,status,printOrder) (select t.docNo, d.shipNo, t.tripNo, 2, getdate() 
                                                from dbo.car_trip AS t INNER JOIN dbo.stock_out_doc AS d ON t.docNo = d.docNo where (t.docNo = @docno) AND (t.carNo = @carno) AND (t.status = 0))";
                        command1.Parameters.Add("@docno", SqlDbType.NVarChar, 15).Value = sDocNo;
                        command1.Parameters.Add("@carno", SqlDbType.NVarChar, 15).Value = input.sCarNo;
                        command1.ExecuteNonQuery();

                    }
                }
                catch (Exception e)
                {
                    result.RESULT = "0|Error";
                    result.EXCEPTION = e.Message;
                    return result;
                }
                connection.Close();
            }
            //set print Queue
            //var iQuery = from ct in dc.car_trips
            //             join sod in dc.stock_out_docs
            //             on ct.docNo equals sod.docNo
            //             where ct.docNo == sDocNo && ct.carNo == input.sCarNo && ct.status == 0
            //             select new { ct.docNo, sod.shipNo, ct.tripNo };


            //if (iQuery.FirstOrDefault() != null)
            //{
            //    foreach (var item in iQuery)
            //    {
            //        item.docNo =  docNo.ToString();
            //        item.shipNo = item.shipNo.ToString();
            //        item.tripNo = item.tripNo;
                    
            //    }

         //   Update Status to Queue Print
            try
            {
                var query = from s in dc.stock_out_prints
                            where s.status == 2
                            select new { s.doc, s.ship, s.trip };
                if (query.FirstOrDefault() != null)
                {
                    foreach (var item in query)
                    {
                        doc = item.doc.ToString();
                        ship = item.ship.ToString();
                        trip = item.trip;
                    }
                }
                else
                {
                    result.RESULT = "0|Error";
                    return result;
                }
                stock_out_print sop1 = dc.stock_out_prints.Where(s => s.doc == doc && s.ship == ship && s.trip == trip).FirstOrDefault();
                if (sop1 != null)
                {
                    sop1.status = 1;
                }
                dc.SubmitChanges();

            }
            catch (Exception e)
            {
                result.RESULT = "0|Error";
                result.EXCEPTION = e.Message;
                return result;
            }
                // update stock_out,product
                sDocNo = GetStockOutDocNoPrint(input.sBarcode);
                using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
                {
                    connection.Open();                   
                    try
                    {
                        using (SqlCommand command1 = new SqlCommand())
                        {
                            command1.Connection = connection;
                            command1.CommandText = @"update car_trip set status=1, dateFinish=getdate()  where status=0 and docNo=@docno and carNo=@carno and exists
                                                   (select * from stock_out as a inner join car_trip as b on a.docNo=b.docNo and a.tripNo=b.tripNo and b.docNo=@docno and b.carNo=@carno)  ";
                            command1.Parameters.Add("@docno", SqlDbType.NVarChar, 15).Value = sDocNo;
                            command1.Parameters.Add("@carno", SqlDbType.NVarChar, 15).Value = input.sCarNo;
                            command1.ExecuteNonQuery();
                        }

                    }
                    catch (Exception e)
                    {
                        result.RESULT = "0|Error";
                        result.EXCEPTION = e.Message;
                        return result;
                    }
                    connection.Close();
                }
            //==============================================================================================
                string path = System.Web.Configuration.WebConfigurationManager.AppSettings["pathFileUpdate"].ToString();
                try
                {
                    if (File.Exists("Data.xml") == false)
                    {
                        XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
                        XmlDocument xml = new XmlDocument();
                        xml.Load(path);
                        int idcount = xml.SelectNodes("Datas/Data").Count;
                        idcount++;
                        XDocument xDocument = XDocument.Load(path);
                        XElement root = xDocument.Element("Datas");
                        IEnumerable<XElement> rows = root.Descendants("Data");
                        XElement firstRow = rows.First();
                        firstRow.AddBeforeSelf(
                            new XElement("Data",
                                new XAttribute("ID", idcount),
                                new XElement("docNo", sDocNo),
                                new XElement("shipNo", ship),
                                new XElement("tripNo", trip),
                                new XElement("userc", input.sUser),
                                new XElement("timec", DateTime.Now),
                                new XElement("isPrint", "1"),
                                new XElement("timep", DateTime.Now)));
                        xDocument.Save(path);
                    }
                    else
                    {
                        XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
                        XmlDocument xml = new XmlDocument();
                        xml.Load(path);
                        int idcount = xml.SelectNodes("Datas/Data").Count;
                        idcount++;
                        //  XmlNodeList xmlNodeList = xml.SelectNodes("Datas/Data");

                        xmlWriterSettings.Indent = true;
                        xmlWriterSettings.NewLineOnAttributes = true;
                        using (XmlWriter xmlWriter = XmlWriter.Create(path, xmlWriterSettings))
                        {
                            xmlWriter.WriteStartDocument();
                            xmlWriter.WriteStartElement("Datas");
                            xmlWriter.WriteStartElement("Data");
                            xmlWriter.WriteAttributeString("ID", idcount.ToString());
                            xmlWriter.WriteElementString("docNo", sDocNo);
                            xmlWriter.WriteElementString("shipNo", ship);
                            xmlWriter.WriteElementString("tripNo", trip.ToString());
                            xmlWriter.WriteElementString("userc", input.sUser);
                            xmlWriter.WriteElementString("timec", DateTime.Now.ToString());
                            xmlWriter.WriteElementString("isPrint", "1");
                            xmlWriter.WriteElementString("timep", DateTime.Now.ToString());
                            xmlWriter.WriteEndElement();
                            xmlWriter.WriteEndElement();
                            xmlWriter.WriteEndDocument();
                            xmlWriter.Flush();
                            xmlWriter.Close();
                        }
                    }
                }
                catch (Exception)
                {
                    {
                        result.RESULT = "0|Error";
                        return result;
                    }
                }
                //==============================================================================================

                result.RESULT = "1|OK";
                result.EXCEPTION = "";
                return result;
        }

        #endregion

        #region Paint

        private bool _4IsItemSendPaint(int sPj_id, int sPd_item)
        {
            int total = 0;

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsFg2010ConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;


                    command1.CommandText = @"SELECT COUNT(*) PAINT FROM (
                                            SELECT PJ_ID,PD_ITEM FROM PAINT_LOAD EXCEPT 
                                            SELECT PJ_ID,PD_ITEM FROM PAINT_ACTION WHERE PS_ID = 1 ) AS G1 
                                            WHERE G1.PJ_ID = @PJ_ID AND G1.PD_ITEM = @ITEM";

                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@PJ_ID", SqlDbType.Int, 5).Value = sPj_id;
                    command1.Parameters.Add("@ITEM", SqlDbType.Int, 4).Value = sPd_item;
                    SqlDataReader dr = command1.ExecuteReader();

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            total = (int)dr["PAINT"];
                        }

                        if (total == 1)
                        {
                            return false ;
                        }        
                    }
                    else
                    {
                        return true;
                    }
                }

                connection.Close();
            }
            return true;
        }

        public mPaintDataShow GetPainShowDataBarcode(iPaintDataShowBarcode input)
        {
            mPaintDataShow result = new mPaintDataShow();

            object[] prm = { input.pj_id , input.pd_item,1  };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsPdFg , "SP_GET_PAINT_SHOW_DATA", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.PROJECT = (string)reader["Project"];
                    result.CODE = (string)reader["Code"];
                    result.DWG = (string)reader["Dwg"];
                    result.TYPE = (string)reader["Type"];
                    result.ITEM = reader["item"].ToString();
                    result.PAINT = reader["Paint"].ToString();
                    result.SEND_IN = (string)reader["SendIn"];
                    result.STATUS = (string)reader["Status"];
                    result.CONTRACTOR = (string)reader["Contractor"];
                    result.SEQ = reader["Seq"].ToString();
                    result.SUB_SEQ = reader["subSeq"].ToString();
                    result.LINE = (string)reader["Line"];
                    result.INSPECTION = (string)reader["Inspection"];
                    result.DATE_FINISH = (string)reader["Date_Finish"];
                }
            }
            else
            {
                result.PROJECT = "-";
                result.CODE = "-";
                result.DWG = "-";
                result.TYPE = "-";
                result.PAINT = "-";
                result.SEND_IN = "-";
                result.STATUS = "-";
                result.CONTRACTOR = "-";
                result.SEQ = "-";
                result.SUB_SEQ = "-";
                result.LINE = "-";
                result.INSPECTION = "-"; 
                result.DATE_FINISH = "-";
            }

            return result;
        }

        public mResult SetInsertPaint(iInsertPaint input) {
            mResult result = new mResult();
            DateTime now = DateTime.Now;           
            try
            {
                McsFg2010DataContext dc = new McsFg2010DataContext();
                //stock_in items = dc.stock_ins.Where(si => si.barcode == input.sBarcode && si.status == 0).FirstOrDefault();

                paint_action icheck = dc.paint_actions.Where(s => s.pj_id == input.PJ_ID && s.pd_item == input.PD_ITEM).FirstOrDefault();


                if (icheck == null)
                {
                    paint_action paintaction = new paint_action()
                    {
                        pj_id = input.PJ_ID,
                        pd_item = input.PD_ITEM,
                        ps_id = 1,
                        users = new string(input.US_ID.Take(20).ToArray()),
                        times=(DateTime)(now),
                        status = 1
                       
                    };
                    
                    dc.paint_actions.InsertOnSubmit(paintaction);
                    dc.SubmitChanges();
                              
                    result.RESULT = "true"; 
                    result.EXCEPTION = "";
                    return result;

                    

                }

                else {
                    result.RESULT = "สถานะส่งชิ้นงานแล้ว";
                    result.EXCEPTION = "";
                    return result;
                }
                                        
                               
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถเชื่อมต่อข้อมูลได้";
                result.EXCEPTION = ex.Message;

                return result;
            }
            //result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
            //result.EXCEPTION = "";
            //return result;
        }

        public List<mProjectPaint> GetProject()
        {
            List<mProjectPaint> result = new List<mProjectPaint>();

            McsAppDataContext  McsApp = new McsAppDataContext ();
            McsFg2010DataContext McsPdFg = new McsFg2010DataContext();

            var paint_load = McsPdFg.paint_loads.ToList();
            var project = McsApp.Projects.ToList();

            var query = from pl in paint_load
                        join pj in project on pl.pj_id equals pj.pj_id
                        where pl.date_paint1 != null 
                        orderby pj.pj_name
                        group new { pl, pj } by new { pl.pj_id,pj.pj_name, pj.pj_lot ,pj.pj_sharp  } into grp                       
                        select new { grp.Key.pj_id, pj_name = grp.Key.pj_name + "-s" + grp.Key.pj_sharp.ToString() + "-lot" + grp.Key.pj_lot.ToString() };

            if (query.FirstOrDefault() != null)
            {
                foreach (var items in query)
                {
                    result.Add(new mProjectPaint()
                    {
                        PJ_ID = items.pj_id.ToString(),
                        PJ_NAME = items.pj_name 
                    });
                }
            }
            return result;
        }     
        public List<mItemPaint> GetTtemPaint(igetItem input)
        {
            List<mItemPaint> result = new List<mItemPaint>();
            
            McsFg2010DataContext McsPdFg = new McsFg2010DataContext();

           // paint_action icheck = dc.paint_actions.Where(s => s.pj_id == input.PJ_ID && s.pd_item == input.PD_ITEM).FirstOrDefault();
           
            paint_load pl = McsPdFg.paint_loads.Where(s => s.pj_id ==input.PJ_ID && s.date_paint1 != null).FirstOrDefault();
            if (pl != null)
            {

                foreach (paint_load items in McsPdFg.paint_loads.Where(s => s.pj_id == input.PJ_ID && s.date_paint1 != null))
                {
                    result.Add(new mItemPaint()
                    {
                        item = items.pd_item
                    });
                }

            }
            
            return result;
        }
        public List<mPaintDataReport> GetPaintShowDataRePort(iPaintDataReport input)
        {
            List<mPaintDataReport> result = new List<mPaintDataReport>();
            McsFg2010DataContext dc = new McsFg2010DataContext();
            
            object[] prm = { input.PJ_ID, input.PJ_ITEM,0 };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsPdFg, "SP_GET_PAINT_SHOW_DATA", prm);
            
            if(reader.HasRows)
            {
            while(reader.Read())
            {
                result.Add(new mPaintDataReport()
                {
                    PROJECT = (string)reader["Project"],
                    ITEM = reader["item"].ToString(),
                    TYPE = (string)reader["type"],
                    CODE = (string)reader["code"],
                    PAINT=reader["paint"].ToString(),
                    STATUS = (string)reader["status"],
                    DATE_FINISH = (string)reader["date_finish"],
                    CONTRACTOR = (string)reader["contractor"],

                });
            }      
            }
            else{
            
            }
            

            return result;
        }
        #endregion

        #region FN
        #region FN Stock In
        private string IsStockInRequest(string Barcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            string result = "";

            var query = (from si in dc.stock_ins
                         where si.barcode == Barcode && si.status == 0
                         select si);

            if (query.Count() > 0)
            {
                result = "True";
            }
            else
            {
                result = "False";
            }

            return result; ;
        }

        private string IsStockInRevRequest(string Barcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            string result = "";

            var query = (from sir in dc.stock_in_revs
                         where sir.barcode == Barcode && sir.status == 0
                         select sir);

            if (query.Count() > 0)
            {
                foreach (var iStockRev in query)
                {
                    result = iStockRev.docNo.ToString();
                }
            }
            else
            {
                result = "False";
            }

            return result; ;
        }

        private bool IsCarNo(string CarNo)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            var query = from ctd in dc.car_truck_drivers
                        where ctd.carno == CarNo && ctd.status == 1
                        select ctd;

            if (query.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsMoveInRequest(string sBarcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            var query = from sm in dc.stock_moves
                        where sm.barcode == sBarcode && sm.status == 2
                        select sm;
            if (query.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private string GetDocNoStockInByBarcode(string Barcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            string result = "";

            stock_in iTop1 = dc.stock_ins.Where(si => si.barcode == Barcode && si.status == 0).FirstOrDefault();

            if (iTop1 != null)
            {
                result = iTop1.docNo.ToString();
            }
            else
            {
                result = "";
            }

            return result;
        }

        private string GetDocNoStockMoveInByBarcode(string Barcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            string result = "";

            stock_move iTop1 = dc.stock_moves.Where(si => si.barcode == Barcode && si.status == 2).FirstOrDefault();

            if (iTop1 != null)
            {
                result = iTop1.docNo.ToString();
            }
            else
            {
                result = "";
            }

            return result;
        }

        private string GetDocNoStockInRevByBarcode(string sBarcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            string result = "";
            var query = from sir in dc.stock_in_revs.Where(s => s.barcode == sBarcode && s.status == 0)
                        select sir.docNo;
            if (query.First() != null)
            {
                foreach (var iDocNo in query)
                {
                    result = iDocNo.ToString();
                }
            }
            else
            {
                result = "";
            }
            return result;
        }
        #endregion

        #region Stock
        private bool IsProductInStock(string sBarcode)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            bool result = true;

            var query = from s in dc.stocks
                        where s.barcode == sBarcode
                        select s;

            if (query.Count() > 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }

            return result;
        }
        #endregion

        #region Shelf
        private bool IsShelfInStock(string sShelf)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            var query = from ss in dc.stock_shelfs
                        where ss.place == sShelf
                        select ss;

            if (query.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool GetShelfProductInfoByBarcode(string sBarcode, string sShelf, int nNo)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            bool bResult = true;

            var query = from s in dc.stocks
                        where s.barcode == sBarcode
                        select new { s.place, s.no };

            foreach (var items in query)
            {
                sShelf = items.place;
                nNo = Int32.Parse(items.no.ToString());
            }

            if (query.Count() == 0)
            {
                bResult = false;
            }
            else if (nNo >= 0)
            {
                bResult = true;
            }

            return bResult;
        }

        private int GetShelfProductTotal(string sShelf)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();

            int nCount = 0;

            var iCount = from s in dc.stocks
                         where s.place == sShelf && s.no > 0
                         select s.barcode;

            nCount = iCount.Count();

            return nCount;
        }

        private bool SetProductInsertOnShelf(string sBarcode, string sShelf, int nNo, string sUser)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            DateTime now = DateTime.Now;
            if (!IsBarcode(sBarcode))
                return false;

            try
            {
                if (IsProductInStock(sBarcode))
                {
                    var query = from s in dc.stocks
                                where s.no >= nNo && s.place == sShelf
                                select s;
                    if (query != null)
                    {
                        foreach (var item in query)
                        {
                            item.no = item.no + 1;
                        }
                        dc.SubmitChanges();
                    }

                    stock iStockUpdatePlace = dc.stocks.Where(s => s.barcode == sBarcode).FirstOrDefault();

                    if (iStockUpdatePlace != null)
                    {
                        iStockUpdatePlace.place = sShelf;
                        iStockUpdatePlace.no = nNo;
                        iStockUpdatePlace.users = sUser;
                        iStockUpdatePlace.dates = (DateTime)(now);

                        dc.SubmitChanges();
                    }

                    return true;
                }
                else
                {
                    var query = from s in dc.stocks
                                where s.no >= nNo && s.place == sShelf
                                select s;
                    if (query != null)
                    {                       
                            foreach (var item in query)
                            {
                                item.no = item.no + 1;
                            }                       
                        dc.SubmitChanges();
                    }

                    stock dtStock = new stock()
                    {
                        status = 1,
                        barcode = new string(sBarcode.Take(20).ToArray()),
                        place = new string(sShelf.ToUpper().Take(10).ToArray()),
                        no = nNo,
                        users = new string(sUser.ToUpper().Take(7).ToArray()),
                        dates = (DateTime)(now),
                        ts = (DateTime)(now)
                    };

                    dc.stocks.InsertOnSubmit(dtStock);
                    dc.SubmitChanges();
                }
                return true;
            }
            catch (Exception ex)
            {               
                return false;
            }
        }

        private bool AddProductIntoShelf(string sBarcode, string sShelf, int nNo, string sUser)
        {
            McsFg2010DataContext dc = new McsFg2010DataContext();
            DateTime now = DateTime.Now;

            stock iStockInsert = dc.stocks.FirstOrDefault();
            try
            {
                stock dtStock = new stock()
                {
                    status = 1,
                    barcode = new string(sBarcode.Take(20).ToArray()),
                    place = new string(sShelf.ToUpper().Take(10).ToArray()),
                    no = nNo,
                    users = new string(sUser.ToUpper().Take(7).ToArray()),
                    dates = (DateTime)(now),
                    ts = (DateTime)(now)
                };

                dc.stocks.InsertOnSubmit(dtStock);
                dc.SubmitChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion

        #region Is Barcode
        private bool IsBarcode(string sBarcode)
        {
            if (IsBarcodeNew(sBarcode))
                return true;
            if (IsBarcodeOld(sBarcode))
                return true;
            return false;
        }

        private bool IsBarcodeOld(string sBarcode)
        {
            sBarcode = sBarcode.ToUpper().Trim();
            if (sBarcode.Length != 12)
                return false;

            if (sBarcode.Substring(7, 1) != "-")
                return false;

            string[] barcode = sBarcode.Split('-');
            if (barcode.Length != 2)
                return false;
            try
            {
                if (int.Parse(barcode[0]) > 0)
                    return false;
            }
            catch (Exception e)
            {
                try
                {
                    if (int.Parse(barcode[1]) == 0)
                        return false;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return true;
        }

        private bool IsBarcodeNew(string sBarcode)
        {
            sBarcode = sBarcode.ToUpper().Trim();
            if (sBarcode.Length != 10)
                return false;

            if (sBarcode.Substring(5, 1) != "-")
                return false;

            string[] barcode = sBarcode.Split('-');
            if (barcode.Length != 2)
                return false;
            try
            {
                if (int.Parse(barcode[0]) == 0)
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            try
            {
                if (int.Parse(barcode[1]) == 0)
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        #endregion
        #endregion

        #endregion

        #region HR
        public mListYearAndUser getUserInGroup(iUserInGroup input)
        {
            mListYearAndUser results = new mListYearAndUser();
            results.getYear = new List<mYear>();
            results.getUserIngroup = new List<mUserInGroup>();

            int[] year = new int[2];
            int thisYear =  Convert.ToInt32(DateTime.Now.ToString("yyyy"));
            int pastYear = thisYear -1;
            year[0] = thisYear;
            year[1] = pastYear;

            foreach (int value in year)
            {
                results.getYear.Add(new mYear
                {
                    YEAR = (int)value
                });
            }

            object[] prm = { input.sUs_Id };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_USER_IN_GROUP_BY_USER", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    results.getUserIngroup.Add(new mUserInGroup()
                    {
                        US_ID = reader["US_ID"].ToString(),
                        FULLNAME = reader["FULLNAME"].ToString()
                    });
                }
            }
            else
            {
                results.getUserIngroup.Add(new mUserInGroup()
                {
                    US_ID = "====",
                    FULLNAME = "===="
                });
            }
            return results;
        }

        public List<mLeaveQuotaByYear> getMasterLeaveQuotaByYear(iLeaveQuotaByYear input)
        {
            List<mLeaveQuotaByYear> results = new List<mLeaveQuotaByYear>();
            
            object[] prm = { input.sUs_Id, input.nYear };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsHrMs, "GET_MASTER_LEAVE_QUOTA_BY_YEAR", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    results.Add(new mLeaveQuotaByYear
                    {
                        LEAVE_TYPE_MSG = reader["LEAVE_TYPE_MSG"].ToString(),
                        NUMBER_OF_QUOTA = reader["NUMBER_OF_QUOTA"].ToString(),
                        NUMBER_OF_QUOTA_USAGE = reader["NUMBER_OF_QUOTA_USAGE"].ToString(),
                        LEAVE_REMAIN = (reader["NUMBER_OF_QUOTA"].ToString() == "-") ? 0 : float.Parse(reader["NUMBER_OF_QUOTA"].ToString()) - float.Parse(reader["NUMBER_OF_QUOTA_USAGE"].ToString()),
                        LIFETIMES = (reader["NUMBER_OF_QUOTA"].ToString() == "-") ? "" : reader["LIFETIMES"].ToString()
                    });
                }
            }
            else
            {
                results.Add(new mLeaveQuotaByYear()
                {
                    LEAVE_TYPE_MSG = "-",
                    NUMBER_OF_QUOTA = "-",
                    NUMBER_OF_QUOTA_USAGE = "-",
                    LEAVE_REMAIN = 0,
                    LIFETIMES = "-"
                });
            }

            return results;
        }

        #endregion

        #region CMMS

        string ConnCMMS = ConfigurationManager.ConnectionStrings["CMMSConnectionString"].ConnectionString;

        #region upload

        public Result UploadImages_CMMS(Stream fileUpload)
        {
            Result result = new Result();

            try
            {
                McsAppDataContext dc = new McsAppDataContext();

                string servPath = "";
                string fileSize = "";

                int point = 0;
                int width = 0;
                int height = 0;

                foreach (PDPC_config items in dc.PDPC_configs)
                {
                    if (items.config_key.Equals("PATH_CMMS")) servPath = items.config_value;
                    if (items.config_key.Equals("PICTURE")) fileSize = items.config_value;
                }

                if (fileSize != "")
                {
                    point = fileSize.IndexOf("x");
                    width = Convert.ToInt32(fileSize.Substring(0, point));
                    height = Convert.ToInt32(fileSize.Substring(point + 1));
                }

                //------------------------------------------------------------------------------------------------------

                var parser = new MultipartFormDataParser(fileUpload);

                var rpId = parser.Parameters["rp_id"].Data;
                var docId = parser.Parameters["doc_id"].Data.Substring(0, 10);

                var file = parser.Files.FirstOrDefault();

                Stream stream = file.Data;

                //------------------------------------------------------------------------------------------------------

                if (!Directory.Exists(servPath)) Directory.CreateDirectory(servPath);

                FileStream targetStream = null;
                Stream sourceStream = stream;

                string fileName = docId + "-X.jpeg";
                string filePath = Path.Combine(servPath, fileName);

                using (targetStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    const int bufferLen = 4096;
                    byte[] buffer = new byte[bufferLen];
                    int count = 0;
                    int totalBytes = 0;

                    while ((count = sourceStream.Read(buffer, 0, bufferLen)) > 0)
                    {
                        totalBytes += count;

                        targetStream.Write(buffer, 0, count);
                    }

                    targetStream.Close();

                    sourceStream.Close();
                }

                resizeImage(filePath, width, height);

                result.result = true;
                result.exception = "";
            }
            catch (Exception ex)
            {
                result.result = false;
                result.exception = ex.Message;
            }

            return result;
        }

        #endregion

        #region Login
        public SetLogin GetLogin(GetLogin input)
        {
            SetLogin login = new SetLogin();

            object[] obj = { input.user_name, input.user_pwd };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_GET_LOGIN", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    login.emp_code = reader["emp_code"].ToString();
                }
            }

            return login;
        }
        #endregion

        #region Employee
        public setEmployeeByEmpCode GetEmployeeByEmpCode(getEmployeeByEmpCode input)
        {
            setEmployeeByEmpCode result = new setEmployeeByEmpCode();

            object obj = input.emp_code;
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_GET_EMPLOYEE_BY_EMP_CODE", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.emp_code = reader["EMP_CODE"].ToString();
                    result.prefix = reader["PREFIX"].ToString();
                    result.full_name = reader["FULL_NAME"].ToString();
                    result.sup_dept_code = reader["SUP_DEPT_CODE"].ToString();
                    result.sup_dept_nme = reader["SUP_DEPT_NME"].ToString();
                    result.dept_code = reader["DEPT_CODE"].ToString();
                    result.dept_nme = reader["DEPT_NME"].ToString();
                    result.under = reader["UNDER"].ToString();
                }
            }
            return result;
        }
        #endregion

        #region GetEmailContract
        public void GetEmailContract(GetEmailContract input)
        {
            //SetEmailContract mail = new SetEmailContract();

            object obj = input.as_type;
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_GET_EMAIL_FOR_NOTIFICATION", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    //mail.receiver = reader["RECEIVER"].ToString();
                    //mail.cc = reader["CC"].ToString();
                    ConfigurationManager.AppSettings.Set("Receiver", reader["RECEIVER"].ToString());
                    ConfigurationManager.AppSettings.Set("CC", reader["CC"].ToString());
                }
            }
            //return mail;
        }
        #endregion

        #region EmailNotification
        public void EmailNotification(emailNotification input)
        {
            object[] obj = { input.rp_id, input.date, input.priority, input.as_id, input.as_name, input.user_report, input.rp_detail };
            string locationId = "";
            string userGroup = "";
            CMMSDataContext cmms = new CMMSDataContext();

            var location = (from ass in cmms.ASSETs
                            where ass.AS_ID == input.as_id
                            select ass.LOCATION_ID).FirstOrDefault();
            if (location != null)
            {
                locationId = location;
            }

            var group = (from emp in cmms.MST_EMPs
                         where emp.FULL_NAME == input.user_report
                         select emp.DEPT_NME).FirstOrDefault();
            if (group != null)
            {
                userGroup = group;
            }

            string receiver = ConfigurationManager.AppSettings["Receiver"].ToString();
            string cc = ConfigurationManager.AppSettings["CC"].ToString();
            string email = ConfigurationManager.AppSettings["Email"].ToString();
            string pwd = ConfigurationManager.AppSettings["Password"].ToString();
            string smtp = ConfigurationManager.AppSettings["SmtpClient"].ToString();
            string html = string.Format(@"<html><head>รายละเอียดการแจ้งซ่อม</head><body>
                                        <br>เลขที่ใบแจ้งซ่อม : {0}</br>
                                        <br>วันเวลาที่ต้องเข้าไปซ่อม : {1}</br>
                                        <br>ความสำคัญ : {2}</br>
                                        <br>รหัสทรัพย์สิน : {3}</br>
                                        <br>ชื่อทรัพย์สิน : {4}</br>
                                        <br>สถานที่ : {7}</br>
                                        <br>ผู้แจ้ง : {5}</br>
                                        <br>กรุ๊ป : {8}</br>
                                        <br>ปัญหาที่แจ้ง : {6}</br>
                                        </body></html>",
                                        obj[0], obj[1], obj[2], obj[3], obj[4], obj[5], obj[6], locationId, userGroup);

            try
            {
                MailMessage mail = new MailMessage();
                mail.IsBodyHtml = true;
                mail.Body = html;
                mail.From = new MailAddress(email);
                mail.To.Add(receiver);
                mail.CC.Add(cc);
                mail.Subject = "รายละเอียดแจ้งซ่อม " + input.rp_id;

                SmtpClient smtpClient = new SmtpClient(smtp);
                smtpClient.Credentials = new System.Net.NetworkCredential(email,pwd);
                smtpClient.EnableSsl = false;
                smtpClient.Send(mail);

            }
            catch (Exception e)
            {
            }
        }
        #endregion

        #region DDL AssetType
        public List<SetAssetType> AssetType(GetAssetType input)
        {
            List<SetAssetType> list = new List<SetAssetType>();

            object obj = input.asset_id;
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_DDL_ASSET_TYPE", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SetAssetType
                    {
                        asset_id = reader["AS_TYPE_ID"].ToString(),
                        asset_name = reader["AS_TYPE_NAME"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #region DDL AssetStatus
        public List<SetAssetStatus> AssetStatus(GetAssetStatus input)
        {
            List<SetAssetStatus> list = new List<SetAssetStatus>();

            object obj = input.asset_id;
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_DDL_ASSET_STATUS", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SetAssetStatus
                    {
                        asset_id = reader["AS_STATUS_ID"].ToString(),
                        asset_name = reader["AS_STATUS_NAME"].ToString()
                    });
                }
            }

            return list;

        }
        #endregion

        #region DDL Evaluate
        public List<SetEvaluate> Evaluate(GetEvaluate input)
        {
            List<SetEvaluate> list = new List<SetEvaluate>();

            object obj = input.evaluate_id;
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_DDL_EVALUATE", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SetEvaluate
                    {
                        evaluate_id = reader["ID"].ToString(),
                        evaluate_name = reader["NAME"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #region DDL Priority
        public List<SetPriority> Priority(GetPriority input)
        {
            List<SetPriority> list = new List<SetPriority>();

            object obj = input.priority_id;
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_DDL_PRIORITY", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SetPriority
                    {
                        priority_id = reader["ID"].ToString(),
                        priority_name = reader["NAME"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #region DDL RepairCause
        public List<DDL_SetRepairCause> DDL_RepairCause(DDL_GetRepairCause input)
        {
            List<DDL_SetRepairCause> list = new List<DDL_SetRepairCause>();

            object obj = input.all;
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_DDL_REPAIR_CAUSE", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new DDL_SetRepairCause
                    {
                        cause_id = (int)reader["CAUSE_ID"],
                        cause_name = reader["CAUSE_NAME"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #region DDL RepairStatus
        public List<SetRepairStatus> RepairStatus(GetRepairStatus input)
        {
            List<SetRepairStatus> list = new List<SetRepairStatus>();

            object obj = input.repairStatus_id;
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_DDL_REPAIR_STATUS", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SetRepairStatus
                    {
                        repairStatus_id = reader["RP_STATUS_ID"].ToString(),
                        repairStatus_name = reader["RP_STATUS_NAME"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #region DDL Team
        public List<SetTeam> Team(GetTeam input)
        {
            List<SetTeam> list = new List<SetTeam>();

            object obj = input.team_id;
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_DDL_TEAM", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SetTeam
                    {
                        team_id = reader["TEAM_ID"].ToString(),
                        team_name = reader["TEAM_NAME"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #region DDL WorkClass
        public List<SetWorkClass> WorkClass(GetWorkClass input)
        {
            List<SetWorkClass> list = new List<SetWorkClass>();

            object obj = input.workClass_id;
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_DDL_WORK_CLASS", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SetWorkClass
                    {
                        workClass_id = reader["WC_ID"].ToString(),
                        workClass_name = reader["WC_NAME"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #region DEL Request
        public queryResult DEL_Request(DEL_GetRequest input)
        {
            queryResult qry = new queryResult();

            object[] obj = { input.rq_id, input.user_update };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_DEL_REQUEST", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    qry.result = reader["ACTION_RESULT_CODE"].ToString();
                    qry.message = reader["ACTION_RESULT_MSG"].ToString();
                }
            }

            return qry;
        }
        #endregion

        #region INS Document
        public queryResult INS_DocumentByUploadImg(INS_DocumentByUploadImg input)
        {
            queryResult result = new queryResult();

            object[] obj = { input.rp_id, input.as_id, input.user_update };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_INS_DOCUMENT_FILE_BY_UPLOAD_IMG", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.result = reader["ACTION_RESULT_CODE"].ToString();
                    result.message = reader["ACTION_RESULT_MSG"].ToString();
                } 
            }

            return result;
        }
        #endregion

        #region INS Repair
        public queryResult INS_Repair(INS_GetRepair input)
        {
            queryResult result = new queryResult();

            object[] obj = { input.as_id, input.priority, input.user_rp, input.request_date, input.rp_detail, input.admin_update };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_INS_REPAIR", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.result = reader["ACTION_RESULT_CODE"].ToString();
                    result.message = reader["ACTION_RESULT_MSG"].ToString();
                }
            }
            return result;
        }
        #endregion

        #region INS RequestByRepair
        public queryResult INS_RequestByRepair(INS_GetRequestByRepair input)
        {
            queryResult qry = new queryResult();

            object[] obj = { input.rp_id, input.cp_item, input.issue, input.user_use, input.user_create, input.rq_detail, input.user_update };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_INS_REQUEST_BY_REPAIR", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    qry.result = reader["ACTION_RESULT_CODE"].ToString();
                    qry.message = reader["ACTION_RESULT_MSG"].ToString();
                }
            }

            return qry;
        }
        #endregion

        #region SEL Asset
        public List<SEL_SetAsset> SEL_Asset(SEL_GetAsset input)
        {
            List<SEL_SetAsset> list = new List<SEL_SetAsset>();

            object[] obj = { input.as_id, input.as_name, input.as_type_id, input.division_code, input.location_id, input.ven_id };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_SEL_ASSET", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SEL_SetAsset
                    {
                        as_id = reader["AS_ID"].ToString(),
                        as_name_eng = reader["AS_NAME_ENG"].ToString(),
                        as_name_thai = reader["AS_NAME_THAI"].ToString(),
                        price = reader["PRICE"].ToString(),
                        ac_code = reader["AC_CODE"].ToString(),
                        as_type_id = reader["AS_TYPE_ID"].ToString(),
                        as_type_name = reader["AS_TYPE_NAME"].ToString(),
                        location_id = reader["LOCATION_ID"].ToString(),
                        location_detail = reader["LOCATION_DETAIL"].ToString(),
                        division_code = reader["DIVISION_CODE"].ToString(),
                        emp_code = reader["EMP_CODE"].ToString(),
                        full_name = reader["FULL_NAME"].ToString(),
                        manufacturer = reader["MANUFACTURER"].ToString(),
                        sn = reader["SN"].ToString(),
                        model = reader["MODEL"].ToString(),
                        wi = reader["WI"].ToString(),
                        img = reader["IMG"].ToString(),
                        as_detail = reader["AS_DETAIL"].ToString(),
                        po_number = reader["PO_NUMBER"].ToString(),
                        po_date = reader["PO_DATE"].ToString(),
                        delivery_date = reader["DELIVERY_DATE"].ToString(),
                        install_date = reader["INSTALL_DATE"].ToString(),
                        expire_date = reader["EXPIRE_DATE"].ToString(),
                        lot_number = reader["LOT_NUMBER"].ToString(),
                        ven_id = reader["VEN_ID"].ToString(),
                        ven_name = reader["VEN_NAME"].ToString(),
                        //ven_lot_number = reader["VEN_LOT_NUMBER"].ToString(),
                        ven_sn = reader["VEN_SN"].ToString(),
                        ven_expire_date = reader["VEN_EXPIRE_DATE"].ToString(),
                        location_ac = reader["LOCATION_AC"].ToString(),
                        admin_update = reader["ADMIN_UPDATE"].ToString(),
                        time_update = reader["TIME_UPDATE"].ToString(),
                        as_status_name = reader["AS_STATUS_NAME"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #region SEL AssetByType
        public List<SEL_SetAssetByType> SEL_AssetByType(SEL_GetAssetByType input)
        {
            List<SEL_SetAssetByType> list = new List<SEL_SetAssetByType>();

            object[] obj = { input.as_type_id, input.search };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_SEL_ASSET_BY_TYPE", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SEL_SetAssetByType
                    {
                        as_id = reader["AS_ID"].ToString(),
                        as_name_eng = reader["AS_NAME_ENG"].ToString(),
                        as_name_thai = reader["AS_NAME_THAI"].ToString(),
                        as_status_name = reader["AS_STATUS_NAME"].ToString(),
                        as_type_id = reader["AS_TYPE_ID"].ToString(),
                        as_type_name = reader["AS_TYPE_NAME"].ToString(),
                        img = reader["IMG"].ToString(),
                        location_detail = reader["LOCATION_DETAIL"].ToString(),
                        location_id = reader["LOCATION_ID"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #region SEL AssetType
        public List<SEL_SetAssetType> SEL_AssetType(SEL_GetAssetType input)
        {
            List<SEL_SetAssetType> list = new List<SEL_SetAssetType>();

            object[] obj = { input.as_type_id, input.as_type_name };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_SEL_ASSET_TYPE", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SEL_SetAssetType
                    {
                        as_type_id = reader["AS_TYPE_ID"].ToString(),
                        as_type_name = reader["AS_TYPE_NAME"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #region SEL Component
        public List<SEL_SetComponent> SEL_Component(SEL_GetComponent input)
        {
            List<SEL_SetComponent> list = new List<SEL_SetComponent>();

            object[] obj = { input.cp_item, input.cp_item_name, input.cp_group, input.cp_type, input.ven_id, input.location_id, input.is_active };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_SEL_COMPONENT", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SEL_SetComponent
                    {
                        cp_item = reader["CP_ITEM"].ToString(),
                        cp_item_name = reader["CP_ITEM_NAME"].ToString(),
                        cp_type_id = reader["CP_TYPE_ID"].ToString(),
                        cp_type_name = reader["CP_TYPE_NAME"].ToString(),
                        cp_gp_id = reader["CP_GP_ID"].ToString(),
                        cp_gp_name = reader["CP_GP_NAME"].ToString(),
                        as_type_id = reader["AS_TYPE_ID"].ToString(),
                        as_type_name = reader["AS_TYPE_NAME"].ToString(),
                        brand = reader["BRAND"].ToString(),
                        model = reader["MODEL"].ToString(),
                        size = reader["SIZE"].ToString(),
                        price_unit = reader["PRICE_UNIT"].ToString(),
                        unit_type = reader["UNIT_TYPE"].ToString(),
                        unit_type_name = reader["UNIT_TYPE_NAME"].ToString(),
                        total_qty = reader["TOTAL_QTY"].ToString(),
                        used_qty = reader["USED_QTY"].ToString(),
                        remain_qty = reader["REMAIN_QTY"].ToString(),
                        po_number = reader["PO_NUMBER"].ToString(),
                        po_date = reader["PO_DATE"].ToString(),
                        delivery_date = reader["DELIVERY_DATE"].ToString(),
                        expire_date = reader["EXPIRE_DATE"].ToString(),
                        ven_id = reader["VEN_ID"].ToString(),
                        img = reader["IMG"].ToString(),
                        detail = reader["DETAIL"].ToString(),
                        is_active = reader["IS_ACTIVE"].ToString(),
                        admin_update = reader["ADMIN_UPDATE"].ToString(),
                        time_update = reader["TIME_UPDATE"].ToString(),
                        location_ac_code = reader["LOCATION_AC_CODE"].ToString(),
                        location_id = reader["LOCATION_ID"].ToString(),
                        ias_active_desc = reader["IS_ACTIVE_DESC"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #region SEL Location
        public List<SEL_SetLocation> SEL_Location(SEL_GetLocation input)
        {
            List<SEL_SetLocation> list = new List<SEL_SetLocation>();

            object[] obj = { input.location_id, input.location_ac_code, input.location_detail };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_SEL_LOCATION", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SEL_SetLocation
                    {
                        location_id = reader["LOCATION_ID"].ToString(),
                        location_ac_code = reader["LOCATION_AC_CODE"].ToString(),
                        location_detail = reader["LOCATION_DETAIL"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #region SEL OperatorById
        public List<SEL_SetOperatorById> SEL_OperatorById(SEL_GetOperatorById input)
        {
            List<SEL_SetOperatorById> list = new List<SEL_SetOperatorById>();

            object[] obj = { input.team_id, input.search };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_SEL_OPERATOR_BY_ID", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SEL_SetOperatorById
                    {
                        emp_code = reader["EMP_CODE"].ToString(),
                        full_name = reader["FULL_NAME"].ToString(),
                        emp_pic = reader["EMP_PIC"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #region SEL Repair
        public List<SEL_SetRepair> SEL_Repair()
        {
            List<SEL_SetRepair> list = new List<SEL_SetRepair>();

            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_SEL_REPAIR");

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SEL_SetRepair
                    {
                        rp_id = reader["RP_ID"].ToString(),
                        as_id = reader["AS_ID"].ToString(),
                        priority = reader["PRIORITY"].ToString(),
                        priority_desc = reader["PRIORITY_DESC"].ToString(),
                        user_rp = reader["USER_RP"].ToString(),
                        full_name = reader["FULL_NAME"].ToString(),
                        rp_date = reader["RP_DATE"].ToString(),
                        wc_id = reader["WC_ID"].ToString(),
                        rp_detail = reader["RP_DETAIL"].ToString(),
                        rp_status_id = reader["RP_STATUS_ID"].ToString(),
                        rp_status_name = reader["RP_STATUS_NAME"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #region SEL RepairByAdd
        public List<SEL_SetRepairByAdd> SEL_RepairByAdd(SEL_GetRepairByAdd input)
        {
            List<SEL_SetRepairByAdd> list = new List<SEL_SetRepairByAdd>();

            object[] obj = { input.start_date, input.end_date, input.rp_id, input.rp_status_id, input.user_login };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_SEL_REPAIR_BY_ADD", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SEL_SetRepairByAdd
                    {
                        rp_id = reader["RP_ID"].ToString(),
                        as_id = reader["AS_ID"].ToString(),
                        as_name_eng = reader["AS_NAME_ENG"].ToString(),
                        priority = (int)reader["PRIORITY"],
                        priority_desc = reader["PRIORITY_DESC"].ToString(),
                        user_rp = reader["USER_RP"].ToString(),
                        full_name = reader["FULL_NAME"].ToString(),
                        rp_date = reader["RP_DATE"].ToString(),
                        wc_id = reader["WC_ID"].ToString(),
                        rp_detail = reader["RP_DETAIL"].ToString(),
                        rp_status_id = (int)reader["RP_STATUS_ID"],
                        rp_status_name = reader["RP_STATUS_NAME"].ToString(),
                        assign_to = reader["ASSIGN_TO"].ToString(),
                        sent_date = reader["SENT_DATE"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #region SEL RepairById
        public SEL_SetRepairById SEL_RepairById(SEL_GetRepairById input)
        {
            SEL_SetRepairById list = new SEL_SetRepairById();

            object obj = input.rp_id;
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_SEL_REPAIR_BY_ID", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.rp_id = reader["RP_ID"].ToString();
                    list.as_type_id = reader["AS_TYPE_ID"].ToString();
                    list.as_type_name = reader["AS_TYPE_NAME"].ToString();
                    list.as_id = reader["AS_ID"].ToString();
                    list.as_name_thai = reader["AS_NAME_THAI"].ToString();
                    list.priority = (int)reader["PRIORITY"];
                    list.priority_desc = reader["PRIORITY_DESC"].ToString();
                    list.user_rp = reader["USER_RP"].ToString();
                    list.full_name = reader["FULL_NAME"].ToString();
                    list.rp_date = reader["RP_DATE"].ToString();
                    list.request_date = reader["REQUEST_DATE"].ToString();
                    list.wc_id = reader["WC_ID"].ToString();
                    list.wc_name = reader["WC_NAME"].ToString();
                    list.rp_detail = reader["RP_DETAIL"].ToString();
                    list.rp_status_id = (int)reader["RP_STATUS_ID"];
                    list.rp_status_name = reader["RP_STATUS_NAME"].ToString();
                    list.team_id = reader["TEAM_ID"].ToString();
                    list.assign_to = reader["ASSIGN_TO"].ToString();
                    list.assign_date = reader["ASSIGN_DATE"].ToString();
                    list.assign_name = reader["ASSIGN_NAME"].ToString();
                    list.start_date = reader["START_DATE"].ToString();
                    list.finish_date = reader["FINISH_DATE"].ToString();
                    list.operator_sent = reader["OPERATOR_SENT"].ToString();
                    list.operator_name = reader["OPERATOR_NAME"].ToString();
                    list.sent_date = reader["SENT_DATE"].ToString();
                    list.user_commit = reader["USER_COMMIT"].ToString();
                    list.user_commit_name = reader["USER_COMMIT_NAME"].ToString();
                    list.commit_date = reader["COMMIT_DATE"].ToString();
                    list.user_comment = reader["USER_COMMENT"].ToString();
                    list.rate = reader["RATE"].ToString();
                    list.rate_desc = reader["RATE_DESC"].ToString();
                    list.as_status_desc = reader["AS_STATUS_DESC"].ToString();
                    list.cause_id = reader["CAUSE_ID"].ToString();
                    list.cause_desc = reader["CAUSE_DESC"].ToString();
                    list.cause_comment = reader["CAUSE_COMMENT"].ToString();
                    list.protection = reader["PROTECTION"].ToString();
                    list.location_id = reader["LOCATION_ID"].ToString();
                    list.location_detail = reader["LOCATION_DETAIL"].ToString();
                }
            }

            return list;
        }
        #endregion

        #region SEL RepairByUserForAssign
        public List<SEL_SetRepairByUserForAssign> SEL_RepairByUserForAssign(SEL_GetRepairByUserForAssign input)
        {
            List<SEL_SetRepairByUserForAssign> list = new List<SEL_SetRepairByUserForAssign>();

            object[] obj = { input.start_date, input.end_date, input.rp_id, input.rp_status_id };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_SEL_REPAIR_BY_USER_FOR_ASSIGN", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SEL_SetRepairByUserForAssign
                    {
                        rp_id = reader["RP_ID"].ToString(),
                        as_id = reader["AS_ID"].ToString(),
                        as_name_eng = reader["AS_NAME_ENG"].ToString(),
                        priority = (int)reader["PRIORITY"],
                        priority_desc = reader["PRIORITY_DESC"].ToString(),
                        user_rp = reader["USER_RP"].ToString(),
                        full_name = reader["FULL_NAME"].ToString(),
                        rp_date = reader["RP_DATE"].ToString(),
                        wc_id = reader["WC_ID"].ToString(),
                        rp_detail = reader["RP_DETAIL"].ToString(),
                        rp_status_id = (int)reader["RP_STATUS_ID"],
                        rp_status_name = reader["RP_STATUS_NAME"].ToString(),
                        assign_to = reader["ASSIGN_TO"].ToString(),
                        sent_date = reader["SENT_DATE"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #region SEL RepairByUserForEvaluate
        public List<SEL_SetRepairByUserForEvaluate> SEL_RepairByUserForEvaluate(SEL_GetRepairByUserForEvaluate input)
        {
            List<SEL_SetRepairByUserForEvaluate> list = new List<SEL_SetRepairByUserForEvaluate>();

            object[] obj = { input.start_date, input.end_date, input.rp_id, input.user_login };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_SEL_REPAIR_BY_USER_FOR_EVALUATE", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SEL_SetRepairByUserForEvaluate
                    {
                        rp_id = reader["RP_ID"].ToString(),
                        as_id = reader["AS_ID"].ToString(),
                        as_name_eng = reader["AS_NAME_ENG"].ToString(),
                        priority = (int)reader["PRIORITY"],
                        priority_desc = reader["PRIORITY_DESC"].ToString(),
                        user_rp = reader["USER_RP"].ToString(),
                        full_name = reader["FULL_NAME"].ToString(),
                        rp_date = reader["RP_DATE"].ToString(),
                        wc_id = reader["WC_ID"].ToString(),
                        rp_detail = reader["RP_DETAIL"].ToString(),
                        rp_status_id = (int)reader["RP_STATUS_ID"],
                        rp_status_name = reader["RP_STATUS_NAME"].ToString(),
                        assign_to = reader["ASSIGN_TO"].ToString(),
                        sent_date = reader["SENT_DATE"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #region SEL RepairByWorkList
        public List<SEL_SetRepairByWorkList> SEL_RepairByWorkList(SEL_GetRepairByWorkList input)
        {
            List<SEL_SetRepairByWorkList> list = new List<SEL_SetRepairByWorkList>();

            object[] obj = { input.start_date, input.end_date, input.rp_id, input.rp_status_id, input.user_login };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_SEL_REPAIR_BY_WORK_LIST", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SEL_SetRepairByWorkList
                    {
                        rp_id = reader["RP_ID"].ToString(),
                        as_id = reader["AS_ID"].ToString(),
                        as_name_eng = reader["AS_NAME_ENG"].ToString(),
                        location_id = reader["LOCATION_ID"].ToString(),
                        priority = (int)reader["PRIORITY"],
                        priority_desc = reader["PRIORITY_DESC"].ToString(),
                        user_rp = reader["USER_RP"].ToString(),
                        full_name = reader["FULL_NAME"].ToString(),
                        rp_date = reader["RP_DATE"].ToString(),
                        wc_id = reader["WC_ID"].ToString(),
                        rp_detail = reader["RP_DETAIL"].ToString(),
                        rp_status_id = (int)reader["RP_STATUS_ID"],
                        rp_status_name = reader["RP_STATUS_NAME"].ToString(),
                        assign_to = reader["ASSIGN_TO"].ToString(),
                        sent_date = reader["SENT_DATE"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #region SEL ReapairWorkListByRpId
        public List<SEL_SetRepairWorkListByRpId> SEL_RepairWorkListByRpId(SEL_GetRepairWorkListByRpId input)
        {
            List<SEL_SetRepairWorkListByRpId> list = new List<SEL_SetRepairWorkListByRpId>();

            object[] obj = { input.start_date, input.end_date, input.rp_id, input.rp_status_id, input.user_login };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_SEL_REPAIR_WORK_LIST_BY_STATUS_ID", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SEL_SetRepairWorkListByRpId
                    {
                        rp_id = reader["RP_ID"].ToString(),
                        as_id = reader["AS_ID"].ToString(),
                        as_name_eng = reader["AS_NAME_ENG"].ToString(),
                        location_id = reader["LOCATION_ID"].ToString(),
                        priority = (int)reader["PRIORITY"],
                        priority_desc = reader["PRIORITY_DESC"].ToString(),
                        user_rp = reader["USER_RP"].ToString(),
                        full_name = reader["FULL_NAME"].ToString(),
                        rp_date = reader["RP_DATE"].ToString(),
                        wc_id = reader["WC_ID"].ToString(),
                        rp_detail = reader["RP_DETAIL"].ToString(),
                        rp_status_id = (int)reader["RP_STATUS_ID"],
                        rp_status_name = reader["RP_STATUS_NAME"].ToString(),
                        assign_to = reader["ASSIGN_TO"].ToString(),
                        sent_date = reader["SENT_DATE"].ToString()
                    });
                }
            }
            return list;
        }
        #endregion

        #region SEL RequestByRepair
        public List<SEL_SetRequestByRepair> SEL_RequestByRepair(SEL_GetRequestByRepair input)
        {
            List<SEL_SetRequestByRepair> list = new List<SEL_SetRequestByRepair>();

            object obj = input.rp_id;
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_SEL_REQUEST_BY_REPAIR", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SEL_SetRequestByRepair
                    {
                        cp_item = reader["CP_ITEM"].ToString(),
                        cp_item_name = reader["CP_ITEM_NAME"].ToString(),
                        issue = (int)reader["ISSUE"],
                        user_create = reader["USER_CREATE"].ToString(),
                        full_name = reader["FULL_NAME"].ToString(),
                        create_date = reader["CREATE_DATE"].ToString(),
                        rq_id = reader["RQ_ID"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #region UPD DocumentByRpID
        public queryResult UPD_DocumentByRpId(UPD_DocumentByRpId input)
        {
            queryResult qry = new queryResult();

            object[] obj = { input.rp_id, input.as_id, input.user_update };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_UPD_DOCUMENT_FILE_BY_RP_ID", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    qry.result = reader["ACTION_RESULT_CODE"].ToString();
                    qry.message = reader["ACTION_RESULT_MSG"].ToString();
                }
            }

            return qry;
        }
        #endregion

        #region UPD DocumentByUploadImgFail
        public queryResult UPD_DocumentByUploadImgFail(UPD_DocumentByUploadImgFail input)
        {
            queryResult qry = new queryResult();

            object obj = input.doc_id;
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_UPD_DOCUMENT_FILE_BY_UPLOAD_IMG_FAIL", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    qry.result = reader["ACTION_RESULT_CODE"].ToString();
                    qry.message = reader["ACTION_RESULT_MSG"].ToString();
                }
            }
            return qry;
        }
        #endregion

        #region UPD Repair
        public queryResult UPD_Repair(UPD_GetRepair input)
        {
            queryResult qry = new queryResult();

            object[] obj = { input.priority, input.user_rp, input.request_date, input.wc_id, input.rp_detial, input.rp_id, input.as_id };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_UPD_REPAIR", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    qry.result = reader["ACTION_RESULT_CODE"].ToString();
                    qry.message = reader["ACTION_RESULT_MSG"].ToString();
                }
            }

            return qry;
        }
        #endregion

        #region UPD RepairByAdminCanncel
        public queryResult UPD_RepairByAdminCancel(UPD_GetRepairByAdminCancel input)
        {
            queryResult qry = new queryResult();

            object[] obj = { input.rp_id, input.user_update, input.remark };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_UPD_REPAIR_BY_ADMIN_CANCEL", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    qry.result = reader["ACTION_RESULT_CODE"].ToString();
                    qry.message = reader["ACTION_RESULT_MSG"].ToString();
                }
            }

            return qry;
        }
        #endregion

        #region UPD RepairByAssign
        public queryResult UPD_RepairByAssign(UPD_GetRepairByAssign input)
        {
            queryResult result = new queryResult();

            object[] obj = { input.rp_id, input.team_id, input.assign_to, input.user_update, input.remark };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_UPD_REPAIR_BY_ASSIGN", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.result = reader["ACTION_RESULT_CODE"].ToString();
                    result.message = reader["ACTION_RESULT_MSG"].ToString();
                }
            }

            return result;
        }
        #endregion

        #region UPD RepairByAssignNone
        public queryResult UPD_RepairByAssignNone(UPD_GetRepairByAssignNone input)
        {
            queryResult result = new queryResult();

            object[] obj = { input.rp_id, input.user_update, input.remark };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_UPD_REPAIR_BY_ASSIGN_NONE", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.result = reader["ACTION_RESULT_CODE"].ToString();
                    result.message = reader["ACTION_RESULT_MSG"].ToString();
                }
            }

            return result;
        }
        #endregion

        #region UPD RepairByEvaluate
        public queryResult UPD_RepairByEvaluate(UPD_GetRepairByEvaluate input)
        {
            queryResult qry = new queryResult();

            object[] obj = { input.rp_id, input.user_commit, input.user_comment, input.rate };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_UPD_REPAIR_BY_EVALUATE", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    qry.result = reader["ACTION_RESULT_CODE"].ToString();
                    qry.message = reader["ACTION_RESULT_MSG"].ToString();
                }
            }

            return qry;
        }
        #endregion

        #region UPD RepairByOperatorOpen
        public queryResult UPD_RepairByOperatorOpen(UPD_GetRepairByOperatorOpen input)
        {
            queryResult qry = new queryResult();

            object obj = input.rp_id;
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_UPD_REPAIR_BY_OPERATOR_OPEN", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    qry.result = reader["ACTION_RESULT_CODE"].ToString();
                    qry.message = reader["ACTION_RESULT_MSG"].ToString();
                }
            }

            return qry;
        }
        #endregion

        #region UPD RepairByOepratorRequest
        public queryResult UPD_RepairByOperatorRequest(UPD_GetRepairByOperatorRequest input)
        {
            queryResult qry = new queryResult();

            object[] obj = { input.rp_id, input.user_update };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_UPD_REPAIR_BY_OPERATOR_REQUEST", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    qry.result = reader["ACTION_RESULT_CODE"].ToString();
                    qry.message = reader["ACTION_RESULT_MSG"].ToString();
                }
            }

            return qry;
        }
        #endregion

        #region UPD RepairByOperatorSent
        public queryResult UPD_RepairByOperatorSent(UPD_GetRepairByOperatorSent input)
        {
            queryResult qry = new queryResult();

            object[] obj = { input.rp_id, input.operator_sent, input.symptom, input.start_date, input.finish_date, input.cause_id, input.cause_comment, input.protection };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_UPD_REPAIR_BY_OPERATOR_SENT", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    qry.result = reader["ACTION_RESULT_CODE"].ToString();
                    qry.message = reader["ACTION_RESULT_MSG"].ToString();
                }
            }

            return qry;
        }
        #endregion

        #region UPD RepairByUploadFailed
        public queryResult UPD_RepairByUploadFailed(UPD_GetRepairByUploadFailed input)
        {
            queryResult qry = new queryResult();

            object[] obj = { input.rp_id, input.exception };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_UPD_REPAIR_BY_UPLOAD_IMG_FAILED", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    qry.result = reader["ACTION_RESULT_CODE"].ToString();
                    qry.message = reader["ACTION_RESULT_MSG"].ToString();
                }
            }

            return qry;
        }
        #endregion

        #region UPD RepairByUserCancel
        public queryResult UPD_RepairByUserCancel(UPD_GetRepairByUserCancel input)
        {
            queryResult result = new queryResult();

            object[] obj = { input.rp_id, input.user_update };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnCMMS, "CMMS_SP_UPD_REPAIR_BY_USER_CANCEL", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.result = reader["ACTION_RESULT_CODE"].ToString();
                    result.message = reader["ACTION_RESULT_MSG"].ToString();
                }
            }

            return result;
        }
        #endregion
        #endregion

    }
}
USE [MCSAPP]
GO

/****** Object:  Table [dbo].[ut_images]    Script Date: 06/18/2015 18:07:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ut_images](
	[item_barcode] [varchar](10) NOT NULL,
	[image_id] [varchar](3) NOT NULL,
	[image_time] [datetime] NULL,
	[image_user] [varchar](7) NULL,
	[comment] [varchar](200) NULL,
	[process] [varchar](1) NULL,
 CONSTRAINT [PK_ut_images] PRIMARY KEY CLUSTERED 
(
	[item_barcode] ASC,
	[image_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

